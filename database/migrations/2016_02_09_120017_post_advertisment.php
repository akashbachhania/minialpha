<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostAdvertisment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_advertisment', function (Blueprint $table) {
            $table->increments('postid');
            $table->string('userid',50);
            $table->dateTime('pdate');
            $table->string('productno',25);
            $table->string('industry',100);
            $table->string('category',100);
            $table->string('subcategory',100);
            $table->string('brand',100);
            $table->string('ptype',5);
            $table->string('currency',10);
            $table->string('uom',50);
            $table->integer('quantity');
            $table->string('location',50);
            $table->string('expdate',50);
            $table->string('expirydate',50)->default(NULL)->nullable();
            $table->string('customerrefno',50);
            $table->text('pakaging');
            $table->string('language',200);
            $table->string('timeframe',50);
            $table->string('country',200);
            $table->string('pstatus',25);
            $table->integer('targetprice');
            $table->string('postno',6);
            $table->string('tempid',100);
            $table->string('archivepost',10)->default(0);
            $table->string('buyer',100)->default(NULL)->nullable();
            $table->string('seller',100)->default(NULL)->nullable();
            $table->string('quotefacility',100);
            $table->tinyInteger('act_inactive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
