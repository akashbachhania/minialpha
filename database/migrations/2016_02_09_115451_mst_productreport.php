<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstProductreport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_productreport', function (Blueprint $table) {
            $table->increments('preportid');
            $table->integer('reportid');
            $table->integer('productid');
            $table->string('reviewbyid',25);
            $table->date('reviewdate');
            $table->string('reviewremark',500);
            $table->string('reviewstatus',25);
            $table->string('approvedby',25);
            $table->date('approveddate');
            $table->string('isactive',1)->default(1)->nullabe();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
