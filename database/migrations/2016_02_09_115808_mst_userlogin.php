<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstUserlogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_userlogin', function (Blueprint $table) {
            $table->string('roletype',10);
            $table->integer('roleid');
            $table->string('userid',25)->default('');
            $table->string('password',255);
            $table->string('authorizedpass',255);
            $table->string('isactive',1)->default(1);
            $table->string('ftlogin',1)->default('N');
            $table->string('timezoneid',100);
            $table->integer('secquiz1')->default(NULL)->nullable();
            $table->integer('secquiz2')->default(NULL)->nullable();
            $table->integer('secquiz3')->default(NULL)->nullable();
            $table->string('secans1',100)->default(NULL)->nullable();
            $table->string('secans2',100)->default(NULL)->nullable();
            $table->string('secans3',100)->default(NULL)->nullable();
            $table->string('visualnoti',1)->default(1)->nullable();
            $table->string('audionoti',1)->default(1)->nullable();
            $table->string('industry',100)->default(NULL)->nullable();
            $table->string('category',500)->default(NULL)->nullable();
            $table->string('subcategory',500)->default(NULL)->nullable();
            $table->string('brand',500)->default(NULL)->nullable();
            $table->dateTime('creationdate');
            $table->primary('userid');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
