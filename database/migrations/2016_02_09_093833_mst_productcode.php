<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstProductcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_productcode', function (Blueprint $table) {
            $table->increments('productcodeid');
            $table->integer('pid');
            $table->integer('prefixid');
            $table->string('refbyid',15);
            $table->string('approveid',15);
            $table->string('code',50);
            $table->string('status',50)->default('');
            $table->string('productcode',100);
            $table->tinyInteger('isactive')->default(1);
            $table->dateTime('refdate');
            $table->dateTime('approvedate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
