<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstUserlogon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_userlogon', function (Blueprint $table) {
            $table->increments('logonid');
            $table->string('userid',25);
            $table->string('ipaddress',25)->default('127.0.0.0');
            $table->string('starttime',25);
            $table->string('finishtime',25);
            $table->string('sessionid',100);
            $table->date('logondate');
            $table->string('roletype',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
