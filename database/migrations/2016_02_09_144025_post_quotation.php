<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostQuotation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_quotation', function (Blueprint $table) {
            $table->increments('quoteid');
            $table->string('quotationno',11);
            $table->string('postno',6);
            $table->string('productno',25);
            $table->bigInteger('price');
            $table->string('currency',10);
            $table->string('uom',50);
            $table->integer('quantity');
            $table->string('location',50);
            $table->string('expdate',50);
            $table->dateTime('expirydate');
            $table->string('language',200);
            $table->string('country',200);
            $table->string('timeframe',50);
            $table->string('details',100);
            $table->string('declinemsg',100);
            $table->string('offerdeclinemsg',50);
            $table->string('acceptmsg',100);
            $table->string('offeracceptmsg',50);
            $table->dateTime('offercrdate');
            $table->string('userid',50);
            $table->string('quationstatus',25);
            $table->string('offerstatus',20);
            $table->date('accdate')->default(NULL)->nullable();
            $table->date('rejdate')->default(NULL)->nullable();
            $table->date('lastcntdate')->default(NULL)->nullable();
            $table->string('isactive',2)->default(1);
            $table->string('counetr_status',50);
            $table->string('offerCounterStatus',50);
            $table->string('postuserid',50);
            $table->string('type',50);
            $table->integer('readunreadst')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
