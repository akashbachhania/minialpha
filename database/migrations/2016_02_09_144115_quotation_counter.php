<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuotationCounter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_counter', function (Blueprint $table) {
            $table->increments('counterid');
            $table->string('quotationno',50);
            $table->integer('quoteid');
            $table->string('postno',50);
            $table->string('productno',50);
            $table->string('acceptedBy',50);
            $table->dateTime('counter_date');
            $table->string('counter_by',50);
            $table->string('counter_price',200);
            $table->string('currency',10);
            $table->integer('counter_quantity');
            $table->string('counter_timeframe',200);
            $table->string('counter_expdate',200);
            $table->string('counter_msg',200);
            $table->string('offeracceptmsg',50);
            $table->string('counetr_status',200);
            $table->string('offerCounterStatus',50);
            $table->string('roletype',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
