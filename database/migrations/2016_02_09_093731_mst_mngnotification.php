<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstMngnotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_mngnotification', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pagename',25);
            $table->string('text',50);
            $table->string('type',25);
            $table->string('filename',100);
            $table->string('status',2)->default(1);
            $table->string('userid',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
