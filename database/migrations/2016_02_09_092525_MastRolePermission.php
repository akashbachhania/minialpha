<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MastRolePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MastRolePermission', function (Blueprint $table) {
            $table->integer('RoleId');
            $table->integer('PageId');
            $table->integer('ViewP');
            $table->integer('AddP');
            $table->integer('EditP');
            $table->integer('DeleteP');
            $table->integer('pagePrint');
            $table->integer('pageExport');
            $table->primary(['RoleId', 'PageId']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
