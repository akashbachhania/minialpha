<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_notification', function (Blueprint $table) {
            $table->increments('n_id');
            $table->integer('p_id');
            $table->string('p_key',50);
            $table->string('n_type',50);
            $table->string('n_msg',200);
            $table->string('n_variable',50);
            $table->string('final_noti',200);
            $table->string('roletype',20);
            $table->integer('n_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
