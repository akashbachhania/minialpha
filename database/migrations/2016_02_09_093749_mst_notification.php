<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_notification', function (Blueprint $table) {
            $table->increments('notiid');
            $table->string('pro_id',50);
            $table->string('userid',25);
            $table->dateTime('msgdate');
            $table->string('msg',500);
            $table->string('type',100);
            $table->string('status',100);
            $table->string('fromuserid',100);
            $table->integer('preportid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
