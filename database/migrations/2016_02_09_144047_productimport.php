<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Productimport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productimport', function (Blueprint $table) {
            $table->bigIncrements('productid');
            $table->string('productno',100);
            $table->date('date');
            $table->string('name',500)->default(NULL)->nullable();
            $table->string('industry',200)->default(NULL)->nullable();
            $table->string('category',200)->default(NULL)->nullable();
            $table->string('subcat',200)->default(NULL)->nullable();
            $table->string('brand',150)->default(NULL)->nullable();
            $table->string('manufacturer',100)->default(NULL)->nullable();
            $table->text('caseweight')->default(NULL)->nullable();
            $table->text('packaging')->default(NULL)->nullable();
            $table->text('shippingcond')->default(NULL)->nullable();
            $table->bigInteger('qtypercase')->default(NULL)->nullable();
            $table->text('des')->default(NULL)->nullable();
            $table->float('mpm')->default(NULL)->nullable();
            $table->text('prefixone')->default(NULL)->nullable();
            $table->string('codeone',50)->default(NULL)->nullable();
            $table->tinyInteger('prefixtwo')->default(NULL)->nullable();
            $table->string('codetwo',50)->default(NULL)->nullable();
            $table->string('prefixthree',50)->default(NULL)->nullable();
            $table->text('codethree')->default(NULL)->nullable();
            $table->text('img1')->default(NULL)->nullable();
            $table->text('img2')->default(NULL)->nullable();
            $table->text('img3')->default(NULL)->nullable();
            $table->text('img4')->default(NULL)->nullable();
            $table->text('img5')->default(NULL)->nullable();
            $table->text('img6')->default(NULL)->nullable();
            $table->text('img7')->default(NULL)->nullable();
            $table->text('img8')->default(NULL)->nullable();
            $table->string('addedby',200);
            $table->string('roletype',200);
            $table->string('pstatus',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
