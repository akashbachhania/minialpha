<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstSubcategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_subcategory', function (Blueprint $table) {
            $table->increments('subcatid');
            $table->integer('catid')->default(NULL)->nullable();
            $table->string('name',100);
            $table->integer('industryid');
            $table->string('isactive',2)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
