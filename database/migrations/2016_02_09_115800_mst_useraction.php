<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstUseraction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_useraction', function (Blueprint $table) {
            $table->increments('actionid');
            $table->string('userid',25);
            $table->string('ipaddress',25)->default('127.0.0.0');
            $table->string('sessionid',100);
            $table->date('actiondate');
            $table->dateTime('actiontime');
            $table->string('actionname',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
