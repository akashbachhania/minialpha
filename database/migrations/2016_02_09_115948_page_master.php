<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_master', function (Blueprint $table) {
            $table->increments('p_id');
            $table->string('p_name',50);
            $table->string('p_show_name',100);
            $table->string('p_path',100);
            $table->string('p_key',100);
            $table->integer('p_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
