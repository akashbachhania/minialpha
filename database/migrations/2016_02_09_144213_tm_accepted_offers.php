<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TmAcceptedOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_accepted_offers', function (Blueprint $table) {
            $table->increments('acc_offr_id');
            $table->integer('counterid');
            $table->string('quotationno',50);
            $table->integer('quoteid');
            $table->string('postno',50);
            $table->string('productno',50);
            $table->string('acceptedBy',50);
            $table->dateTime('acc_date');
            $table->string('acc_price',50);
            $table->integer('acc_quantity');
            $table->string('acc_timeframe',200);
            $table->string('acc_expdate',200);
            $table->string('status',50);
            $table->string('noti_status',20);
            $table->string('tm_userID',20);
            $table->string('offerType',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
