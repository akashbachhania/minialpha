<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstPageName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_pageName', function (Blueprint $table) {
            $table->increments('rowid');
            $table->string('pageName',100);
            $table->string('pageCheckAdd',2)->default('Y');
            $table->string('pageCheckEdit',2)->default('Y');
            $table->string('pageCheckDelete',2)->default('Y');
            $table->string('pageCheckPrint',2)->default('Y');
            $table->string('pageCheckExport',2)->default('Y');
            $table->string('userid',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
