<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\UserRole;
use App\Timezone;
use App\MastPage;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(adminSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(Time_zone::class);
        $this->call(Mast_Page::class);
    }
}

class adminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	

    	DB::table('mst_userlogin')->truncate();

		DB::table('mst_userlogin')->insert([
            'roletype'=> 'AD',
            'roleid' => '5',
            'userid' => 'UUZZ-9984',
            'password' => bcrypt('password123'),
            'authorizedpass'=>bcrypt('password123'),
            'isactive'=>'1',
            'ftlogin'=>'Y',
            'timezoneid'=>'Asia/Kolkata',
            'secquiz1'=>6,
            'secquiz2'=>8,
            'secquiz3'=>3,
            'secans1'=>'father',
            'secans2'=>'mother',
            'secans3'=>'animal',
            'creationdate'=>'2016-02-17 16:01:00',
            'created_at'=>'2016-02-17 16:01:00',
            'updated_at'=>'2016-02-17 16:01:00'
        ]);
    }

}

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mst_userrole')->truncate();

        UserRole::insert([
            'name'        => 'Account Manager',
            'isactive'    => '1',
            'code'        => 'AM',
            'rolename'    => 'Account Manager'
        ]);

        UserRole::insert([
            'name'        => 'Quote Facilitator',
            'isactive'    => '1',
            'code'        => 'QF',
            'rolename'    => 'Quote Facilitator'
        ]);

        UserRole::insert([
            'name'        => 'Trigger Man',
            'isactive'    => '1',
            'code'        => 'TM',
            'rolename'    => 'Trigger Man'
        ]);

        UserRole::insert([
            'name'        => 'Administrator',
            'isactive'    => '1',
            'code'        => 'AD',
            'rolename'    => 'Sub Administrator'
        ]);

        UserRole::insert([
            'name'        => 'Administrator',
            'isactive'    => '1',
            'code'        => 'AD',
            'rolename'    => 'Administrator'
        ]);

        UserRole::insert([
            'name'        => 'Account Manager',
            'isactive'    => '1',
            'code'        => 'AM',
            'rolename'    => 'Partner'
        ]);
    }
}

class Time_zone extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mst_timezone')->truncate();

        $zones = '';
        $timestamp = time();
        foreach(timezone_identifiers_list() as $key => $zone) {
      
            date_default_timezone_set($zone);
            
            $zones = 'UTC/GMT ' . date('P', $timestamp);
            
            Timezone::insert([
                'name'           => $zone,
                'isactive'       => '1',
                'timezonevalue'  => $zones,
                'created_at'     => Date('Y-m-d h:i:s'),
                'updated_at'     => Date('Y-m-d h:i:s')
            ]);
      
        }        
    }
}


class Mast_Page extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('MastPage')->truncate();
    
        MastPage::insert([
            'PageId'       => 3,
            'PageName'     => '',
            'Module'       => 'Customers',
            'DisplayName'  => 'Customers',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 4,
            'PageName'     => 'mstcustomer',
            'Module'       => 'Customers',
            'DisplayName'  => 'Browse Customers',
            'Parent_Id'    => 3, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 5,
            'PageName'     => '',
            'Module'       => 'Product Catalog',
            'DisplayName'  => 'Product Catalog',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 6, 
            'PageName'     => 'add-product',
            'Module'       => 'Product Catalog',
            'DisplayName'  => 'Add New Product',
            'Parent_Id'    => 5, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 7,
            'PageName'     => 'product-catalog',
            'Module'       => 'Product Catalog',
            'DisplayName'  => 'Browse Products',
            'Parent_Id'    => 5, 
            'Idx'          => 2,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 8,
            'PageName'     => 'suggested-product',
            'Module'       => 'Product Catalog',
            'DisplayName'  => 'Suggested Product',
            'Parent_Id'    => 5, 
            'Idx'          => 3,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 9,
            'PageName'     => 'suggested-product-code',
            'Module'       => 'Product Catalog',
            'DisplayName'  => 'Suggested Product Code',
            'Parent_Id'    => 5, 
            'Idx'          => 4,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 10,
            'PageName'     => 'reported-product',
            'Module'       => 'Product Catalog',
            'DisplayName'  => 'Reported Product',
            'Parent_Id'    => 5, 
            'Idx'          => 5,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 11,
            'PageName'     => '',
            'Module'       => 'Postings',
            'DisplayName'  => 'Postings',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 12,
            'PageName'     => 'posting-catalog',
            'Module'       => 'Postings',
            'DisplayName'  => 'Browse Postings',
            'Parent_Id'    => 11, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 13,
            'PageName'     => 'my-posting',
            'Module'       => 'Postings',
            'DisplayName'  => 'My Postings',
            'Parent_Id'    => 11, 
            'Idx'          => 2,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 14,
            'PageName'     => 'import-post',
            'Module'       => 'Postings',
            'DisplayName'  => 'Import Postings',
            'Parent_Id'    => 11, 
            'Idx'          => 4,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 15,
            'PageName'     => '',
            'Module'       => 'Product List Download',
            'DisplayName'  => 'Products List Download',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 16,
            'PageName'     => 'product-listdownload',
            'Module'       => 'Product List Download',
            'DisplayName'  => 'Products List Download',
            'Parent_Id'    => 15, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 17,
            'PageName'     => '',
            'Module'       => 'Quote Queue',
            'DisplayName'  => 'Quote Queue',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 18,
            'PageName'     => 'quoteandpost_que',
            'Module'       => 'Quote Queue',
            'DisplayName'  => 'Quote Queue',
            'Parent_Id'    => 17, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 19,
            'PageName'     => '',
            'Module'       => 'Orders',
            'DisplayName'  => 'Orders',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 20,
            'PageName'     => 'tm-accepted-offers',
            'Module'       => 'Orders',
            'DisplayName'  => 'Accepted Offers',
            'Parent_Id'    => 19, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 21,
            'PageName'     => 'tm-completed-offers',
            'Module'       => 'Orders',
            'DisplayName'  => 'Completed Offers',
            'Parent_Id'    => 19, 
            'Idx'          => 2,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 22,
            'PageName'     => 'tm-cancelled-order',
            'Module'       => 'Orders',
            'DisplayName'  => 'Cancelled Orders',
            'Parent_Id'    => 19, 
            'Idx'          => 3,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 23,
            'PageName'     => '',
            'Module'       => 'Settings',
            'DisplayName'  => 'Settings',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 24,
            'PageName'     => 'my-setting',
            'Module'       => 'Settings',
            'DisplayName'  => 'My Settings',
            'Parent_Id'    => 23, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 25,
            'PageName'     => 'notification-setting',
            'Module'       => 'Settings',
            'DisplayName'  => 'Notifications',
            'Parent_Id'    => 23, 
            'Idx'          => 2,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 26,
            'PageName'     => 'notification',
            'Module'       => 'Settings',
            'DisplayName'  => 'Notifications Settings',
            'Parent_Id'    => 23, 
            'Idx'          => 3,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 27,
            'PageName'     => 'import-data',
            'Module'       => 'Settings',
            'DisplayName'  => 'Import Data',
            'Parent_Id'    => 23, 
            'Idx'          => 4,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 28,
            'PageName'     => 'mstindustry',
            'Module'       => 'Settings',
            'DisplayName'  => 'Industry List',
            'Parent_Id'    => 23, 
            'Idx'          => 5,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 29,
            'PageName'     => 'mstcategory',
            'Module'       => 'Settings',
            'DisplayName'  => 'Category List',
            'Parent_Id'    => 23, 
            'Idx'          => 6,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 30,
            'PageName'     => 'mstsubcategory',
            'Module'       => 'Settings',
            'DisplayName'  => 'Sub Category List',
            'Parent_Id'    => 23, 
            'Idx'          => 7,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);      
        MastPage::insert([
            'PageId'       => 31,
            'PageName'     => 'mstbrand',
            'Module'       => 'Settings',
            'DisplayName'  => 'Brands List',
            'Parent_Id'    => 23, 
            'Idx'          => 8,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 32,
            'PageName'     => 'mstlocation',
            'Module'       => 'Settings',
            'DisplayName'  => 'Location',
            'Parent_Id'    => 23, 
            'Idx'          => 9,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 34,
            'PageName'     => 'mstsecurity-que',
            'Module'       => 'Settings',
            'DisplayName'  => 'Security Question List',
            'Parent_Id'    => 23, 
            'Idx'          => 11,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 35,
            'PageName'     => 'mstterms-condition',
            'Module'       => 'Settings',
            'DisplayName'  => 'Terms & Conditions',
            'Parent_Id'    => 23, 
            'Idx'          => 12,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 36,
            'PageName'     => 'databackup',
            'Module'       => 'Settings',
            'DisplayName'  => 'Database Backup',
            'Parent_Id'    => 23, 
            'Idx'          => 13,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 37,
            'PageName'     => 'maskproductname',
            'Module'       => 'Settings',
            'DisplayName'  => 'Website Settings',
            'Parent_Id'    => 23, 
            'Idx'          => 15,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 38,
            'PageName'     => 'menu_permission',
            'Module'       => 'Settings',
            'DisplayName'  => 'Menu Permission',
            'Parent_Id'    => 23, 
            'Idx'          => 16,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 39,
            'PageName'     => '',
            'Module'       => 'Reports',
            'DisplayName'  => 'Reports',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 40,
            'PageName'     => 'report',
            'Module'       => 'Reports',
            'DisplayName'  => 'Reports',
            'Parent_Id'    => 39, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 41,
            'PageName'     => '',
            'Module'       => 'Users',
            'DisplayName'  => 'Users',
            'Parent_Id'    => 0, 
            'Idx'          => 0,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 42,
            'PageName'     => 'userdetails',
            'Module'       => 'Users',
            'DisplayName'  => 'View Users',
            'Parent_Id'    => 41, 
            'Idx'          => 1,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 43,
            'PageName'     => 'activity',
            'Module'       => 'Users',
            'DisplayName'  => 'Users Activity',
            'Parent_Id'    => 41, 
            'Idx'          => 2,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 44,
            'PageName'     => 'create',
            'Module'       => 'Users',
            'DisplayName'  => 'Create New User',
            'Parent_Id'    => 41, 
            'Idx'          => 3,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 45,
            'PageName'     => 'mstrole',
            'Module'       => 'Settings',
            'DisplayName'  => 'Role List',
            'Parent_Id'    => 23, 
            'Idx'          => 17,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 46,
            'PageName'     => 'databaserestore',
            'Module'       => 'Settings',
            'DisplayName'  => 'Database Restore',
            'Parent_Id'    => 23, 
            'Idx'          => 14,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);
        MastPage::insert([
            'PageId'       => 47,
            'PageName'     => 'my-offer',
            'Module'       => 'Postings',
            'DisplayName'  => 'My Offers',
            'Parent_Id'    => 11, 
            'Idx'          => 3,
            'created_at'   => Date('Y-m-d h:i:s'),
            'updated_at'   => Date('Y-m-d h:i:s')
        ]);


    }
}