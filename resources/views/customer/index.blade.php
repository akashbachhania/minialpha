@extends('layout.master')

@section('content')
<section>
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Customers</h3>

            @if( Auth::user()->roletype == 'AD' )

            <a href="#" class="pull-right btn btn-success" id="btnnew" data-toggle="modal">Add New Customers</a>            
            <a href="{{ url('setting/importData?data-type=Customers') }}" class="pull-right btn btn-primary m_r5">Import Customers</a>

            @else

                @if( $checkVal[0]['pagePrint']=='1' )
                    
                    <a href="{{ url('setting/import-data?data-type=Customers') }}" class="pull-right btn btn-primary m_r5">Import Customers</a>

                @endif

                @if( $checkVal[0]['pageAdd']=='1' )

                    <a href="#" class="pull-right btn btn-success" id="btnnew" data-toggle="modal">Add New Customers</a>    

                @endif
            
            @endif

            @if ( Auth::user()->roletype == 'AD')
              <a class="pull-right black m_r5">Bulk Action
                <select class="table-group-action-input input-inline input-small input-sm" id="statuschange">
                  <option value="" selected>Change Status</option>
                  <option value="ACTIVE">Active</option>
                  <option value="DEACTIVE">Inactive</option>
                </select></a>
            @else

                @if($checkVal[0]['pageEdit']=='1')
                  <a class="pull-right black m_r5">Bulk Action
                  <select class="table-group-action-input input-inline input-small input-sm" id="statuschange">
                    <option value="" selected>Change Status</option>
                    <option value="ACTIVE">Active</option>
                    <option value="DEACTIVE">Inactive</option>
                  </select></a>
                @endif

            @endif      
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="customerTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="4%"><input type="checkbox" class="group-checkable" name="checkcust" id="checkcust" value="" data-set="#sample_1 .checkboxes" /></th>
                  <th width="20%">Creation Date</th>
                  <th width="20%">Customer Reference No.</th>
                  <th width="10%">Ref. UserID</th>
                  <th width="5%">Status</th>
                  <th width="26%">Actions</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" style="text-align:left">Customer Form </h4>
      </div>
     
      <div class="modal-body" style="text-align:left">
        <form action="#" id="form_sample_2" class="form-horizontal" novalidate>
          <div class="form-body">
            <div id="msg" name="msg"></div>

            @if(Auth::user()->roletype == "AD")
             
              <div class="form-group">
                <label class="control-label col-md-3">Account manager <span class="required" 9> * </span> </label>
                <div class="col-md-8">
                  
                    <select class="form-control" id="acm" name="acm">
                    <option value="">Select Account manager</option>
                      @foreach($users as $user)
                      <option value="{{ $user->userid }}">{{ $user->userid }}</option>
                      @endforeach
                    </select>
                  
                </div>
              </div>

            @endif
            
            <div class="form-group">
              <label class="control-label col-md-3">Ref No. <span class="required"> * </span> </label>
              <div class="col-md-8">
                
                <input type="hidden" id="roletype" value="{{ Auth::user()->roletype }}" name="roletype">
                  <input type="text" class="form-control" maxlength="80" id="name" name="name">
                  <div id="msgg" style="display: block; margin-bottom: 1px; clear: both; position: relative; top: -1px; margin-left: 6px;font-size: 14px;"></div>
                  <span style="display:none">
                  <input type="text" class="form-control" maxlength="80" value="0" id="id" name="id">
                  </span>
                  <input type="hidden" class="form-control" name="checkProduct" id="checkProduct" value="">
                
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Status <span class="required" 9> * </span> </label>
              <div class="col-md-8">
                
                  <select class="form-control" id="status" name="status">
                    <option value="1" selected>ACTIVE</option>
                    <option value="0">INACTIVE</option>
                  </select>
                
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer"> <a href="#" id="btnclose" class="btn btn-default"><i class="fa"> </i>&nbsp; Close</a>
        <button type="button" id="btnsave" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@include('customer.js')
@endsection