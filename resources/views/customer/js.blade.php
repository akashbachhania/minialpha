@if(\Request::input('delete') == 'success')
<script> 
$(document).ready(
function() {

    $.ajax({
        url: "service/noti6.php",
        type: "post",
        cache: false,
        dataType:'html',
            success: function(res2){
                if(res2=='bothclose')
                {
                
                }
                if(res2=='audioclose')
                {
                 var msg = "Customer deleted successfully";
                shownoti(msg); 
                }
                if(res2=='vedioclose')
                {
                    $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');

                    $('#sound')[0].play(); 
                }
                if(res2=='bothstart')
                {
                 var msg = "Customer deleted successfully";
                 shownoti(msg); 
                 $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                 $('#sound')[0].play(); 
                }
             }
        });


 });
</script>
@endif

<script type="text/javascript">

 function shownoti(message){
    $(function() {
    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];
    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;
    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})
}
</script>

<script type="text/javascript">
    var customerTable = $('#customerTable').DataTable( {

        "ajax": {
            "url":root +"/customer/customerTable_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
        ],
        columns: [
        { data: 'userCheck' },
        { data: 'edate' },
        { data: 'refno' },
        { data: 'refuserid' },
        { data: 'msg'},
        { data: 'edit' }         
        ]
    } );
    
    $('#statuschange').change(function() {

        var exportData = '';
        var i = 0;
         $.each($("input[name='checkcust']:checked"), function(){            

            exportData=exportData+"'"+($(this).val())+"',";
            i++;
        });

        
        exportData = exportData.substring(0, exportData.length - 1);

        if(i != 0) {

            var statusData = $("#statuschange option:selected").val();

            input_box = confirm("Are you sure you want to change the Status?");

            if (input_box==true && statusData !='')

            {
                
                var datas = {
                    statusActionType:'shatusChange',
                    statusData:statusData,
                    checkcust:exportData,
                    id:i
                }   

                $.ajax({

                    type: 'POST',

                    url: root + '/customer/customerTable_ajax',

                    cache: false,

                    data: datas,

                    success: function(data) {
                        if( data == 'Success' ){
                            var customerTable = $('#customerTable').DataTable( {
                                destroy:true,
                                "ajax": {
                                    "url":root +"/customer/customerTable_ajax",
                                    "dataSrc": "",
                                    "type": 'POST',
                                },
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [ 0 ] }
                                ],
                                columns: [
                                { data: 'userCheck' },
                                { data: 'edate' },
                                { data: 'refno' },
                                { data: 'refuserid' },
                                { data: 'msg'},
                                { data: 'edit' }         
                                ]
                            } );
                        }
                    }   
                });    
                
                $("#statuschange option[value='']").attr('selected', 'selected');

                // $('#statuschange').selectmenu('refresh');

            }
        }
    });

    $("#btnnew").on("click", function() {

        $('#basic').modal('show');
        $("#msgg").hide();
        //$('#status').val('');

        $('#name').val('');

        $("label.error").hide();

        $(".error").removeClass("error");

        setTimeout(function() {

            $('#name').focus();

        }, 1000);

        $('#form_sample_2')[0].reset();

    });

    $("#btnclose").on("click", function() {

        $("label.error").hide();

        $(".error").removeClass("error");

        $('#status').val('');

        $('#name').val('');
        
        $("#msgg").hide();

        $('#basic').modal('hide');



    });


    $("#btnsave").on("click", function() {

        var name = $('#name').val();

        var status = $('#status').val();
        
        var checkProduct = $('#checkProduct').val();

        var id = $('#id').val();
        var roletype = $('#roletype').val();

        if(roletype != "AM")
        {
            var acm = $('#acm').val();
             //alert(acm);
            if (acm =='') {
                alert('Account manager must be Required');
                return;
            }
        }
        if (name == '' || status == '') {
            alert('Customer Name must be Required');
            return;
        }  
        
        if (checkProduct == '0') {
            return;
        }

        if (id == "0") {

            var acm = $('#acm').val();
            var data = {

                Type: 'add',
                acm: acm,
                name: name,
                status: status

            };

            $.ajax({

                type: 'POST',

                url: root + '/customer/customerTable_ajax',

                cache: false,

                data: data,

                success: function(response) {

                    if (response.msg == 'SUCCESS') {

                        $('#basic').modal('hide');

                        $("#msgg").hide();

                        $("#checkProduct").val('');

                        $("#name").val('');

                        var customerTable = $('#customerTable').DataTable( {
                            destroy:true,
                            "ajax": {
                                "url":root +"/customer/customerTable_ajax",
                                "dataSrc": "",
                                "type": 'POST',
                            },
                            "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [ 0 ] }
                            ],
                            columns: [
                            { data: 'userCheck' },
                            { data: 'edate' },
                            { data: 'refno' },
                            { data: 'refuserid' },
                            { data: 'msg'},
                            { data: 'edit' }         
                            ]
                        } );

                        var id1 = 'ncustomer';
                        var lognoti = "lognoti";
                        $.ajax({
                            url: "./functions.php",
                            type: "post",
                            data:{ 'id': id1 , 'lognoti': lognoti},
                            cache: false,
                            success: function(res){
                            //alert(res);
                            
                                $.ajax({
                                    url: "service/noti6.php",
                                    type: "post",
                                    data:{id1:id1},
                                    cache: false,
                                    dataType:'html',
                                    success: function(res2){
                                          //alert(res2);
                                        if(res2=='bothclose')
                                        {
                                        
                                        }
                                        if(res2=='audioclose')
                                        {
                                        //alert(res2);
                                        var message = res;
                                        shownoti(message);
                                        }
                                        if(res2=='vedioclose')
                                        {
                                            $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                                
                                            $('#sound')[0].play(); 
                                        }
                                        if(res2=='bothstart')
                                        {
                                        //alert(res2);
                                            var message = res;
                                            shownoti(message);
                                            $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                                            $('#sound')[0].play(); 
                                        }
                                     }
                                });
                                        
                            //var message = res;
                            //shownoti(message);
                            }
                        });
                                //shownoti("User Created Successfully"); // refresh div after 5 secs
                     

                    }

                    if (response.msg == 'EROOR') {

                        alert(response.name);

                    }

                },

                error: function(jqXHR, textStatus, errorThrown) {
                }

            });

        }
        else {

                //var data = "Type=update&name=" + name + "&status=" + status+"&id="+id;

                var data = {

                    Type: 'update',

                    name: name,

                    status: status,

                    id: id

                };

                $.ajax({

                    type: 'POST',

                    url: root + '/customer/customerTable_ajax',

                    cache: false,

                    data: data,

                    success: function(response) {

                        // response = jQuery.parseJSON(data);

                        if (response.msg == 'SUCCESS') {

                            $('#basic').modal('hide');

                            $("label.error").hide();

                            $(".error").removeClass("error");

                            // grid.getDataTable().ajax.reload();

                            $("#msgg").hide();

                            $("#checkProduct").val('');
                            var customerTable = $('#customerTable').DataTable( {
                                destroy:true,
                                "ajax": {
                                    "url":root +"/customer/customerTable_ajax",
                                    "dataSrc": "",
                                    "type": 'POST',
                                },
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [ 0 ] }
                                ],
                                columns: [
                                { data: 'userCheck' },
                                { data: 'edate' },
                                { data: 'refno' },
                                { data: 'refuserid' },
                                { data: 'msg'},
                                { data: 'edit' }         
                                ]
                            } );
                        var id2 = 'upcustomer';
                            var lognoti = "lognoti";
                     $.ajax({
                                        url: "./functions.php",
                                        type: "post",
                                        data:{ 'id': id2 , 'lognoti': lognoti},
                                        cache: false,
                                        success: function(res){
                                        //alert(res);
                                        
                                        $.ajax({
                                            url: "service/noti6.php",
                                            type: "post",
                                            data:{id2:id2},
                                            cache: false,
                                            dataType:'html',
                                                    success: function(res2){
                                                    //alert(res2);
                                                        if(res2=='bothclose')
                                                        {
                                                        
                                                        }
                                                        if(res2=='audioclose')
                                                        {
                                                        //alert(res2);
                                                    var message = res;
                                                    shownoti(message);
                                                        }
                                                        if(res2=='vedioclose')
                                                        {
                                                $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                                    
                                     $('#sound')[0].play(); 
                                                        }
                                                    if(res2=='bothstart')
                                                    {
                                                    //alert(res2);
                                                    var message = res;
                                                    shownoti(message);
                                                $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                                     $('#sound')[0].play(); 
                                                    }
                                                 }
                                            });
                                        
                                        //var message = res;
                                        //shownoti(message);
                                        }
                                });
                                //shownoti("User Created Successfully"); // refresh div after 5 secs
                            

                        }

                        if (response.msg == 'EROOR') {

                            alert(response.name);

                        }

                    },

                error: function(jqXHR, textStatus, errorThrown) {

                    //commit(false);

                }

            });

        }
    });
</script>

<script type="text/javascript">
$("#name").change(function() {

    var pname = $("#name").val();

    if(pname != '')

    {

    $.ajax({

        type: "POST",

        url: root+ "/customer/customerTable_ajax",

        async: false,

        data: {

            pname: $("#name").val(),

            Type: 'exist'

        },

        success: function(data, status, xhr) {

            if (data == 'exist') {

                $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');

                $("#checkProduct").val('0');

            } else {

                $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');

                $("#checkProduct").val('1');

            }

        },

        error: function(jqXHR, textStatus, errorThrown) {

            alert(errorThrown);

            commit(false);

        }

    });

  }

  else  

  {

    $("#msgg").show().html('<span></span>');

  }

});    
</script>

<script type="text/javascript">

    function editme(obj) {

        $('#basic').modal('show');

        $('#status').val('');

        $('#name').val('');

        $('#id').val(obj);

        setTimeout(function() {

            $('#name').focus();

        }, 1000);

    $('#name').attr('readonly', 'readonly');

        var data = "Type=search&id=" + obj;

        $.ajax({

            type: 'POST',

            url: root + '/customer/customerTable_ajax',

            cache: false,

            data: data,

            success: function(response) {

                // response = jQuery.parseJSON(data);

                $('#status').val(response.status);

                $('#name').val(response.name);
                $('select#acm').val(response.refuserid);

                $("#msgg").hide();

                $("#checkProduct").val('');

            },

            error: function(jqXHR, textStatus, errorThrown) {

                //commit(false);

            }

        });

    }

function deleteme(obj) {

        var data = "Type=delete&id=" + obj;

        if (confirm("Are you sure you wish to delete this Customer?")) {
            $.ajax({
                type: 'POST',

                url: root + '/customer/customerTable_ajax',

                cache: false,

                data: data,

                success: function(response) {
                  //document.write(data);
                  //alert(data);

                    // response = jQuery.parseJSON(data);
                    

                    if (response.msg == 'ERROR') {
                        shownoti(response.name);
                    } else {
                        window.location = "{{ url('customer/browse') }}" + "?delete=success";
                    }

                    //grid.getDataTable().ajax.reload();

                },

                error: function(jqXHR, textStatus, errorThrown) {

                    //commit(false);

                }

            });

        }

    }    
</script>