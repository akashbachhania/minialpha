@extends('layout.master')

@section('content')

<!-- Small boxes (Stat box) -->
<div class="row">
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-aqua">
	    <div class="inner">
	      @if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )

	      <h3>{{ $mypostcount }}</h3>

	      @else

	      <h3>{{ 0 }}</h3>

	      @endif

	      <p>My Postings</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-bag"></i>
	    </div>
	    @if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
	    <a href="my-posting.php?key=13" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	    @endif
	  </div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-green">
	    <div class="inner">
	      @if( Auth::user()->roletype == 'AM' )
	      <h3>{{ $allofferCount }}</h3>
	      @else
	      <h3>{{ 0 }}</h3>
	      @endif
	      <p>My Offers</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-stats-bars"></i>
	    </div>
	    @if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
	    <a href="my-offer.php?key=47" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	    @endif
	  </div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-yellow">
	    <div class="inner">
	      @if ((Auth::user()->roletype == 'AM'))
        	    <h3>{{ ($allpostcount - $allofferacceptedCount) }}</h3>
          
          @elseif ((Auth::user()->roletype == 'AD'))
            	<h3>{{ $allpostcount }}</h3>
          @else
            	<h3>{{ "0" }}</h3>
       	  @endif
	      
	      <p>View Postings</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-person-add"></i>
	    </div>
	    @if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
	    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	    @endif
	  </div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-red">
	    <div class="inner">
	      @if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
	      <h3>{{ $allproductCount }}</h3>
	      @else
	      {{ 0 }}
	      @endif
	      <p>Product Catalog</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-pie-graph"></i>
	    </div>
	    @if( Auth::user()->roletype == 'AD' || Auth::user()->roletype == 'AM' )
	    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	  	@endif
	  </div>
	</div><!-- ./col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-6">
		<div class="box box-success" style="border: 1px solid #00a65a!important">
			<div class="box-header" style="background: #00a65a;color: #fff;">
				<i class="fa fa-bell-o"></i>
				<h3 class="box-title">
					Offers
				</h3>
			</div>
			<div class="box-body" style="min-height: 360px;max-height: 360px;">
				<ul class="todo-list">
					@if( !empty($sqlq) &&  (count($sqlq) > 0) )
						@foreach ($sqlq as $value)
							<?php $data = explode( ' ', $value->offercrdate); ?>

						
							<li>
							@if( Auth::user()->roletype == 'AM' )

		                    	@if(!empty($mypostcount))
									<span class="text">
										<a href="myposting-detail-view.php?post_id={{ $value->postno }}&quotationno={{ $value->quotationno }}" class="ofercss">{{ $value->details }} - "{{ $value->postAdverstisment->ptype }}&nbsp;{{ $value->quantity }}&nbsp;{{ $value->product->name }}"</a>
									</span>			
									<small class="label label-default pull-right">{{ $data[0] }}</small>
								@endif
							
							@elseif (Auth::user()->roletype == 'TM')

								<span class="text">
									<a href="detail_accp_offer.php?product_id={{ $value->productno }}&postno={{ $value->postno }}&quotationno={{ $value->quotationno }}&acc_offr_id={{ $value->acc_offr_id }}&pid={{ $value->product->productid }}" class="ofercss">{{ $value->status }} - "{{ $value->postAdverstisement->ptype }} &nbsp; $value->acc_quantity }}&nbsp;{{ $value->product->name }}"</a>
								</span>
								<small class="label label-default pull-right">{{ $data[0] }}</small>

							@endif

								
		                    </li>
		            		
		            	@endforeach
						
					@endif
						
					@if( Auth::user()->roletype == 'QF')
						
						{{ $swlw }}

					@endif	
		        </ul>

			</div>

			<div class="box-footer clearfix no-border">
			  <a href="my-posting.php?key=13" class="pull-right">See All Records  <i class="fa fa-arrow-circle-o-right"></i></a>
              <!-- <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button> -->
            </div>

		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-success" style="border: 1px solid #00a65a!important">
			<div class="box-header" style="background: #00a65a;color: #fff;">
				<i class="fa fa-bell-o"></i>
				<h3 class="box-title" style="">
					Notifications
				</h3>
			</div>
			<div class="box-body" style="min-height: 360px;max-height: 360px;">
				<ul class="feeds" id="noti">
                </ul>
			</div>
			<div class="box-footer clearfix no-border">
				<a href="notification-setting.php?key=25" class="pull-right">See All Records   <i class="fa fa-arrow-circle-o-right"></i></a> 
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-success" style="border: 1px solid #00a65a!important">
			<div class="box-header" style="color: #fff;background: #00a65a">
				<i class="fa fa-cogs"></i>
				<h3 class="box-title" style="">
					My Postings
				</h3>
				<div class="box-tools">
					<a href="my-posting.php?key=13" class="allmore" style="color: #fff;"> View My Postings </a>
				</div>				
			</div>

			<div class="box-body">
				<div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover">
		              <thead>
		                <tr>
		                  <th> Posting ID </th>
		                  <th> Date Posted </th>
		                  <th> Brand </th>
		                  <th> Manufacturer </th>
		                  <th> Type </th>
		                  <th> Quantity </th>
		                  <th> Location </th>
		                  <th> Product Name </th>
		                  <th> Price </th>
		                  <th> Curr </th>
		                  <th> Curr Ref </th>
		                  <th> Status </th>
		                  <th> Actions </th>
		                
		                   
		                </tr>
		              </thead>
		              <tbody>

		              	@foreach( $mypostings as $myposting )

		              		@if((Auth::user()->roletype == 'AD') || (Auth::user()->roletype == 'AM'))
								@if(!empty($mypostcount))
									
				<tr>
                  <td style="white-space: nowrap;">{{ isset($myposting->postno)?$myposting->postno:'' }} </td>
                  <td style="white-space: nowrap;">{{ isset($myposting->pdate)?date('F d Y',strtotime($myposting->pdate)):'' }} </td>
                  <td style="white-space: nowrap;">{{ isset($myposting->product->brand->name)?$myposting->product->brand->name:'' }} </td>
                   <td style="white-space: nowrap;">{{ isset($myposting->product->manufacture)?$myposting->product->manufacture:'' }} </td>
                  <td style="white-space: nowrap;">{{ isset($myposting->ptype)?$myposting->ptype:'' }} </td>
                  <td style="white-space: nowrap;">{{ isset($myposting->quantity)?$myposting->quantity:'' }}</td>
                   <td style="white-space: nowrap;">{{ isset($myposting->location)?$myposting->location:'' }}</td>
                  <td style="white-space: nowrap;">{{ isset($myposting->product->name)?$myposting->product->name:'' }} </td>
                  <td style="white-space: nowrap;">{{ number_format(isset($myposting->targetprice)?$myposting->targetprice:'',2) }} </td>
                  <td style="white-space: nowrap;">{{ isset($myposting->currency)?$myposting->currency:'' }}</td>
                  <td style="white-space: nowrap;">{{ isset($myposting->customerrefno)?$myposting->customerrefno:'' }} </td>
                  <td style="white-space: nowrap;">{{ isset($myposting->pstatus)?$myposting->pstatus:'' }} </td>
                   <td style="white-space: nowrap;"><a href="myposting-detail-view/{{ isset($myposting->postno)?$myposting->postno:'' }}">View </a>| <a href="edit-add.php?post_id={{ isset($myposting->postno)?$myposting->postno:'' }}">Edit</a> | <a href="javascript:cancelme({{ isset($myposting->postno)?$myposting->postno:'' }})">Cancel</a> </td>
                </tr>

								@endif
							@endif
		              	@endforeach

		              </tbody>
		            </table>
		        </div>				
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-success" style="border: 1px solid #CB5A5E!important">
			<div class="box-header" style="color: #fff;background: #CB5A5E">
				<i class="fa fa-cogs"></i>
				<h3 class="box-title" style="">
					Recent Postings
				</h3>
				<div class="box-tools">
					<a href="my-posting.php?key=13" class="allmore" style="color: #fff;"> View All Postings </a>
				</div>				
			</div>

			<div class="box-body">
				<div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover">
		            	
		            	<thead>
			                <tr>
			                    <th>Posting ID</th>
			                    <th>Date Posted</th>
			                    <th> Industry</th>
			                    <th>Category </th>
			                    <th> Brand</th>
			                    <th> Manufacturer </th>
			                    <th> Type </th>
			                    <th> Quantity </th>
			                    <th> Location </th>
			                    <th> Product Name </th>
			                    <th> Price </th>
			                    <th> Curr. </th>
			                    <th> Exp. Date</th>
			                </tr>
		                </thead>
		                <tbody>

		                	@foreach( $recentpostings as $recentposting)

		                		@if((Auth::user()->roletype == 'AD') || (Auth::user()->roletype == 'AM'))
									@if(!empty($allpostcount))
										
				<tr class='clickable-row' data-href="posting-detail-view.php?post_id={{ $recentposting->postno }}">
                    <td style="white-space: nowrap;">{{ $recentposting->postno }} </td>
                    <td style="white-space: nowrap;">{{ date('F d Y',strtotime($recentposting->pdate)) }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->product->industry->name }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->product->category->name }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->product->brand->name }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->product->manufacture }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->ptype }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->quantity }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->location }}</td>
                    <td style="white-space: nowrap;">{{ $recentposting->product->name }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->targetprice }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->currency }} </td>
                    <td style="white-space: nowrap;">{{ $recentposting->timeframe }} </td>
                </tr>

									@endif
								@endif
		                	@endforeach

		                </tbody>
		            </table>
		        </div>	
			</div>

		</div>
	</div>
</div>
@include('dashboard.js')
@endsection