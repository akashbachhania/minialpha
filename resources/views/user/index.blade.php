@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
      </div>
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Users</h3>
          <a href="{{ url('create') }}" class="btn btn-success pull-right">Create New User</a>
        </div><!-- /.box-header -->
        <div class="box-body">

          <table id="userdetail" class="table table-bordered table-striped">
            <thead>
              <tr>
                <!-- <th></th> -->
                <th>User ID</th>
                <th>User Role</th>
                <th>Creation Date</th>
                <th>Last Login</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
    </section>
    @endsection