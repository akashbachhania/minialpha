<script type="text/javascript">
    $(document).ready(function() {
        $("#indmultipleSelect").change(function() {
            var industries = [];
            $.each($(".industry option:selected"), function() {
                industries.push($(this).val());
            });
            //alert(industries.join(","));
            var selectedindustry = industries.join(",");
            var indmode = "selectindustry";
            var formData = {
                ind_id: selectedindustry,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#subcat").empty();
                    $("#cat").empty();
                    $("#brands").empty();
                    var div_data = "<option value=>Select Sub Categories</option>";
                    $(div_data).appendTo("#subcat");
                    var div_data = "<option value=>Select  Categories</option>";
                    $(div_data).appendTo("#cat");
                    var div_data2 = "<option value=>Select  Brands</option>";
                    $(div_data2).appendTo("#brands");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#cat");
                            });
                        $.each(data.brandData, function(i, data)
                            {
                                var div_data2 = "<option value=" + data.brandid + ">" + data.name + "</option>";
                                $(div_data2).appendTo("#brands");
                            });
                    } else
                    {
                        alert("Categories does not exist");
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#cat").change(function() {
            var categories = [];
            $.each($(".category option:selected"), function() {
                categories.push($(this).val());
            });
            //alert(industries.join(","));
            var selectcategories = categories.join(",");
            var indmode = "selectsubcategory";
            var formData = {
                cat_id: selectcategories,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#subcat").empty();
                    var div_data = "<option value=>Select Sub Categories</option>";
                    $(div_data).appendTo("#subcat");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#subcat");
                            });
                    } else
                    {
                        //alert("SubCategories does not exist");	
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
//    	<!---------------default page------------------!>
		var selected = $('#showval option:selected');
			//alert(todate);
		var showlaue =selected.val();
		var fromdate = $('#from').val();
		var todate = $('#to').val();
		var reuserid = $('#reuserid').val();
			
			  
        var ractionmade = "reportgenerate";
        var formData = {
				showlaue: showlaue,
				fromdate: fromdate,
				todate: todate,
				ractionmade: ractionmade,
				reuserid: reuserid
        }; 
			
			//Array 
        $.ajax({
            type: "POST",
            url: root + "/userreports",
            async: false,
            data: formData,
            //dataType: 'json',
            success: function(data) {
             //alert(data);
             //
               if (data != null)
               {
						  // alert(data.ptype)
				 $('#buypost').empty();
				   $('#buypost').append(data);
				   $("#defaultrow").hide();
                } 
				else
                {
				$('#buypost').append('<tr class="success"><td width="10%" colspan="3">Data does not exist</td></tr>');
				 $("#defaultrow").hide();
                    //alert("Data does not exist");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
                commit(false);
            }
        });
  
//        <!-----------------------End -----------------------!>
        $("#reportgo").click(function() {
				var selected = $('#showval option:selected');
					//alert(todate);
				var showlaue =selected.val();
				var fromdate = $('#from').val();
				var todate = $('#to').val();
				var reuserid = $('#reuserid').val();
			
			  
            var ractionmade = "reportgenerate";
            var formData = {
					showlaue: showlaue,
					fromdate: fromdate,
					todate: todate,
					ractionmade: ractionmade,
					reuserid: reuserid
            }; 
			
			//Array 
            $.ajax({
                type: "POST",
                url: "userreports.php",
                async: false,
                data: formData,
                //dataType: 'json',
                success: function(data) {
                 // alert(data);
                 //document.write(data);
                   if (data != null)
                   {
							  // alert(data.ptype)
					 $('#buypost').empty();
					   $('#buypost').append(data);
					   $("#defaultrow").hide();
                    } 
					else
                    {
					$('#buypost').append('<tr class="success"><td width="10%" colspan="3">Data does not exist</td></tr>');
					 $("#defaultrow").hide();
                        //alert("Data does not exist");
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>