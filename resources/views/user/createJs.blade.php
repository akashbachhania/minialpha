<script type="text/javascript">
	jQuery(document).ready(function() {
       var indmode="industaction";
         var formData = {
            _token:$('input[name=_token]').val(),
            type:indmode
         };
         $.ajax({
            type: "POST",
            url: "{{url('setting/industry_ajax')}}",
            data: formData,
            dataType: 'json',
            success: function(data) {
               $.each(data.industry, function(i, data)
                  {
                  console.log(data);
                     var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
                     $(div_data).appendTo("#indmultipleSelect");
                  });
            }
         });
   });

    $(document).ready(function() {
        $("#indmultipleSelect").change(function() {
            var industries = [];
            $.each($(".industry option:selected"), function() {
                industries.push($(this).val());
            });
            //alert(industries.join(","));
            var selectedindustry = industries.join(",");
            var indmode = "selectindustry";
            var formData = {
                ind_id: selectedindustry,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#subcat").empty();
                    $("#cat").empty();
                    $("#brands").empty();
                    var div_data = "<option value=>Select Sub Categories</option>";
                    $(div_data).appendTo("#subcat");
                    var div_data = "<option value=>Select  Categories</option>";
                    $(div_data).appendTo("#cat");
                    var div_data2 = "<option value=>Select  Brands</option>";
                    $(div_data2).appendTo("#brands");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#cat");
                            });
                        $.each(data.brandData, function(i, data)
                            {
                                var div_data2 = "<option value=" + data.brandid + ">" + data.name + "</option>";
                                $(div_data2).appendTo("#brands");
                            });
                    } else
                    {
                        //alert("Categories does not exist");	
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });	
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#cat").change(function() {
            var categories = [];
            $.each($(".category option:selected"), function() {
                categories.push($(this).val());
            });
            //alert(industries.join(","));
            var selectcategories = categories.join(",");
            var indmode = "selectsubcategory";
            var formData = {
                cat_id: selectcategories,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#subcat").empty();
                    var div_data = "<option value=>Select Sub Categories</option>";
                    $(div_data).appendTo("#subcat");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#subcat");
                            });
                    } else
                    {
                        //alert("SubCategories does not exist");				
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>