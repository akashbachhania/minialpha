@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">User Activity</h3>
            <div class="pagination-panel pull-right"> 
               <a href="#" id="btnnew" class="btn green" data-toggle="modal">
                 <i class="fa fa-plus"> </i>&nbsp; Add New 
               </a> 
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
             <div class="row">
        <div class="col-md-12">
          <div class="input-icon right"> <i class="icon-magnifier"></i>
            <input type="text" autocomplete="off" class="search form-control input-circle" name="searchid" id="searchid" placeholder="Enter Product Name...">
          </div>
          <div id="result" style="position:absolute; z-index:5; margin-left:5px; width:96%; font-weight:600; background-color: #ffffff; border: opx solid black; opacity: 0.8; filter: alpha(opacity=60); "></div>
        </div>
            {!! Form::open(array('url' => 'product/store','method'=>'post','id'=>'form_add_product','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
            
               <div class="col-md-5">
                  <div class="blog-img blog-tag-data"> <img src="{{asset('img/large-blank.png')}}" alt="" class="img-responsive hideMeBoy"><br>
                     <div class="col-md-12" style="padding-left:0px;">
                        <div id="ParentDiv">
                           <div id="filediv">
                              <input name="file[]" type="file" id="file786" class="myClass" accept="image/*" />
                              <br>
                           </div>
                           <div id="filedivBawa" style="display:none;">
                              <input name="file[]" type="file" id="file786" class="myClass" accept="image/*" /><br>
                           </div>
                           <input type="button" id="add_more" class="upload btn btn-success" value="Upload"/>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7">
                  
                  <div class="table-responsive">
                    <table class="table" border="1" bordercolor="#dadcdd">
                      <tbody>
                        <tr bgcolor="#ffffff">
                          <td id="herry" width="30%"><strong>Product ID </strong></td>
                          <td id="herry" width="70%"><div align="left" id="productid"> </div></td>
                        </tr>
                        <tr bgcolor="#f9f9f9">
                          <td id="herry" width="30%"><strong>Product Name </strong></td>
                          <td id="herry" width="70%"><div align="left" id="name"></div></td>
                        </tr>
                        <tr>
                          <td id="herry" width="30%"><strong>Industry</strong> </td>
                          <td id="herry" width="70%"><div align="left" id="industryid"> </div></td>
                        </tr>
                        <tr bgcolor="#f9f9f9">
                          <td id="herry" width="30%"><strong>Category </strong></td>
                          <td id="herry" width="70%"><div align="left" id="catid"> </div></td>
                        </tr>
                        <tr>
                          <td id="herry" width="30%"><strong>Sub-Category </strong></td>
                          <td id="herry" width="70%"><div align="left" id="subcatid"> </div></td>
                        </tr>
                        <tr bgcolor="#f9f9f9">
                          <td id="herry" width="30%"><strong>Brand</strong> </td>
                          <td id="herry" width="70%"><div align="left" id="brandid"></div></td>
                        </tr>
                        <tr>
                          <td id="herry" width="30%"><strong>Manufacturer </strong></td>
                          <td id="herry" width="70%"><div align="left" id="manufacture"></div></td>
                        </tr>
                        <tr bgcolor="#f9f9f9">
                          <td id="herry" width="30%"><strong>Case Weight </strong> </td>
                          <td id="herry" width="70%"><div align="left" id="caseweight"></div></td>
                        </tr>
                        <tr>
                          <td id="herry" width="30%"><strong>Quantity Per Case </strong></td>
                          <td id="herry" width="70%"><div align="left" id="qtypercase"></div></td>
                        </tr>
                        <tr bgcolor="#f9f9f9">
                          <td id="herry" width="30%"><strong>Packaging </strong></td>
                          <td id="herry" width="70%"><div align="left" id="pakaging"></div></td>
                        </tr >
                        <tr>
                          <td id="herry" width="30%"><strong>Shipping Conditions </strong></td>
                          <td id="herry" width="70%"><div align="left" id="shipingcondition"></div></td>
                        </tr>
                      
                      </tbody>
                    </table>
                  </div>
            </form>
         </div><!-- /.box -->
    </div>
  </div>
</section>
<script type="text/javascript">

        $(".search").keyup(function() {
            var searchid = $(this).val();
            var dataString = {searchid:searchid,'_token': $('input[name=_token]').val()};
            if (searchid != '') {
                $.ajax({
                    type: "POST",
                    url: "search.php",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        $("#result").html(html).show();
                    }
                });
            }
      else
      {
       $.ajax({
                    type: "POST",
                    url: "search.php",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        $("#result").html('').show();
                    }
                });
      }
            return false;
        });
        jQuery(document).live("click", function(e) {
            var $clicked = $(e.target);
            if (!$clicked.hasClass("search")) {
                jQuery("#result").fadeOut();
            }
        });
        $('#searchid').click(function() {
            jQuery("#result").fadeIn();
        });

</script>
<style type="text/css">
.content {
  width:900px;
  margin:0 auto;
}
/*  #searchid
  {
    width:500px;
    border:solid 1px #000;
    padding:10px;
    font-size:14px;
  }
*/  /*#result
  {
    position:absolute;
    width:500px;
    padding:10px;
    display:none;
    margin-top:-1px;
    border-top:0px;
    overflow:hidden;
    border:1px #CCC solid;
    background-color: white;
  }*/
  .show {
  padding:10px;
  border-bottom:1px #999 dashed;
  font-size:15px;
  height:50px;
}
.show:hover {
  background:#1caf9a;
  color:#fff;
  cursor:pointer;
}
</style>

@endsection