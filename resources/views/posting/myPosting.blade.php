@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-md-2">
                <h3 class="box-title">My Postings</h3>    
              </div>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                      <div class="form-group">
                         <label class="m_r10">
                           <input type="radio" name="archived" value="1" id="archived">
                           Archived
                         </label>      
                         <label class="m_r10">
                           <input type="radio" name="archived" value="2" id="archived">
                           Active
                         </label>
                         <label>
                           <input type="radio" name="archived" value="3" id="archived" checked="checked">
                           All
                         </label>
                      </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                      @if ( Auth::user()->roletype == 'AD' ) 
                        <a href="{{ url('posting/create') }}" class="btn btn-success pull-right">Post New Ad</a>
                      @else 
                        
                        @if($checkVal[0]['AddP']=='1')
                          <a href="{{ url('posting/create') }}" class="btn btn-success pull-right">Post New Ad</a>
                        @endif

                      @endif        
                  </div>
                </div>
              </div>
            </div>
            
          </div><!-- /.box-header -->
          <div class="box-body">

              <table id="myPostingTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="8%"> Posting ID </th>
                    <th width="10%"> Date Posted </th>
                    <th width="5%"> Brand </th>
                    <th width="5%"> Type </th>
                    <th width="5%"> Quantity</th>
                    <th width="29%"> Product Name </th>
                    <th width="5%"> Price </th>
                    <th width="5%"> Curr</th>
                    <th width="8%"> Cust Ref</th>
                    <th width="5%"> Status </th>
                    <!-- <th width="30%"> Action</th> -->
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@include('posting.myPostingJs')
@endsection