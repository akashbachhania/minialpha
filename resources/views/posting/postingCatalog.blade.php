@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Postings</h3>
            <a href="{{ url('posting/create') }}" class="btn btn-success pull-right">Post New Ad</a>
          </div><!-- /.box-header -->
          <div class="box-body">
            @if(Auth::user()->roletype == "AD")
              <table id="postingCatalogTable1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th><input class="group-checkable" type="checkbox" data-set="#sample_1 .checkboxes"></th>
                    <th> Posting ID </th>
                    <th> Date Posted </th>
                    <th> Industry </th>
                    <th> Category </th>
                    <th> Brand </th>
                    <th> Type</th>
                    <th> Quantity</th>
                    <th> Product Name </th>
                    <th> Exp Date </th>
                    <th> Status </th>
                  </tr>
                </thead>
              </table>

            @else
              <table id="postingCatalogTable2" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th> Posting ID </th>
                    <th> Date Posted </th>
                    <th> Industry </th>
                    <th> Category </th>
                    <th> Brand </th>
                    <th> Type</th>
                    <th> Quantity</th>
                    <th> Product Name </th>
                    <th> Exp Date </th>
                  </tr>
                  
                </thead>
                <tbody>
                </tbody>
              </table>
            @endif
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@endsection