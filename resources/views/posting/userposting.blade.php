@extends('layout.master')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Customer Postings</h3>
      </div>
      <div class="box-body table-responsive">
        <table id="customerPostingTable" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="8%"> Posting ID </th>
              <th width="10%"> Date Posted </th>
              <th width="5%"> Brand </th>
              <th width="5%"> Type</th>
              <th width="5%"> Quantity</th>
              <th width="30%"> Product Name </th>
              <th width="5%"> Price </th>
              <th width="5%"> Curr </th>
              <th width="10%"> Cust Ref </th>

              @if( Auth::user()->roletype == 'AM' )
              <th width="5%"> Status </th>
              <th width="12%"> Action </th>
              @else
              <th width="5%"> Status </th>
              @endif
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@include('posting.userpostingJs')
@endsection