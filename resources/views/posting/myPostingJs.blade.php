<script type="text/javascript">
	
    var myPostingTable = $('#myPostingTable').DataTable( {

	    "ajax": {
	        "url":"myPosting_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	    },
	    columns: [
	    { data: 'postno' },
	    { data: 'sdate' },
	    { data: 'brand' },
	    { data: 'ptype' },
	    { data: 'quantity' },
	    { data: 'productName' },
	    { data: 'price' },
	    { data: 'currency'},
	    { data: 'customerrefno'},
	    { data: 'poststatus'},
	    // { data: 'edit'}
	 
	    ]
	});
    function cancelme(obj) {
        var data = "Type=cancelpost&postid=" + obj;
        if (confirm("Are you sure you wish to cancel this Posting?")) {
            $.ajax({
                type: 'POST',
                url: root + '/posting/cancelPosting',
                cache: false,
                data: data,
                success: function(data, status, xhr) {
                    //alert(data);
                    window.location = root + '/posting/myPosting';
                },
                error: function(jqXHR, textStatus, errorThrown) {}
            });
        }
    }
</script>