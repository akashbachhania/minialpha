@extends('layout.master')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
      		<h3 class="box-title">Create New Ad</h3>
          </div>
          <div class="box-body">
          	<div class="row">
          		<div class="col-md-12">
 	              <input type="text" autocomplete="off" class="search form-control input-circle" name="searchid" id="searchid" placeholder="Enter Product Name...">
 	              <div id="result" style="position:absolute; z-index:5; margin-left:5px; width:96%; font-weight:600; background-color: #ffffff; border: opx solid black; opacity: 0.8; filter: alpha(opacity=60); "></div>
          		</div>
          	</div>
          	<div class="row" style="margin-top: 10px;">
          		<div class="col-md-4">
          			<div class="blog-img blog-tag-data" id="fimges"> 
          				<img src="{{asset('img/large-blank.png')}}" alt="" class="img-responsive hideMeBoy">
	                  </div>
          		</div>
          		<div class="col-md-8">
          			
		            <div class="table-responsive no-padding">
		              <table id="productviewtable" class="table table-hover">
		                <tbody><tr>
		                  <td><b>Product ID</b></td>
		                  <td>:</td>
		                  <td id="productid"></td>
		                </tr>
		                <tr>
		                  <td><b>Product Name</b></td>
		                  <td>:</td>
		                  <td id="name"></td>
		                </tr>
		                <tr>
		                	<td><b>Industry</b></td>
		                	<td>:</td>
		                	<td id="industryid"></td>
		                </tr>
		                <tr>
		                  <td><b>Category</b></td>
		                  <td>:</td>
		                  <td id="catid"></td>
		                </tr>
		                <tr>
		                  <td><b>Sub-Category</b></td>
		                  <td>:</td>
		                  <td id="subcatid"></td>
		                </tr>
		                <tr>
		                  <td><b>Brand</b></td>
		                  <td>:</td>
		                  <td id="brandid"></td>
		                </tr>
		                <tr>
		                  <td><b>Manufacturer</b></td>
		                  <td>:</td>
		                  <td id="manufacture"></td>
		                </tr>
		                <tr>
		                  <td><b>Case Weight</b></td>
		                  <td>:</td>
		                  <td id="caseweight"></td>
		                </tr>
		                <tr>
		                  <td><b>Quantity Per Case</b></td>
		                  <td>:</td>
		                  <td id="qtypercase"></td>
		                </tr>
		                <tr>
		                  <td><b>Packaging</b></td>
		                  <td>:</td>
		                  <td id="pakaging"></td>
		                </tr>
		                <tr>
		                  <td><b>Shipping Conditions</b></td>
		                  <td>:</td>
		                  <td id="shipingcondition"></td>
		                </tr>
		                <tr bgcolor="{{ $x }}">
		                  <td><b>Product Codes </b></td>
		                  <td>:</td>
		                  <td id="procodes"></td>
		                </tr>
		                
						  @if((Auth::user()->roletype =="AD")||(Auth::user()->roletype =="SA"))
							
		                    <tr {{ ($x=='#ffffff')?"bgcolor='#f9f9f9'":"bgcolor='#ffffff'" }}>
		                      <td><b>Description</b></td>
		                      <td>:</td>
		                      <td id="description"></td>
		                    </tr>

						   @else
							
		                    <tr {{ ($x =='#ffffff')?"bgcolor='#f9f9f9'":"bgcolor='#ffffff'" }}>
		                      <td><b>Description</b></td>
		                      <td>:</td>
		                      <td id="description"></td>
		                    </tr>
		                    @endif
		                
		              </tbody></table>
		            </div><!-- responsive table div -->
          		</div>
          	</div>

          	<div class="row">
          		<div class="col-md-3">
          			<label class="pull-right"> * Denotes Required Fields</label>
          		</div>
          		<div class="col-md-9">
          			<form action="{{ url('posting/createPosting') }}" id="crateadd" name="crateadd" method="post" class="form-horizontal" onsubmit="return(validate());">

          				<input type="hidden" name="addprono" id="addprono"/>
                        
                        <div class="form-group">
                          <label class="col-md-4 control-label">Type <span class="required" aria-required="true">*</span>:</label>
                          <div class="col-md-6">
                            
                              <select class="form-control" name="addtype" id="addtype">
                                <option value="">Select Type</option>
                                <option value="BUY">Buy</option>
                                <option value="SELL">Sell</option>
                              </select>
                            
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-md-4 control-label">Target Price <span class="required" aria-required="true">*</span>:</label>
                          <div class="col-md-6">
                            
                              <input type="text" class="form-control" name="addprice" id="addprice" placeholder="Enter Price">
                            
                          </div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Currency<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		
                        		<select class="form-control" name="addcurrency" id="addcurrency">
	                                <option value="" selected>Select Currency</option>
    									
										@foreach( $currencies as $currency )
									
									      <option value="{{ $currency->name }}">{{ $currency->name }}</option>
	                                    
	                                    @endforeach
	                                  </select>
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Unit Of Measurement :</label>

                        	<div class="col-md-6">
                        		<select class="form-control" name="adduom" id="adduom">
                                    <option value="">Select Unit of Measurement</option>

                                    @foreach( $uoms as $uom )
                                    	<option value="{{ $uom->name }}">{{ $uom->name }}</option>
                                    @endforeach

                                </select>
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Quantity<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		<input type="text" class="form-control" name="addqty" id="addqty" placeholder="Enter Quantity">
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Location<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		<select class="form-control" name="addlocation" id="addlocation">
                                    <option value="">Select Location</option>

                                    @foreach( $locations as $location )
                                    	<option value="{{ $location->name }}">{{ $location->name }}</option>
                                    @endforeach

                                </select>
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Expiry Date Range<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		<select class="form-control" name="addexpdrange" id="addexpdrange">
                                    <option value="">Select Expiry Date Range</option>

                                    @foreach( $expdateranges as $expdaterange )
                                    	<option value="{{ $expdaterange->name }}">{{ $expdaterange->name }}</option>
                                    @endforeach

                                </select>
                        	</div>
                        </div>

                        <div class="form-group" id="epd">
                        	<label class="col-md-4 control-label">Exact Expiry Date :</label>

                        	<div class="col-md-6">
                        		<div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                      </div>
			                      <input type="text" name="datepicker" class="form-control pull-right" id="datepicker">
			                    </div>
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Customer Refrence No.<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		<select class="form-control" name="addcurrefno" id="addcurrefno">
                                    <option value="">Select Customer</option>

                                    @foreach( $cusrefnos as $cusrefno )
                                    	<option value="{{ $cusrefno->refno }}">{{ $cusrefno->refno }}</option>
                                    @endforeach

                                </select>
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Packaging Language(s)<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		<select multiple class="form-control industry" id="selectedlanguage" name="selectedlanguage[]">
                                    <option value="">Select Language</option>

                                    @foreach( $languages as $language )
	                                    <option value="{{ $language->languageid }}" {{ ($language->name == "English") ? 'Selected="Selected"':"" }} >{{ $language->name }}</option>
                                    @endforeach

                                </select>
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Timeframe<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		<select class="form-control" name="addtimeframe" id="addtimeframe">
                                    <option value="">Select a Timeframe</option>

                                    @foreach( $timeframes as $timeframe )
                                    	<option value="{{ $timeframe->name }}">{{ $timeframe->name }}</option>>
                                    @endforeach
                                </select>	
                        	</div>
                        </div>

                        <div class="form-group">
                        	<label class="col-md-4 control-label">Country Of Origin<span class="required" aria-required="true">*</span>:</label>

                        	<div class="col-md-6">
                        		<select  class="form-control" name="addcountry[]"   id="addcountry" multiple class="form-control country">
                                  <option value="">Select Countries</option>

                                  @foreach( $countries as $country )

                                  	<option value="{{ $country->name }}">{{ $country->name }}</option>

                                  @endforeach
                                </select>
                        	</div>
                        </div>

                        @if( Auth::user()->roletype == 'AD' )


                        	<input type="hidden" name="key" id="key" value="12" class="btn green">

                            <a href="my-posting.php" class="pull-right m_r20">
                            	<button type="button" id="cancel" class="btn btn-default">Cancel</button>
                            </a>

                            <input type="submit" name="createadd" id="createadd" value="Submit" class="btn btn-success pull-right m_r5">

                        @else

                        	@if($checkVal[0]->AddP == '1')


		                        	<input type="hidden" name="key" id="key" value="12" class="btn green">

		                            <a href="my-posting.php" class="pull-right m_r20">
		                            	<button type="button" id="cancel" class="btn btn-default">Cancel</button>
		                            </a>

		                            <input type="submit" name="createadd" id="createadd" value="Submit" class="btn btn-success pull-right m_r5">

		                    @endif
		                @endif	
          			</form>
          		</div>
          	</div>
          </div>
        </div>
    </div>
</div>
@include('posting.js')
@endsection