@if(\Request::input('updatepost')=='true')

<script type="text/javascript"> alert('Post Updated Successfully'); </script>

@endif

@if( Auth::user()->roletype == 'AD' )
<script type="text/javascript">
	
	var customerPostingTable = $('#customerPostingTable').DataTable( {

	    "ajax": {
	        "url":root+"/posting/user-posting_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	        data:{
	        	postuserid:"{{ \Request::segment(3) }}"
	        }
	    },
	    columns: [
	    { data: 'postno' },
	    { data: 'sdate' },
	    { data: 'brand' },
	    { data: 'ptype' },
	    { data: 'quantity' },
	    { data: 'productName' },
	    { data: 'price'},
	    { data: 'currency'},
	    { data: 'customerrefno'},
	    { data: 'edit1'}
	 
	    ]
	});

	setInterval( function () {
	    customerPostingTable.ajax.reload();
	}, 3000 );	
</script>
@else
<script type="text/javascript">
	
	var customerPostingTable = $('#customerPostingTable').DataTable( {

	    "ajax": {
	        "url":root+"/posting/user-posting_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	        data:{
	        	postuserid:"{{ \Request::segment(3) }}"
	        }
	    },
	    columns: [
	    { data: 'postno' },
	    { data: 'sdate' },
	    { data: 'brand' },
	    { data: 'ptype' },
	    { data: 'quantity' },
	    { data: 'productName' },
	    { data: 'price'},
	    { data: 'currency'},
	    { data: 'customerrefno'},
	    { data : 'poststatus'},
	    { data: 'edit1'}
	 
	    ]
	});

	setInterval( function () {
	    customerPostingTable.ajax.reload();
	}, 3000 );	
</script>
@endif

<script type="text/javascript">
    function saveData(val) {

        var promode = "deleteproduct";

        var formData = {

            pro_id: val,

            actionmode: promode

        };

        $.ajax({

            url: "functions.php",

            type: "POST",

            data: formData,

            success: function(data) {

                alert(data);

            }

        });

    }
</script>
<script>
    function cancelme(obj) {
        var data = "Type=cancelcustpost&postid=" + obj;
        if (confirm("Do You Want to Cancel this Posting?")) {
            $.ajax({
                type: 'POST',
                url: 'functions.php',
                cache: false,
                data: data,
                success: function(data, status, xhr) {
                   //alert(data);
				   //document.write(data);
                    window.location = "";
                },
                error: function(jqXHR, textStatus, errorThrown) {}
            });
        }
    }

    jQuery(document).ready(function(){
    	var checkAD='{{ Auth::user()->roletype }}';
		
		if(checkAD=="AD"){

	      var table = $('#customerPostingTable').DataTable();
	      
	      $('#customerPostingTable tbody').on('click', 'tr td:not(:First-child,:last-child)', function() {
	            var data = table.row(this).data();
	            var page = $(this).parent().find('td a').attr('href');
	            window.location.href = page;
	             //window.open(page, '_blank');
	      });
		}
    });
</script>
<script type="text/javascript">
    function getState(val) {

        var indmode = "category";

        $('#sub_cat').empty();

        var formData = {

            ind_id: val,

            actionmode: indmode

        };

        $.ajax({

            type: "POST",

            url: "functions.php",

            data: formData,

            dataType: 'json',

            success: function(data) {

                $("#cat_id").empty();

                var div_data = "<option value=0>Select Category</option>";

                $(div_data).appendTo("#cat_id");

                $.each(data.itemData, function(i, data) {

                    var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";

                    $(div_data).appendTo("#cat_id");

                });

            }

        });

    }

    //sub category function start

    function subcat(val) {

        var indmode = "subcat";

        var formData = {

            cat_id: val,

            actionmode: indmode

        }; //Array 

        $.ajax({

            type: "POST",

            url: "functions.php",

            data: formData,

            dataType: 'json',

            success: function(data) {

                $("#sub_cat").empty();

                var div_data = "<option value=0>Select Sub Category</option>";

                $(div_data).appendTo("#sub_cat");

                $.each(data.itemData, function(i, data) {

                    var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";

                    $(div_data).appendTo("#sub_cat");

                });

            }

        });

    }

    function getbrand(val) {

        var indmode = "brand";

        var formData = {

            ind_id: val,

            actionmode: indmode

        }; //Array 

        $.ajax({

            type: "POST",

            url: "functions.php",

            data: formData,

            dataType: 'json',

            success: function(data) {

                $("#brand").empty();

                var div_data = "<option value=0>Select Brand</option>";

                $(div_data).appendTo("#brand");

                $.each(data.itemData, function(i, data) {

                    var div_data = "<option value=" + data.brandid + ">" + data.name + "</option>";

                    $(div_data).appendTo("#brand");

                });

            }

        });

    }
</script>