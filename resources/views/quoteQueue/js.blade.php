<script type="text/javascript">
	
    var posting_quoteandpost = $('#posting_quoteandpost').DataTable( {

	    "ajax": {
	        "url":"quoteandpost_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	    },
	    columns: [
	    { data: 'postno' },
	    { data: 'sdate' },
	    { data: 'ptype' },
	    { data: 'quantity' },
	    { data: 'productNameshow' },
	    { data: 'price' },
	    { data: 'currency'},
	    { data: 'timeframe'},
	    { data: 'expdate'},
	    { data: 'poststatus'}
	 
	    ]
	});
    $('#posting_quoteandpost tbody').on('click', 'tr td:not(:first-child)', function () { 
        var data = posting_quoteandpost.row(this).data();
        var page = $(this).parent().find('td #quoest').attr('href');
        window.location.href = page;
    });

	setInterval( function () {
	    posting_quoteandpost.ajax.reload();
	}, 3000 );  


    var quotes_quoteandpost = $('#quotes_quoteandpost').DataTable( {

	    "ajax": {
	        "url":"quoteandpost1_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	    },
	    columns: [
	    { data: 'quoteid' },
	    { data: 'offerdate' },
	    { data: 'ptype' },
	    { data: 'quantity' },
	    { data: 'productNameshow' },
	    { data: 'price' },
	    { data: 'currency'},
	    { data: 'timeframe'},
	    { data: 'expdate'},
	    { data: 'quationstatus'}
	 
	    ]
	});

	// setInterval( function () {
	//     quotes_quoteandpost.ajax.reload();
	// }, 3000 );  

</script>