@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Postings</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="posting_quoteandpost" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Post No</th>
                  <th>Date Quote</th>
                  <th>Type</th>
                  <th>Quantity</th>

                  @if(Auth::user()->roletype != 'AD')

                     
                     @if($checkMaskVal[0]['maskstatus']=='1')
                         
                        <th> Product Name</th>
                     
                     @else
                        <th> Product ID</th>
                           
                     @endif
                  
                  @else

                      <th> Product Name</th>
                  
                  @endif

                  <th> Price </th>
                  <th> Curr </th>
                  <th>Timeframe </th>
                  <th> Exp Date </th>
                  <th> Status </th>

                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Quotes</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="quotes_quoteandpost" class="table table-bordered table-striped">
              <thead>
                 <tr>
                    <th> Quote ID </th>
                    <th> Date Quote </th>
                    <th> Type </th>
                    <th> Quantity</th>

                       
                    @if(Auth::user()->roletype != 'AD')
                    
                    
                      @if($checkMaskVal[0]['maskstatus']=='1')
                      
                         <th> Product Name</th>
                       
                      @else
                      
                         <th> Product ID</th>
                             
                      @endif
                    
                    @else
                      
                    <th> Product Name</th>
                    
                    @endif
                      <th> Price </th>
                      <th> Curr </th>
                      <th>Timeframe </th>
                      <th> Exp Date </th>
                      <th> Status </th>
                 </tr>
              </thead>

              <tbody>
              </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>

</section>
@include('quoteQueue.js')
@endsection