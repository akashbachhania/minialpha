<script type="text/javascript">
   
   function offeradduser(obj, obj1,obj2) {
    //$('#basic').modal('show');
    var userpostId ="{{ \Request::segment(3) }}";
	//alert(userpostId);
    var usertype =obj1;
	var userpost =obj2;
	var userptype="{{ \Request::segment(5) }}";
	var userid ="{{ Auth::user()->userid }}";
	var data={Type:'offertypeuser',userpostId:userpostId,usertype:usertype,userid:userid,userpost:userpost,userptype:userptype};
	
	        $.ajax({
	        type: "POST",
	        url: root+ '/quoteQueue/quoteDetailViewFunction',
	        data: data,
	        cache: false,
	        success: function(data) {

				if(data=='1')
				{
                	var res5 = "Successfully assigned "+usertype;
				}
				else
				{
				 	var res5 = "Successfully Unassigned Submitted "+usertype;
				}
                var message5 = res5;
                // shownoti(message5);
                $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
				setTimeout(function() {
                    window.location.href = "";
                }, 2000);
	        }
	    });
    }

    function adduser(obj, obj1,obj2) {
        //$('#basic').modal('show');
        var userpostId ="{{ \Request::segment(3) }}";
		//alert(userpostId);
        var usertype =obj1;
		var userpost =obj2;
		var userid ="{{ Auth::user()->userid }}";
		var data={Type:'typeuser',userpostId:userpostId,usertype:usertype,userid:userid,userpost:userpost};
		
        $.ajax({
	        type: "POST",
	        url: root+ '/quoteQueue/quoteDetailViewFunction',
	        data: data,
	        cache: false,
	        success: function(data) {
				if(data=='2')
				{
                	var res5 = "Successfully assigned "+usertype;
				}
				else
				{
				 	var res5 = "Successfully Unassigned "+usertype;
				}
                var message5 = res5;
                // shownoti(message5);
                $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
				
	        }
	    });
    }

    jQuery(document).ready(function() {
    	var offerhistoryTable = $('#offerhistoryTable').DataTable({
			"ajax": {
	            "url":root+ "/quoteQueue/offerHistory",
	            "dataSrc": "",
	            "data": {'post_id':postno},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'quotationno'},
	            { data: 'dateOffered' },
	            { data: 'user' },
	            { data: 'price' },
	            { data: 'curr' },
	            { data: 'uom' },
	            { data: 'quantity' },
	            { data: 'timeframe' },
	            { data: 'expdaterange' }
	        ],
		});

        $('#offerhistoryTable tbody').on('click', 'tr td:not(:nth-child(3))', function() {
            var data3 = table.row(this).data();
            var page3 = $(this).parent().find('td #postrow').attr('href');
            window.location.href = page3;
        });    	

    	var activeSellPostingTable = $('#activeSellPostingTable').DataTable({
			"ajax": {
	            "url":root+ "/quoteQueue/activeSellPosting",
	            "dataSrc": "",
	            "data": {'prodno': '{{ \Request::segment(4) }}','ptype':'{{ \Request::segment(5) }}'},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'sdate'},
	            { data: 'productno' },
	            { data: 'postno' },
	            { data: 'ptype' },
	            { data: 'quantity' },
	            { data: 'targetprice' },
	            { data: 'currency' },
	            { data: 'uom' },
	            { data: 'timeframe' },
	            { data: 'expdate'}
	        ],
		});

        $('#activeSellPostingTable tbody').on('click', 'tr td:not(:nth-child(3))', function() {
            var data2 = table.row(this).data();
            var page2 = $(this).parent().find('td #postrow').attr('href');
            window.location.href = page2;
        });    	

    	var activeBuyPostingTable = $('#activeBuyPostingTable').DataTable({
			"ajax": {
	            "url":root+ "/quoteQueue/activeBuyPosting",
	            "dataSrc": "",
	            "data": {'prodno': '{{ \Request::segment(4) }}','ptype':'{{ \Request::segment(5) }}'},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'sdate'},
	            { data: 'productno' },
	            { data: 'postno' },
	            { data: 'ptype' },
	            { data: 'quantity' },
	            { data: 'targetprice' },
	            { data: 'currency' },
	            { data: 'uom' },
	            { data: 'timeframe' },
	            { data: 'expdate'}
	        ],
		});

        $('#activeBuyPostingTable tbody').on('click', 'tr td:not(:nth-child(3))', function() {
            var data1 = table.row(this).data();
            var page1 = $(this).parent().find('td #postrow').attr('href');
            window.location.href = page1;
        });    	

    	var historicalTransactionTable = $('#historicalTransactionTable').DataTable({
			"ajax": {
	            "url":root+ "/quoteQueue/historicalTransaction",
	            "dataSrc": "",
	            "data": {'prodno': '{{ \Request::segment(4) }}','postno':'{{ \Request::segment(3) }}'},
	            "type": 'POST',
	        },
	        columns: [
	        	{ data: 'sdate'},
	            { data: 'productno' },
	            { data: 'postno' },
	            { data: 'ptype' },
	            { data: 'quantity' },
	            { data: 'targetprice' },
	            { data: 'currency' },
	            { data: 'uom' },
	            { data: 'timeframe' },
	            { data: 'expdate'},
	            { data: 'status1'}
	        ],
		});

        $('#historicalTransactionTable tbody').on('click', 'tr td:not(:nth-child(3))', function() {
            var data = table.row(this).data();
            var page = $(this).parent().find('td #postrow').attr('href');
            window.location.href = page;
        });    	

		$(".clickable-row").click(function() {
		    //alert('hello');
		    window.document.location = $(this).data("href");
		});          

        $("#btnsave").on("click", function() {
            var name = $('#name').val();
            var userpostId = $('#userpostId').val();
            var usertype = $('#usertype').val();
            if (name == '') {
                alert('Name must be Required');
                return;
            } else {
                var data = {
                    Type: 'typeuser',
                    name: name,
                    userpostId: userpostId,
                    usertype: usertype,
                    userid: userid
                };
                $.ajax({
                    type: 'POST',
                    url: root+ '/quoteQueue/quoteDetailViewFunction',
                    cache: false,
                    data: data,
                    success: function(data) {
                        //alert(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        //commit(false);
                    }
                });
            }
        });

        function init() {
            // Clear forms here
           // var inputVal = document.getElementById("qquantity").value = "";
            var authpass = document.getElementById("authpass").value = "";
        }
        window.onload = init;

        var postno = '{{ \Request::segment(3) }}';

     	var stresult ='{{ $mypoststatus }}';
     	var type = "fetchquotepostdata";
        var dataString = 'postno=' + postno + '&type=' + type;

        if (postno != '') {
            $.ajax({
            	type: "POST",
                url: root + "/quoteQueue/fetchFunction",
                data: dataString,
                cache: false,
                success: function(response) {
                	console.log(response);
                	var pro_id = response.productid;
                    var postno = response.postno;
                    $('#postno').html(response.postno);
                    $('#postno').val(response.postno);
                    $('#productno').val(response.productno);
                    var mpmvalue = response.mpm;
                    if (mpmvalue != null) {
                        $('#mpmval').html(response.mpm);
                    } else {
                        var mpval = 0;
                        $('#mpmval').html(mpval);
                    }
                    $('#puserid').val(response.userid);
                    $('#pdate').html(response.pdate);
  					if((response.ptype=='BUY') || (response.ptype=='Buy'))
  					{
	  					$('#buyertr').show();
	  					$('#sellertr').hide();
  					}
  					if((response.ptype=='SELL') || (response.ptype=='Sell'))
  					{
	  					$('#sellertr').show();
	  					$('#buyertr').hide();
  					}
  					$('#pname').html(response.postno);
                    $('#proid').html(response.productno);
                    $('#productid2').val(response.productno);
                    $('#userid').html(response.userid);
                    $('#name').html(response.name);
                    $('#curr').html(response.currency);
                    $('#pcurr').val(response.currency);
                    $('#puom').val(response.uom);
                    $('#plocation').val(response.location);
                    $('#pexpirydate').val(response.expirydate);
                    $('#pplang').val(response.pakaging);
                    $('#plang').html(response.pakaging);
                    $('#shipcond').html(response.shipingcondition);
                    $('#corigin').html(response.country);
          					$('#buyer').html(response.buyer);
          					$('#seller').html(response.seller);
                    var uid = response.userid;
                    $('#cusrefno').html(response.customerrefno);
                    $('#targetprice').html(response.targetprice);
                    $('#location').html(response.location);
                    $('#pstatus').html(response.pstatus);
                    var poststatus = response.pstatus;

                    if ((poststatus =="Accepted")) {
      					$('#savequote').attr("disabled", "disabled");
      				}
      				else
  					{
	                    $("#savequote").prop("disabled", false);
  					}
  					$('#Timeframe').html(response.timeframe);
                    var exactexpdrange = response.expirydate;
                    if (exactexpdrange != null) {
                        $('#eed').html(response.expirydate);
                    } else {
                        $('#eed').html('Not Applicable');
                    }
                    if (exactexpdrange !='') {
                        $('#eed').html(response.expirydate);
                    } else {
                        $('#eed').html('Not Applicable');
                    }
                    $('#expdrange').html(response.expdate);
                    $('#uom').html(response.uom);
                    $('#qty').html(response.quantity);
                    $('#qquantity').val(response.quantity);
                    $('#ptype').html(response.ptype);
                    var posttype = response.ptype;
                    if (posttype == 'BUY') {
                        $("#expd").hide();
                    }
                    var type2 = "fetchcurrentoffers";
                    var dataString2 = {'postno' : postno, 'type' : type2};
                    
                    var currentOfferstable = $('#currentOfferstable').DataTable( {

			            "ajax": {
			                "url":root+ "/quoteQueue/fetchFunction",
			                "dataSrc": "",
                            "data": dataString2,
			                "type": 'POST',
			            },
			            columns: [
			            	{ data: 'quotationno'},
			                { data: 'dateOffered' },
			                { data: 'user' },
			                { data: 'price' },
			                { data: 'curr' },
			                { data: 'uom' },
			                { data: 'quantity' },
			                { data: 'timeframe' },
			                { data: 'expdaterange' }
			            ],
			            "language": {
					        "emptyTable": "No Current Offer Found"
					    },
			        });

                }
            });
        }
        return false;
    });    


</script>

<script type="application/javascript">
    jQuery(document).ready(function() {
        $("#go").on("click", function() {
            $('#basic').modal('show');
        });
    });
    $("#btnclose").click(function() {
        $('#basic').modal('hide');
        $('#form_sample_2')[0].reset();
    });
    jQuery(document).ready(function() {
        var authpass = "{{ Auth::user()->authorizedpass }}";
        
        $("#savequote").click(function() {
            if ($("#qprice").val() == '') {
                alert('Price Required');
                $("#qprice").focus();
                return false;
            }
            if ($("#qcurrency").val() == '') {
                alert('Currency Required');
                $("#qcurrency").focus();
                return false;
            }
            if ($("#qquantity").val() == '') {
                alert('quantity Required');
                $("#qquantity").focus();
                return false;
            }
            if ($("#qtimeframe").val() == '') {
                alert('Timeframe Required');
                $("#qtimeframe").focus();
                return false;
            }
            if ($("#qexpdtarnge").val() == '') {
                alert('Expiry Date Range Required');
                $("#qexpdtarnge").focus();
                return false;
            }
            if( $("#authpass").val() != authpass ){

				alert("Authorization Failed");
				$("#authpass").focus();
				return false;
			}
			
            if ($("#authpass").val() == '') {
                alert('Authorized code is required');
                $("#authpass").focus();
                return false;
            } else {
                //var postno = $('#postno').val();
				 var postno = '{{ \Request::segment(3) }}';
				//alert(postno);
                var productno = $('#productno').val();
                var counter_price = $("#qprice").val();
                var counter_quantity = $("#qquantity").val();
                var counter_timeframe = $("#qtimeframe").val();
                var counter_expdate = $("#qexpdtarnge").val();
                var puserid = $('#puserid').val();
                var type = 'addofferbyqf';
                var postcurr = $('#qcurrency').val();
                var postuom = $('#puom').val();
                var postloc = $('#plocation').val();
                var postexpiry = $('#pexpirydate').val();
                var postlang = $('#pplang').val();
				var data = {
			        type: type,
			        postno: postno,
			        productno: productno,
			        price: counter_price,
			        currency: postcurr,
			        uom: postuom,
			        quantity: counter_quantity,
			        location: postloc,
			        expdate: counter_expdate,
					expirydate: postexpiry,
			        language: postlang,
			        timeframe:counter_timeframe,
			        puserid: puserid,
			      }; 
            
                $.ajax({
                    type: "POST",
                    url: root + "/quoteQueue/addofferbyqf",
                    data: data,
                    cache: false,
					async: false,
					dataType: 'html',
                    success: function(data) {
					//alert(data);
                        if (data == 1) {
                            var res5 = "Successfully submitted offer.";
                            var message5 = res5;
                            // shownoti(message5);
                            $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
                            $('#form_add_product')[0].reset();
                            setTimeout(function() {
                                window.location.href = "quotesandpost_que";
                            }, 2000);
                        }
                    }
                });
            }
        });
    });
</script>

<script>
    jQuery(document).ready(function() {
        var qu_userid ="{{ Auth::user()->userid }}";
        var datapost={
            post_id:"{{ \Request::segment(3) }}",
            productno:"{{ \Request::segment(4) }}",
            typeMode:"checkQuotation",
        }
        $.ajax({
                type: "POST",
                url: root + "/quoteQueue/fetchFunction",
                data: datapost,
                cache: false,
                success:function(response)
                {
                    // response = jQuery.parseJSON(data);
                    var userId=response.userid;
                    if(qu_userid!=userId){
                        $('#savequote').attr("disabled", "disabled");
                    }
                
                }
            });
    });
</script>