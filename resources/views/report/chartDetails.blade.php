@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">User Activity</h3>
            <a href="{{(Request::segment(2) != '') ? url('exportActivity/'.Request::segment(2)): url('exportActivity') }}" class="pull-right btn btn-success col-sm-2">Export</a>
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="useractivity" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <!-- <th></th> -->
                  <th>User ID</th>
                  <th>Last Login</th>
                  <th>IP Address</th>
                  <th>Activity</th>
                </tr>
              </thead>
                <tbody>

                    <tr>
                      <!-- <td></td> -->
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>

              <tbody>
            </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@endsection