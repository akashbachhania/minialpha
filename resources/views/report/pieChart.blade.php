@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
          <div class="box-header">
          	 <h3 class="box-title">
          	 	{{ $headTitle }}
          	 </h3>
          </div>

          <div class="box-body">
          	<div class="row">
          		<div class="col-md-12">
          			<div class="box box-success">
          				<div class="box-header" style="background: #00a65a">
          					<h3 class="box-title" style="color: #fff;"><i class="fa fa-gift"></i>&nbsp;&nbsp;{{ $ra }}</h3>
          				</div>
          				<div class="box-body">
          					
          				</div>
          			</div>
          		</div>
          	</div>
          </div>

        </div>
    </div>
  </div>

@endsection