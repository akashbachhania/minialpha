<script type="text/javascript">

    var roleListTable = $('#roleListTable').DataTable( {

        "ajax": {
            "url":root + "/setting/roleList_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'name' },
        { data: 'basename' },
        { data: 'edit' }
     
        ]
    });

    setInterval( function () {
        roleListTable.ajax.reload();
    }, 5000 );
	
</script>

<script>
    function editme(obj) {
        $('#basic').modal('show');
        // $('#status').val('');
        $('#name').val('');
        $('#id').val(obj);
        setTimeout(function() {
            $('#name').focus();
        }, 1000);
        var data = "Type=search&id=" + obj;
        $.ajax({
            type: 'POST',
            url: root + '/setting/roleList_ajax',
            cache: false,
            data: data,
            success: function(response) {
                // response = jQuery.parseJSON(data);
                $('#status').val(response.status);
                $('#name').val(response.name);
				$('#baserole').val(response.baserole);
                $("#msgg").hide();
                $("#checkProduct").val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }

    function deleteme(obj) {
        var data = "Type=delete&id=" + obj;
        if (confirm("Do You Want to Delete this?")) {
            $.ajax({
                type: 'POST',
                url: root + '/setting/roleList_ajax',
                cache: false,
                data: data,
                success: function(response) {
                    // response = jQuery.parseJSON(data);
                    if (response.msg == 'ERROR') {
                        alert(response.name)
                    } else {
                        window.location = root + '/setting/roleList';
                    }
                    //grid.getDataTable().ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#name").change(function() {
            var pname = $("#name").val();
            if (pname != '')
            {
                $.ajax({
                    type: "POST",
                    url: root + '/setting/roleList_ajax',
                    async: false,
                    data: {
                        pname: $("#name").val(),
                        Type: 'exist'
                    },
                    success: function(data, status, xhr) {
                        if (data == 'exist') {
                            $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                            $("#checkProduct").val('0');
                        } else {
                            $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                            $("#checkProduct").val('1');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                        commit(false);
                    }
                });
            } else
            {
                $("#msgg").show().html('<span></span>');
            }
        });
        $("#btnclose").click(function() {
            $("#msgg").hide();
            $("#checkProduct").val('');
            $("#name").val('');
            $('#name').attr('autocomplete', 'false');
 			$('#basic').modal('hide');
        });
    });
    $("#btnnew").on("click", function() {
        $('#basic').modal('show');
        $("#msgg").hide();
        $("#checkProduct").val('');
    });

$("#btnsave").on("click", function() {
            var name = $('#name').val();
			var bname = $('#baserole').val();
            var status = $('#status').val();
            var id = $('#id').val();
            // $("#form_sample_2").valid();
            if (name == '' || status == '') {
                alert('Role Name Required');
                return;
            }
			if (bname == '' || status == '') {
                alert('Base Role Required');
                return;
            }
            if (id == "0") {
                //var data = "Type=add&name=" + name + "&status=" + status;
                var data = {
                    Type: 'add',
                    name: name,
					baserole: bname,
                    status: status
                };
                $.ajax({
                    type: 'POST',
                    url: root + '/setting/roleList_ajax',
                    cache: false,
                    data: data,
                    success: function(response) {
                        // response = jQuery.parseJSON(data);
                        if (response.msg == 'SUCCESS') {
                            $('#basic').modal('hide');
                            // grid.getDataTable().ajax.reload();
                            $("#msgg").hide();
                            $("#checkProduct").val('');
                            $("#name").val('');
                        }
                        if (response.msg == 'EROOR') {
                            alert(response.name);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        //commit(false);
                    }
                });
            } //if finish share
            else {
                //var data = "Type=update&name=" + name + "&status=" + status+"&id="+id;
                var data = {
                    Type: 'update',
                    name: name,
					baserole: bname,
                    status: status,
                    id: id
                };
                $.ajax({
                    type: 'POST',
                    url: root + '/setting/roleList_ajax',
                    cache: false,
                    data: data,
                    success: function(response) {
                        // response = jQuery.parseJSON(data);
                        if (response.msg == 'SUCCESS') {
                            $('#basic').modal('hide');
                            $("label.error").hide();
                            $(".error").removeClass("error");
                            // grid.getDataTable().ajax.reload();
                            $("#msgg").hide();
                            $("#checkProduct").val('');
                        }
                        if (response.msg == 'EROOR') {
                            alert(response.name);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        //commit(false);
                    }
                });
            }
        });    
</script>