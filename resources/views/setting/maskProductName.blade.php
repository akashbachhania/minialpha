@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
          <div class="box-header">
          	 <h3 class="box-title">
          	 	Website Settings
          	 </h3>
          </div>

          <div class="box-body">
          	<div class="row">
          		<div class="col-md-12">
          			<div class="box box-success" style="border: 1px solid #00a65a!important">
          				<div class="box-header" style="background: #00a65a">
          					<h3 class="box-title" style="color: #fff;"><i class="fa fa-gift"></i>&nbsp;&nbsp;Mask Product Name Quotes</h3>
          				</div>
          				<div class="box-body" style="min-height: 200px;">
          					<form>
          						<div class="form-group row m_t30">
          							<div class="col-md-12 text-center">
          								<label>
					                      Mask the Product Name from the Quote&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					                      <input type="checkbox" name="maskName" id="maskName" class="flat-red">
					                      
					                    </label>
          							</div>
          						</div>
          					</form>
          				</div>
          			</div>
          		</div>
          	</div>
          </div>

        </div>
    </div>
  </div>
@include('setting.maskProductNameJs')
@endsection