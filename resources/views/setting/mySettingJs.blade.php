<script type="text/javascript">

 function shownoti(message){
	$(function() {
    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg ;
    }
    var toasts = [
        new Toast('success', 'toast-top-right', message),
    ];
    toastr.options.positionClass = 'toast-top-full-width';
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = 2000;
    toastr.options.fadeOut = 250;
    toastr.options.fadeIn = 250;
    var i = 0;
   /* $('#tryMe').click(function () {
        $('#tryMe').prop('disabled', true);*/
        delayToasts();
    //});
    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);
        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }
    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})
}
</script>

@if(isset($_GET['page'])=="ndashboard")

  <script> 
    $(document).ready(function() {
                
        var id = "<?php echo $_GET['page']; ?>";
        var lognoti = "lognoti";
         $.ajax({
            url: "functions.php",
            type: "post",
            data:{ 'id': id , 'lognoti': lognoti},
            cache: false,
            success: function(res){
            //alert(res);
            
                        $.ajax({
                url: "service/noti6.php",
                type: "post",
                data:{id:id},
                cache: false,
                dataType:'html',
                        success: function(res2){
                        //alert(res2);
                            if(res2=='bothclose')
                            {
                            
                            }
                            if(res2=='audioclose')
                            {
                            //alert(res2);
                        var message = res;
                        shownoti(message);
                            }
                            if(res2=='vedioclose')
                            {
                    $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
        
         $('#sound')[0].play(); 
                            }
                        if(res2=='bothstart')
                        {
                        //alert(res2);
                        var message = res;
                        shownoti(message);
                    $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
         $('#sound')[0].play(); 
                        }
                     }
                });
            
            //var message = res;
            //shownoti(message);
            }
    });
    //shownoti("User Created Successfully"); // refresh div after 5 secs
         });
     </script>
@endif
@if(isset($_GET['page'])=="nsetting")
            
    <script> 
        $(document).ready(function() {
            
            var id = "{{ \Request::input('page') }}";
            
            var lognoti = "lognoti";
            
            $.ajax({
                url: "functions.php",
                type: "post",
                data:{ 'id': id , 'lognoti': lognoti},
                cache: false,
                success: function(res){
                //alert(res);
                            $.ajax({
                    url: "service/noti6.php",
                    type: "post",
                    data:{id:id},
                    cache: false,
                    dataType:'html',
                            success: function(res2){
                            //alert(res2);
                                if(res2=='bothclose')
                                {
                                
                                }
                                if(res2=='audioclose')
                                {
                                //alert(res2);
                            var message = res;
                            shownoti(message);
                                }
                                if(res2=='vedioclose')
                                {
                        $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
            
             $('#sound')[0].play(); 
                                }
                            if(res2=='bothstart')
                            {
                            //alert(res2);
                            var message = res;
                            shownoti(message);
                        $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
             $('#sound')[0].play(); 
                            }
                         }
                    });
                //var message = res;
                //shownoti(message);
                }
            });
                //shownoti("User Created Successfully"); // refresh div after 5 secs
        });
    </script>
@endif


<script type="text/javascript">
    function checkPasslength(){

      var s = $('#pwd').val();
      // check for null or too short
      if (!s || s.length < 6) {
       return false;
      }
      // check for a number
      if (/[0-9]/.test(s) === false) {
       return false;
      }
      // check for a capital letter
      if ((/[a-z]/.test(s)) === false) {
       return false;
      }
      
      return true; // return true if all conditions are met

    }

    $("#submit-msetting").click(function(event){
        // event.preventDefault();
        if($("#timezone").val()=='' || $("#timezone").val()==0){

            alert('Timezone Required');

            $("#timezone").focus();

            return false;

        }

        var pass1 = $("#pwd").value;

        var pass2 = $("#cpwd").value;

        if (pass1 != pass2) {

            alert("Passwords Do not match");

            $("#cpwd").focus();

            return false;

        }

        if(checkPasslength()==false && (pass1!=''))
        {
            alert("Pasword must have at least 6 characters and at least one letter and one number."); // prompt user

            $("#pwd").focus(); //set focus back to control

            return false;

        }
        if($("#sque1").val()=='' || $("#sque1").val()==0){

            alert('Security Question 1 Required');

            $("#sque1").focus();

            return false;

        }

        if($("#sans1").val()==''){

            alert('Security Answer 1 Required');

            $("#sans1").focus();

            return false;

        }

        if($("#sque2").val()=='' || $("#sque2").val()==0){

            alert('Security Question 2 Required');

            $("#sque2").focus();

            return false;

        }

        if($("#sans2").val()==''){

            alert('Security Answer 2 Required');

            $("#sans2").focus();

            return false;

        }

        if($("#sque3").val()=='' || $("#sque3").val()==0){

            alert('Security Question 3 Required');

            $("#sque3").focus();

            return false;

        }

        if($("#sans3").val()==''){

            alert('Security Answer 3 Required');

            $("#sans3").focus();

            return false;

        }

        if($("#authpass").val()==''){

            alert('Authorize Password Required');

            $("#authpass").focus();

            return false;

        }

        if($("#authorize").val()!=''){

            $.get(root + '/checkauthpass',{authpass:$("#authpass").val()},function(data){
                if(data != 'success'){
                    alert('Authorization Failed');
                    evet.preventDefault();
                    return false;
                }
            });
        
        }

        else{   

            $("#form_settings").submit();                            

        }

    });

    $("#cancel").click(function(){ location.href="{{ url('dashboard') }}" });
</script>