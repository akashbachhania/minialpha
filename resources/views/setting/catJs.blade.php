<script type="text/javascript">
$(document).ready(function() {
    $('#datatable_ajax').DataTable( {
              paging: false,
              searching: false,
              "bSort": false,
              "ajax": {
                  "url": "category_ajax",
                  dataSrc: "",
                  "type": 'POST',
                  "data": {
                  _token: $('input[name=_token]').val(),
                 }

              // ajax source
              } ,
              columns: [
                { data: 'industry' },
                { data: 'category' },
                { data: 'action' },
              ]
    } );
}); 
setInterval( function () {
  $('#datatable_ajax').DataTable().ajax.reload();
}, 3000 );


</script>

<script type="text/javascript">
  
  jQuery(document).ready(function() {
    
    $("#name").change(function() {

       checkDuplicate();

    });

    

    $("#industry").change(function() {

       checkDuplicate();

    });

});


function checkDuplicate ()

{      
        var pname = $("#name").val();
        var iid = $("#industry").val();

        if(pname != '' && iid != '' && iid != '0')
        {
            $.ajax({
                type: "POST",
                url: root + '/setting/category_ajax',
                async: false,
                data: {
                        pname: $("#name").val(),

                        type: 'exist',

                        iid :$("#industry").val(),

                        _token: $('input[name=_token]').val(),
                },
                success: function(data, status, xhr) {

                    if (data == 'exist') {
                        $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                        $("#checkProduct").val('0');
                    } else {
                        $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                        $("#checkProduct").val('1');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        } 
        else
        {
            $("#msgg").show().html('<span></span>');
        }
}  



// this show a new model for add industry
$("#btnnew").on("click", function() {

            $('#basic').modal('show');
            // $('#status').val('');
            $('#name').val('');
            $("label.error").hide();
            $(".error").removeClass("error");
            setTimeout(function() {
                $('#name').focus();
            }, 1000);
});

// this close add industry model


$(document).on("click","#btnclose", function() {
 
    // $('#status').val('');
    $('#name').val('');
    $('#basic').modal('hide');
    $("#msgg").hide();
    $("#checkProduct").val('');
    $('#name').attr('autocomplete', 'false');
});

// this will add and update industry

$("#btnsave").on("click", function() {
   
    
      var name = $('#name').val();

      var status = $('#status').val();

      var checkProduct = $('#checkProduct').val();

      var iid = $('#industry').val();

      var id = $('#id').val();

      if (name == '' || status == '') {

        alert('Category Name must be Required');

        return;

      }

      if (iid == '') {

        alert('Industry Name must be Required');

        return;

      }
      if( id == '0'){
          if(name != '' && iid != '' && iid != '0')
          {

            var data = {
                type: 'add',
                name: name,
                _token: $('input[name=_token]').val(),
                status: status,
                iid: iid

            };
            $.ajax({
                 type: 'POST',
                 url: root + '/setting/category_ajax',
                 cache: false,
                 data: data,
                 async: false,
                 success: function(data) {
                   
                    if (data.msg == 'SUCCESS') {
                                    
                        //shownoti("Industry Added Successfully");
                        $('#basic').modal('hide');
                        var table = $('#datatable_ajax').DataTable();
                        table.ajax.reload();
                        $("#msgg").hide();
                        $("#checkProduct").val('');
                        $("#name").val('');
                        $("#industry").val('');
                    }
                    if (data.msg == 'EROOR') {
                       
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    } //if finish share
    else {
        
        var data = {
            name: name,
            type: 'update',
            status: status,
            _token: $('input[name=_token]').val(),
            id: id,
            iid: iid
        };
        $.ajax({
            type: 'POST',
            url: root + '/setting/category_ajax',
            cache: false,
            data: data,
            async: false,
            success: function(data) {
                
                if (data.msg == 'SUCCESS') {
             //shownoti("Industry Updated Successfully");
             
                    $('#basic').modal('hide');
                    $("label.error").hide();
                    $(".error").removeClass("error");
                   var table = $('#datatable_ajax').DataTable();
                   table.ajax.reload();
                    $("#msgg").hide();
                    $("#checkProduct").val('');
                     $("#industry").val('');
                     $('#id').val(0);
                }
                if (data.msg == 'EROOR') {
                    //alert(response.name);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }
});
 function editme(obj) {
        $('#basic').modal('show');
        // $('#status').val('');
        $('#name').val('');
        $('#id').val(obj);
     
        setTimeout(function() {
            $('#name').focus();
        }, 1000);
         var data = {
            type: 'search',
            _token: $('input[name=_token]').val(),
            id: obj
        };
        $.ajax({
            type: 'POST',
            url: root + '/setting/category_ajax',
            cache: false,
            data: data,
            dataSrc:"Category",
            data: data,
            success: function(data) {
               
                $('#status').val(data.status);
                $('#name').val(data.name);
                $('#industry').val(data.industryid);
                $("#msgg").hide();
                $("#checkProduct").val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }

    function deleteme(obj) {
       var data = {
            type: 'delete',
           
            _token: $('input[name=_token]').val(),
            id: obj
        };
        if (confirm("Are you sure you wish to delete this Category?")) {
            $.ajax({
                type: 'POST',
                url: root + '/setting/category_ajax',
                cache: false,
                data: data,
                
                success: function(data) {
                    
                    if (data.msg == 'ERROR') {
                        alert(response.name)
                    } else {
           
                        window.location = 'category';
                    }
                     $('#datatable_ajax').DataTable().ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    }
</script>