<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>

<script>
  $(function () {

    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('trems-con');
    //bootstrap WYSIHTML5 - text editor
    // $(".textarea").wysihtml5();
  });
</script>

<script type="text/javascript">
	function shownoti(message) {
		$(function() {
			function Toast(type, css, msg) {
				this.type = type;
				this.css = css;
				this.msg = msg;
			}
			var toasts = [
				new Toast('success', 'toast-top-right', message),
			];
			toastr.options.positionClass = 'toast-top-full-width';
			toastr.options.extendedTimeOut = 0; //1000;
			toastr.options.timeOut = 2000;
			toastr.options.fadeOut = 250;
			toastr.options.fadeIn = 250;
			var i = 0;
			/* $('#tryMe').click(function () {

			     $('#tryMe').prop('disabled', true);*/
			delayToasts();
			//});
			function delayToasts() {
				if (i === toasts.length) {
					return;
				}
				var delay = i === 0 ? 0 : 2100;
				window.setTimeout(function() {
					showToast();
				}, delay);
				// re-enable the button        
				if (i === toasts.length - 1) {
					window.setTimeout(function() {
						prop('disabled', false);
						i = 0;
					}, delay + 1000);
				}
			}

			function showToast() {
				var t = toasts[i];
				toastr.options.positionClass = t.css;
				toastr[t.type](t.msg);
				i++;
				delayToasts();
			}
		})
	}
</script>

<script type="text/javascript">

    $("#createadd").click(function(){
        // var trems_condition=$('#trems-con').val();
        var trems_condition = CKEDITOR.instances['trems-con'].getData();

        if (trems_condition == '')
		{
			alert('Terms and condition empty');
		}
		else
		 {
            $.ajax({
                type: 'POST',
                url: root + '/setting/termsAndConditions_ajax',
                cache: false,
                data: {type:'trems',trems_condition:trems_condition},
                success: function(data) {
                        if (data == 1) {
							 var res5 = "Successfully updated the Terms & Conditions.";
							 
							           		$.ajax({
											url: "service/noti6.php",
											type: "post",
											cache: false,
											dataType:'html',
													success: function(res2){
													//alert(res2);
														if(res2=='bothclose')
														{
														
														}
														if(res2=='audioclose')
														{
											               shownoti(res5);
														   setTimeout(function() {
                                window.location.href = "dashboard.php";
                            }, 2000);
														}
														if(res2=='vedioclose')
														{
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									
									 $('#sound')[0].play(); 
														}
													if(res2=='bothstart')
													{
													//alert(res2);
													//var message = res;
													//shownoti(message);
												    	shownoti(res5);
														setTimeout(function() {
                                window.location.href = "dashboard.php";
                            }, 2000);
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									 $('#sound')[0].play(); 
													}
												 }
											});
							
                        }
                },

                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    });	
</script>