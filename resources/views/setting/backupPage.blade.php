@extends('layout.master')

@section('content')
<div class="row">

  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#backup" data-toggle="tab">Backup</a></li>
        <li><a href="#restore" data-toggle="tab">Clear Database</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="backup">
          <!-- Post -->
          <div class="post">
            <div class="user-block text-center">
              <h3>To take a Backup, please click here:&nbsp;&nbsp;<a href="{{url('backup')}}" class="fa fa-download red"></a></h3>
            </div><!-- /.user-block -->
          </div><!-- /.post -->
        </div><!-- /.tab-pane -->

        <div class="tab-pane" id="restore">
          <div class="post">
            <div class="user-block text-center">
              <h3>Are you sure you wish to clear the database?&nbsp;&nbsp;<i class="fa fa-trash red"></i></h3>
            </div>
          </div>
        </div><!-- /.tab-pane -->
      </div><!-- /.tab-content -->
    </div><!-- /.nav-tabs-custom -->
  </div><!-- /.col -->
</div><!-- /.row -->
@endsection