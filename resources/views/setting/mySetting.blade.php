@extends('layout.master')

@section('content')

  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          	<h3 class="box-title"> Settings </h3>
          </div>

          <div class="box-body">
          	<div class="row m_t30">
          		<div class="col-md-12">
          			<label>* denotes required fields </label>
          		</div>
          	</div>

          	<form action="{{ url('setting/updateMySetting') }}" id="form_settings" class="form-horizontal" method="post">
          		<div class="row">
          		
                  <input type="hidden" id="userid" name="userid" value="{{ Auth::user()->userid }}">
	          	  
	          	  <div class="col-md-6">

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Time Zone <span class="required">*</span></label>

	          			<div class="col-lg-8">

                            <select class="form-control" name="timezone" id="timezone">
                              <option value="">Select Time Zone</option>
                              @foreach( $timezones as $timezone )
                              <option value="{{ $timezone->timezonevalue }}" {{ (Auth::user()->timezoneid == $timezone->timezonevalue)?'selected="selected"':'' }}>
                              {{ $timezone->name }}
                              </option>
                              @endforeach
                            </select>
                        </div>
	          		</div>

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Change Password </label>
	          			
	          			<div class="col-lg-8">
                        	<input type="password" class="form-control" name="pwd" id="pwd" maxlength="15" placeholder="Enter New Password">
                        </div>
	          		
	          		</div>

	          		<div class="form-group row">
	   					<label class="col-lg-4 control-label">Confirm Password </label>
                        <div class="col-lg-8">
                            <input type="password" class="form-control" name="cpwd" id="cpwd" maxlength="15" placeholder="Confirm New Password">
                        </div>       			
	          		</div>			

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Security Question1 <span class="required">*</span></label>
                      	
                      	<div class="col-lg-8">
                        	
	                        <select class="form-control" name="sque1" id="sque1">
	                          <option value="">Select Security Question</option>
	                          
	                          @foreach( $secque as $secq )
	                          
	                          <option value="{{ $secq->securityid }}" {{ (Auth::user()->secquiz1 == $secq->securityid) ? 'selected="selected"':'' }}>
	                          {{ $secq->name }}
	                          </option>

	                          @endforeach
	                        </select>
                     	
                     	</div>
	          		
	          		</div>

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Security Answer1 <span class="required">*</span></label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="sans1" id="sans1" value="{{ Auth::user()->secans1 }}" maxlength="35" placeholder="Enter Security Answer">
                        </div>
	          		
	          		</div>

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Security Question2 <span class="required">*</span></label>
                        
                        <div class="col-lg-8">
                            
                            <select class="form-control" name="sque2" id="sque2">
                              <option value="">Select Security Question</option>
                              
                              @foreach( $secque1 as $secq1 )
                              
                              <option value="{{ $secq1->securityid }}" {{ (Auth::user()->secquiz2 == $secq1->securityid) ? 'selected="selected"':'' }}>
                              {{ $secq1->name }}
                              </option>

                              @endforeach
                            </select>
                        </div>
	          		</div>

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Security Answer2 <span class="required">*</span></label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="sans2" id="sans2" value="{{ Auth::user()->secans2 }}" maxlength="35" placeholder="Enter Security Answer">
                        </div>
	          		
	          		</div>	          	  
	   
	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Security Question3 <span class="required">*</span></label>
                        
                        <div class="col-lg-8">
                            
                            <select class="form-control" name="sque3" id="sque3">
                              <option value="">Select Security Question</option>
                              @foreach( $secque2 as $secq2 )
                              <option value="{{ $secq2->securityid }}" {{ (Auth::user()->secquiz3 == $secq2->securityid) ? 'selected="selected"':'' }}>
                              {{ $secq2->name }}
                              </option>
                              @endforeach
                            </select>
                        </div>
	          		
	          		</div>

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Security Answer3 <span class="required">*</span></label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="sans3" id="sans3" value="{{ Auth::user()->secans3 }}" maxlength="35" placeholder="Enter Security Answer">
                        </div>
	          		
	          		</div>

	          		<div class="form-group row">
	          			
	          			<label class="col-lg-4 control-label">Authorize Changes <span class="required">*</span></label>
                        
                        <div class="col-lg-8">
                            <input type="password" class="form-control" placeholder="Enter Password" id="authpass" name="authpass" maxlength="15">
                        </div>
	          		
	          		</div>




	          	  </div>
	          	  
	          	  <div class="col-md-6">
	          			<label>Notifications</label>

	          			<div class="form-group row">
	          				
	          				<label class="col-lg-4 control-label">Visual Notification</label>
                            
                            <div class="col-lg-8">
	                            <select class="form-control" name="vnotification" id="vnotification">
	                              <option value="1" {{ (Auth::user()->visualnoti == 1)? 'selected="selected"':'' }}>Enabled</option>
	                              <option value="0" {{ (Auth::user()->visualnoti == 0)? 'selected="selected"':'' }}>Disabled</option>
	                            </select>
                            </div>
	          			
	          			</div>

	          			<div class="form-group row">
	          				
	          				<label class="col-lg-4 control-label">Audio Notification</label>
                          	
                          	<div class="col-lg-8">
	                            <select class="form-control" name="anotification" id="anotification">
	                              <option value="1"  {{ (Auth::user()->audionoti == 1)? 'selected="selected"':'' }}>Enabled</option>
	                              <option value="0"  {{ (Auth::user()->audionoti == 0)? 'selected="selected"':'' }}>Disabled</option>
	                            </select>
                          	</div>

	          			</div>

	          	  </div>
          	    </div>

          	    <div class="row">
          	    	<div class="col-md-12">
          	    		<div class="form-group row">
                          <div class="col-md-4 col-md-push-2">
                            <button type="submit" id="submit-msetting" name="submit_msetting" class="btn btn-success">Submit</button>
                      		
                      		<button type="button" id="cancel" class="btn btn-default">Cancel</button>
          	    		  </div>
          	    		</div>
          	    	</div>
          	    </div>
          	</form>
          </div>
        </div>
    </div>
  </div>
@include('setting.mySettingJs')
@endsection