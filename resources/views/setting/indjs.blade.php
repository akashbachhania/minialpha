<script type="text/javascript">
  function shownoti(message) {
    $(function() {
      function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = msg;
      }
      var toasts = [
        new Toast('success', 'toast-top-right', message),
      ];
      toastr.options.positionClass = 'toast-top-full-width';
      toastr.options.extendedTimeOut = 0; //1000;
      toastr.options.timeOut = 2000;
      toastr.options.fadeOut = 250;
      toastr.options.fadeIn = 250;
      var i = 0;
      /* $('#tryMe').click(function () {

           $('#tryMe').prop('disabled', true);*/
      delayToasts();
      //});
      function delayToasts() {
        if (i === toasts.length) {
          return;
        }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function() {
          showToast();
        }, delay);
        // re-enable the button        
        if (i === toasts.length - 1) {
          window.setTimeout(function() {
            prop('disabled', false);
            i = 0;
          }, delay + 1000);
        }
      }

      function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
      }
    })
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#datatable_ajax').DataTable( {
       
                paging: false,
                 searching: false,
                
                "bSort": false,
                // "bSortable": false, // default record count per page
                "ajax": {
                    "url": root + "/setting/industry_ajax",
                     dataSrc: "",
                     "type": 'POST',
                    
                    "data": {
                    
                   
                    _token: $('input[name=_token]').val(),
                   }

                // ajax source
                } ,
                columns: [
                { data: 'name' },
                { data: 'action' },
             
                ]
    } );
} ); 

 
setInterval( function () {
    $('#datatable_ajax').DataTable().ajax.reload();
}, 10000 );   
</script>
<script type="text/javascript">
  jQuery(document).ready(function() {
        $("#name").change(function() {
            var pname = $("#name").val();
            if (pname != '')
            {
                $.ajax({
                    type: "POST",
                    url: root + "/setting/industry_ajax",
                    async: false,
                    data: {
                        pname: $("#name").val(),
                        type: 'exist',
                        _token: $('input[name=_token]').val(),
                    },
                    success: function(data, status, xhr) {
                        if (data == 'exist') {
                            $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                            $("#checkProduct").val('0');
                        } else {
                            $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                            $("#checkProduct").val('1');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                        commit(false);
                    }
                });
            } else
            {
                $("#msgg").show().html('<span></span>');
            }
        });
 });

// this show a new model for add industry
$("#btnnew").on("click", function() {

            $('#basic').modal('show');
            // $('#status').val('');
            $('#name').val('');
            $("label.error").hide();
            $(".error").removeClass("error");
            setTimeout(function() {
                $('#name').focus();
            }, 1000);
});

// this close add industry model


$(document).on("click","#btnclose", function() {
 
    // $('#status').val('');
    $('#name').val('');
    $('#basic').modal('hide');
    $("#msgg").hide();
    $("#checkProduct").val('');
    $('#name').attr('autocomplete', 'false');
});

// this will add and update industry

$("#btnsave").on("click", function() {
    var name = $('#name').val();
    var status = $('#status').val();
    var id = $('#id').val();
    //alert(id);
    
    if (name == '' || status == '') {
        alert('Industry Name must be Required');
        return;
    }
    if (id == "0") {
        //var data = "Type=add&name=" + name + "&status=" + status;
        var data = {
            type: 'add',
            name: name,
            _token: $('input[name=_token]').val(),
            status: status

        };
        $.ajax({
             type: 'POST',
             url: root + "/setting/industry_ajax",
             cache: false,
             data: data,
             async: false,
             success: function(data) {
               
                if (data.msg == 'SUCCESS') {
                                
                    shownoti("Industry Added Successfully");
                    $('#basic').modal('hide');
                    var table = $('#datatable_ajax').DataTable();
                    table.ajax.reload();
                    $("#msgg").hide();
                    $("#checkProduct").val('');
                    $("#name").val('');
                }
                if (data.msg == 'EROOR') {
                   
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    } //if finish share
    else {
        
        var data = {
            name: name,
            type: 'update',
            status: status,
            _token: $('input[name=_token]').val(),
            id: id
        };
        $.ajax({
            type: 'POST',
            url: root + "/setting/industry_ajax",
            cache: false,
            data: data,
            async: false,
            success: function(data) {
                
                if (data.msg == 'SUCCESS') {
             //shownoti("Industry Updated Successfully");
             
                    $('#basic').modal('hide');
                    $("label.error").hide();
                    $(".error").removeClass("error");
                   var table = $('#datatable_ajax').DataTable();
                   table.ajax.reload();
                    $("#msgg").hide();
                    $("#checkProduct").val('');
                    $('#id').val(0);
                }
                if (data.msg == 'EROOR') {
                    //alert(response.name);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }
});
 function editme(obj) {
        $('#basic').modal('show');
        // $('#status').val('');
        $('#name').val('');
        $('#id').val(obj);
        setTimeout(function() {
            $('#name').focus();
        }, 1000);
         var data = {
            type: 'search',
           
            _token: $('input[name=_token]').val(),
            id: obj
        };
        $.ajax({
            type: 'POST',
            url: root + "/setting/industry_ajax",
            cache: false,
            data: data,
            dataSrc:"industry",
            data: data,
            success: function(data) {
               
                $('#status').val(data.status);
                $('#name').val(data.name);
                $("#msgg").hide();
                $("#checkProduct").val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }

    function deleteme(obj) {
       var data = {
            type: 'delete',
           
            _token: $('input[name=_token]').val(),
            id: obj
        };
        if (confirm("Are you sure you wish to delete this Industry?")) {
            $.ajax({
                type: 'POST',
                url: root + "/setting/industry_ajax",
                cache: false,
                data: data,
                
                success: function(data) {
                    
                    if (data.msg == 'ERROR') {
                        alert(data.name)
                    } else {
           
                        window.location = 'industry';
                    }
                     $('#datatable_ajax').DataTable().ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    }
</script>