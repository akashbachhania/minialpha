@extends('layout.master')

@section('content')

<div class="row">
	<div class="col-xs-12">
    	<div class="box">
      		<div class="box-header">
        		<h3 class="box-title">Menu Permission</h3>
        	</div>
        	<div class="box-body">
        		<form>
        			<div class="form-body">
        				<div class="form-group row">
        					<div class="col-md-5">
        						<label class="col-md-2" style="font-size: 22px;font-weight: 500">Role</label>
        						<div class="col-md-8">
        							<select name="usersrole" class="form-control" id="usersrole">
				                      <option value="">Select</option>
				                      @foreach( $roles as $role )

				                      	<option value="{{ $role->roleid }}">{{ $role->rolename }}</option>

				                      @endforeach
        							</select>
        						</div>
        					</div>
        					<div class="col-md-5">
        						<label class="col-md-3" style="font-size: 22px;font-weight: 500">Module</label>
        						<div class="col-md-8">
        							<select name="userspage" class="form-control" id="userspage">
				                      <option value="">Select</option>
				                      @foreach( $mPages as $mPage )
				                      <option value="{{ $mPage->PageId }}">{{ $mPage->DisplayName }}</option>
				                      @endforeach
				                    </select>
        						</div>
        					</div>
        					<div class="col-md-2">
        						<button id="rolebutton" class="btn btn-success">Submit</button>
        					</div>
        				</div>
        			</div>
        		</form>	
        	</div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">

			<div class="box-body table-responsive">
				<form method="post" action="{{ url('setting/rolePermissionUpdate') }}">
					<table class="table table-striped table-hover" id="menuPermissionTable">
						<thead>
	                      <tr role="row" class="heading">
	                        <th width="40%"> Page Name </th>
	                        <th width="10%"> View </th>
	                        <th width="10%"> Add </th>
	                        <th width="10%"> Edit </th>
	                        <th width="10%"> Delete </th>
	                         <th width="10%"> Import </th>
	                        <th width="10%"> Export </th> 
	                      </tr>
	                    </thead>
	                    <tbody>
	                    </tbody>
					</table>
					<div class="form-group" id="menusubmit">
						<input type="submit" id="Submit" class="btn green" value="Save" />
					</div>
				</form>


			</div>
		</div>
	</div>
</div>
@include('setting.menuPermissionJs')
@endsection