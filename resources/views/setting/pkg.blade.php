<script type="text/javascript">
    
    $(document).ready(function() {
        var packagingTable = $('#packagingTable').DataTable( {
           
            "ajax": {
                "url":root + "/setting/packaging_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'srno' },
            { data: 'packaging' },
            { data: 'action' }
         
            ]
        } );

        setInterval( function () {
            packagingTable.ajax.reload();
        }, 3000 );
    } ); 

     
   $("#btnnew").on("click", function() {

            $('#basic').modal('show');
            // $('#status').val('');
            $('#name').val('');
            $("label.error").hide();
            $(".error").removeClass("error");
            setTimeout(function() {
                $('#name').focus();
            }, 1000);
    });

    // this close add industry model


    $(document).on("click","#btnclose", function() {
     
        // $('#status').val('');
        $('#name').val('');
        $('#basic').modal('hide');
        $("#msgg").hide();
        $("#checkProduct").val('');
        $('#name').attr('autocomplete', 'false');
    });

    $("#name").change(function() {
        var pname = $("#name").val();
        if (pname != '')
        {
            $.ajax({
                type: "POST",
                url: root + "/setting/packaging_ajax",
                async: false,
                data: {
                    pname: $("#name").val(),
                    type: 'exist'
                },
                success: function(data, status, xhr) {
                    if (data == 'exist') {
                        $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                        $("#checkProduct").val('0');
                    } else {
                        $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                        $("#checkProduct").val('1');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        } else
        {
            $("#msgg").show().html('<span></span>');
        }
    });


    $("#btnsave").on("click", function() {
        var name = $('#name').val();
        var status = $('#status').val();
        var id = $('#id').val();
        // alert(id);
        
        if (name == '' || status == '') {
            alert('Packaging Name must be Required');
            return;
        }
        if (id == "0") {
            
            var data = {
                type: 'add',
                name: name,
                status: status
            };

            $.ajax({
                 type: 'POST',
                 url: root + '/setting/packaging_ajax',
                 cache: false,
                 data: data,
                 async: false,
                 success: function(data) {
                   
                    if (data.msg == 'SUCCESS') {
                                    
                        // shownoti("Industry Added Successfully");
                        $('#basic').modal('hide');
                        var table = $('#packagingTable').DataTable();
                        table.ajax.reload();
                        $("#msgg").hide();
                        $("#checkProduct").val('');
                        $("#name").val('');
                    }
                    if (data.msg == 'EROOR') {
                       
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        } //if finish share
        else {
            
            var data = {
                name: name,
                type: 'update',
                status: status,
                id: id
            };
            $.ajax({
                type: 'POST',
                url: root + '/setting/packaging_ajax',
                cache: false,
                data: data,
                async: false,
                success: function(data) {
                    
                    if (data.msg == 'SUCCESS') {
                 //shownoti("Industry Updated Successfully");
                 
                        $('#basic').modal('hide');
                        $("label.error").hide();
                        $(".error").removeClass("error");
                       var table = $('#packagingTable').DataTable();
                       table.ajax.reload();
                        $("#msgg").hide();
                        $("#checkProduct").val('');
                        $('#id').val(0);
                    }
                    if (data.msg == 'EROOR') {
                        //alert(response.name);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    });

    function editme(obj) {
        $('#basic').modal('show');
        // $('#status').val('');
        $('#name').val('');
        $('#id').val(obj);
        setTimeout(function() {
            $('#name').focus();
        }, 1000);
         var data = {
            type: 'search',
            id: obj
        };

        $.ajax({
            type: 'POST',
            url: root + '/setting/packaging_ajax',
            cache: false,
            data: data,
            dataSrc:"packaging",
            data: data,
            success: function(data) {
               
                $('#status').val(data.status);
                $('#name').val(data.name);
                $("#msgg").hide();
                $("#checkProduct").val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }

    function deleteme(obj) {
       var data = {
            type: 'delete',
            id: obj
        };

        if (confirm("Are you sure you wish to delete this Packaging?")) {
            $.ajax({
                type: 'POST',
                url: root + '/setting/packaging_ajax',
                cache: false,
                data: data,
                
                success: function(data) {
                    
                    if (data.msg == 'ERROR') {
                        alert(data.name)
                    } else {
           
                        window.location = root + '/setting/packaging';
                    }
                     $('#packagingTable').DataTable().ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    }

</script>