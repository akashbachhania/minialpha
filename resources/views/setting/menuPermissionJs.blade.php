<script type="text/javascript">
	var userRole = '';
	var userPage = '';
	var form;
	function shownoti(message) {
		$(function() {
			function Toast(type, css, msg) {
				this.type = type;
				this.css = css;
				this.msg = msg;
			}
			var toasts = [
				new Toast('success', 'toast-top-right', message),
			];
			toastr.options.positionClass = 'toast-top-full-width';
			toastr.options.extendedTimeOut = 0; //1000;
			toastr.options.timeOut = 2000;
			toastr.options.fadeOut = 250;
			toastr.options.fadeIn = 250;
			var i = 0;
			/* $('#tryMe').click(function () {

			     $('#tryMe').prop('disabled', true);*/
			delayToasts();
			//});
			function delayToasts() {
				if (i === toasts.length) {
					return;
				}
				var delay = i === 0 ? 0 : 2100;
				window.setTimeout(function() {
					showToast();
				}, delay);
				// re-enable the button        
				if (i === toasts.length - 1) {
					window.setTimeout(function() {
						prop('disabled', false);
						i = 0;
					}, delay + 1000);
				}
			}

			function showToast() {
				var t = toasts[i];
				toastr.options.positionClass = t.css;
				toastr[t.type](t.msg);
				i++;
				delayToasts();
			}
		})
	}
</script>

      <script type="text/javascript">
	  var notimsg ='{{ \Request::input("update") }}';
	  //alert(notimsg);
	  if(notimsg=="success")
	  {
	  //alert('rama');
      var message = "Changes have been made";
      function shownoti(message) {
        $(function() {
            function Toast(type, css, msg) {
                this.type = type;
                this.css = css;
                this.msg = msg;
            }
            var toasts = [
                new Toast('success', 'toast-top-right', message),
            ];
            toastr.options.positionClass = 'toast-top-full-width';
            toastr.options.extendedTimeOut = 0; //1000;
            toastr.options.timeOut = 2000;
            toastr.options.fadeOut = 250;
            toastr.options.fadeIn = 250;
            var i = 0;
            /* $('#tryMe').click(function () {

                 $('#tryMe').prop('disabled', true);*/
            delayToasts();
            //});
            function delayToasts() {
                if (i === toasts.length) {
                    return;
                }
                var delay = i === 0 ? 0 : 2100;
                window.setTimeout(function() {
                    showToast();
                }, delay);
                // re-enable the button        
                if (i === toasts.length - 1) {
                    window.setTimeout(function() {
                        prop('disabled', false);
                        i = 0;
                    }, delay + 1000);
                }
            }

            function showToast() {
                var t = toasts[i];
                toastr.options.positionClass = t.css;
                toastr[t.type](t.msg);
                i++;
                delayToasts();
            }
        })
    }
	   }
      </script>

<script type="text/javascript">
	
    // var menuPermissionTable = $('#menuPermissionTable').DataTable( {

    //     "ajax": {
    //         "url":root+"/setting/menuPermission_ajax",
    //         "dataSrc": "",
    //         "type": 'POST',
    //     },
    //     columns: [
    //     { data: 'pageName' },
    //     { data: 'pageView' },
    //     { data: 'pageAdd' },
    //     { data: 'pageEdit' },
    //     { data: 'pageDelete' },
    //     { data: 'pageImport' },
    //     { data: 'pageExport'}
     
    //     ]
    // });

    $("#Submit").click(function(){
            var res5 = "Successfully Updated menu Permission.";
            var message5 = res5;
            shownoti(message5);
            $('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
       
    });    
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
			
		var roleid='{{ \Request::input("roleid") }}';
		var pageID='{{ \Request::input("pageID") }}';
		if(roleid !="" && pageID !="")
		{
			$("#usersrole").val(roleid);
			$("#userspage").val(pageID);
			$('#menusubmit').show();
		}
		else
		{
		$('#menusubmit').hide();
		}
        $("#name").change(function() {
            var pname = $("#name").val();
            if (pname != '')
            {
                $.ajax({
                    type: "POST",
                    url: "service/industry_ajax.php",
                    async: false,
                    data: {
                        pname: $("#name").val(),
                        Type: 'exist'
                    },
                    success: function(data, status, xhr) {
                        if (data == 'exist') {
                            $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                            $("#checkProduct").val('0');
                        } else {
                            $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                            $("#checkProduct").val('1');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                        commit(false);
                    }
                });
            } else
            {
                $("#msgg").show().html('<span></span>');
            }
        });
        $("#btnclose").click(function() {
            $("#msgg").hide();
            $("#checkProduct").val('');
            $("#name").val('');
            $('#name').attr('autocomplete', 'false');
        });
    });
    $("#btnnew").on("click", function() {
        $('#basic').modal('show');
        $("#msgg").hide();
        $("#checkProduct").val('');
    });

	var menuPermissionTable = $('#menuPermissionTable').DataTable( {

	    "ajax": {
	        "url":root+"/setting/menuPermission_ajax",
	        "dataSrc": "",
	        "type": 'POST',
	        "data":form,
	    },
	    columns: [
		    { data: 'pageName' },
		    { data: 'view' },
		    { data: 'add' },
		    { data: 'edit' },
		    { data: 'delete' },
		    { data: 'import' },
		    { data: 'export'}
		 
    	]
	});

	$("#rolebutton").on("click", function(event) { 
		event.preventDefault();
		var valueUserRole = $("#usersrole option:selected").val();
		var valueUserPage = $("#userspage option:selected").val();
		// menuPermissionTable.destroy();
		// menuPermissionTable = undefined;
		if(valueUserRole !="" && valueUserPage !="")
		{
			
			$('#menusubmit').show();
	        
			var userRole = valueUserRole;
			var userPage = valueUserPage;


			var form = {
	        	userRole:userRole,
	        	userPage:userPage
	        }		


			var menuPermission = $('#menuPermissionTable').DataTable( {
				destroy:true,
			    "ajax": {
			        "url":root+"/setting/menuPermission_ajax",
			        "dataSrc": "",
			        "type": 'POST',
			        "data":form,
			    },
			    columns: [
				    { data: 'pageName' },
				    { data: 'view' },
				    { data: 'add' },
				    { data: 'edit' },
				    { data: 'delete' },
				    { data: 'print' },
				    { data: 'export'}
				 
		    	]
			});
			
		}
		else
		{
		 alert("Please Select The Role And Module");
		}
	});


</script> 