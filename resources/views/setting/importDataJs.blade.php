<script>
    jQuery(document).ready(function() {

		$("#submitButton").click(function() {
			var uploadFile = $("#uploadFile").val();
			var re = /(\.csv)$/i;
			if ($("#itype").val() == '' || $("#itype").val() == 0) {
				alert('Data Type Required');
				return false;
			}
			if ((uploadFile == '') && (!$('#serverchk').checked)) {
				alert('File Upload Required');
				return false;
			}
			if (!re.exec(uploadFile) && (!$('#serverchk').checked))
			{
                alert("File extension not supported!");
                return false;
            }
            if ($("#authchange").val() == '') {
                alert('Authorize Password Required');
                return false;
            }
            if ($("#authorize").val() != $("#authchange").val()) {
                alert('Authorization Failed');
                return false;
            } else {

                $("#form_importdata").submit();
            }
        });
        $("#cancel").click(function() {
            location.href = '{{ url("dashboard") }}';
        });

    });



    function sample()
    {
        var x = $("#itype").val();
        
        if (x == "Brands")
        {
            $('#sample-file').html('<a href="{{ url("sample/brands") }}" >Download Sample File</a>');
        }
        if (x == "Categories")
        {
            $('#sample-file').html('<a href="{{ url("sample/category") }}" >Download Sample File</a>');
        }
        if (x == "Industries")
        {
            $('#sample-file').html('<a href="{{ url("sample/industry") }}" >Download Sample File</a>');
        }
        if (x == "Products")
        {
            $('#sample-file').html('<a href="{{ url("sample/products") }}" >Download Sample File</a>');
        }
        if (x == "Subcategories")
        {
           $('#sample-file').html('<a href="{{ url("sample/sub-category") }}" >Download Sample File</a>');
        }
        if (x == "Posting")
        {
            $('#sample-file').html('<a href="{{ url("posting/sample") }}" >Download Sample File</a>');
        }
        if (x == "Customers")
        {
            var role = "{{ Auth::user()->roletype }}";
            if ((role == "AD") || (role == "SA"))
            {
               $('#sample-file').html('<a href="{{ url("sample/customer") }}" >Download Sample File</a>');
            } else
            {
                $('#sample-file').html('<a href="{{ url("sample/customers") }}" >Download Sample File</a>');
            }
        }
        if (x == "Users")
        {
            $('#sample-file').html('<a href="{{ url("sample/user") }}" >Download Sample File</a>');
        }
    }
	
function showMe (box) {
if($('#serverchk').checked)
	 {
	      $("#uploadFile").disabled = true;
		  
		  //$("#form_importdata").action = "import-pro_server.php";
	 }
else if (!$('#serverchk').checked) 
    {
	     $("#uploadFile").disabled = false;
		// $("#form_importdata").action = "import-pro.php";
    }    
 }

</script>