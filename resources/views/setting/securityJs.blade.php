<script type="text/javascript">
    var securityQueTable = $('#securityQueTable').DataTable( {

        "ajax": {
            "url":root + "/setting/mstsecurity_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
	        { data: 'name' },
	        { data: 'qtype' },
	        { data: 'edit' }
        ]
    });

    setInterval( function () {
        securityQueTable.ajax.reload();
    }, 3000 );


    function editme(obj) {

        $("#msgg").show().html('<span></span>');

        $('#basic').modal('show');

        // $('#status').val('');

        $('#name').val('');

        $('#id').val(obj);

        setTimeout(function() {

            $('#question').focus();

        }, 1000);

        var data = "Type=search&id=" + obj;

        $.ajax({

            type: 'POST',

            url: root + '/setting/mstsecurity_ajax',

            cache: false,

            data: data,

            success: function(response) {

                // response = jQuery.parseJSON(data);

                $('#status').val(response.status);

                $('#name').val(response.name);

                $('#question').val(response.qtype);

            },

            error: function(jqXHR, textStatus, errorThrown) {

                //commit(false);

            }

        });

    }



    function deleteme(obj) {

        var data = "Type=delete&id=" + obj;

        if (confirm("Are you sure you want to delete this?")) {

            $.ajax({

                type: 'POST',

                url: root + '/setting/mstsecurity_ajax',

                cache: false,

                data: data,

                success: function(response) {

                    // response = jQuery.parseJSON(data);

                    if (response.msg == 'ERROR') {

                        alert(response.name)

                    } else {

                        window.location = root + '/setting/mstsecurity';

                    }

                    //grid.getDataTable().ajax.reload();

                },

                error: function(jqXHR, textStatus, errorThrown) {

                    //commit(false);

                }

            });

        }

    }    	
</script>

<script type="text/javascript">

function checkDuplicate ()

{

    var pname = $("#name").val();

    //var iid = $("#question").val();

    if(pname != '')

    {

        $("#msgg").show().html('<span></span>');

        $.ajax({

            type: "POST",

            url: root + "/setting/mstsecurity_ajax",

            async: true,

            data: {

                pname: $("#name").val(),

                //iid :$("#question").val(),

                Type: 'exist'

            },

            success: function(data, status, xhr) {

                if (data == 'exist') {

                    $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');

                    $("#checkProduct").val('0');

                } else {

                    $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');

                    $("#checkProduct").val('1');

                }

            },

            error: function(jqXHR, textStatus, errorThrown) {

                alert(errorThrown);

                commit(false);

            }

        });

    }

  else  

  {

    $("#msgg").show().html('<span></span>');

  }



}


	$("#name").change(function() {

	    checkDuplicate();

	});



	$("#btnclose").click(function() {

	    $("label.error").hide();

        $(".error").removeClass("error");

        // $('#status').val('');

        $('#name').val('');

        $('#question').val('');

        $('#basic').modal('hide');

	});



	$("#btnnew").on("click", function() {

            $('#basic').modal('show');

			 $("#msgg").hide();

			$("#checkProduct").val('');

        });

</script>

<script type="text/javascript">
    
    $("#btnsave").on("click", function() {

        var name = $('#name').val();

        var status = $('#status').val();

        var iid = $('#question').val();

        var id = $('#id').val();

        // $("#form_sample_2").valid();

        if (name == '' || status == '') {

            alert('Question must be Required');

            return;

        }

        if (iid == '') {

            alert('Question Type must be Required');

            return;

        }

        if (id == "0") {

            //var data = "Type=add&name=" + name + "&status=" + status+"&iid="+iid;

            var data = {

                Type: 'add',

                name: name,

                status: status,

                iid: iid

            };

            $.ajax({

                type: 'POST',

                url: root + '/setting/mstsecurity_ajax',

                cache: false,

                data: data,

                success: function(response) {

                    // response = jQuery.parseJSON(data);

                    if (response.msg == 'SUCCESS') {

                        $('#basic').modal('hide');

                        // grid.getDataTable().ajax.reload();

						$("#msgg").hide();

                        $("#checkProduct").val('');

                        $("#name").val('');

                    }

                    if (response.msg == 'EROOR') {

                        alert(response.name);

                    }

                },

                error: function(jqXHR, textStatus, errorThrown) {

                    //commit(false);

                }

            });

        } //if finish share

        else {

            //var data = "Type=update&name=" + name + "&status=" + status+"&id="+id+"&iid="+iid;

            var data = {

                Type: 'update',

                name: name,

                status: status,

                id: id,

                iid: iid

            };

            $.ajax({

                type: 'POST',

                url: root + '/setting/mstsecurity_ajax',

                cache: false,

                data: data,

                success: function(response) {

                    // response = jQuery.parseJSON(data);

                    if (response.msg == 'SUCCESS') {

                        $('#basic').modal('hide');

                        $("label.error").hide();

                        $(".error").removeClass("error");

                        // grid.getDataTable().ajax.reload();

						$("#msgg").hide();

                        $("#checkProduct").val('');

                        $("#name").val('');

                    }

                    if (response.msg == 'EROOR') {

                        alert(response.name);

                    }

                },

                error: function(jqXHR, textStatus, errorThrown) {

                    //commit(false);

                }

            });

        }

    });	
</script>