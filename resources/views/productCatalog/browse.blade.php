@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-md-2">
                <h3 class="box-title m_t10">Product Catalog</h3>    
              </div>
              <div class="col-md-5"></div>
              <div class="col-md-5">
                <a class="btn btn-info m_t10" id="show">Filter</a>    
                @if( Auth::user()->roletype == 'AD' )
                <a href="{{ url('setting/importData?data-type=Products') }}" class="btn btn-primary m_t10">Import New Products</a>

                @else

                    @if( $checkVal[0]['pagePrint']=='1' )
                      <a href="{{ url('setting/importData?data-type=Products') }}" class="btn btn-primary m_t10">Import New Products</a>
                    @endif

                @endif

                @if( Auth::user()->roletype == 'AD' )    
                <a href="{{ url('product/add') }}" class="btn btn-success m_t10">Add New Products</a>
                @else
                  @if( $checkVal[0]['AddP']=='1' )
                    <a href="{{ url('product/add') }}" class="btn btn-success m_t10">Add New Products</a>
                  @endif
                @endif

              </div>

            </div>

            <div class="row m_t30">
              <div class="col-md-12" id="dvfilter">
                <div class="col-md-3 zee-col">
                  <div class="form-group">
                    <select class="form-control industry" name="industry[]" id="industry" onChange="getState(this.value);getbrand(this.value);" multiple="multiple">
                      <option value="0" class="selectitem">Select Industries</option>
                      
                    </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div id="categroy" class="form-group">
                    <select class="form-control category" onchange="subcat(this.value);" name="category[]" id="cat_id" multiple="multiple">
                      <option value="0">Select categories</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 zee-category">
                  <div class="form-group">
                    <select class="form-control" name="scategory[]" id="sub_cat" multiple="multiple">
                      <option value="0">Select Sub-categories</option>
                    </select>
                    </div>
                </div>
                <div class="col-md-2 zee-category">
                  <div class="form-group">
                    <select class="form-control" id="brand" name="brand[]" multiple="multiple">
                      <option value="0">Select Brands</option>
                    </select>
                    </div>
                </div>
                <div class="col-md-1 zee-category">
                  <div class="form-group">
                    <button type="button" class="btn btn-danger zee-filter m_t20" id="filter">Show</button>
                  </div>
                </div>

              </div>
            </div>
            
          </div><!-- /.box-header -->
          <div class="box-body">
            @if( Auth::user()->roletype == 'AM' )
              <table id="product-catalog1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th> Product ID </th>
                    <th> Industry </th>
                    <th> Category </th>
                    <th> Brand </th>
                    <th> Product Name </th>
                    <th> Product Codes </th>
                  
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            @endif

            @if( Auth::user()->roletype != 'AM' )
              <table id="product-catalog2" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th> Product ID </th>
                    <th> Industry </th>
                    <th> Category </th>
                    <th> Brand </th>
                    <th> Product Name </th>
                    <th> Product Codes </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            @endif
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>
@include('productCatalog.browseJs')
@endsection