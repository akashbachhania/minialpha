<script type="text/javascript">
	
	jQuery(document).ready(function(){

		var viewProductadTable = $('#viewProductadTable').DataTable( {

	        "ajax": {
	            "url": root + "/product/viewProductadTable",
	            "data" : {
	            	pro_id : "{{ \Request::segment(3) }}"
	            },
	            "dataSrc": "",
	            "type": 'POST',
	        },
	        columns: [
	        { data: 'userid' },
	        { data: 'userrole' },
	        { data: 'creationdate' },
	        { data: 'lastlogin' },
	        { data: 'status' },
	        { data: 'action' }
	     
	        ]
	    });


	    $('#viewProductadTable tbody').on('click', 'tr td:not(:first-child)', function () {	
	     	var data = table.row(this).data();
	        var page = $(this).parent().find('td a').attr('href');
	   		window.location.href = page;
	    });

	    $('#dvfilter').hide();
	    $('#show').click(function(){
		    $('#dvfilter').toggle('slow');
		});


		$("#createselad").on("click", function() {
			var selAd =  $('#selAd').val();
			var productID='{{ $productid[0] }}'+':'+selAd;
			
			if (document.createad.selAd.value=='CreateNewAd')
			{
			 window.location.href = "{{url('product/create-new-add/') .'/'. $productid[0] }}";
				//document.createad.selAd.focus();
				//alert('Please Select the Ad');
				//return (false);// this will not allow form to submit
			}
			else
			{
				 window.location.href = "{{url('product/view-productad/') }}" + "/" + productID;
			}
		});
	});
</script>