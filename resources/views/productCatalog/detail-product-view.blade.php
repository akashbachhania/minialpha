@extends('layout.master')

@section('content')

          <div class="row">
          	<div class="col-md-4">
          		<div class="box box-primary" style="min-height: 486px">
	                <div class="box-header with-border">
	                	<h4>Product ID: {{ $product->productno }}</h4>
	                </div><!-- /.box-header -->
	                <div class="box-body">
	                  @if( empty($image[0]) )
	                  
	                  	<img class="img-responsive" src="{{ asset('img/noimg.jpg') }}" style="min-height: 400px">
	 
	                  @else

						<div class="BigImage">
                          <div class="MagicZoom" id="image">
              				<div class="MagicZoom">
                        	  <a href="{{ url('productimg/').'/'. ($image[0]->imageurl) }}" id="imageshref" class="MagicZoom" rel="zoom-position:inner;zoom-fade:true">
                      			<img class="img-responsive" src="{{ url('productimg/thumb').'/'. ($image[0]->imageurl) }}" style="min-height: 400px">
                      		 </a>
                          	</div>
                          </div>
                        </div>
	                
	                  @endif
	                </div><!-- /.box-body -->
	              </div>
          	</div>

            
            <div class="col-md-8">
              <div class="box">
                <div class="box-header">
                  <div class="row">
                    <div class="col-md-4">
                        <h3 class="box-title">Product Details</h3>    
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                      <form action="#" class="form-horizontal" name="createad" id="createad">
                        <div class="form-group row">
                          <div class="col-md-9">
                            <select class="form-control" name="selAd" id="selAd">
                              <option value="CreateNewAd">Create New Ad</option>
                              <option value="Buy">View Buy Ads</option>
                              <option value="Sell">View Sell Ads</option>
                            </select>
                          </div>
                          <div class="col-md-3 clear-mar">
                            <button type="button" class="btn btn-success green" id="createselad"> Go</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  
                  
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="productviewtable" class="table table-hover">
                    <tr>
                      <td><b>Product Name</b></td>
                      <td>:</td>
                      <td>{{ $product->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Industry</b></td>
                      <td>:</td>
                      <td>{{ $product->industry->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Category</b></td>
                      <td>:</td>
                      <td>{{ $product->category->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Sub-Category</b></td>
                      <td>:</td>
                      <td>{{ $product->subcategory->name or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Brand</b></td>
                      <td>:</td>
                      <td>{{ $product->brand->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Manufacturer</b></td>
                      <td>:</td>
                      <td>{{ $product->manufacture or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Case Weight</b></td>
                      <td>:</td>
                      <td>{{ $product->caseweight or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Quantity Per Case</b></td>
                      <td>:</td>
                      <td>{{ $product->qtypercase or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Packing</b></td>
                      <td>:</td>
                      <td>{{ $product->pakaging or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Shipping Status</b></td>
                      <td>:</td>
                      <td>{{ $product->shipingcondition or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Minimum Profit Margin</b></td>
                      <td>:</td>
                      <td>{{ $product->mpm }} %</td>
                    </tr>
                    <tr>
                      <td><b>Product Codes</b></td>
                      <td>:</td>
                      <td>
	                      @foreach( $product->productCode as $code )
	                      
	                      	{{ $code->productcode }}      
	                      
	                      @endforeach

	                     &nbsp;&nbsp;
	                     <a data-toggle="modal" href="#static">+Add</a>                
                      </td>
                    </tr>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
          <div class="row">
          	<div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Description</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Related Products</a></li>
                  <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">Recommended</a></li>

                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
					           {{ $product->description }}                  
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                  	@foreach( $relatedProducts as $relatedProduct )



                  	@endforeach
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                  
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div>
          </div>

  <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title" style="color:#3aada3;"><strong>Add Product Code "{{ $product->name }}"</strong></h4>
        </div>
        <div class="modal-body">
          <form action="" id="form_productcode" class="form-horizontal" novalidate="novalidate">
            <input type="hidden" class="form-control" name="pid" id="pid" value="{{ \Request::segment(3) }}">
            <div class="form-group" style="margin-top:5px;" >
              <div class="col-md-12">
                
                <select class="form-control" name="prefix" id="prefix">
                
                  <option value="">Select Prefix</option>

                  @foreach( $productPrefixs as $prefix )
                  
                  <option value="{{ $prefix->proprefixid }}">{{$prefix->name}}</option>
                  
                  @endforeach
                
                </select>

              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <input type="text" class="form-control" name="code" id="code" placeholder="Enter Code">
              </div>
            </div>
            <div class="form-actions" align="left">
              <div class="row">
                <div class="col-md-12">
                  <button type="button" id="btnsave" class="btn btn-primary">Submit</button>
                  <button type="button" id="btnclose" data-dismiss="modal" class="btn btn-default">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@include('productCatalog.detail-product-viewJs')
@endsection