@extends('layout.master')

@section('content')

          <div class="row">
          	<div class="col-md-4">
          		<div class="box box-primary" style="min-height: 486px">
	                <div class="box-header with-border">
	                	<h4>View {{ $productid[1] }}</h4>
	                </div><!-- /.box-header -->
	                <div class="box-body">
	                  
                     @if($results!=0)

                        @if (strpos($product->productImage[0]->imageurl,'http://') !== false)
                           
                        <div class="BigImage"> <img src="{{ $product->productImage[0]->imageurl }}" alt="" width="300" > </div> 

                        @else

                            <div class="BigImage">
                             <div class="MagicZoom" id="image">
                                <div class="MagicZoom">
                                    <a href="{{ url('/').'/productimg/'.$product->productImage[0]->imageurl }}" id="imageshref" class="MagicZoom" rel="zoom-position:inner;zoom-fade:true">
                                      <img id="bimg" src="{{ url('/').'/productimg/thumb/'.$product->productImage[0]->imageurl }}" alt="" width="300">
                                    </a>
                                </div>
                              </div>
                            </div>


                        @endif

                     @else

                        <div class="BigImage"> <img src="{{ url('/') }}/productimg/image4.jpg" alt="" width="258" height="309"> </div>
                     
                     @endif
	                </div><!-- /.box-body -->
	              </div>
          	</div>

            
            <div class="col-md-8">
              <div class="box">
                <div class="box-header">
                  <div class="row">
                    <div class="col-md-4">
                        <!-- <h3 class="box-title">Product Details</h3>     -->
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                      <form action="#" class="form-horizontal" name="createad" id="createad">
                        <div class="form-group row">
                          <div class="col-md-9">
                            <select class="form-control" name="selAd" id="selAd">
                              <option value="CreateNewAd">Create New Ad</option>
                              <option value="Buy">View Buy Ads</option>
                              <option value="Sell">View Sell Ads</option>
                            </select>
                          </div>
                          <div class="col-md-3 clear-mar">
                            <button type="button" class="btn btn-success green" id="createselad"> Go</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  
                  
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="productviewtable" class="table table-hover">
                    <tr>
                      <td><b>Product ID</b></td>
                      <td>:</td>
                      <td>{{ $product->productno }}</td>
                    </tr>
                    <tr>
                      <td><b>Product Name</b></td>
                      <td>:</td>
                      <td>{{ $product->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Industry</b></td>
                      <td>:</td>
                      <td>{{ $product->industry->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Category</b></td>
                      <td>:</td>
                      <td>{{ $product->category->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Sub-Category</b></td>
                      <td>:</td>
                      <td>{{ $product->subcategory->name or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Brand</b></td>
                      <td>:</td>
                      <td>{{ $product->brand->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Manufacturer</b></td>
                      <td>:</td>
                      <td>{{ $product->manufacture or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Case Weight</b></td>
                      <td>:</td>
                      <td>{{ $product->caseweight or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Quantity Per Case</b></td>
                      <td>:</td>
                      <td>{{ $product->qtypercase or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Packing</b></td>
                      <td>:</td>
                      <td>{{ $product->pakaging or '' }}</td>
                    </tr>
                    <tr>
                      <td><b>Shipping Status</b></td>
                      <td>:</td>
                      <td>{{ $product->shipingcondition or '' }}</td>
                    </tr>                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
          <div class="row">
          	<div class="col-md-12">
                <div class="box">
                  <div class="box-header">
                    <!-- <h3 class="box-title">Suggested Products</h3> -->
                  </div><!-- /.box-header -->
                  <div class="box-body table-responsive">
                    <table id="viewProductadTable" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th width="8%"> Posting ID </th>
                          <th width="10%"> Date Posted </th>
                          <th width="10%"> Industry </th>
                          <th width="10%"> Category </th>
                          <th width="5%"> Brand </th>
                          <th width="5%"> Type</th>
                          <th width="5%"> Quantity</th>
                          <th width="39%"> Product Name </th>
                          <th width="8%"> Exp Date </th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->              
            </div>
          </div>
@include('productCatalog.viewProductadJs')
@endsection