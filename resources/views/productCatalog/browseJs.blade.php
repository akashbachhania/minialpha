<script type="text/javascript">

function deleteme(obj) {

    var data = {
        type: 'delete',
       
        _token: $('input[name=_token]').val(),
        id: obj
    };
    if (confirm("Are you sure you wish to delete this Product?")) {
        $.ajax({
            type: 'POST',
            url: 'product_delete',
            cache: false,
            data: data,
            
            success: function(data) {
                
                if (data.msg == 'ERROR') {
                    alert(data.name)
                } else {
       
                    // window.location = root + '/product/browse';
                }
                 // $('#datatable_ajax').DataTable().ajax.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }
}


jQuery(document).ready(function() {
   
    var productCatalogTable1 = $('#product-catalog1').DataTable( {

        "ajax": {
            "url":"browseProduct_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'productid' },
        { data: 'industry' },
        { data: 'category' },
        { data: 'brand' },
        { data: 'productname' },
        { data: 'productcodes' }
     
        ]
    } );

    setInterval( function () {
        productCatalogTable1.ajax.reload();
    }, 3000 );


    var productCatalogTable2 = $('#product-catalog2').DataTable( {

        "ajax": {
            "url":"browseProduct_ajax",
            "dataSrc": "",
            "type": 'POST',
        },
        columns: [
        { data: 'productid' },
        { data: 'industry' },
        { data: 'category' },
        { data: 'brand' },
        { data: 'productname' },
        { data: 'productcodes' },
        { data: 'action' }
     
        ]
    } );

    setInterval( function () {
        productCatalogTable2.ajax.reload();
    }, 3000 );      


    $('#dvfilter').hide();
   
    $('#show').click(function() {

        $('#dvfilter').toggle('slow');
        document.cookie ="show=showopen";
        document.cookie ="bStateSave=true";
    });
   
    var showvalue = '{{ $_COOKIE['show'] }}';
   
    if (showvalue == 'showopen') {
        $('#dvfilter').toggle('slow');
    }
   
    $('#product-catalog1 tbody').on('click', 'tr td:not(:last-child)', function() {
        
        var data = productCatalogTable1.row(this).data();
        var page = $(this).parent().find('td a').attr('href');
        // alert(page);
        window.location.href = page;
        //window.open(page, '_blank');
    });

    $('#product-catalog2 tbody').on('click', 'tr td:not(:last-child)', function() {
        var data = productCatalogTable1.row(this).data();
        var page = $(this).parent().find('td a').attr('href');
        window.location.href = page;
        //window.open(page, '_blank');
    });
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var indmode="industaction";
        var industryvalue="{{ $_COOKIE['industryData'] }}";
        alert(industryvalue);
        var formData = {
            type: indmode
        };
        $.ajax({
            type: "POST",
            url: root + "/industry_ajax",
            data: formData,
            dataType: 'json',
            success: function(data) {
                // console.log(data.industry);
                $.each(data.industry, function(i, data){
                    
                    var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
        
                    $(div_data).appendTo("#industry");
                        
                });
            }
            
        });
    });

</script>
<script type="text/javascript">
     $(document).ready(function() {
        $("#industry").change(function() {
            var industries = [];
            $.each($(".industry option:selected"), function() {
                industries.push($(this).val());
            });
            //alert(industries.join(","));
            var selectedindustry = industries.join(",");
            var indmode = "selectindustry";
            var formData = {
                ind_id: selectedindustry,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    $("#cat_id").empty();
                    $("#brand").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    var div_data = "<option value='0'>Select  Categories</option>";
                    $(div_data).appendTo("#cat_id");
                    var div_data2 = "<option value='0'>Select  Brands</option>";
                    $(div_data2).appendTo("#brand");

                    if (data != null && data != '')
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#cat_id");
                            });
                        $.each(data.brandData, function(i, data)
                            {
                                var div_data2 = "<option value=" + data.brandid + ">" + data.name + "</option>";
                                $(div_data2).appendTo("#brand");
                            });
                    } else
                    {
                        //alert("Categories does not exist");   
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#cat_id").change(function() {
            var categories = [];
            $.each($(".category option:selected"), function() {
                categories.push($(this).val());
            });
            //alert(industries.join(","));
            var selectcategories = categories.join(",");
            var indmode = "selectsubcategory";
            var formData = {
                cat_id: selectcategories,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "{{ url('/multiselect') }}",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#sub_cat");
                            });
                    } else
                    {
                        //alert("SubCategories does not exist");                
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //alert(errorThrown);
                    commit(false);
                }
            });
        });
    });

</script>
<script type="text/javascript">
    $('#filter').click(function() {
        
        var categoryData = $('#cat_id option:selected').val();
        var scategoryData = $('#sub_cat option:selected').val();
        var brandData = $('#brand option:selected').val();
        var status = $('#status option:selected').val();
        
        var industry = [];
        
        $.each($("#industry option:selected"), function(){            
            industry.push($(this).val());
        });
        
        var industryData = industry.join(",");
        
        var cat_id = [];
        
        $.each($("#cat_id option:selected"), function(){            
            cat_id.push($(this).val());
        });
        
        var categoryData = cat_id.join(",");
        
        var sub_cat = [];
        
        $.each($("#sub_cat option:selected"), function(){            
            sub_cat.push($(this).val());
        });
        
        var scategoryData = sub_cat.join(",");
        
        var brand = [];
        
        $.each($("#brand option:selected"), function(){            
            brand.push($(this).val());
        });
        
        var brandData = brand.join(",");
        // alert("You have selected the country - " + countries.join(", "));
        
        var productCatalogTable1 = $('#product-catalog1').DataTable( {
            destroy:true,
            "ajax": {
                "url":"browseProduct_ajax",
                "dataSrc": "",
                "type": 'POST',
                data:{
                    productActionType:"group_filter",
                    industryData:industryData,
                    categoryData:categoryData,
                    scategoryData:scategoryData,
                    brandData:brandData,
                    status:status,
                },
            },
            columns: [
            { data: 'productid' },
            { data: 'industry' },
            { data: 'category' },
            { data: 'brand' },
            { data: 'productname' },
            { data: 'productcodes' }
         
            ]
        } );

        var productCatalogTable2 = $('#product-catalog2').DataTable( {
            destroy:true,
            "ajax": {
                "url":"browseProduct_ajax",
                "dataSrc": "",
                "type": 'POST',
                data:{
                    productActionType:"group_filter",
                    industryData:industryData,
                    categoryData:categoryData,
                    scategoryData:scategoryData,
                    brandData:brandData,
                    status:status,
                },
            },
            columns: [
            { data: 'productid' },
            { data: 'industry' },
            { data: 'category' },
            { data: 'brand' },
            { data: 'productname' },
            { data: 'productcodes' },
            { data: 'action' }
         
            ]
        } );

        setInterval( function () {
            productCatalogTable2.ajax.reload();
        }, 3000 );      
        
    });
</script>