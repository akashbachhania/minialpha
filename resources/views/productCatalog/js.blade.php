<script type="text/javascript">
	$(document).ready(function(){
		$("#datepicker").daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});

	    $('#addtype').on('change', function() {
	      if ( this.value == 'BUY')
	      {
			$("#epd").hide();
	      }
		  if ( this.value == '')
	      {
			$("#epd").hide();
	      }
	      if (this.value == 'SELL')
	      {
			$("#epd").show();
	      }
	    });		
	});
</script>
<script type="text/javascript">
    function validate()
      {
		var addprice = document.getElementById("addprice").value;
		var addcurrency = document.getElementById("addcurrency").value;
		var addqty = document.getElementById("addqty").value;
		var addlocation = document.getElementById("addlocation").value;
		var addexpdrange = document.getElementById("addexpdrange").value;
		var addcurrefno = document.getElementById("addcurrefno").value;
		var addtimeframe = document.getElementById("addtimeframe").value;
		
         /*if( searchid =="" )
         {
            alert( "Please Select a Product!" );
			document.getElementById("searchid").focus();
            return false;
         }*/
         if( document.crateadd.addtype.value == "" )
         {
            alert( "Please Select Add Type!" );
			document.getElementById("addtype").focus();
            return false;
         }
		 if(addprice =="" )
         {
            alert( "Please Fill Product Price!" );
			document.getElementById("addprice").focus();
            return false;
         }
		 if( document.crateadd.addcurrency.value == "" )
         {
            alert( "Please Select Currency!" );
			document.getElementById("addcurrency").focus();
            return false;
         }
		 if(addqty =="" )
         {
            alert( "Please Fill Product Quantity!" );
			document.getElementById("addqty").focus();
            return false;
         }
		 if( document.crateadd.addlocation.value == "" )
         {
            alert( "Please Select Location!" );
			document.getElementById("addlocation").focus();
            return false;
         }
		 if( document.crateadd.addexpdrange.value == "" )
         {
            alert( "Please Select Exp. Daterange!" );
			document.getElementById("addexpdrange").focus();
            return false;
         }
		 if( document.crateadd.addcurrefno.value == "" )
         {
            alert( "Please Select Customer Refrence No.!" );
			document.getElementById("addcurrefno").focus();
            return false;
         }
		 if( document.crateadd.addtimeframe.value == "" )
         {
            alert( "Please Select Timeframe!" );
			document.getElementById("addtimeframe").focus();
            return false;
         }
         return( true );
      }
</script>
<script>
	jQuery(document).ready(function() {
		var pro_id = '{{ \Request::segment(3) }}';
		var type = "fetchalldata";
		var dataString = 'pro_id=' + pro_id + '&type=' + type;
		if(pro_id!='')
		{
			$.ajax({
			type: "POST",
			url: root + "/product/fetchalldata",
			data: dataString,
			cache: false,
			success: function(response)
			{
				// response = jQuery.parseJSON(data);
				//alert(data);
				 $('#productid').html(response.productno);
			     var pro_id =response.productid;
	             $('#productid').html(response.postno);
				 $('#pdate').html(response.pdate);
				 $('#name').html(response.name);
				 $('#productid').html(response.productno);
				 $('#postno').val(response.postno);
				  $('#addprono').val(response.productno);
				 $('#name').html(response.name);
				 $('#industryid').html(response.industry);
				 $('#catid').html(response.category);
				 $('#subcatid').html(response.subcategory);
				 $('#brandid').html(response.brand);
				 $('#manufacture').html(response.manufacture);
				 $('#pakaging').html(response.pakaging);
				 $('#shipingcondition').html(response.shipingcondition);
				 $('#procodes').html(response.procodes);
				 $('#caseweight').html(response.caseweight);
				 $('#pstatus').html(response.pstatus);
				 $('#timeframe').html(response.timeframe);
				 $('#description').html(response.description);
				var exactexpdrange =  response.expirydate;
				if(exactexpdrange!='0000-00-00 00:00:00')
				{
					$('#expirydate').html(response.expirydate);
					}
					else
					{
						$('#expirydate').html('Not Applicable');
						}
				 $('#expdate').html(response.expdate);
				 $('#uom').html(response.uom);
				 
				 var qtyprcase = response.qtypercase;
	               // alert(qtyprcase);
	                if((qtyprcase =='') || (qtyprcase == 0))
	                {
						
					}
					else
					{
						$('#qtypercase').html(response.qtypercase);
					}			 			 			 
								 			 
				 $('#ptype').html(response.ptype);
				 
				
				$("#fimges").html(response.image).show();
				
				
				$("#procodes").html(response.productcodes).show();
				 
			}
			});
		}
	return false;  
	});
</script>