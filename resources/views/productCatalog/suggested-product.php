<?php
session_start();
setcookie("brandData", "", time()-3600);
setcookie("categoryData", "", time()-3600);
setcookie("industryData", "", time()-3600);
setcookie("scategoryData", "", time()-3600);
setcookie("productActionType", "", time()-3600); 
setcookie("customActionName", "", time()-3600); 
setcookie("status", "", time()-3600);
setcookie("dateData", "", time()-3600); 
include 'config.php';
include ('functions.php');

if (!isset($_SESSION['userid']))
	{
	header('location:login.php');
	}

include ('commonhead.php');
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script type="text/javascript">
var __lc = {};
__lc.license = 6594891;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>

<!-- END PAGE LEVEL STYLES -->
<script type="text/javascript">
    function shownoti(message) {
        $(function() {
            function Toast(type, css, msg) {
                this.type = type;
                this.css = css;
                this.msg = msg;
            }
            var toasts = [
                new Toast('success', 'toast-top-right', message),
            ];
            toastr.options.positionClass = 'toast-top-full-width';
            toastr.options.extendedTimeOut = 0; //1000;
            toastr.options.timeOut = 2000;
            toastr.options.fadeOut = 250;
            toastr.options.fadeIn = 250;
            var i = 0;
            /* $('#tryMe').click(function () {

                 $('#tryMe').prop('disabled', true);*/
            delayToasts();
            //});
            function delayToasts() {
                if (i === toasts.length) {
                    return;
                }
                var delay = i === 0 ? 0 : 2100;
                window.setTimeout(function() {
                    showToast();
                }, delay);
                // re-enable the button        
                if (i === toasts.length - 1) {
                    window.setTimeout(function() {
                        prop('disabled', false);
                        i = 0;
                    }, delay + 1000);
                }
            }

            function showToast() {
                var t = toasts[i];
                toastr.options.positionClass = t.css;
                toastr[t.type](t.msg);
                i++;
                delayToasts();
            }
        })
    }
</script>
<style>
@media only screen and (max-width: 600px) {
.zee-filter {
    background-color: #8e5fa2;
    color: #fff;
    margin-top: 10px;
}
}
</style>
</head><!-- END HEAD -->
<!-- BEGIN HEADER -->
<?php include('header.php');?>
<!-- END HEADER -->
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <?php include('sidebar.php');?>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <h3 class="page-title"> Suggested Products </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
         <li> <a href="dashboard.php">Home</a> <i class="fa fa-angle-right"></i> </li>
          <li> <a href="product-catalog.php">Product Catalog</a> <i class="fa fa-angle-right"></i> </li>
          <li> <a href="#">Suggested Products </a> </li>
        </ul>
      </div>
      <?php if($_REQUEST['add']=='true')
			{
			 ?>
        <script> 
				$(document).ready(
							function() {
							//alert(333333333);
									$.ajax({
											url: "service/noti6.php",
											type: "post",
											cache: false,
											dataType:'html',
													success: function(res2){
													//alert(res2);
														if(res2=='bothclose')
														{
														
														}
														if(res2=='audioclose')
														{
  															 shownoti("Product status updated"); 
															}
														if(res2=='vedioclose')
														{
												$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									
									 $('#sound')[0].play(); 
														}
													if(res2=='bothstart')
													{
   										shownoti("Product status updated"); 
		$('<audio  id="sound" ><source src="assets/notifiles/3/0396.ogg" type="audio/ogg"><source src="assets/notifiles/3/0396.mp3" type="audio/mpeg"><source src="assets/notifiles/3/0396.wav" type="audio/wav"></audio>').appendTo('body');
									 $('#sound')[0].play(); 
													}
												 }
											});
                     });
				 </script>
          
		<?php }?>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
          <!-- Begin: life time stats -->
          <div class="portlet">
            <div class="row" id="a" name="a"> </div>
            <div class="col-md-12 zee-margin" style="margin-bottom:20px;">
              <div class="row">
                <div class="col-md-3">
                  <div class="pagination-panel"> Sort By :
                    <select class="table-group-action-input form-control input-inline input-small input-sm" id="sort">
                      <option value="brand-ASC" selected>Brand - A to Z</option>
                      <option value="brand-DESC">Brand - Z to A</option>
                      <option value="date-ASC">Date - Earliest to Latest</option>
                      <option value="date-DESC">Date - Latest to Earliest</option>
                      <option value="industry-ASC">Industry - A to Z</option>
                      <option value="industry-DESC">Industry - Z to A</option>
                      <option value="productno-ASC">Product ID - A to Z</option>
                      <option value="productno-DESC">Product ID - Z to A</option>
                      <option value="productName-ASC">Product Name - A to Z</option>
                      <option value="productName-DESC">Product Name - Z to A</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-8"> </div>
                <div class="col-md-1">
                  <button type="button" class="btn zee-filter" id="show">Filter</button>
                </div>
              </div>
            </div>
            <?php if($_SESSION['roletype'] == "AD")	{?>
            <div class="col-md-12 zee-margin" id="dvfilter" style="margin-bottom:0px;">
              <div class="col-md-3 zee-col">
                <div class="form-group">
                  <select class="form-control industry" name="industry[]" id="industry" onChange="getState(this.value);getbrand(this.value);" multiple="multiple">
                    <option value="0" class="selectitem">Select Industries</option>
                  </select>
                </div>
              </div>
              <div class="col-md-2 zee-category">
                <div id="categroy" class="form-group">
                  <select class="form-control category" onchange="subcat(this.value);" name="category[]" id="cat_id" multiple="multiple">
                    <option value="0">Select categories</option>
                  </select>
                </div>
              </div>
              <div class="col-md-2 zee-category">
              	<div class="form-group">
                <select class="form-control" name="scategory[]" id="sub_cat" multiple="multiple">
                  <option value="0">Select Sub-categories</option>
                </select>
                </div>
              </div>
              <div class="col-md-2 zee-category">
              	<div class="form-group">
                <select class="form-control" id="brand" name="brand[]" multiple="multiple">
                  <option value="0">Select Brands</option>
                </select>
                </div>
              </div>
              <div class="col-md-2 zee-category">
              	<div class="form-group">
                <select class="form-control" id="status">
                  <option value="0">Select Status</option>
                  <option value="APPROVED">Approved</option>
                  <option value="WAITING">Waiting</option>
                  <option value="REJECTED">Rejected</option>
                  <option value="PROCESSING">Processing</option>
                </select>
                </div>
              </div>
              <div class="col-md-1 zee-category">
              	<div class="form-group">
                <button type="button" class="btn zee-filter" id="filter">Show</button>
                </div>
              </div>
            </div>
            <?php } else {?>
            <div class="col-md-12 zee-margin" id="dvfilter" style="margin-bottom:0px;">
              <div class="col-md-3 zee-col">
                <div class="form-group">
                  <select class="form-control industry" name="industry[]" id="industry" onChange="getState(this.value);getbrand(this.value);" multiple="multiple">
                    <option value="0" class="selectitem">Select Industries</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 zee-category">
                <div id="categroy" class="form-group">
                  <select class="form-control category" onchange="subcat(this.value);" name="category[]" id="cat_id" multiple="multiple">
                    <option value="0">Select categories</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 zee-category">
              	<div class="form-group">
                <select class="form-control" name="scategory[]" id="sub_cat" multiple="multiple">
                  <option value="0">Select Sub-categories</option>
                </select>
                </div>
              </div>
              <div class="col-md-2 zee-category">
              	<div class="form-group">
                <select class="form-control" id="brand" name="brand[]" multiple="multiple">
                  <option value="0">Select Brands</option>
                </select>
                </div>
              </div>
              <div class="col-md-1 zee-category">
              	<div class="form-group">
                <button type="button" class="btn zee-filter" id="filter">Show</button>
                </div>
              </div>
            </div>
            <?php }?>
            <div class="portlet-body">
              <div class="table-container">
                <div class="table-actions-wrapper" style="tex-align:left"> <span> Sory By : </span>
                  <select class="table-group-action-input form-control input-inline input-small input-sm">
                    <option value="0">Select...</option>
                    <option value="Cancel">Cancel</option>
                    <option value="Cancel">Hold</option>
                    <option value="Cancel">On Hold</option>
                    <option value="Close">Close</option>
                  </select>
                  <button class="btn btn-sm yellow table-group-action-submit1"><i class="fa fa-check"></i> Submit</button>
                </div>
                <table class="table table-striped table-bordered table-hover" id="datatable_suggest_product">
                  <thead>
                    <tr role="row" class="zeeproduct-heading">
                      <th > Date Suggested </th>
                      <th > Suggested By </th>
                      <th > Product Id </th>
                      <th > Industry </th>
                      <th > Brand </th>
                      <th > Product Name </th>
                      <th > Product Code </th>
                      <th > Status </th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- End: life time stats -->
        </div>
      </div>
      <!-- END PAGE CONTENT-->
    </div>
  </div>
  <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include('footer.php'); ?>
<!-- END FOOTER -->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/global/scripts/datatable.js"></script>
<script src="assets/admin/pages/scripts/table-suggest-product.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        TableSuggestProduct.init();
        $('#dvfilter').hide();
        $('#show').click(function() {
            $('#dvfilter').toggle('slow');
        });
		<?php if($_SESSION['roletype'] == "AD") {?>
        var table = $('#datatable_suggest_product').DataTable();
        $('#datatable_suggest_product tbody').on('click', 'tr', function() {
            var data = table.row(this).data();
            var page = $(data[5]).attr('href');
            window.location.href = page;
        });
		<?php } ?>
    });
</script>
<script type="text/javascript">
jQuery(document).ready(function() {
 var indmode="industaction";
    var formData = {
        actionmode: indmode
    };
    $.ajax({
        type: "POST",
        url: "functions.php",
        data: formData,
        dataType: 'json',
        success: function(data) {
            $.each(data.itemData, function(i, data)
                {
				//alert(data.industryid);
                    var div_data = "<option value=" + data.industryid + ">" + data.name + "</option>";
                    $(div_data).appendTo("#industry");
                });
        }
    });
});
    //sub category function start
 $(document).ready(function() {
        $("#industry").change(function() {
            var industries = [];
            $.each($(".industry option:selected"), function() {
                industries.push($(this).val());
            });
            //alert(industries.join(","));
            var selectedindustry = industries.join(",");
            var indmode = "selectindustry";
            var formData = {
                ind_id: selectedindustry,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "functions.php",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    $("#cat_id").empty();
                    $("#brand").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    var div_data = "<option value='0'>Select  Categories</option>";
                    $(div_data).appendTo("#cat_id");
                    var div_data2 = "<option value='0'>Select  Brands</option>";
                    $(div_data2).appendTo("#brand");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.catid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#cat_id");
                            });
                        $.each(data.brandData, function(i, data)
                            {
                                var div_data2 = "<option value=" + data.brandid + ">" + data.name + "</option>";
                                $(div_data2).appendTo("#brand");
                            });
                    } else
                    {
                        //alert("Categories does not exist");	
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#cat_id").change(function() {
            var categories = [];
            $.each($(".category option:selected"), function() {
                categories.push($(this).val());
            });
            //alert(industries.join(","));
            var selectcategories = categories.join(",");
            var indmode = "selectsubcategory";
            var formData = {
                cat_id: selectcategories,
                actionmode: indmode
            }; //Array 
            $.ajax({
                type: "POST",
                url: "functions.php",
                async: false,
                data: formData,
                dataType: 'json',
                success: function(data, status, xhr) {
                    //alert(data);
                    $("#sub_cat").empty();
                    var div_data = "<option value='0'>Select Sub Categories</option>";
                    $(div_data).appendTo("#sub_cat");
                    if (data != null)
                    {
                        $.each(data.itemData, function(i, data)
                            {
                                var div_data = "<option value=" + data.subcatid + ">" + data.name + "</option>";
                                $(div_data).appendTo("#sub_cat");
                            });
                    } else
                    {
                        //alert("SubCategories does not exist");				
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    commit(false);
                }
            });
        });
    });
</script>

</script>
</body></html>