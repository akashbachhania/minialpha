// this check industry name is exist in our database or not
jQuery(document).ready(function() {
        $("#name").change(function() {
            var pname = $("#name").val();
            if (pname != '')
            {
                $.ajax({
                    type: "POST",
                    url: Url,
                    async: false,
                    data: {
                        pname: $("#name").val(),
                        type: 'exist',
                        _token: $('input[name=_token]').val(),
                    },
                    success: function(data, status, xhr) {
                        if (data == 'exist') {
                            $("#msgg").show().html('<span class="redProduct"><i class="glyphicon glyphicon-remove"></i> Already exist</span>');
                            $("#checkProduct").val('0');
                        } else {
                            $("#msgg").show().html('<span class="greenProduct"><i class="glyphicon glyphicon-ok"></i> Available</span>');
                            $("#checkProduct").val('1');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                        commit(false);
                    }
                });
            } else
            {
                $("#msgg").show().html('<span></span>');
            }
        });
 });

// this show a new model for add industry
$("#btnnew").on("click", function() {

            $('#basic').modal('show');
            $('#status').val('');
            $('#name').val('');
            $("label.error").hide();
            $(".error").removeClass("error");
            setTimeout(function() {
                $('#name').focus();
            }, 1000);
});

// this close add industry model


$(document).on("click","#btnclose", function() {
 
    $('#status').val('');
    $('#name').val('');
    $('#basic').modal('hide');
    $("#msgg").hide();
    $("#checkProduct").val('');
    $('#name').attr('autocomplete', 'false');
});

// this will add and update industry

$("#btnsave").on("click", function() {
    var name = $('#name').val();
    var question = $('#question').val();
    var status = $('#status').val();
    var id = $('#id').val();
    //alert(id);
    if (question == '') {
        alert('Question type must be Required');
        return;
    }
    else if (name == '' || status == '') {
        alert('Question must be Required');
        return;
    }
    if (id == "0") {
        //var data = "Type=add&name=" + name + "&status=" + status;
        var data = {
            type: 'add',
            name: name,
            question:question,
            _token: $('input[name=_token]').val(),
            status: status

        };
        $.ajax({
             type: 'POST',
             url: Url,
             cache: false,
             data: data,
             async: false,
             success: function(data) {
               
                if (data.msg == 'SUCCESS') {
                                
                    shownoti("Industry Added Successfully");
                    $('#basic').modal('hide');
                    var table = $('#datatable_ajax').DataTable();
                    table.ajax.reload();
                    $("#msgg").hide();
                    $("#checkProduct").val('');
                    $("#name").val('');
                }
                if (data.msg == 'EROOR') {
                   
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    } //if finish share
    else {
        
        var data = {
            name: name,
            type: 'update',
            status: status,
            _token: $('input[name=_token]').val(),
            id: id
        };
        $.ajax({
            type: 'POST',
            url: Url,
            cache: false,
            data: data,
            async: false,
            success: function(data) {
                
                if (data.msg == 'SUCCESS') {
             //shownoti("Industry Updated Successfully");
             
                    $('#basic').modal('hide');
                    $("label.error").hide();
                    $(".error").removeClass("error");
                   var table = $('#datatable_ajax').DataTable();
                   table.ajax.reload();
                    $("#msgg").hide();
                    $("#checkProduct").val('');
                    $('#id').val(0);
                }
                if (data.msg == 'EROOR') {
                    //alert(response.name);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }
});
 function editme(obj) {
        $('#basic').modal('show');
        $('#status').val('');
        $('#name').val('');
        $('#id').val(obj);
        setTimeout(function() {
            $('#name').focus();
        }, 1000);
         var data = {
            type: 'search',
           
            _token: $('input[name=_token]').val(),
            id: obj
        };
        $.ajax({
            type: 'POST',
            url: Url,
            cache: false,
            data: data,
            dataSrc:"industry",
            data: data,
            success: function(data) {
               
                $('#status').val(data.status);
                $('#name').val(data.name);
                $("#msgg").hide();
                $("#checkProduct").val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //commit(false);
            }
        });
    }

    function deleteme(obj) {
       var data = {
            type: 'delete',
           
            _token: $('input[name=_token]').val(),
            id: obj
        };
        if (confirm("Are you sure you wish to delete this Industry?")) {
            $.ajax({
                type: 'POST',
                url: Url,
                cache: false,
                data: data,
                
                success: function(data) {
                    
                    if (data.msg == 'ERROR') {
                        alert(data.name)
                    } else {
           
                        window.location = 'industry';
                    }
                     $('#datatable_ajax').DataTable().ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        }
    }