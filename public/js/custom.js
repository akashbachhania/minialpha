$(document).ready(function(){

    if(String(window.location).match(/userdetails/)){
        
        var userdetailtable = $('#userdetail').DataTable( {

            "ajax": {
                "url":"userdetail_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'userid' },
            { data: 'userrole' },
            { data: 'creationdate' },
            { data: 'lastlogin' },
            { data: 'status' },
            { data: 'action' }
         
            ]
        });

        setInterval( function () {
            userdetailtable.ajax.reload();
        }, 3000 );  
    }

    /*else if( String(window.location).match(/acceptedoffers/) ){

        $('#acceptedoffers').dataTable();

    }*/
    else if(String(window.location).match(/activity/)){
        var url = $(location).attr('href');
        
        var fn = url.split('/').reverse()[0];

        var lm = url.split('/').reverse()[1];
        
        if( lm == 'activity' ){
            if( fn != '' ){

                var useravtivitytable = $('#useractivity').DataTable( {

                    "ajax": {
                        "url":root + "/activity_ajax/"+fn,
                        "dataSrc": "",
                        "type": 'GET',
                    },
                    columns: [
                    { data: 'userid' },
                    { data: 'lastlogin' },
                    { data: 'ipaddress' },
                    { data: 'activity' }
                 
                    ]
                } );

                setInterval( function () {
                    useravtivitytable.ajax.reload();
                }, 3000 );

            }
            
        }
        else if( fn == 'activity' ){

                var useravtivitytable = $('#useractivity').DataTable( {

                    "ajax": {
                        "url":root + "/activity_ajax",
                        "dataSrc": "",
                        "type": 'GET',
                    },
                    columns: [
                    { data: 'userid' },
                    { data: 'lastlogin' },
                    { data: 'ipaddress' },
                    { data: 'activity' }
                 
                    ]
                } );

                setInterval( function () {
                    useravtivitytable.ajax.reload();
                }, 3000 );

            }

       
        $('#exportcsv').click(function() {

                $.ajax({
                    url: root +'/exportActivity',
                    success: function (resp) {
                        
                    }
                });
            
        });
    }
    else if(String(window.location).match(/create/) || String(window.location).match(/edit/) ){
        $(document).ready(function(){
            $("#from,#to").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
        });
        function checkPass(){
            var pass = $("#pass").val();
            var conpass = $("#conpass").val();
            
            if (pass != conpass){
                alert('Passwords do not Match!');
                $("#conpass").parent().parent().addClass('has-error');
                $("#conpass").focus();
                return false;
            }
            $("#conpass").parent().parent().removeClass('has-error');
            return true;

        }

        function checkPassLength(){
            var s = $('#pass').val();
            // check for null or too short
            if (!s || s.length < 6) {
                alert("Pasword must have at least 6 characters."); // prompt user
                $("#pass").parent().parent().addClass('has-error');
                $("#pass").focus();
                return false;
            }
            // check for a number
            else if (/[0-9]/.test(s) === false) {
                alert("Pasword must have at least one number."); // prompt user
                $("#pass").parent().parent().addClass('has-error');
                $("#pass").focus();
                return false;
            }
            // check for a capital letter
            else if (/[a-zA-Z]/.test(s) === false) {
                alert("Pasword must have at least one letter."); // prompt user
                $("#pass").parent().parent().addClass('has-error');
                $("#pass").focus();
                return false;
            }
            $("#pass").parent().parent().removeClass('has-error');
            return true; // return true if all conditions are met
        }

        $("#role,#authpass").change(function(){
            $(this).parent().parent().removeClass('has-error');
        });

        $('#submitButton').click(function(event){
            event.preventDefault();
            var authpass = "{{ Auth::user()->authorizedpass }}";
            
            if ($("#role").val() == 0){
        
                $("#role").parent().parent().addClass('has-error');
                alert("User Role Required"); // prompt user
                $("#role").focus(); //set focus back to control
                return false;
        
            }
        
            if ($("#role").val() ==1)
            {
                var options = $('#indmultipleSelect > option:selected');
                if (options.length == 0) {
                    alert('Industries are Required');
                    $("#indmultipleSelect").focus();
                    return false;
                }
            }

            if ($("#authpass").val() == '') {
                alert('Authorized code is required');
                $("#authpass").parent().parent().addClass('has-error');
                $("#authpass").focus();
                return false;
            } else {
                $.get(root + '/checkauthpass',{authpass:$("#authpass").val()},function(data){
                    if(data == 'success'){
                        $("form").submit();
                    }
                    else{
                        alert('Authorized Password Incorrect!');
                        $("#authpass").parent().parent().addClass('has-error');
                        $("#authpass").focus();
                    }
                    
                });
            }
            
        });
            

    }
    else if(String(window.location).match(/dbbackup/)){
        $('.fa-download').on('click',function(){
            
            // $.get(root + '/backup',function(data){
                // alert(data);             
            // });
        });
    }
    else if(String(window.location).match(/browse/)){
        
    }

    else if(String(window.location).match(/detail-product-view/)){

          $("#btnsave").on("click", function() {
            var prefix = $('#prefix').val();
            var code = $('#code').val();
            var pid = $('#pid').val();
            // $("#form_productcode").valid();
            if (prefix == '' || code == '') {
                alert('Please Enter The Prefix Code');
                return;
            }
            
            var data = {
                Type: 'addcode',
                prefix: prefix,
                code: code,
                pid: pid
            };
            $.ajax({
                type: 'POST',
                url: root + '/product/addProductPrefix_ajax',
                cache: false,
                data: data,
                success: function(data) {
                    // alert(data);
                    console.log(data);
                    // response = jQuery.parseJSON(data);
                    if (data.msg == 'SUCCESS') {
                        $('#basic').modal('hide');
                        // shownoti("Successfully submitted product code request");
                        
                        setTimeout(function() {
                            window.location.href = root+ "/product/detail-product-view/" + data.pid;
                        }, 1000);
                    }
                    if (data.msg == 'ERROR') {
                        alert(data.name);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //commit(false);
                }
            });
        });

    }
    else if(String(window.location).match(/suggestedProductCode/)){

        var suggestedProductCodeTable = $('#suggestedProductCodeTable').DataTable( {

            "ajax": {
                "url":"suggestedProductCode_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'suggestdate' },
            { data: 'userid' },
            { data: 'industry' },
            { data: 'brand'},
            { data: 'productName' },
            { data: 'productCode' },
            { data: 'status' },
            // { data: 'edit'}
         
            ]
        } );

        setInterval( function () {
            suggestedProductCodeTable.ajax.reload();
        }, 3000 );

    }
    else if(String(window.location).match(/suggestedProduct/)){

        var suggestedProductTable = $('#suggestedProductTable').DataTable( {

            "ajax": {
                "url":"suggestedProduct_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'dateSuggested' },
            { data: 'suggestedBy' },
            { data: 'productId' },
            { data: 'industry' },
            { data: 'brand'},
            { data: 'productName' },
            { data: 'productCode' },
            { data: 'status' }
         
            ]
        } );

        setInterval( function () {
            suggestedProductTable.ajax.reload();
        }, 3000 );

    }
    else if(String(window.location).match(/reportedProduct/)){

        var reportedProductTable = $('#reportedProductTable').DataTable( {

            "ajax": {
                "url":"reportedProduct_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'suggestdate' },
            { data: 'userid' },
            { data: 'industry' },
            { data: 'brand'},
            { data: 'productName' },
            { data: 'reportCode' },
            { data: 'msg' }
         
            ]
        } );

        // setInterval( function () {
        //     reportedProductTable.ajax.reload();
        // }, 3000 );

        $(document).on('click', '#reportedProductTable tbody tr', function () {   
                // alert('HELLO');
                // var data = $(this).find('a').attr('href');
                // alert(data);           
                // window.location.href = data;
            });

    }
    else if(String(window.location).match(/postingCatalog/)){

        var postingCatalogTable1 = $('#postingCatalogTable1').DataTable( {

            "ajax": {
                "url":"postingCatalog_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'productCheck' },
            { data: 'postno' },
            { data: 'sdate' },
            { data: 'industry'},
            { data: 'category' },
            { data: 'brand' },
            { data: 'ptype' },
            { data: 'quantity'},
            { data: 'productName'},
            { data: 'timeframe'},
            { data: 'enableaction'}
         
            ]
        } );
        var postingCatalogTable2 = $('#postingCatalogTable2').DataTable( {

            "ajax": {
                "url":"postingCatalog_ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'postno' },
            { data: 'sdate' },
            { data: 'industry'},
            { data: 'category' },
            { data: 'brand' },
            { data: 'ptype' },
            { data: 'quantity'},
            { data: 'productName'},
            { data: 'timeframe'}
         
            ]
        } );    

        setInterval( function () {
            postingCatalogTable1.ajax.reload();
        }, 3000 );

        $('#postingCatalogTable1 tbody').on('click', 'tr td:not(:First-child,:last-child)', function() {
            var data = postingCatalogTable1.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
             //window.open(page, '_blank');
        });

        setInterval( function () {
            postingCatalogTable2.ajax.reload();
        }, 3000 );

        $('#postingCatalogTable2 tbody').on('click', 'tr td:not(:First-child,:last-child)', function() {
            var data = postingCatalogTable2.row(this).data();
            var page = $(this).parent().find('td a').attr('href');
            window.location.href = page;
             //window.open(page, '_blank');
        });    

    }else if( String(window.location).match(/posting/) ){

        $('#userposting').dataTable();

    }
    else if( String(window.location).match(/import-post/) ){

    }

    else if(String(window.location).match(/my-offers/)){

        var myOffersTable = $('#myOffersTable').DataTable( {

            "ajax": {
                "url":"my-offers-ajax",
                "dataSrc": "",
                "type": 'POST',
            },
            columns: [
            { data: 'postno' },
            { data: 'quotationno' },
            { data: 'offerdate' },
            { data: 'IndustryName' },
            { data: 'brandName'},
            { data: 'producttype' },
            { data: 'qtypercase' },
            { data: 'productName' },
            { data: 'offerPrice'},
            { data: 'offercurrency'},
            { data: 'offertimeframe'},
            { data: 'expdate'},
            { data: 'quationstatus'}
         
            ]
        } );

        setInterval( function () {
            myOffersTable.ajax.reload();
        }, 3000 );

    }
});