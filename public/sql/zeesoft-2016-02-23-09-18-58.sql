-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: minialpha
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mst_userlogin`
--

DROP TABLE IF EXISTS `mst_userlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_userlogin` (
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authorizedpass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `ftlogin` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `timezoneid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `secquiz1` int(11) DEFAULT NULL,
  `secquiz2` int(11) DEFAULT NULL,
  `secquiz3` int(11) DEFAULT NULL,
  `secans1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visualnoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `audionoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `industry` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_userlogin`
--

LOCK TABLES `mst_userlogin` WRITE;
/*!40000 ALTER TABLE `mst_userlogin` DISABLE KEYS */;
INSERT INTO `mst_userlogin` VALUES ('AM',6,'FFRR-2029','$2y$10$31djmJFM8Jl/96Bu43ydbO9BtyDdRr4m3BzQnHKG03d71QAijYVx6','$2y$10$gF/KDna6WOmgUXVShYnF2.G.rX8ld0qnxUnh.KC6lyLNjyftyJMwG','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','1','2','2','2016-02-18 10:06:28','hqRNpGDZF5UPZedZl7qAD0B3xgM8IAKHcAlPdgPvCYFVMIypB51MwEQebJcy','2016-02-18 15:06:28','2016-02-18 22:39:46'),('AM',1,'FMTB-5098','$2y$10$ydooDqsgUeQ7pffZbAEFz.dB1Xv0R84pAthn1V1zQ8jOuKGekyDEC','$2y$10$vDTeMSSt.RBYd0lLNLMTWe028K.pFrCVg9HDc1yQ0IsLrrjHChe.O','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','1','1','2016-02-18 10:04:10','QtgpKCF5nQhE7QIstUbH0T37rfYFDtQtEaHDH4yAaOBccfU5g2cqnMQ9prDF','2016-02-18 15:04:10','2016-02-18 21:50:32'),('TM',3,'IBOX-8877','$2y$10$r/tpXERlZqprX7VBORIBKuyipRGZXdjMlvTrhNcIVULsB99Ej5Sfe','$2y$10$PtaLbFyzDcEw55GOB/bYbeVUfG3sgt0LIlnqKgZcexHW.KV51QtvK','0','N','Atlantic/Azores',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','3','1','1','1','2016-02-18 12:36:28',NULL,'2016-02-18 17:36:28','2016-02-18 17:36:28'),('AD',4,'NTVZ-5516','$2y$10$9lj6cGN6Y7wjJCAKhersjuwVD4QqiUvUri64gLADevEc8ZQI7jMlO','$2y$10$eJpalnXnPVFUMOuPcl4u2OBEP1s.uCP4mtDqyuDK3/aiuUOUFoeuW','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','1','1','2016-02-18 10:05:50',NULL,'2016-02-18 15:05:50','2016-02-18 15:05:50'),('QF',2,'TONO-1588','$2y$10$NKQtR98UJJgasi4UexUb4eLyA2YtSWc1yoRcSFq2uLwVhF3tszMqC','$2y$10$NvBmkOs3IMuGRMGI09KJvu1WFCuByfXsbyj/NStU9E6jYTyG7KC9y','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','1','2','2','1','2016-02-18 10:04:44',NULL,'2016-02-18 15:04:44','2016-02-18 15:04:44'),('TM',3,'TQBU-0602','$2y$10$Lb.J2rfaunAa0m4gMjG5Z.1MyPJ8aSfJa0WRKJCDTsKKscuElW3Se','$2y$10$FNsOhdANnV34GIEOwiVjRemjJCbO2ZHloCcsW4.4Kleoi5y16Hs3C','1','N','Asia/Kolkata',NULL,NULL,NULL,NULL,NULL,NULL,'1','1','4','2','2','1','2016-02-18 10:05:17',NULL,'2016-02-18 15:05:17','2016-02-18 15:05:17'),('AD',5,'UUZZ-9984','$2y$10$il5jzmicQ1/Wi0/DoToRs.znWiBLifDSVldsPs8DyvNYaKOLFrUVa','$2y$10$UOJyvHIxIg9IlhvjER38VeUcMy04Sm4AZdwBHrcBcbk1s2fIQRO9m','1','Y','Asia/Kolkata',6,8,3,'father','mother','animal','1','1',NULL,NULL,NULL,NULL,'2016-02-17 16:01:00','l6agObfuGu7yWRGT24zLWW7WVACcONyeVkrMhHd1Y8eSCS141LzECgQG0VAJ','2016-02-17 21:01:00','2016-02-19 21:43:52');
/*!40000 ALTER TABLE `mst_userlogin` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-23  3:00:32
