-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2016 at 11:03 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zeesoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `countdata`
--

CREATE TABLE `countdata` (
  `id` int(11) NOT NULL,
  `recordcount` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `count_notification`
--

CREATE TABLE `count_notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `userid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tmsg` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `maskproname`
--

CREATE TABLE `maskproname` (
  `id` int(10) UNSIGNED NOT NULL,
  `maskstatus` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mastpage`
--

CREATE TABLE `mastpage` (
  `PageId` int(10) UNSIGNED NOT NULL,
  `PageName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DisplayName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Parent_Id` int(11) NOT NULL,
  `Level_Idx` int(11) NOT NULL,
  `Idx` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mastrolepermission`
--

CREATE TABLE `mastrolepermission` (
  `RoleId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `ViewP` int(11) NOT NULL,
  `AddP` int(11) NOT NULL,
  `EditP` int(11) NOT NULL,
  `DeleteP` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_permission`
--

CREATE TABLE `menu_permission` (
  `rowId` int(11) NOT NULL,
  `pageView` int(11) NOT NULL,
  `pageAdd` int(11) NOT NULL,
  `pageEdit` int(11) NOT NULL,
  `pageDelete` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_09_090421_countdata', 1),
('2016_02_09_090448_count_nitification', 1),
('2016_02_09_092454_maskproname', 1),
('2016_02_09_092506_MastPage', 1),
('2016_02_09_092525_MastRolePermission', 1),
('2016_02_09_092544_menu_permission', 1),
('2016_02_09_092618_mst_admin_report', 1),
('2016_02_09_092641_mst_brand', 1),
('2016_02_09_092652_mst_category', 1),
('2016_02_09_092700_mst_country', 1),
('2016_02_09_092726_mst_currency', 1),
('2016_02_09_092746_mst_exp_daterange', 1),
('2016_02_09_092807_mst_industry', 1),
('2016_02_09_093639_mst_language', 1),
('2016_02_09_093701_mst_location', 1),
('2016_02_09_093731_mst_mngnotification', 1),
('2016_02_09_093741_mst_model', 1),
('2016_02_09_093749_mst_notification', 1),
('2016_02_09_093807_mst_pageName', 1),
('2016_02_09_093821_mst_product', 1),
('2016_02_09_093833_mst_productcode', 1),
('2016_02_09_115416_mst_productimages', 1),
('2016_02_09_115442_mst_productprefix', 1),
('2016_02_09_115451_mst_productreport', 1),
('2016_02_09_115505_mst_productsimilar', 1),
('2016_02_09_115517_mst_report', 1),
('2016_02_09_115601_mst_security', 1),
('2016_02_09_115625_mst_shipping', 1),
('2016_02_09_115651_mst_subcategory', 1),
('2016_02_09_115708_mst_timeframe', 1),
('2016_02_09_115738_mst_timezone', 1),
('2016_02_09_115747_mst_uom', 1),
('2016_02_09_115800_mst_useraction', 1),
('2016_02_09_115808_mst_userlogin', 1),
('2016_02_09_115817_mst_userlogon', 1),
('2016_02_09_115826_mst_userrole', 1),
('2016_02_09_115907_old_useractivitypurge', 1),
('2016_02_09_115948_page_master', 1),
('2016_02_09_115958_page_notification', 1),
('2016_02_09_120017_post_advertisment', 1),
('2016_02_09_120037_post_customer', 1),
('2016_02_09_144025_post_quotation', 1),
('2016_02_09_144047_productimport', 1),
('2016_02_09_144115_quotation_counter', 1),
('2016_02_09_144152_test', 1),
('2016_02_09_144157_test1', 1),
('2016_02_09_144213_tm_accepted_offers', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_admin_report`
--

CREATE TABLE `mst_admin_report` (
  `reportid` int(10) UNSIGNED NOT NULL,
  `reportname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reportsql` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_brand`
--

CREATE TABLE `mst_brand` (
  `brandid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `industryid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_brand`
--

INSERT INTO `mst_brand` (`brandid`, `name`, `isactive`, `industryid`, `created_at`, `updated_at`) VALUES
(1, 'brand 1', '1', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_category`
--

CREATE TABLE `mst_category` (
  `catid` int(10) UNSIGNED NOT NULL,
  `industryid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_category`
--

INSERT INTO `mst_category` (`catid`, `industryid`, `name`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 2, 'test 11', '1', NULL, NULL),
(2, 2, 'test 12', '1', NULL, NULL),
(3, 5, 'test 41', '1', NULL, NULL),
(4, 5, 'test 42', '1', NULL, NULL),
(5, 3, 'test 31', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_country`
--

CREATE TABLE `mst_country` (
  `countryid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_currency`
--

CREATE TABLE `mst_currency` (
  `currencyid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_exp_daterange`
--

CREATE TABLE `mst_exp_daterange` (
  `exprangeid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_industry`
--

CREATE TABLE `mst_industry` (
  `industryid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_industry`
--

INSERT INTO `mst_industry` (`industryid`, `name`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 'test', '1', NULL, NULL),
(2, 'test 1', '1', NULL, NULL),
(3, 'test 2', '1', NULL, NULL),
(4, 'test 3', '1', NULL, NULL),
(5, 'test 4', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_language`
--

CREATE TABLE `mst_language` (
  `languageid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_location`
--

CREATE TABLE `mst_location` (
  `locationid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_mngnotification`
--

CREATE TABLE `mst_mngnotification` (
  `id` int(10) UNSIGNED NOT NULL,
  `pagename` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_model`
--

CREATE TABLE `mst_model` (
  `modelid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_notification`
--

CREATE TABLE `mst_notification` (
  `notiid` int(10) UNSIGNED NOT NULL,
  `pro_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `msgdate` datetime NOT NULL,
  `msg` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fromuserid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `preportid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_pagename`
--

CREATE TABLE `mst_pagename` (
  `rowid` int(10) UNSIGNED NOT NULL,
  `pageName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pageCheckAdd` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckEdit` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckDelete` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckPrint` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckExport` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_product`
--

CREATE TABLE `mst_product` (
  `productid` int(10) UNSIGNED NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `brandid` int(11) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `subcatid` int(11) DEFAULT NULL,
  `pakaging` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipingcondition` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qtypercase` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `manufacture` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpm` int(11) DEFAULT NULL,
  `addedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `roletype` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img0` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img3` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img4` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img5` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img6` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img7` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code1` int(11) NOT NULL,
  `codevalue1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code2` int(11) NOT NULL,
  `codevalue2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code3` int(11) DEFAULT NULL,
  `codevalue3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productcode`
--

CREATE TABLE `mst_productcode` (
  `productcodeid` int(10) UNSIGNED NOT NULL,
  `pid` int(11) NOT NULL,
  `prefixid` int(11) NOT NULL,
  `refbyid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `approveid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `productcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `refdate` datetime NOT NULL,
  `approvedate` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productimages`
--

CREATE TABLE `mst_productimages` (
  `productimageid` int(10) UNSIGNED NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `imageurl` text COLLATE utf8_unicode_ci NOT NULL,
  `srno` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productprefix`
--

CREATE TABLE `mst_productprefix` (
  `proprefixid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productreport`
--

CREATE TABLE `mst_productreport` (
  `preportid` int(10) UNSIGNED NOT NULL,
  `reportid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `reviewbyid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `reviewdate` date NOT NULL,
  `reviewremark` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `reviewstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approvedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approveddate` date NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productsimilar`
--

CREATE TABLE `mst_productsimilar` (
  `similarid` int(10) UNSIGNED NOT NULL,
  `pid` int(11) NOT NULL,
  `spid` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_report`
--

CREATE TABLE `mst_report` (
  `reportid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_security`
--

CREATE TABLE `mst_security` (
  `securityid` int(10) UNSIGNED NOT NULL,
  `qtype` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_shipping`
--

CREATE TABLE `mst_shipping` (
  `shipid` int(10) UNSIGNED NOT NULL,
  `productid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_subcategory`
--

CREATE TABLE `mst_subcategory` (
  `subcatid` int(10) UNSIGNED NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_subcategory`
--

INSERT INTO `mst_subcategory` (`subcatid`, `catid`, `name`, `industryid`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 4, 'test 42 sub', 5, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_timeframe`
--

CREATE TABLE `mst_timeframe` (
  `timeframeid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_timezone`
--

CREATE TABLE `mst_timezone` (
  `timezoneid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `timezonevalue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_timezone`
--

INSERT INTO `mst_timezone` (`timezoneid`, `name`, `isactive`, `timezonevalue`, `created_at`, `updated_at`) VALUES
(1, 'Africa/Abidjan', '1', 'UTC/GMT +00:00', NULL, NULL),
(2, 'Africa/Accra', '1', 'UTC/GMT +00:00', NULL, NULL),
(3, 'Africa/Addis_Ababa', '1', 'UTC/GMT +03:00', NULL, NULL),
(4, 'Africa/Algiers', '1', 'UTC/GMT +01:00', NULL, NULL),
(5, 'Africa/Asmara', '1', 'UTC/GMT +03:00', NULL, NULL),
(6, 'Africa/Bamako', '1', 'UTC/GMT +00:00', NULL, NULL),
(7, 'Africa/Bangui', '1', 'UTC/GMT +01:00', NULL, NULL),
(8, 'Africa/Banjul', '1', 'UTC/GMT +00:00', NULL, NULL),
(9, 'Africa/Bissau', '1', 'UTC/GMT +00:00', NULL, NULL),
(10, 'Africa/Blantyre', '1', 'UTC/GMT +02:00', NULL, NULL),
(11, 'Africa/Brazzaville', '1', 'UTC/GMT +01:00', NULL, NULL),
(12, 'Africa/Bujumbura', '1', 'UTC/GMT +02:00', NULL, NULL),
(13, 'Africa/Cairo', '1', 'UTC/GMT +02:00', NULL, NULL),
(14, 'Africa/Casablanca', '1', 'UTC/GMT +00:00', NULL, NULL),
(15, 'Africa/Ceuta', '1', 'UTC/GMT +01:00', NULL, NULL),
(16, 'Africa/Conakry', '1', 'UTC/GMT +00:00', NULL, NULL),
(17, 'Africa/Dakar', '1', 'UTC/GMT +00:00', NULL, NULL),
(18, 'Africa/Dar_es_Salaam', '1', 'UTC/GMT +03:00', NULL, NULL),
(19, 'Africa/Djibouti', '1', 'UTC/GMT +03:00', NULL, NULL),
(20, 'Africa/Douala', '1', 'UTC/GMT +01:00', NULL, NULL),
(21, 'Africa/El_Aaiun', '1', 'UTC/GMT +00:00', NULL, NULL),
(22, 'Africa/Freetown', '1', 'UTC/GMT +00:00', NULL, NULL),
(23, 'Africa/Gaborone', '1', 'UTC/GMT +02:00', NULL, NULL),
(24, 'Africa/Harare', '1', 'UTC/GMT +02:00', NULL, NULL),
(25, 'Africa/Johannesburg', '1', 'UTC/GMT +02:00', NULL, NULL),
(26, 'Africa/Juba', '1', 'UTC/GMT +03:00', NULL, NULL),
(27, 'Africa/Kampala', '1', 'UTC/GMT +03:00', NULL, NULL),
(28, 'Africa/Khartoum', '1', 'UTC/GMT +03:00', NULL, NULL),
(29, 'Africa/Kigali', '1', 'UTC/GMT +02:00', NULL, NULL),
(30, 'Africa/Kinshasa', '1', 'UTC/GMT +01:00', NULL, NULL),
(31, 'Africa/Lagos', '1', 'UTC/GMT +01:00', NULL, NULL),
(32, 'Africa/Libreville', '1', 'UTC/GMT +01:00', NULL, NULL),
(33, 'Africa/Lome', '1', 'UTC/GMT +00:00', NULL, NULL),
(34, 'Africa/Luanda', '1', 'UTC/GMT +01:00', NULL, NULL),
(35, 'Africa/Lubumbashi', '1', 'UTC/GMT +02:00', NULL, NULL),
(36, 'Africa/Lusaka', '1', 'UTC/GMT +02:00', NULL, NULL),
(37, 'Africa/Malabo', '1', 'UTC/GMT +01:00', NULL, NULL),
(38, 'Africa/Maputo', '1', 'UTC/GMT +02:00', NULL, NULL),
(39, 'Africa/Maseru', '1', 'UTC/GMT +02:00', NULL, NULL),
(40, 'Africa/Mbabane', '1', 'UTC/GMT +02:00', NULL, NULL),
(41, 'Africa/Mogadishu', '1', 'UTC/GMT +03:00', NULL, NULL),
(42, 'Africa/Monrovia', '1', 'UTC/GMT +00:00', NULL, NULL),
(43, 'Africa/Nairobi', '1', 'UTC/GMT +03:00', NULL, NULL),
(44, 'Africa/Ndjamena', '1', 'UTC/GMT +01:00', NULL, NULL),
(45, 'Africa/Niamey', '1', 'UTC/GMT +01:00', NULL, NULL),
(46, 'Africa/Nouakchott', '1', 'UTC/GMT +00:00', NULL, NULL),
(47, 'Africa/Ouagadougou', '1', 'UTC/GMT +00:00', NULL, NULL),
(48, 'Africa/Porto-Novo', '1', 'UTC/GMT +01:00', NULL, NULL),
(49, 'Africa/Sao_Tome', '1', 'UTC/GMT +00:00', NULL, NULL),
(50, 'Africa/Tripoli', '1', 'UTC/GMT +02:00', NULL, NULL),
(51, 'Africa/Tunis', '1', 'UTC/GMT +01:00', NULL, NULL),
(52, 'Africa/Windhoek', '1', 'UTC/GMT +02:00', NULL, NULL),
(53, 'America/Adak', '1', 'UTC/GMT -10:00', NULL, NULL),
(54, 'America/Anchorage', '1', 'UTC/GMT -09:00', NULL, NULL),
(55, 'America/Anguilla', '1', 'UTC/GMT -04:00', NULL, NULL),
(56, 'America/Antigua', '1', 'UTC/GMT -04:00', NULL, NULL),
(57, 'America/Araguaina', '1', 'UTC/GMT -03:00', NULL, NULL),
(58, 'America/Argentina/Buenos_Aires', '1', 'UTC/GMT -03:00', NULL, NULL),
(59, 'America/Argentina/Catamarca', '1', 'UTC/GMT -03:00', NULL, NULL),
(60, 'America/Argentina/Cordoba', '1', 'UTC/GMT -03:00', NULL, NULL),
(61, 'America/Argentina/Jujuy', '1', 'UTC/GMT -03:00', NULL, NULL),
(62, 'America/Argentina/La_Rioja', '1', 'UTC/GMT -03:00', NULL, NULL),
(63, 'America/Argentina/Mendoza', '1', 'UTC/GMT -03:00', NULL, NULL),
(64, 'America/Argentina/Rio_Gallegos', '1', 'UTC/GMT -03:00', NULL, NULL),
(65, 'America/Argentina/Salta', '1', 'UTC/GMT -03:00', NULL, NULL),
(66, 'America/Argentina/San_Juan', '1', 'UTC/GMT -03:00', NULL, NULL),
(67, 'America/Argentina/San_Luis', '1', 'UTC/GMT -03:00', NULL, NULL),
(68, 'America/Argentina/Tucuman', '1', 'UTC/GMT -03:00', NULL, NULL),
(69, 'America/Argentina/Ushuaia', '1', 'UTC/GMT -03:00', NULL, NULL),
(70, 'America/Aruba', '1', 'UTC/GMT -04:00', NULL, NULL),
(71, 'America/Asuncion', '1', 'UTC/GMT -03:00', NULL, NULL),
(72, 'America/Atikokan', '1', 'UTC/GMT -05:00', NULL, NULL),
(73, 'America/Bahia', '1', 'UTC/GMT -03:00', NULL, NULL),
(74, 'America/Bahia_Banderas', '1', 'UTC/GMT -06:00', NULL, NULL),
(75, 'America/Barbados', '1', 'UTC/GMT -04:00', NULL, NULL),
(76, 'America/Belem', '1', 'UTC/GMT -03:00', NULL, NULL),
(77, 'America/Belize', '1', 'UTC/GMT -06:00', NULL, NULL),
(78, 'America/Blanc-Sablon', '1', 'UTC/GMT -04:00', NULL, NULL),
(79, 'America/Boa_Vista', '1', 'UTC/GMT -04:00', NULL, NULL),
(80, 'America/Bogota', '1', 'UTC/GMT -05:00', NULL, NULL),
(81, 'America/Boise', '1', 'UTC/GMT -07:00', NULL, NULL),
(82, 'America/Cambridge_Bay', '1', 'UTC/GMT -07:00', NULL, NULL),
(83, 'America/Campo_Grande', '1', 'UTC/GMT -03:00', NULL, NULL),
(84, 'America/Cancun', '1', 'UTC/GMT -05:00', NULL, NULL),
(85, 'America/Caracas', '1', 'UTC/GMT -04:30', NULL, NULL),
(86, 'America/Cayenne', '1', 'UTC/GMT -03:00', NULL, NULL),
(87, 'America/Cayman', '1', 'UTC/GMT -05:00', NULL, NULL),
(88, 'America/Chicago', '1', 'UTC/GMT -06:00', NULL, NULL),
(89, 'America/Chihuahua', '1', 'UTC/GMT -07:00', NULL, NULL),
(90, 'America/Costa_Rica', '1', 'UTC/GMT -06:00', NULL, NULL),
(91, 'America/Creston', '1', 'UTC/GMT -07:00', NULL, NULL),
(92, 'America/Cuiaba', '1', 'UTC/GMT -03:00', NULL, NULL),
(93, 'America/Curacao', '1', 'UTC/GMT -04:00', NULL, NULL),
(94, 'America/Danmarkshavn', '1', 'UTC/GMT +00:00', NULL, NULL),
(95, 'America/Dawson', '1', 'UTC/GMT -08:00', NULL, NULL),
(96, 'America/Dawson_Creek', '1', 'UTC/GMT -07:00', NULL, NULL),
(97, 'America/Denver', '1', 'UTC/GMT -07:00', NULL, NULL),
(98, 'America/Detroit', '1', 'UTC/GMT -05:00', NULL, NULL),
(99, 'America/Dominica', '1', 'UTC/GMT -04:00', NULL, NULL),
(100, 'America/Edmonton', '1', 'UTC/GMT -07:00', NULL, NULL),
(101, 'America/Eirunepe', '1', 'UTC/GMT -05:00', NULL, NULL),
(102, 'America/El_Salvador', '1', 'UTC/GMT -06:00', NULL, NULL),
(103, 'America/Fort_Nelson', '1', 'UTC/GMT -07:00', NULL, NULL),
(104, 'America/Fortaleza', '1', 'UTC/GMT -03:00', NULL, NULL),
(105, 'America/Glace_Bay', '1', 'UTC/GMT -04:00', NULL, NULL),
(106, 'America/Godthab', '1', 'UTC/GMT -03:00', NULL, NULL),
(107, 'America/Goose_Bay', '1', 'UTC/GMT -04:00', NULL, NULL),
(108, 'America/Grand_Turk', '1', 'UTC/GMT -04:00', NULL, NULL),
(109, 'America/Grenada', '1', 'UTC/GMT -04:00', NULL, NULL),
(110, 'America/Guadeloupe', '1', 'UTC/GMT -04:00', NULL, NULL),
(111, 'America/Guatemala', '1', 'UTC/GMT -06:00', NULL, NULL),
(112, 'America/Guayaquil', '1', 'UTC/GMT -05:00', NULL, NULL),
(113, 'America/Guyana', '1', 'UTC/GMT -04:00', NULL, NULL),
(114, 'America/Halifax', '1', 'UTC/GMT -04:00', NULL, NULL),
(115, 'America/Havana', '1', 'UTC/GMT -05:00', NULL, NULL),
(116, 'America/Hermosillo', '1', 'UTC/GMT -07:00', NULL, NULL),
(117, 'America/Indiana/Indianapolis', '1', 'UTC/GMT -05:00', NULL, NULL),
(118, 'America/Indiana/Knox', '1', 'UTC/GMT -06:00', NULL, NULL),
(119, 'America/Indiana/Marengo', '1', 'UTC/GMT -05:00', NULL, NULL),
(120, 'America/Indiana/Petersburg', '1', 'UTC/GMT -05:00', NULL, NULL),
(121, 'America/Indiana/Tell_City', '1', 'UTC/GMT -06:00', NULL, NULL),
(122, 'America/Indiana/Vevay', '1', 'UTC/GMT -05:00', NULL, NULL),
(123, 'America/Indiana/Vincennes', '1', 'UTC/GMT -05:00', NULL, NULL),
(124, 'America/Indiana/Winamac', '1', 'UTC/GMT -05:00', NULL, NULL),
(125, 'America/Inuvik', '1', 'UTC/GMT -07:00', NULL, NULL),
(126, 'America/Iqaluit', '1', 'UTC/GMT -05:00', NULL, NULL),
(127, 'America/Jamaica', '1', 'UTC/GMT -05:00', NULL, NULL),
(128, 'America/Juneau', '1', 'UTC/GMT -09:00', NULL, NULL),
(129, 'America/Kentucky/Louisville', '1', 'UTC/GMT -05:00', NULL, NULL),
(130, 'America/Kentucky/Monticello', '1', 'UTC/GMT -05:00', NULL, NULL),
(131, 'America/Kralendijk', '1', 'UTC/GMT -04:00', NULL, NULL),
(132, 'America/La_Paz', '1', 'UTC/GMT -04:00', NULL, NULL),
(133, 'America/Lima', '1', 'UTC/GMT -05:00', NULL, NULL),
(134, 'America/Los_Angeles', '1', 'UTC/GMT -08:00', NULL, NULL),
(135, 'America/Lower_Princes', '1', 'UTC/GMT -04:00', NULL, NULL),
(136, 'America/Maceio', '1', 'UTC/GMT -03:00', NULL, NULL),
(137, 'America/Managua', '1', 'UTC/GMT -06:00', NULL, NULL),
(138, 'America/Manaus', '1', 'UTC/GMT -04:00', NULL, NULL),
(139, 'America/Marigot', '1', 'UTC/GMT -04:00', NULL, NULL),
(140, 'America/Martinique', '1', 'UTC/GMT -04:00', NULL, NULL),
(141, 'America/Matamoros', '1', 'UTC/GMT -06:00', NULL, NULL),
(142, 'America/Mazatlan', '1', 'UTC/GMT -07:00', NULL, NULL),
(143, 'America/Menominee', '1', 'UTC/GMT -06:00', NULL, NULL),
(144, 'America/Merida', '1', 'UTC/GMT -06:00', NULL, NULL),
(145, 'America/Metlakatla', '1', 'UTC/GMT -08:00', NULL, NULL),
(146, 'America/Mexico_City', '1', 'UTC/GMT -06:00', NULL, NULL),
(147, 'America/Miquelon', '1', 'UTC/GMT -03:00', NULL, NULL),
(148, 'America/Moncton', '1', 'UTC/GMT -04:00', NULL, NULL),
(149, 'America/Monterrey', '1', 'UTC/GMT -06:00', NULL, NULL),
(150, 'America/Montevideo', '1', 'UTC/GMT -03:00', NULL, NULL),
(151, 'America/Montserrat', '1', 'UTC/GMT -04:00', NULL, NULL),
(152, 'America/Nassau', '1', 'UTC/GMT -05:00', NULL, NULL),
(153, 'America/New_York', '1', 'UTC/GMT -05:00', NULL, NULL),
(154, 'America/Nipigon', '1', 'UTC/GMT -05:00', NULL, NULL),
(155, 'America/Nome', '1', 'UTC/GMT -09:00', NULL, NULL),
(156, 'America/Noronha', '1', 'UTC/GMT -02:00', NULL, NULL),
(157, 'America/North_Dakota/Beulah', '1', 'UTC/GMT -06:00', NULL, NULL),
(158, 'America/North_Dakota/Center', '1', 'UTC/GMT -06:00', NULL, NULL),
(159, 'America/North_Dakota/New_Salem', '1', 'UTC/GMT -06:00', NULL, NULL),
(160, 'America/Ojinaga', '1', 'UTC/GMT -07:00', NULL, NULL),
(161, 'America/Panama', '1', 'UTC/GMT -05:00', NULL, NULL),
(162, 'America/Pangnirtung', '1', 'UTC/GMT -05:00', NULL, NULL),
(163, 'America/Paramaribo', '1', 'UTC/GMT -03:00', NULL, NULL),
(164, 'America/Phoenix', '1', 'UTC/GMT -07:00', NULL, NULL),
(165, 'America/Port-au-Prince', '1', 'UTC/GMT -05:00', NULL, NULL),
(166, 'America/Port_of_Spain', '1', 'UTC/GMT -04:00', NULL, NULL),
(167, 'America/Porto_Velho', '1', 'UTC/GMT -04:00', NULL, NULL),
(168, 'America/Puerto_Rico', '1', 'UTC/GMT -04:00', NULL, NULL),
(169, 'America/Rainy_River', '1', 'UTC/GMT -06:00', NULL, NULL),
(170, 'America/Rankin_Inlet', '1', 'UTC/GMT -06:00', NULL, NULL),
(171, 'America/Recife', '1', 'UTC/GMT -03:00', NULL, NULL),
(172, 'America/Regina', '1', 'UTC/GMT -06:00', NULL, NULL),
(173, 'America/Resolute', '1', 'UTC/GMT -06:00', NULL, NULL),
(174, 'America/Rio_Branco', '1', 'UTC/GMT -05:00', NULL, NULL),
(175, 'America/Santa_Isabel', '1', 'UTC/GMT -08:00', NULL, NULL),
(176, 'America/Santarem', '1', 'UTC/GMT -03:00', NULL, NULL),
(177, 'America/Santiago', '1', 'UTC/GMT -03:00', NULL, NULL),
(178, 'America/Santo_Domingo', '1', 'UTC/GMT -04:00', NULL, NULL),
(179, 'America/Sao_Paulo', '1', 'UTC/GMT -02:00', NULL, NULL),
(180, 'America/Scoresbysund', '1', 'UTC/GMT -01:00', NULL, NULL),
(181, 'America/Sitka', '1', 'UTC/GMT -09:00', NULL, NULL),
(182, 'America/St_Barthelemy', '1', 'UTC/GMT -04:00', NULL, NULL),
(183, 'America/St_Johns', '1', 'UTC/GMT -03:30', NULL, NULL),
(184, 'America/St_Kitts', '1', 'UTC/GMT -04:00', NULL, NULL),
(185, 'America/St_Lucia', '1', 'UTC/GMT -04:00', NULL, NULL),
(186, 'America/St_Thomas', '1', 'UTC/GMT -04:00', NULL, NULL),
(187, 'America/St_Vincent', '1', 'UTC/GMT -04:00', NULL, NULL),
(188, 'America/Swift_Current', '1', 'UTC/GMT -06:00', NULL, NULL),
(189, 'America/Tegucigalpa', '1', 'UTC/GMT -06:00', NULL, NULL),
(190, 'America/Thule', '1', 'UTC/GMT -04:00', NULL, NULL),
(191, 'America/Thunder_Bay', '1', 'UTC/GMT -05:00', NULL, NULL),
(192, 'America/Tijuana', '1', 'UTC/GMT -08:00', NULL, NULL),
(193, 'America/Toronto', '1', 'UTC/GMT -05:00', NULL, NULL),
(194, 'America/Tortola', '1', 'UTC/GMT -04:00', NULL, NULL),
(195, 'America/Vancouver', '1', 'UTC/GMT -08:00', NULL, NULL),
(196, 'America/Whitehorse', '1', 'UTC/GMT -08:00', NULL, NULL),
(197, 'America/Winnipeg', '1', 'UTC/GMT -06:00', NULL, NULL),
(198, 'America/Yakutat', '1', 'UTC/GMT -09:00', NULL, NULL),
(199, 'America/Yellowknife', '1', 'UTC/GMT -07:00', NULL, NULL),
(200, 'Antarctica/Casey', '1', 'UTC/GMT +08:00', NULL, NULL),
(201, 'Antarctica/Davis', '1', 'UTC/GMT +07:00', NULL, NULL),
(202, 'Antarctica/DumontDUrville', '1', 'UTC/GMT +10:00', NULL, NULL),
(203, 'Antarctica/Macquarie', '1', 'UTC/GMT +11:00', NULL, NULL),
(204, 'Antarctica/Mawson', '1', 'UTC/GMT +05:00', NULL, NULL),
(205, 'Antarctica/McMurdo', '1', 'UTC/GMT +13:00', NULL, NULL),
(206, 'Antarctica/Palmer', '1', 'UTC/GMT -03:00', NULL, NULL),
(207, 'Antarctica/Rothera', '1', 'UTC/GMT -03:00', NULL, NULL),
(208, 'Antarctica/Syowa', '1', 'UTC/GMT +03:00', NULL, NULL),
(209, 'Antarctica/Troll', '1', 'UTC/GMT +00:00', NULL, NULL),
(210, 'Antarctica/Vostok', '1', 'UTC/GMT +06:00', NULL, NULL),
(211, 'Arctic/Longyearbyen', '1', 'UTC/GMT +01:00', NULL, NULL),
(212, 'Asia/Aden', '1', 'UTC/GMT +03:00', NULL, NULL),
(213, 'Asia/Almaty', '1', 'UTC/GMT +06:00', NULL, NULL),
(214, 'Asia/Amman', '1', 'UTC/GMT +02:00', NULL, NULL),
(215, 'Asia/Anadyr', '1', 'UTC/GMT +12:00', NULL, NULL),
(216, 'Asia/Aqtau', '1', 'UTC/GMT +05:00', NULL, NULL),
(217, 'Asia/Aqtobe', '1', 'UTC/GMT +05:00', NULL, NULL),
(218, 'Asia/Ashgabat', '1', 'UTC/GMT +05:00', NULL, NULL),
(219, 'Asia/Baghdad', '1', 'UTC/GMT +03:00', NULL, NULL),
(220, 'Asia/Bahrain', '1', 'UTC/GMT +03:00', NULL, NULL),
(221, 'Asia/Baku', '1', 'UTC/GMT +04:00', NULL, NULL),
(222, 'Asia/Bangkok', '1', 'UTC/GMT +07:00', NULL, NULL),
(223, 'Asia/Beirut', '1', 'UTC/GMT +02:00', NULL, NULL),
(224, 'Asia/Bishkek', '1', 'UTC/GMT +06:00', NULL, NULL),
(225, 'Asia/Brunei', '1', 'UTC/GMT +08:00', NULL, NULL),
(226, 'Asia/Chita', '1', 'UTC/GMT +08:00', NULL, NULL),
(227, 'Asia/Choibalsan', '1', 'UTC/GMT +08:00', NULL, NULL),
(228, 'Asia/Colombo', '1', 'UTC/GMT +05:30', NULL, NULL),
(229, 'Asia/Damascus', '1', 'UTC/GMT +02:00', NULL, NULL),
(230, 'Asia/Dhaka', '1', 'UTC/GMT +06:00', NULL, NULL),
(231, 'Asia/Dili', '1', 'UTC/GMT +09:00', NULL, NULL),
(232, 'Asia/Dubai', '1', 'UTC/GMT +04:00', NULL, NULL),
(233, 'Asia/Dushanbe', '1', 'UTC/GMT +05:00', NULL, NULL),
(234, 'Asia/Gaza', '1', 'UTC/GMT +02:00', NULL, NULL),
(235, 'Asia/Hebron', '1', 'UTC/GMT +02:00', NULL, NULL),
(236, 'Asia/Ho_Chi_Minh', '1', 'UTC/GMT +07:00', NULL, NULL),
(237, 'Asia/Hong_Kong', '1', 'UTC/GMT +08:00', NULL, NULL),
(238, 'Asia/Hovd', '1', 'UTC/GMT +07:00', NULL, NULL),
(239, 'Asia/Irkutsk', '1', 'UTC/GMT +08:00', NULL, NULL),
(240, 'Asia/Jakarta', '1', 'UTC/GMT +07:00', NULL, NULL),
(241, 'Asia/Jayapura', '1', 'UTC/GMT +09:00', NULL, NULL),
(242, 'Asia/Jerusalem', '1', 'UTC/GMT +02:00', NULL, NULL),
(243, 'Asia/Kabul', '1', 'UTC/GMT +04:30', NULL, NULL),
(244, 'Asia/Kamchatka', '1', 'UTC/GMT +12:00', NULL, NULL),
(245, 'Asia/Karachi', '1', 'UTC/GMT +05:00', NULL, NULL),
(246, 'Asia/Kathmandu', '1', 'UTC/GMT +05:45', NULL, NULL),
(247, 'Asia/Khandyga', '1', 'UTC/GMT +09:00', NULL, NULL),
(248, 'Asia/Kolkata', '1', 'UTC/GMT +05:30', NULL, NULL),
(249, 'Asia/Krasnoyarsk', '1', 'UTC/GMT +07:00', NULL, NULL),
(250, 'Asia/Kuala_Lumpur', '1', 'UTC/GMT +08:00', NULL, NULL),
(251, 'Asia/Kuching', '1', 'UTC/GMT +08:00', NULL, NULL),
(252, 'Asia/Kuwait', '1', 'UTC/GMT +03:00', NULL, NULL),
(253, 'Asia/Macau', '1', 'UTC/GMT +08:00', NULL, NULL),
(254, 'Asia/Magadan', '1', 'UTC/GMT +10:00', NULL, NULL),
(255, 'Asia/Makassar', '1', 'UTC/GMT +08:00', NULL, NULL),
(256, 'Asia/Manila', '1', 'UTC/GMT +08:00', NULL, NULL),
(257, 'Asia/Muscat', '1', 'UTC/GMT +04:00', NULL, NULL),
(258, 'Asia/Nicosia', '1', 'UTC/GMT +02:00', NULL, NULL),
(259, 'Asia/Novokuznetsk', '1', 'UTC/GMT +07:00', NULL, NULL),
(260, 'Asia/Novosibirsk', '1', 'UTC/GMT +06:00', NULL, NULL),
(261, 'Asia/Omsk', '1', 'UTC/GMT +06:00', NULL, NULL),
(262, 'Asia/Oral', '1', 'UTC/GMT +05:00', NULL, NULL),
(263, 'Asia/Phnom_Penh', '1', 'UTC/GMT +07:00', NULL, NULL),
(264, 'Asia/Pontianak', '1', 'UTC/GMT +07:00', NULL, NULL),
(265, 'Asia/Pyongyang', '1', 'UTC/GMT +08:30', NULL, NULL),
(266, 'Asia/Qatar', '1', 'UTC/GMT +03:00', NULL, NULL),
(267, 'Asia/Qyzylorda', '1', 'UTC/GMT +06:00', NULL, NULL),
(268, 'Asia/Rangoon', '1', 'UTC/GMT +06:30', NULL, NULL),
(269, 'Asia/Riyadh', '1', 'UTC/GMT +03:00', NULL, NULL),
(270, 'Asia/Sakhalin', '1', 'UTC/GMT +10:00', NULL, NULL),
(271, 'Asia/Samarkand', '1', 'UTC/GMT +05:00', NULL, NULL),
(272, 'Asia/Seoul', '1', 'UTC/GMT +09:00', NULL, NULL),
(273, 'Asia/Shanghai', '1', 'UTC/GMT +08:00', NULL, NULL),
(274, 'Asia/Singapore', '1', 'UTC/GMT +08:00', NULL, NULL),
(275, 'Asia/Srednekolymsk', '1', 'UTC/GMT +11:00', NULL, NULL),
(276, 'Asia/Taipei', '1', 'UTC/GMT +08:00', NULL, NULL),
(277, 'Asia/Tashkent', '1', 'UTC/GMT +05:00', NULL, NULL),
(278, 'Asia/Tbilisi', '1', 'UTC/GMT +04:00', NULL, NULL),
(279, 'Asia/Tehran', '1', 'UTC/GMT +03:30', NULL, NULL),
(280, 'Asia/Thimphu', '1', 'UTC/GMT +06:00', NULL, NULL),
(281, 'Asia/Tokyo', '1', 'UTC/GMT +09:00', NULL, NULL),
(282, 'Asia/Ulaanbaatar', '1', 'UTC/GMT +08:00', NULL, NULL),
(283, 'Asia/Urumqi', '1', 'UTC/GMT +06:00', NULL, NULL),
(284, 'Asia/Ust-Nera', '1', 'UTC/GMT +10:00', NULL, NULL),
(285, 'Asia/Vientiane', '1', 'UTC/GMT +07:00', NULL, NULL),
(286, 'Asia/Vladivostok', '1', 'UTC/GMT +10:00', NULL, NULL),
(287, 'Asia/Yakutsk', '1', 'UTC/GMT +09:00', NULL, NULL),
(288, 'Asia/Yekaterinburg', '1', 'UTC/GMT +05:00', NULL, NULL),
(289, 'Asia/Yerevan', '1', 'UTC/GMT +04:00', NULL, NULL),
(290, 'Atlantic/Azores', '1', 'UTC/GMT -01:00', NULL, NULL),
(291, 'Atlantic/Bermuda', '1', 'UTC/GMT -04:00', NULL, NULL),
(292, 'Atlantic/Canary', '1', 'UTC/GMT +00:00', NULL, NULL),
(293, 'Atlantic/Cape_Verde', '1', 'UTC/GMT -01:00', NULL, NULL),
(294, 'Atlantic/Faroe', '1', 'UTC/GMT +00:00', NULL, NULL),
(295, 'Atlantic/Madeira', '1', 'UTC/GMT +00:00', NULL, NULL),
(296, 'Atlantic/Reykjavik', '1', 'UTC/GMT +00:00', NULL, NULL),
(297, 'Atlantic/South_Georgia', '1', 'UTC/GMT -02:00', NULL, NULL),
(298, 'Atlantic/St_Helena', '1', 'UTC/GMT +00:00', NULL, NULL),
(299, 'Atlantic/Stanley', '1', 'UTC/GMT -03:00', NULL, NULL),
(300, 'Australia/Adelaide', '1', 'UTC/GMT +10:30', NULL, NULL),
(301, 'Australia/Brisbane', '1', 'UTC/GMT +10:00', NULL, NULL),
(302, 'Australia/Broken_Hill', '1', 'UTC/GMT +10:30', NULL, NULL),
(303, 'Australia/Currie', '1', 'UTC/GMT +11:00', NULL, NULL),
(304, 'Australia/Darwin', '1', 'UTC/GMT +09:30', NULL, NULL),
(305, 'Australia/Eucla', '1', 'UTC/GMT +08:45', NULL, NULL),
(306, 'Australia/Hobart', '1', 'UTC/GMT +11:00', NULL, NULL),
(307, 'Australia/Lindeman', '1', 'UTC/GMT +10:00', NULL, NULL),
(308, 'Australia/Lord_Howe', '1', 'UTC/GMT +11:00', NULL, NULL),
(309, 'Australia/Melbourne', '1', 'UTC/GMT +11:00', NULL, NULL),
(310, 'Australia/Perth', '1', 'UTC/GMT +08:00', NULL, NULL),
(311, 'Australia/Sydney', '1', 'UTC/GMT +11:00', NULL, NULL),
(312, 'Europe/Amsterdam', '1', 'UTC/GMT +01:00', NULL, NULL),
(313, 'Europe/Andorra', '1', 'UTC/GMT +01:00', NULL, NULL),
(314, 'Europe/Athens', '1', 'UTC/GMT +02:00', NULL, NULL),
(315, 'Europe/Belgrade', '1', 'UTC/GMT +01:00', NULL, NULL),
(316, 'Europe/Berlin', '1', 'UTC/GMT +01:00', NULL, NULL),
(317, 'Europe/Bratislava', '1', 'UTC/GMT +01:00', NULL, NULL),
(318, 'Europe/Brussels', '1', 'UTC/GMT +01:00', NULL, NULL),
(319, 'Europe/Bucharest', '1', 'UTC/GMT +02:00', NULL, NULL),
(320, 'Europe/Budapest', '1', 'UTC/GMT +01:00', NULL, NULL),
(321, 'Europe/Busingen', '1', 'UTC/GMT +01:00', NULL, NULL),
(322, 'Europe/Chisinau', '1', 'UTC/GMT +02:00', NULL, NULL),
(323, 'Europe/Copenhagen', '1', 'UTC/GMT +01:00', NULL, NULL),
(324, 'Europe/Dublin', '1', 'UTC/GMT +00:00', NULL, NULL),
(325, 'Europe/Gibraltar', '1', 'UTC/GMT +01:00', NULL, NULL),
(326, 'Europe/Guernsey', '1', 'UTC/GMT +00:00', NULL, NULL),
(327, 'Europe/Helsinki', '1', 'UTC/GMT +02:00', NULL, NULL),
(328, 'Europe/Isle_of_Man', '1', 'UTC/GMT +00:00', NULL, NULL),
(329, 'Europe/Istanbul', '1', 'UTC/GMT +02:00', NULL, NULL),
(330, 'Europe/Jersey', '1', 'UTC/GMT +00:00', NULL, NULL),
(331, 'Europe/Kaliningrad', '1', 'UTC/GMT +02:00', NULL, NULL),
(332, 'Europe/Kiev', '1', 'UTC/GMT +02:00', NULL, NULL),
(333, 'Europe/Lisbon', '1', 'UTC/GMT +00:00', NULL, NULL),
(334, 'Europe/Ljubljana', '1', 'UTC/GMT +01:00', NULL, NULL),
(335, 'Europe/London', '1', 'UTC/GMT +00:00', NULL, NULL),
(336, 'Europe/Luxembourg', '1', 'UTC/GMT +01:00', NULL, NULL),
(337, 'Europe/Madrid', '1', 'UTC/GMT +01:00', NULL, NULL),
(338, 'Europe/Malta', '1', 'UTC/GMT +01:00', NULL, NULL),
(339, 'Europe/Mariehamn', '1', 'UTC/GMT +02:00', NULL, NULL),
(340, 'Europe/Minsk', '1', 'UTC/GMT +03:00', NULL, NULL),
(341, 'Europe/Monaco', '1', 'UTC/GMT +01:00', NULL, NULL),
(342, 'Europe/Moscow', '1', 'UTC/GMT +03:00', NULL, NULL),
(343, 'Europe/Oslo', '1', 'UTC/GMT +01:00', NULL, NULL),
(344, 'Europe/Paris', '1', 'UTC/GMT +01:00', NULL, NULL),
(345, 'Europe/Podgorica', '1', 'UTC/GMT +01:00', NULL, NULL),
(346, 'Europe/Prague', '1', 'UTC/GMT +01:00', NULL, NULL),
(347, 'Europe/Riga', '1', 'UTC/GMT +02:00', NULL, NULL),
(348, 'Europe/Rome', '1', 'UTC/GMT +01:00', NULL, NULL),
(349, 'Europe/Samara', '1', 'UTC/GMT +04:00', NULL, NULL),
(350, 'Europe/San_Marino', '1', 'UTC/GMT +01:00', NULL, NULL),
(351, 'Europe/Sarajevo', '1', 'UTC/GMT +01:00', NULL, NULL),
(352, 'Europe/Simferopol', '1', 'UTC/GMT +03:00', NULL, NULL),
(353, 'Europe/Skopje', '1', 'UTC/GMT +01:00', NULL, NULL),
(354, 'Europe/Sofia', '1', 'UTC/GMT +02:00', NULL, NULL),
(355, 'Europe/Stockholm', '1', 'UTC/GMT +01:00', NULL, NULL),
(356, 'Europe/Tallinn', '1', 'UTC/GMT +02:00', NULL, NULL),
(357, 'Europe/Tirane', '1', 'UTC/GMT +01:00', NULL, NULL),
(358, 'Europe/Uzhgorod', '1', 'UTC/GMT +02:00', NULL, NULL),
(359, 'Europe/Vaduz', '1', 'UTC/GMT +01:00', NULL, NULL),
(360, 'Europe/Vatican', '1', 'UTC/GMT +01:00', NULL, NULL),
(361, 'Europe/Vienna', '1', 'UTC/GMT +01:00', NULL, NULL),
(362, 'Europe/Vilnius', '1', 'UTC/GMT +02:00', NULL, NULL),
(363, 'Europe/Volgograd', '1', 'UTC/GMT +03:00', NULL, NULL),
(364, 'Europe/Warsaw', '1', 'UTC/GMT +01:00', NULL, NULL),
(365, 'Europe/Zagreb', '1', 'UTC/GMT +01:00', NULL, NULL),
(366, 'Europe/Zaporozhye', '1', 'UTC/GMT +02:00', NULL, NULL),
(367, 'Europe/Zurich', '1', 'UTC/GMT +01:00', NULL, NULL),
(368, 'Indian/Antananarivo', '1', 'UTC/GMT +03:00', NULL, NULL),
(369, 'Indian/Chagos', '1', 'UTC/GMT +06:00', NULL, NULL),
(370, 'Indian/Christmas', '1', 'UTC/GMT +07:00', NULL, NULL),
(371, 'Indian/Cocos', '1', 'UTC/GMT +06:30', NULL, NULL),
(372, 'Indian/Comoro', '1', 'UTC/GMT +03:00', NULL, NULL),
(373, 'Indian/Kerguelen', '1', 'UTC/GMT +05:00', NULL, NULL),
(374, 'Indian/Mahe', '1', 'UTC/GMT +04:00', NULL, NULL),
(375, 'Indian/Maldives', '1', 'UTC/GMT +05:00', NULL, NULL),
(376, 'Indian/Mauritius', '1', 'UTC/GMT +04:00', NULL, NULL),
(377, 'Indian/Mayotte', '1', 'UTC/GMT +03:00', NULL, NULL),
(378, 'Indian/Reunion', '1', 'UTC/GMT +04:00', NULL, NULL),
(379, 'Pacific/Apia', '1', 'UTC/GMT +14:00', NULL, NULL),
(380, 'Pacific/Auckland', '1', 'UTC/GMT +13:00', NULL, NULL),
(381, 'Pacific/Bougainville', '1', 'UTC/GMT +11:00', NULL, NULL),
(382, 'Pacific/Chatham', '1', 'UTC/GMT +13:45', NULL, NULL),
(383, 'Pacific/Chuuk', '1', 'UTC/GMT +10:00', NULL, NULL),
(384, 'Pacific/Easter', '1', 'UTC/GMT -05:00', NULL, NULL),
(385, 'Pacific/Efate', '1', 'UTC/GMT +11:00', NULL, NULL),
(386, 'Pacific/Enderbury', '1', 'UTC/GMT +13:00', NULL, NULL),
(387, 'Pacific/Fakaofo', '1', 'UTC/GMT +13:00', NULL, NULL),
(388, 'Pacific/Fiji', '1', 'UTC/GMT +12:00', NULL, NULL),
(389, 'Pacific/Funafuti', '1', 'UTC/GMT +12:00', NULL, NULL),
(390, 'Pacific/Galapagos', '1', 'UTC/GMT -06:00', NULL, NULL),
(391, 'Pacific/Gambier', '1', 'UTC/GMT -09:00', NULL, NULL),
(392, 'Pacific/Guadalcanal', '1', 'UTC/GMT +11:00', NULL, NULL),
(393, 'Pacific/Guam', '1', 'UTC/GMT +10:00', NULL, NULL),
(394, 'Pacific/Honolulu', '1', 'UTC/GMT -10:00', NULL, NULL),
(395, 'Pacific/Johnston', '1', 'UTC/GMT -10:00', NULL, NULL),
(396, 'Pacific/Kiritimati', '1', 'UTC/GMT +14:00', NULL, NULL),
(397, 'Pacific/Kosrae', '1', 'UTC/GMT +11:00', NULL, NULL),
(398, 'Pacific/Kwajalein', '1', 'UTC/GMT +12:00', NULL, NULL),
(399, 'Pacific/Majuro', '1', 'UTC/GMT +12:00', NULL, NULL),
(400, 'Pacific/Marquesas', '1', 'UTC/GMT -09:30', NULL, NULL),
(401, 'Pacific/Midway', '1', 'UTC/GMT -11:00', NULL, NULL),
(402, 'Pacific/Nauru', '1', 'UTC/GMT +12:00', NULL, NULL),
(403, 'Pacific/Niue', '1', 'UTC/GMT -11:00', NULL, NULL),
(404, 'Pacific/Norfolk', '1', 'UTC/GMT +11:00', NULL, NULL),
(405, 'Pacific/Noumea', '1', 'UTC/GMT +11:00', NULL, NULL),
(406, 'Pacific/Pago_Pago', '1', 'UTC/GMT -11:00', NULL, NULL),
(407, 'Pacific/Palau', '1', 'UTC/GMT +09:00', NULL, NULL),
(408, 'Pacific/Pitcairn', '1', 'UTC/GMT -08:00', NULL, NULL),
(409, 'Pacific/Pohnpei', '1', 'UTC/GMT +11:00', NULL, NULL),
(410, 'Pacific/Port_Moresby', '1', 'UTC/GMT +10:00', NULL, NULL),
(411, 'Pacific/Rarotonga', '1', 'UTC/GMT -10:00', NULL, NULL),
(412, 'Pacific/Saipan', '1', 'UTC/GMT +10:00', NULL, NULL),
(413, 'Pacific/Tahiti', '1', 'UTC/GMT -10:00', NULL, NULL),
(414, 'Pacific/Tarawa', '1', 'UTC/GMT +12:00', NULL, NULL),
(415, 'Pacific/Tongatapu', '1', 'UTC/GMT +13:00', NULL, NULL),
(416, 'Pacific/Wake', '1', 'UTC/GMT +12:00', NULL, NULL),
(417, 'Pacific/Wallis', '1', 'UTC/GMT +12:00', NULL, NULL),
(418, 'UTC', '1', 'UTC/GMT +00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_uom`
--

CREATE TABLE `mst_uom` (
  `uomid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mst_useraction`
--

CREATE TABLE `mst_useraction` (
  `actionid` int(10) UNSIGNED NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_useraction`
--

INSERT INTO `mst_useraction` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`, `created_at`, `updated_at`) VALUES
(1, 'admin', '127.0.0.0', 'asdasdwefwsfwef', '2016-02-11', '2016-02-11 00:00:00', 'asdads', '2016-02-10 18:30:00', '2016-02-10 18:30:00'),
(2, 'KSTF-0840', '127.0.0.0', 'asdaksjhfkjnvdweiufh23rkjflshkj', '2016-02-12', '2016-02-12 07:28:40', 'Added User', '2016-02-12 01:58:40', '2016-02-12 01:58:40'),
(3, 'TSWE-2053', '127.0.0.0', 'asdaksjhfkjnvdweiufh23rkjflshkj', '2016-02-12', '2016-02-12 09:16:54', 'Added User', '2016-02-12 03:46:54', '2016-02-12 03:46:54'),
(4, 'MWKG-5235', '127.0.0.0', 'asdaksjhfkjnvdweiufh23rkjflshkj', '2016-02-12', '2016-02-12 09:27:47', 'Added User', '2016-02-12 03:57:47', '2016-02-12 03:57:47'),
(5, 'POAP-6882', '127.0.0.0', 'asdaksjhfkjnvdweiufh23rkjflshkj', '2016-02-12', '2016-02-12 09:58:55', 'Added User', '2016-02-12 04:28:55', '2016-02-12 04:28:55');

-- --------------------------------------------------------

--
-- Table structure for table `mst_userlogin`
--

CREATE TABLE `mst_userlogin` (
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `authorizedpass` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `ftlogin` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `timezoneid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `secquiz1` int(11) DEFAULT NULL,
  `secquiz2` int(11) DEFAULT NULL,
  `secquiz3` int(11) DEFAULT NULL,
  `secans1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visualnoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `audionoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `industry` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_userlogin`
--

INSERT INTO `mst_userlogin` (`roletype`, `roleid`, `userid`, `password`, `authorizedpass`, `isactive`, `ftlogin`, `timezoneid`, `secquiz1`, `secquiz2`, `secquiz3`, `secans1`, `secans2`, `secans3`, `visualnoti`, `audionoti`, `industry`, `category`, `subcategory`, `brand`, `creationdate`, `created_at`, `updated_at`) VALUES
('AD', 5, 'admin', 'password123', 'password123', '1', 'Y', 'US/Eastern', 6, 8, 3, 'father', 'mother', 'animal', '1', '1', '', '', '', '', '2015-07-21 02:20:16', NULL, NULL),
('TM', 3, 'GPUJ-2680', 'password1', 'password1', '1', 'N', 'Australia/Sydney', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '', '', '', '', '2015-12-17 16:22:38', NULL, NULL),
('AM', 1, 'IXBB-4438', 'password123', 'password1', '1', 'Y', 'US/Eastern', 6, 8, 9, 'mother', 'father', 'food', '1', '1', '1', '', '', '', '2015-12-17 16:12:18', NULL, NULL),
('AM', 1, 'JUQI-3911', 'password1', 'password1', '1', 'Y', 'US/Central', 6, 8, 9, 'mother', 'father', 'food', '1', '1', '3,1', '', '', '', '2015-12-17 16:20:25', NULL, NULL),
('AD', 5, 'KSTF-0840', 'asdasd1', 'password123', '1', 'N', 'Asia/Kolkata', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '4,1', '1', '2', '1,2', '2016-02-12 07:28:40', '2016-02-12 01:58:40', '2016-02-12 01:58:40'),
('AM', 1, 'MSOX-3592', 'password1', 'password1', '1', 'N', 'US/Pacific', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '3,2', '', '', '', '2015-12-17 16:21:19', NULL, NULL),
('AD', 4, 'MWKG-5235', '', 'asdasd', '1', 'N', 'Atlantic/Azores', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '', '', '', '', '2016-02-12 09:27:47', '2016-02-12 03:57:47', '2016-02-12 03:57:47'),
('AM', 18, 'OORQ-0229', 'password1', 'password1', '1', 'Y', 'US/Eastern', 7, 14, 3, 'ottawa', 'strawars', 'rasputin', '1', '1', '3', '', '', '', '2016-01-18 11:53:31', NULL, NULL),
('AM', 18, 'POAP-6882', '', 'asdasd', '1', 'N', 'Atlantic/Azores', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '', '', '', '', '2016-02-12 09:58:55', '2016-02-12 04:28:55', '2016-02-12 04:28:55'),
('QF', 2, 'TSWE-2053', '', 'asdasd', '1', 'N', 'Atlantic/Azores', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '3,4', '', '', '', '2016-02-12 09:16:54', '2016-02-12 03:46:54', '2016-02-12 03:46:54'),
('QF', 2, 'YSZJ-4486', 'password1', 'password1', '1', 'N', 'US/Eastern', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '3,1,2', '', '', '', '2016-01-12 05:36:23', NULL, NULL),
('QF', 2, 'YWFZ-6999', 'password1', 'password1', '1', 'N', 'US/Hawaii', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '3,1,2', '', '', '', '2015-12-17 16:22:06', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_userlogon`
--

CREATE TABLE `mst_userlogon` (
  `logonid` int(10) UNSIGNED NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `starttime` datetime NOT NULL,
  `finishtime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logondate` date NOT NULL,
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_userlogon`
--

INSERT INTO `mst_userlogon` (`logonid`, `userid`, `ipaddress`, `starttime`, `finishtime`, `sessionid`, `logondate`, `roletype`, `created_at`, `updated_at`) VALUES
(1, 'BACV-7542', '112.196.136.78', '2015-10-30 07:30:49', '', 'cp3219ddm229djscci53bmv921', '2015-10-30', 'AM', NULL, NULL),
(2, 'OGJJ-0337', '104.236.195.130', '2015-10-30 07:37:12', '', 'vgk7slmf0sngivc8u70rlriji0', '2015-10-30', 'TM', NULL, NULL),
(3, 'admin', '112.196.136.78', '2015-10-30 08:11:57', '', 'm8a5nu3q4eljnjhne3hufr9151', '2015-10-30', 'AD', NULL, NULL),
(4, 'TEED-0538', '112.196.136.78', '2015-10-30 08:12:19', '2015-10-30 09:29:33am', '1v7ub1kacpai10e45mdui5mkr0', '2015-10-30', 'QF', NULL, NULL),
(5, 'admin', '112.196.136.78', '2015-10-30 08:29:38', '', 's2nbqdpn09ev820bn9pqbfbhe0', '2015-10-30', 'AD', NULL, NULL),
(6, 'admin', '112.196.136.78', '2015-10-31 00:41:56', '', 'um8eh82f906d37kkhn7f7612a0', '2015-10-31', 'AD', NULL, NULL),
(7, 'admin', '112.196.136.78', '2015-10-31 00:49:00', '', '0k48hr58bhjp3vj01ojhh5hjp3', '2015-10-31', 'AD', NULL, NULL),
(8, 'admin', '112.196.136.78', '2015-10-31 00:57:27', '', 'gbjgli2hnv19p6oafoiie1q081', '2015-10-31', 'AD', NULL, NULL),
(9, 'admin', '112.196.136.78', '2015-10-31 01:00:52', '', 'ukuj85l12id22kgefffcsmk7b5', '2015-10-31', 'AD', NULL, NULL),
(10, 'admin', '112.196.136.78', '2015-10-31 01:03:32', '2015-10-31 02:03:34am', '8garjivp1c0cuu5llrhvcpup71', '2015-10-31', 'AD', NULL, NULL),
(11, 'BACV-7542', '112.196.136.78', '2015-10-31 01:03:43', '', 'qb1rui0l2frlj3qfsmokeearg1', '2015-10-31', 'AM', NULL, NULL),
(12, 'BACV-7542', '112.196.136.78', '2015-10-31 01:13:18', '', '38ftnonjn7cq6nu3e48766ftq5', '2015-10-31', 'AM', NULL, NULL),
(13, 'CMHY-0522', '112.196.136.78', '2015-10-31 01:16:48', '', '03ib750vq1i4tattu47dqivft5', '2015-10-31', 'AM', NULL, NULL),
(14, 'admin', '112.196.136.78', '2015-10-31 02:07:57', '', '8tvbb0012omkolqsjj3m975rp2', '2015-10-31', 'AD', NULL, NULL),
(15, 'admin', '112.196.136.78', '2015-10-31 03:06:25', '', 'jduaqni1g3pccj15p0bgapmi82', '2015-10-31', 'AD', NULL, NULL),
(16, 'BACV-7542', '112.196.136.78', '2015-10-31 03:08:05', '2015-10-31 05:27:44am', '8spjb0nrterpkvvndrui86pfm6', '2015-10-31', 'AM', NULL, NULL),
(17, 'admin', '112.196.136.78', '2015-10-31 03:13:36', '2015-10-31 06:05:34am', 'bifkn02i2ie0m5irg20cefqr12', '2015-10-31', 'AD', NULL, NULL),
(18, 'admin', '112.196.136.78', '2015-10-31 04:15:52', '', 's8a2ktiosn5i4m1ul906b9ikl3', '2015-10-31', 'AD', NULL, NULL),
(19, 'CMHY-0522', '112.196.136.78', '2015-10-31 04:28:11', '', '63q6ihrm6a81k4kn0qp2kvlc56', '2015-10-31', 'AM', NULL, NULL),
(20, 'admin', '112.196.136.78', '2015-10-31 05:05:54', '', 'qj27umuh0i1nir606nk6d538u3', '2015-10-31', 'AD', NULL, NULL),
(21, 'admin', '112.196.136.78', '2015-10-31 05:06:05', '2015-10-31 06:14:52am', 'u426d6mumphc4p2q4ho6fhsia1', '2015-10-31', 'AD', NULL, NULL),
(22, 'CMHY-0522', '112.196.136.78', '2015-10-31 05:14:57', '2015-10-31 07:18:27am', '52itoieoddtmeaphlbbal42ih1', '2015-10-31', 'AM', NULL, NULL),
(23, 'BACV-7542', '112.196.136.78', '2015-10-31 05:15:16', '', 'h8uutmjadhb6l071n43dsmtnh5', '2015-10-31', 'AM', NULL, NULL),
(24, 'admin', '112.196.136.78', '2015-10-31 05:45:42', '2015-10-31 07:56:20am', 'fldsrv00ik1queag130uqncr22', '2015-10-31', 'AD', NULL, NULL),
(25, 'OGJJ-0337', '45.55.246.162', '2015-10-31 05:49:09', '2015-10-31 09:11:52am', 'r5l13i6q6fnk6vsinq42hptq71', '2015-10-31', 'TM', NULL, NULL),
(26, 'admin', '112.196.136.78', '2015-10-31 06:05:47', '', 'kgj5b0qkqidk00pnmt4jorehe5', '2015-10-31', 'AD', NULL, NULL),
(27, 'BACV-7542', '112.196.136.78', '2015-10-31 06:17:58', '', '24vuq9i94ck8dq5mdnhis5er35', '2015-10-31', 'AM', NULL, NULL),
(28, 'admin', '112.196.136.78', '2015-10-31 06:18:32', '2015-10-31 08:48:25am', 'qgbt588ur9cat2o1jpbnv9ocg3', '2015-10-31', 'AD', NULL, NULL),
(29, 'BACV-7542', '112.196.136.78', '2015-10-31 06:56:35', '', '1ovp1quq3pnqvpnu1l7g96tr66', '2015-10-31', 'AM', NULL, NULL),
(30, 'TEED-0538', '112.196.136.78', '2015-10-31 07:48:29', '2015-10-31 08:55:30am', 'g56io0h4jqj68ch8lk7aobgpb7', '2015-10-31', 'QF', NULL, NULL),
(31, 'admin', '112.196.136.78', '2015-10-31 07:55:41', '2015-10-31 09:31:51am', '48mmo7dersiso9haejot83hqu4', '2015-10-31', 'AD', NULL, NULL),
(32, 'CMHY-0522', '112.196.136.78', '2015-10-31 07:56:28', '', 'mg1vaopfi0j8l2gghmkoj5n1c4', '2015-10-31', 'AM', NULL, NULL),
(33, 'TEED-0538', '112.196.136.78', '2015-10-31 08:02:10', '', 's7oqdrku66brf7lt1933tsd7h1', '2015-10-31', 'QF', NULL, NULL),
(34, 'admin', '112.196.136.78', '2015-10-31 09:05:56', '2015-10-31 09:08:15am', 'jis2b8uimt9ed4k86tracanmr3', '2015-10-31', 'AD', NULL, NULL),
(35, 'CMHY-0522', '112.196.136.78', '2015-10-31 08:08:35', '2015-10-31 09:09:18am', 'lm0kd7kr3aoltlmfgp9r7eo7p3', '2015-10-31', 'AM', NULL, NULL),
(36, 'TEED-0538', '112.196.136.78', '2015-10-31 08:09:26', '', '35it5kplme61dsfl6f3872u4m5', '2015-10-31', 'QF', NULL, NULL),
(37, 'TEED-0538', '45.55.199.109', '2015-10-31 08:12:09', '', 'pkdcjo7u3tl22h4garsjo6meq7', '2015-10-31', 'QF', NULL, NULL),
(38, 'admin', '112.196.136.78', '2015-10-31 08:22:22', '2015-10-31 09:26:49am', '85uhisfmq63ogc774b3529tsl6', '2015-10-31', 'AD', NULL, NULL),
(39, 'CMHY-0522', '112.196.136.78', '2015-10-31 08:26:55', '', 'j6l2ai6f6p9i2p4214et6n5hv7', '2015-10-31', 'AM', NULL, NULL),
(40, 'TEED-0538', '112.196.136.78', '2015-10-31 08:31:55', '2015-10-31 09:39:47am', 'qen8an09r6ishkp2rstvu6que6', '2015-10-31', 'QF', NULL, NULL),
(41, 'CMHY-0522', '112.196.136.78', '2015-10-31 08:34:37', '', '3jni7bc0emsbsunan8him242v3', '2015-10-31', 'AM', NULL, NULL),
(42, 'admin', '112.196.136.78', '2015-10-31 08:39:52', '2015-10-31 09:40:07am', '8iddn3j6pqs5lubb6drpso2t44', '2015-10-31', 'AD', NULL, NULL),
(43, 'BACV-7542', '112.196.136.78', '2015-10-31 08:40:26', '2015-10-31 09:41:02am', 'tr2sdn07jm0d1j2sid7659n886', '2015-10-31', 'AM', NULL, NULL),
(44, 'admin', '112.196.136.78', '2015-10-31 08:41:06', '2015-10-31 09:41:22am', 'aouvatlr6qknf283ida80h1ig0', '2015-10-31', 'AD', NULL, NULL),
(45, 'OGJJ-0337', '112.196.136.78', '2015-10-31 08:41:25', '', 'imipe8iejnj0sjup4i4n854ku5', '2015-10-31', 'TM', NULL, NULL),
(46, 'admin', '112.196.136.78', '2015-11-02 00:53:17', '', 'hnetiu95qbnj4m6jifr5jasiu2', '2015-11-02', 'AD', NULL, NULL),
(47, 'admin', '112.196.136.78', '2015-11-02 00:57:44', '', 'uoohsa124olms1hpanuq2afdu7', '2015-11-02', 'AD', NULL, NULL),
(48, 'admin', '112.196.136.78', '2015-11-02 01:20:33', '2015-11-02 01:38:44am', 'qni06ttq3jnmvfh9cvcni1oa75', '2015-11-02', 'AD', NULL, NULL),
(49, 'admin', '112.196.136.78', '2015-11-02 01:32:38', '2015-11-02 01:36:10am', 'b3o327lqs6bo1ft3c2n1j0gsf0', '2015-11-02', 'AD', NULL, NULL),
(50, 'TEED-0538', '112.196.136.78', '2015-11-02 01:36:49', '2015-11-02 06:03:36am', 'rrkkc7ne3fnhngq4ib2vmogia6', '2015-11-02', 'QF', NULL, NULL),
(51, 'TEED-0538', '112.196.136.78', '2015-11-02 02:11:41', '', 'ko3s0tf1sm4iahpu6cljvugiq1', '2015-11-02', 'QF', NULL, NULL),
(52, 'OGJJ-0337', '112.196.136.78', '2015-11-02 02:12:12', '', '6c5b1n8hvsqcd497qk459btt51', '2015-11-02', 'TM', NULL, NULL),
(53, 'CMHY-0522', '112.196.136.78', '2015-11-02 02:13:09', '', 'tko315no1rm24a7k7amhiveop7', '2015-11-02', 'AM', NULL, NULL),
(54, 'BACV-7542', '112.196.136.78', '2015-11-02 02:16:09', '', 'gq7nu3kbjg61gtihlm9rbc48k7', '2015-11-02', 'AM', NULL, NULL),
(55, 'CMHY-0522', '112.196.136.78', '2015-11-02 02:18:37', '', 'tqpnl0a3om4egeiitlhorkmte7', '2015-11-02', 'AM', NULL, NULL),
(56, 'CMHY-0522', '112.196.136.78', '2015-11-02 02:38:25', '2015-11-02 07:40:14am', 'm5rr0s6cjqt7ohafh1kii4tmd7', '2015-11-02', 'AM', NULL, NULL),
(57, 'BACV-7542', '112.196.136.78', '2015-11-02 04:22:13', '2015-11-02 04:58:05am', '3rak50rtieut1fd8tvmd087s62', '2015-11-02', 'AM', NULL, NULL),
(58, 'admin', '104.236.225.168', '2015-11-02 04:27:28', '2015-11-02 05:15:41am', 'ppvfr835jbovuuht18qnici9s6', '2015-11-02', 'AD', NULL, NULL),
(59, 'admin', '112.196.136.78', '2015-11-02 04:58:07', '2015-11-02 05:00:04am', 'c0cm3023leik3566va4lr4n5j0', '2015-11-02', 'AD', NULL, NULL),
(60, 'BACV-7542', '112.196.136.78', '2015-11-02 05:00:20', '', 'a17lguk5vap0rqhgpgs4jh67o5', '2015-11-02', 'AM', NULL, NULL),
(61, 'admin', '112.196.136.78', '2015-11-02 05:15:29', '2015-11-02 05:29:24am', 'k70qibfldqih1r2hrbq1fm3u14', '2015-11-02', 'AD', NULL, NULL),
(62, 'TEED-0538', '104.236.225.168', '2015-11-02 05:15:54', '', 'nupvn8bgvh2mh0b740bpq8klr3', '2015-11-02', 'QF', NULL, NULL),
(63, 'TEED-0538', '112.196.136.78', '2015-11-02 05:29:39', '', 'pkuvsvtotlgp09fscjd24d76r7', '2015-11-02', 'QF', NULL, NULL),
(64, 'admin', '104.236.195.130', '2015-11-02 05:31:53', '', 'agngfgu0femnles77onn648p22', '2015-11-02', 'AD', NULL, NULL),
(65, 'admin', '112.196.136.78', '2015-11-02 06:03:59', '', '3jofjl18mgr3qtbvq7a2ss3pk4', '2015-11-02', 'AD', NULL, NULL),
(66, 'admin', '104.131.14.167', '2015-11-02 07:35:20', '', 'ilgucr4hvn7061qtr4etqm6a03', '2015-11-02', 'AD', NULL, NULL),
(67, 'CMHY-0522', '112.196.136.78', '2015-11-02 07:40:33', '', 'kn3ur5m127s0c87hpnfopiaj76', '2015-11-02', 'AM', NULL, NULL),
(68, 'admin', '112.196.136.78', '2015-11-02 08:23:04', '', 'pi5nsoi4jtnq6kvkqb5s1hjj11', '2015-11-02', 'AD', NULL, NULL),
(69, 'OGJJ-0337', '112.196.136.78', '2015-11-02 08:28:41', '', 'ajoegmeqod390ii3dnb895br45', '2015-11-02', 'TM', NULL, NULL),
(70, 'admin', '112.196.136.78', '2015-11-02 08:55:43', '', 'raqe3v5qpirf25jd3n186vajo3', '2015-11-02', 'AD', NULL, NULL),
(71, 'admin', '66.49.249.43', '2015-11-02 13:45:52', '', 'lkrop1fkveddu8e0o39urn0qj5', '2015-11-02', 'AD', NULL, NULL),
(72, 'admin', '66.49.249.43', '2015-11-02 13:46:46', '', 'udlu5ptd9il44r43f86j60l0q4', '2015-11-02', 'AD', NULL, NULL),
(73, 'FXIO-8905', '66.49.249.43', '2015-11-02 14:00:58', '2015-11-02 16:12:22pm', 'keei9q00947i31jgqql06ou787', '2015-11-02', 'AM', NULL, NULL),
(74, 'RAUU-1599', '66.49.249.43', '2015-11-02 14:01:05', '', 'tkhqa8cmjfmqt91haqgg173fh2', '2015-11-02', 'AM', NULL, NULL),
(75, 'admin', '66.49.249.43', '2015-11-02 14:42:26', '2015-11-02 14:46:16pm', '4rfjuovbj2rsn9omu8pea0bbc3', '2015-11-02', 'AD', NULL, NULL),
(76, 'admin', '66.49.249.43', '2015-11-02 14:46:25', '', '4810rls1aln5vvlo05dn45c241', '2015-11-02', 'AD', NULL, NULL),
(77, 'admin', '66.49.249.43', '2015-11-02 16:07:45', '', '11vl9dhdbhc6dnoenqj1hijqt7', '2015-11-02', 'AD', NULL, NULL),
(78, 'FTTP-8399', '66.49.249.43', '2015-11-02 16:09:43', '2015-11-02 16:16:49pm', 'c3brrce2c6rj8efrnghnjsktl7', '2015-11-02', 'TM', NULL, NULL),
(79, 'ZJBX-0533', '66.49.249.43', '2015-11-02 16:09:50', '', '9bocub69dc73j8jrkp754uiqa4', '2015-11-02', 'QF', NULL, NULL),
(80, 'FXIO-8905', '66.49.249.43', '2015-11-02 16:12:25', '2015-11-02 16:24:05pm', 'knr6qhg82jh4ggjjv1gfrsldr3', '2015-11-02', 'AM', NULL, NULL),
(81, 'ZJBX-0533', '66.49.249.43', '2015-11-02 16:17:24', '', 'l5lnu3g46imu738aasmkstm5g3', '2015-11-02', 'QF', NULL, NULL),
(82, 'FTTP-8399', '66.49.249.43', '2015-11-02 16:24:17', '', '10175q8cdfrs74u78mop43e0i7', '2015-11-02', 'TM', NULL, NULL),
(83, 'admin', '112.196.136.78', '2015-11-03 00:51:33', '', '6cfeke4tcspmfl1qvbefq5bjj2', '2015-11-03', 'AD', NULL, NULL),
(84, 'admin', '112.196.136.78', '2015-11-03 01:15:47', '', 'dktooevp415r3hrqsrdpvh9ho6', '2015-11-03', 'AD', NULL, NULL),
(85, 'admin', '112.196.136.78', '2015-11-03 02:33:28', '2015-11-03 07:01:24am', '9upnemmfr4vcdu2b4mv3u4c192', '2015-11-03', 'AD', NULL, NULL),
(86, 'FXIO-8905', '112.196.136.78', '2015-11-03 02:46:42', '2015-11-03 03:19:48am', '80pg61653lhnh2j7dhi0u5ffh4', '2015-11-03', 'AM', NULL, NULL),
(87, 'RAUU-1599', '112.196.136.78', '2015-11-03 02:47:04', '2015-11-03 03:02:11am', '76f9b9sjdclmv673il14fpsgg5', '2015-11-03', 'AM', NULL, NULL),
(88, 'VUQQ-3341', '112.196.136.78', '2015-11-03 03:02:14', '', '3o7n1j3v04irnfcn0tp4mj05m3', '2015-11-03', 'AM', NULL, NULL),
(89, 'FXIO-8905', '112.196.136.78', '2015-11-03 03:19:56', '', 'qkqtg4jsklqv6kff4831khgjv6', '2015-11-03', 'AM', NULL, NULL),
(90, 'FXIO-8905', '112.196.136.78', '2015-11-03 03:19:57', '2015-11-03 09:39:58am', 'flnf31hkmrh2v75u13g7t1s360', '2015-11-03', 'AM', NULL, NULL),
(91, 'VUQQ-3341', '104.131.161.230', '2015-11-03 04:20:58', '2015-11-03 04:50:49am', 'iqnicn738b6iih8nv8dkpmced1', '2015-11-03', 'AM', NULL, NULL),
(92, 'FXIO-8905', '104.131.161.230', '2015-11-03 04:51:13', '2015-11-03 08:09:32am', 'ibfg3c4rnlc02hg7lvrttskng0', '2015-11-03', 'AM', NULL, NULL),
(93, 'FXIO-8905', '112.196.136.78', '2015-11-03 06:22:40', '2015-11-03 09:08:46am', '67aso1s6p3a93mlqssahrmqjt3', '2015-11-03', 'AM', NULL, NULL),
(94, 'ZJBX-0533', '112.196.136.78', '2015-11-03 06:24:43', '', '0s8me9hu9f632235agnbbggev4', '2015-11-03', 'QF', NULL, NULL),
(95, 'ZJBX-0533', '112.196.136.78', '2015-11-03 06:40:30', '', '68ebl3a3503a47a0kkm3cakgk7', '2015-11-03', 'QF', NULL, NULL),
(96, 'ZJBX-0533', '112.196.136.78', '2015-11-03 07:01:34', '2015-11-03 09:14:43am', 'v1h9t4306ejq89qeue7go2fac6', '2015-11-03', 'QF', NULL, NULL),
(97, 'admin', '112.196.136.78', '2015-11-03 07:11:04', '', 'n24sfhuje3lir9fcqj2d4u94t5', '2015-11-03', 'AD', NULL, NULL),
(98, 'RAUU-1599', '45.55.246.162', '2015-11-03 08:13:07', '', 't7o7d0qal164cvn079jvj3cpo6', '2015-11-03', 'AM', NULL, NULL),
(99, 'admin', '112.196.136.78', '2015-11-03 08:59:39', '', 'vebdk5cnu7pul2ev0ocai11uq0', '2015-11-03', 'AD', NULL, NULL),
(100, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:01:33', '', 'u5qll191l11bbd7hs89840gdd5', '2015-11-03', 'AM', NULL, NULL),
(101, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:01:47', '2015-11-03 09:03:30am', 'c22lkub1mjon0mdavsu1i7d1v4', '2015-11-03', 'AM', NULL, NULL),
(102, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:03:37', '2015-11-03 09:41:27am', '57r6kb2puiedv81nk2akrc3kd7', '2015-11-03', 'AM', NULL, NULL),
(103, 'admin', '112.196.136.78', '2015-11-03 09:09:05', '2015-11-03 10:29:52am', '5nat2i5ka1q9n8d0ucnrp4r9h1', '2015-11-03', 'AD', NULL, NULL),
(104, 'FXIO-8905', '112.196.136.78', '2015-11-03 09:15:09', '', 'sug13mpfc2mnup9hud0ftvng81', '2015-11-03', 'AM', NULL, NULL),
(105, 'ZJBX-0533', '112.196.136.78', '2015-11-03 09:40:03', '2015-11-03 10:20:41am', 'mf85ir8fs1b7is6bobttm026h4', '2015-11-03', 'QF', NULL, NULL),
(106, 'VUQQ-3341', '112.196.136.78', '2015-11-03 09:41:29', '', 'pjhhb10j2ohjkuai7sid8iuf91', '2015-11-03', 'AM', NULL, NULL),
(107, 'admin', '112.196.136.78', '2015-11-03 10:18:51', '2015-11-03 10:19:52am', 'jtt04n2h8bup4fdhg41ovrggn3', '2015-11-03', 'AD', NULL, NULL),
(108, 'RAUU-1599', '112.196.136.78', '2015-11-03 10:19:55', '', '0gtfeh4809j84i75hkf5f839n1', '2015-11-03', 'AM', NULL, NULL),
(109, 'admin', '112.196.136.78', '2015-11-03 10:20:49', '', 'gt6cb56bf3aghvg8kjongb33b5', '2015-11-03', 'AD', NULL, NULL),
(110, 'ZJBX-0533', '112.196.136.78', '2015-11-03 10:21:46', '', 'n7ec1oqrjnq3lbbcimju2ega35', '2015-11-03', 'QF', NULL, NULL),
(111, 'admin', '66.49.237.16', '2015-11-03 12:47:09', '', 'emnmg246b9elhf0gv7i21na121', '2015-11-03', 'AD', NULL, NULL),
(112, 'admin', '66.49.237.16', '2015-11-03 13:55:34', '2015-11-03 14:08:19pm', 'kja1rdb7s298jrdccsdg2f2b00', '2015-11-03', 'AD', NULL, NULL),
(113, 'FTTP-8399', '66.49.237.16', '2015-11-03 14:00:26', '2015-11-03 14:49:06pm', 'e270d901fpp025ikd4spmt4d92', '2015-11-03', 'TM', NULL, NULL),
(114, 'admin', '66.49.237.16', '2015-11-03 14:08:22', '2015-11-03 14:08:25pm', 'fifen8sv5jiatdu65qr0o519n2', '2015-11-03', 'AD', NULL, NULL),
(115, 'admin', '66.49.237.16', '2015-11-03 14:08:28', '2015-11-03 14:49:27pm', '09e6q33di46m6o63n0jl2e34q2', '2015-11-03', 'AD', NULL, NULL),
(116, 'FXIO-8905', '66.49.237.16', '2015-11-03 14:49:10', '', 'h9jedo6tce6itmjvin5uraro65', '2015-11-03', 'AM', NULL, NULL),
(117, 'ZJBX-0533', '66.49.237.16', '2015-11-03 14:49:35', '2015-11-03 14:54:14pm', 'e5e1fejq45iconavt6me8m2195', '2015-11-03', 'QF', NULL, NULL),
(118, 'admin', '66.49.237.16', '2015-11-03 14:54:18', '2015-11-03 15:01:40pm', '5cnhft0js6gtqs7p83l3hvg0d4', '2015-11-03', 'AD', NULL, NULL),
(119, 'admin', '66.49.237.16', '2015-11-03 15:07:01', '', 'acmf69hpccme5eor51pk7lfrs7', '2015-11-03', 'AD', NULL, NULL),
(120, 'admin', '112.196.136.78', '2015-11-04 00:54:36', '2015-11-04 01:35:34am', 'qml8airktc91npmanp5eijhv06', '2015-11-04', 'AD', NULL, NULL),
(121, 'admin', '112.196.136.78', '2015-11-04 01:19:42', '2015-11-04 01:19:53am', 'fbufbi0it02q7c73ae8i8jia70', '2015-11-04', 'AD', NULL, NULL),
(122, 'admin', '112.196.136.78', '2015-11-04 01:19:56', '', 'rhldmsm6hl3vkbafu4fujqrr66', '2015-11-04', 'AD', NULL, NULL),
(123, 'admin', '112.196.136.78', '2015-11-04 01:35:43', '2015-11-04 06:50:12am', 'liujos4jk2ei32h0coah6ellb5', '2015-11-04', 'AD', NULL, NULL),
(124, 'admin', '112.196.136.78', '2015-11-04 01:42:08', '', 'p58q6u8oq0s0r86ioe2ir2upm0', '2015-11-04', 'AD', NULL, NULL),
(125, 'admin', '112.196.136.78', '2015-11-04 02:08:35', '', 'o1c7p0unraiojmaq4nudrpch90', '2015-11-04', 'AD', NULL, NULL),
(126, 'admin', '112.196.136.78', '2015-11-04 02:18:55', '', '06me71856tqtn1fu28rlhdbvv0', '2015-11-04', 'AD', NULL, NULL),
(127, 'admin', '112.196.136.78', '2015-11-04 02:22:26', '2015-11-04 07:31:54am', 's54l2is4c8n7ndlh098s820ig2', '2015-11-04', 'AD', NULL, NULL),
(128, 'admin', '112.196.136.78', '2015-11-04 02:51:05', '', '1rr4aj4uq1mteerv597ja66gc0', '2015-11-04', 'AD', NULL, NULL),
(129, 'admin', '112.196.136.78', '2015-11-04 03:32:35', '', 'tqs7co8tl5rian4l0cju9tstb5', '2015-11-04', 'AD', NULL, NULL),
(130, 'admin', '112.196.136.78', '2015-11-04 03:34:57', '', 't7evbi5uebu2huu1dpgnuctfp2', '2015-11-04', 'AD', NULL, NULL),
(131, 'admin', '112.196.136.78', '2015-11-04 03:35:57', '', '2kn4e0cs5csuaqnatisnfrg104', '2015-11-04', 'AD', NULL, NULL),
(132, 'admin', '112.196.136.78', '2015-11-04 04:17:20', '', 'm5s05o1o7sukf8bgvhmf4m09j1', '2015-11-04', 'AD', NULL, NULL),
(133, 'admin', '112.196.136.78', '2015-11-04 04:21:25', '', 'kos8eap3tn1n4l2o3v654s6893', '2015-11-04', 'AD', NULL, NULL),
(134, 'admin', '112.196.136.78', '2015-11-04 04:37:43', '', 'o5iu1de9fgtlqr03sl9fc924u5', '2015-11-04', 'AD', NULL, NULL),
(135, 'admin', '112.196.136.78', '2015-11-04 06:12:26', '', 'er0hql0qkvl9jo55q679the6s0', '2015-11-04', 'AD', NULL, NULL),
(136, 'FXIO-8905', '112.196.136.78', '2015-11-04 06:51:28', '', 'fjvfjj9g6lk2al7sr0gifgt8a1', '2015-11-04', 'AM', NULL, NULL),
(137, 'RAUU-1599', '112.196.136.78', '2015-11-04 07:25:03', '2015-11-04 08:04:36am', 'h8q0noqv1i94a67g8ggqs9bjh3', '2015-11-04', 'AM', NULL, NULL),
(138, 'FXIO-8905', '112.196.136.78', '2015-11-04 07:25:37', '', '231holcrtngulrs4foplfhv8s7', '2015-11-04', 'AM', NULL, NULL),
(139, 'ZJBX-0533', '112.196.136.78', '2015-11-04 07:31:58', '2015-11-04 08:00:23am', 'kq2d3h9oiq7umndiq13vcaq8m7', '2015-11-04', 'QF', NULL, NULL),
(140, 'admin', '112.196.136.78', '2015-11-04 07:33:42', '', 'qgabbitk6qhcmg4v7ljngkqab7', '2015-11-04', 'AD', NULL, NULL),
(141, 'admin', '112.196.136.78', '2015-11-04 08:00:28', '2015-11-04 10:04:13am', 're6ak283olluf604pp45nnprg5', '2015-11-04', 'AD', NULL, NULL),
(142, 'VUQQ-3341', '112.196.136.78', '2015-11-04 08:04:40', '2015-11-04 08:04:58am', '174uc8ql8jdle7tdqspitromb1', '2015-11-04', 'AM', NULL, NULL),
(143, 'admin', '112.196.136.78', '2015-11-04 09:51:02', '2015-11-04 09:56:09am', 'cm9mkp79lp83kp0im0dm4t5m17', '2015-11-04', 'AD', NULL, NULL),
(144, 'admin', '112.196.136.78', '2015-11-04 09:56:11', '', 'd876qvugdg7fhbvmma1gvdme75', '2015-11-04', 'AD', NULL, NULL),
(145, 'FXIO-8905', '112.196.136.78', '2015-11-04 10:04:17', '', 'gu743nqkfimr0mfqbeaejitpd3', '2015-11-04', 'AM', NULL, NULL),
(146, 'admin', '106.76.136.45', '2015-11-04 12:01:37', '2015-11-04 15:05:40pm', 'e4h3icf7kuf0rhm7d9bhuav735', '2015-11-04', 'AD', NULL, NULL),
(147, 'VUQQ-3341', '106.76.136.45', '2015-11-04 12:09:36', '2015-11-04 12:11:38pm', 'p8cgce6j80rjs16781ifqfuuq3', '2015-11-04', 'AM', NULL, NULL),
(148, 'FXIO-8905', '106.76.136.45', '2015-11-04 12:11:59', '', 'mqeruhshuekimncqkj060st8s2', '2015-11-04', 'AM', NULL, NULL),
(149, 'admin', '66.49.237.16', '2015-11-04 15:16:51', '', 'pll7htret1ko6dnmsgi7fh93b7', '2015-11-04', 'AD', NULL, NULL),
(150, 'admin', '66.49.237.16', '2015-11-04 15:19:10', '', 'u8cfod3jjfvb8ult6g280j1723', '2015-11-04', 'AD', NULL, NULL),
(151, 'admin', '66.49.157.92', '2015-11-04 16:49:07', '2015-11-04 16:49:35pm', 'f9gjlmah7d0ntard0ns3fr7qj5', '2015-11-04', 'AD', NULL, NULL),
(152, 'admin', '112.196.136.78', '2015-11-05 00:42:56', '2015-11-05 03:02:47am', 'd6u3005jrv31b7gqp817gle8t1', '2015-11-05', 'AD', NULL, NULL),
(153, 'admin', '112.196.136.78', '2015-11-05 01:01:26', '2015-11-05 01:21:14am', 'lr1e3f4bljrvemvm33sft4acq4', '2015-11-05', 'AD', NULL, NULL),
(154, 'admin', '112.196.136.78', '2015-11-05 01:19:09', '', '0i8q7nmgg2g781vdbrqh7cqck5', '2015-11-05', 'AD', NULL, NULL),
(155, 'ZJBX-0533', '112.196.136.78', '2015-11-05 01:19:49', '2015-11-05 01:21:21am', 'qqo9n0viarlrjrbi3k3560jai7', '2015-11-05', 'QF', NULL, NULL),
(156, 'FXIO-8905', '112.196.136.78', '2015-11-05 01:21:46', '', 'nlfr240acdda79eaupa4keqk56', '2015-11-05', 'AM', NULL, NULL),
(157, 'RAUU-1599', '112.196.136.78', '2015-11-05 01:21:59', '', 'jf5rsa7qgl2n31cenl5sasp6m1', '2015-11-05', 'AM', NULL, NULL),
(158, 'admin', '112.196.136.78', '2015-11-05 01:39:55', '', '5ijk6k21atgfl9iipaks37lhb1', '2015-11-05', 'AD', NULL, NULL),
(159, 'FXIO-8905', '112.196.136.78', '2015-11-05 03:00:38', '', '09j1ll08sftkrdv3odnlu1o4o1', '2015-11-05', 'AM', NULL, NULL),
(160, 'FXIO-8905', '112.196.136.78', '2015-11-05 03:02:50', '', 'rv600movrecqi3t332bq6i00a1', '2015-11-05', 'AM', NULL, NULL),
(161, 'admin', '112.196.136.78', '2015-11-05 04:30:38', '2015-11-05 08:15:05am', '0540t6cf0p2bkrnje7ppcbdqo0', '2015-11-05', 'AD', NULL, NULL),
(162, 'admin', '112.196.136.78', '2015-11-05 04:43:38', '', '6tt3oeeaafh0b44kertm9qv942', '2015-11-05', 'AD', NULL, NULL),
(163, 'RAUU-1599', '112.196.136.78', '2015-11-05 06:02:06', '', 'mt13bos023tppo46b1ebvc9il7', '2015-11-05', 'AM', NULL, NULL),
(164, 'RAUU-1599', '112.196.136.78', '2015-11-05 06:39:10', '2015-11-05 09:40:40am', 'km8k7elsfqbicrl4rbn2h6j8n3', '2015-11-05', 'AM', NULL, NULL),
(165, 'FXIO-8905', '112.196.136.78', '2015-11-05 08:05:50', '', 'ro33lp4ilnklt6i9lsm5vpdgc7', '2015-11-05', 'AM', NULL, NULL),
(166, 'ZJBX-0533', '112.196.136.78', '2015-11-05 08:15:09', '2015-11-05 09:17:15am', '42nni31nuat8micrt9rlk5ji51', '2015-11-05', 'QF', NULL, NULL),
(167, 'ZJBX-0533', '112.196.136.78', '2015-11-05 09:17:19', '2015-11-05 09:19:43am', '7g15o8voiac5tlptdb10vlv6d5', '2015-11-05', 'QF', NULL, NULL),
(168, 'admin', '112.196.136.78', '2015-11-05 09:19:47', '', 'eki6ao74d8slvhhragjqrd3l77', '2015-11-05', 'AD', NULL, NULL),
(169, 'FXIO-8905', '112.196.136.78', '2015-11-05 09:24:54', '', '4vrg35ak0gsifiri7sbgdq53n3', '2015-11-05', 'AM', NULL, NULL),
(170, 'RAUU-1599', '112.196.136.78', '2015-11-05 09:40:43', '', 'fmfmkog1p53h9b4vmmp9gog926', '2015-11-05', 'AM', NULL, NULL),
(171, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:12:49', '', 'p7omqohrs4hqep60ttojrmaft5', '2015-11-05', 'AM', NULL, NULL),
(172, 'RAUU-1599', '112.196.136.78', '2015-11-05 10:13:48', '', 'hljajpbd6k1nlff615v4elv2p4', '2015-11-05', 'AM', NULL, NULL),
(173, 'ZJBX-0533', '112.196.136.78', '2015-11-05 10:16:07', '', 'pbc7d3ih2mct582vdi6bedtr22', '2015-11-05', 'QF', NULL, NULL),
(174, 'VUQQ-3341', '112.196.136.78', '2015-11-05 10:21:58', '', 'vehvfr5jl8rrqefka8mql8mfc1', '2015-11-05', 'AM', NULL, NULL),
(175, 'FXIO-8905', '159.203.111.47', '2015-11-05 10:23:38', '2015-11-05 10:38:53am', 'jm5v5ninsaiqi2epis56e2hji6', '2015-11-05', 'AM', NULL, NULL),
(176, 'admin', '112.196.136.44', '2015-11-05 10:24:51', '', 'tc0ujiindn15rdshi5j7gnm947', '2015-11-05', 'AD', NULL, NULL),
(177, 'admin', '112.196.136.78', '2015-11-05 10:26:39', '', 'd7j9c8dri14a2bqtgq38a464b3', '2015-11-05', 'AD', NULL, NULL),
(178, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:32:07', '', '80m81fib9mjhvubr4eh8l48j40', '2015-11-05', 'AM', NULL, NULL),
(179, 'RAUU-1599', '112.196.136.78', '2015-11-05 10:35:48', '', 'p1hshh2p1sk7t8oafhulh6gje7', '2015-11-05', 'AM', NULL, NULL),
(180, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:36:57', '', '0u01gs4kh0es9nhpohhv5v67t5', '2015-11-05', 'AM', NULL, NULL),
(181, 'admin', '112.196.136.44', '2015-11-05 10:49:16', '', 'c3rqvnrc722tb6kep4ss9s7df4', '2015-11-05', 'AD', NULL, NULL),
(182, 'FXIO-8905', '112.196.136.78', '2015-11-05 10:53:49', '', 'tk3r4ohe8c9mn3imlf5lsa6q11', '2015-11-05', 'AM', NULL, NULL),
(183, 'RAUU-1599', '112.196.136.78', '2015-11-05 11:12:12', '', 'seju7vo9b32hdg3bpe27q9cco3', '2015-11-05', 'AM', NULL, NULL),
(184, 'admin', '112.196.136.78', '2015-11-06 00:38:02', '2015-11-06 04:48:12am', 'qlooms0rbbjpqdri13hdpvg6s6', '2015-11-06', 'AD', NULL, NULL),
(185, 'admin', '112.196.136.78', '2015-11-06 00:54:11', '', 'gbo9ln3fv94etsr3967t16ean0', '2015-11-06', 'AD', NULL, NULL),
(186, 'RAUU-1599', '112.196.136.78', '2015-11-06 01:00:50', '2015-11-06 01:07:42am', '46opijt0dokquu25lfj0g7drm6', '2015-11-06', 'AM', NULL, NULL),
(187, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:02:52', '2015-11-06 01:06:08am', '6j06053oslvoldpqvq4ft27797', '2015-11-06', 'AM', NULL, NULL),
(188, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:06:10', '2015-11-06 01:07:14am', '3ku34roihdpfugf3jnjs1lgkd4', '2015-11-06', 'AM', NULL, NULL),
(189, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:07:16', '2015-11-06 01:15:09am', '0uken6lkb2eafv838v807vnmc2', '2015-11-06', 'AM', NULL, NULL),
(190, 'RAUU-1599', '112.196.136.78', '2015-11-06 01:07:46', '2015-11-06 01:15:11am', 'ndj25bgvspse4uofi9j74m79o0', '2015-11-06', 'AM', NULL, NULL),
(191, 'RAUU-1599', '112.196.136.78', '2015-11-06 01:18:18', '', 'qspq30siv9cu57nm5t2oj7u5q3', '2015-11-06', 'AM', NULL, NULL),
(192, 'VUQQ-3341', '112.196.136.78', '2015-11-06 01:18:20', '2015-11-06 07:14:50am', '06gj812nl7vl2clqf5ulm7cgj5', '2015-11-06', 'AM', NULL, NULL),
(193, 'ZJBX-0533', '112.196.136.78', '2015-11-06 02:08:31', '', 'nv2d3m3dl1hq4c6ujukb4mqv41', '2015-11-06', 'QF', NULL, NULL),
(194, 'FTTP-8399', '112.196.136.78', '2015-11-06 02:11:17', '2015-11-06 05:10:13am', 'p0oe5ale1s9cdtj0b2intus7c4', '2015-11-06', 'TM', NULL, NULL),
(195, 'RAUU-1599', '112.196.136.78', '2015-11-06 02:24:25', '', 'nacm4k37rlgfhkkej933nb7g04', '2015-11-06', 'AM', NULL, NULL),
(196, 'admin', '112.196.136.78', '2015-11-06 02:30:04', '', '4kjvlcjenp2hpp8ug4tkv4r0n1', '2015-11-06', 'AD', NULL, NULL),
(197, 'VUQQ-3341', '112.196.136.78', '2015-11-06 02:39:30', '2015-11-06 05:23:06am', 'pq27nre6qfas9ol22stelck621', '2015-11-06', 'AM', NULL, NULL),
(198, 'ZJBX-0533', '112.196.136.78', '2015-11-06 02:42:32', '', 'te1e009v46ubvb7cne0r5bvqo4', '2015-11-06', 'QF', NULL, NULL),
(199, 'admin', '112.196.136.44', '2015-11-06 03:14:23', '', 'okpids7jpca45gn6irponmr476', '2015-11-06', 'AD', NULL, NULL),
(200, 'admin', '112.196.136.44', '2015-11-06 03:16:31', '', '1m4de1rg6ie5pkug9e1nu5fth2', '2015-11-06', 'AD', NULL, NULL),
(201, 'admin', '112.196.136.78', '2015-11-06 04:48:21', '2015-11-06 09:08:04am', 'ph68rr20h5en71o2b9u9he1950', '2015-11-06', 'AD', NULL, NULL),
(202, 'ZJBX-0533', '112.196.136.78', '2015-11-06 05:10:26', '', 'bf725nnppt61fhqcp2k3oqqhi2', '2015-11-06', 'QF', NULL, NULL),
(203, 'RAUU-1599', '112.196.136.78', '2015-11-06 05:19:20', '2015-11-06 06:47:36am', 'i8i3qphtjfm7h7mgk5j6kpi6k4', '2015-11-06', 'AM', NULL, NULL),
(204, 'RAUU-1599', '112.196.136.78', '2015-11-06 05:24:06', '2015-11-06 06:42:54am', 'el8v3k9e3nj8nihiu7k9ne92c7', '2015-11-06', 'AM', NULL, NULL),
(205, 'RAUU-1599', '192.96.201.167', '2015-11-06 06:41:16', '2015-11-06 08:58:53am', 'jbil7m61s1cnle3tl1pvvc2s23', '2015-11-06', 'AM', NULL, NULL),
(206, 'VUQQ-3341', '112.196.136.78', '2015-11-06 06:43:44', '', '6o2qu5gh0o44avf25inavkfgj0', '2015-11-06', 'AM', NULL, NULL),
(207, 'FTTP-8399', '112.196.136.78', '2015-11-06 06:47:47', '', 're9442468qf0lrctse636gkci5', '2015-11-06', 'TM', NULL, NULL),
(208, 'VUQQ-3341', '112.196.136.78', '2015-11-06 07:14:55', '2015-11-06 07:14:59am', '6bl8peqbeaja33mnirh4gar0o7', '2015-11-06', 'AM', NULL, NULL),
(209, 'VUQQ-3341', '112.196.136.78', '2015-11-06 07:15:08', '2015-11-06 07:15:15am', 'gnmc8rijckbtsj1bq2t76pq435', '2015-11-06', 'AM', NULL, NULL),
(210, 'VUQQ-3341', '112.196.136.78', '2015-11-06 07:15:22', '', 'sdidt1nnqo8hkg8qcun65h2tv2', '2015-11-06', 'AM', NULL, NULL),
(211, 'RAUU-1599', '112.196.136.78', '2015-11-06 09:08:07', '2015-11-06 10:21:24am', '1lph77l3au9p4m7ennopftuao5', '2015-11-06', 'AM', NULL, NULL),
(212, 'admin', '112.196.136.78', '2015-11-06 10:14:51', '', 'vo8dva4f7u3vr5rin1vamchpa5', '2015-11-06', 'AD', NULL, NULL),
(213, 'admin', '112.196.136.78', '2015-11-06 10:21:29', '', 'p0mp5ouvuj2db9ikafn053rpj5', '2015-11-06', 'AD', NULL, NULL),
(214, 'RAUU-1599', '112.196.136.78', '2015-11-06 10:24:34', '', '9im082kr8l28ad7d9tcejqnen0', '2015-11-06', 'AM', NULL, NULL),
(215, 'VUQQ-3341', '112.196.136.78', '2015-11-06 10:24:47', '', '2rmgp4ov7b2gkf9ll8t5m8psd5', '2015-11-06', 'AM', NULL, NULL),
(216, 'CXBZ-9999', '112.196.136.78', '2015-11-06 10:30:40', '', 'khn3turo1ar09d93cn5jtf7ue3', '2015-11-06', 'AM', NULL, NULL),
(217, 'admin', '112.196.136.78', '2015-11-07 00:50:31', '2015-11-07 01:44:11am', '3veeniik0h4so65g1ciso80dl3', '2015-11-07', 'AD', NULL, NULL),
(218, 'admin', '112.196.136.78', '2015-11-07 01:13:21', '2015-11-07 03:25:36am', '6oj6m6rcl97kjfk62f5el09d96', '2015-11-07', 'AD', NULL, NULL),
(219, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:14:40', '', 'kogaq9sg81ie4hcmrcqsfslvk3', '2015-11-07', 'QF', NULL, NULL),
(220, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:43:35', '', 'tvk1g9dr4aavsb5i199mf7e173', '2015-11-07', 'QF', NULL, NULL),
(221, 'admin', '112.196.136.78', '2015-11-07 01:44:42', '2015-11-07 03:03:49am', 'dr6mahra7414iqm80cj8e0a3u5', '2015-11-07', 'AD', NULL, NULL),
(222, 'FXIO-8905', '112.196.136.78', '2015-11-07 01:52:28', '2015-11-07 01:53:05am', 'mfhmhv6jrgdmu4i4cgvamliqm3', '2015-11-07', 'AM', NULL, NULL),
(223, 'RAUU-1599', '112.196.136.78', '2015-11-07 01:53:23', '2015-11-07 02:44:26am', 'bmiebsfaedvsi5ve8gh1lce3l0', '2015-11-07', 'AM', NULL, NULL),
(224, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:54:15', '2015-11-07 01:54:26am', '3dq1b1qbr37n5dbid5lvkhg525', '2015-11-07', 'QF', NULL, NULL),
(225, 'ZJBX-0533', '112.196.136.78', '2015-11-07 01:54:39', '2015-11-07 02:54:14am', 'k365d68rtc4nkgtmgaq8tejqb3', '2015-11-07', 'QF', NULL, NULL),
(226, 'ZJBX-0533', '112.196.136.78', '2015-11-07 02:43:36', '', '86bbjie4cp56tbpl1j8eu292s0', '2015-11-07', 'QF', NULL, NULL),
(227, 'ZJBX-0533', '112.196.136.78', '2015-11-07 02:44:37', '2015-11-07 05:39:30am', 'k9urlgo4l5l9qhoiqrud126j81', '2015-11-07', 'QF', NULL, NULL),
(228, 'VUQQ-3341', '112.196.136.78', '2015-11-07 02:54:27', '2015-11-07 02:54:54am', '5ul3t53qkrhaevueh8shv2uha7', '2015-11-07', 'AM', NULL, NULL),
(229, 'FXIO-8905', '112.196.136.78', '2015-11-07 02:55:04', '2015-11-07 02:55:09am', 'cq17r2ikaiccjuc1nd7bb4qvr2', '2015-11-07', 'AM', NULL, NULL),
(230, 'CXBZ-9999', '112.196.136.78', '2015-11-07 02:55:29', '2015-11-07 02:55:58am', 'ndj703gemb3nvpktltriptjfi4', '2015-11-07', 'AM', NULL, NULL),
(231, 'RAUU-1599', '112.196.136.78', '2015-11-07 02:56:09', '', 'qahu3b1r7bit6oc0tpvrafb5m1', '2015-11-07', 'AM', NULL, NULL),
(232, 'VUQQ-3341', '112.196.136.78', '2015-11-07 03:03:52', '2015-11-07 11:45:49am', 'rlet8hhdn2ji0k3f3vloi08gq0', '2015-11-07', 'AM', NULL, NULL),
(233, 'RAUU-1599', '112.196.136.78', '2015-11-07 03:25:57', '2015-11-07 04:32:24am', 'akknq8uaa33ed2hcucvujd0m53', '2015-11-07', 'AM', NULL, NULL),
(234, 'VUQQ-3341', '112.196.136.78', '2015-11-07 04:32:33', '2015-11-07 05:24:39am', 'lko84t1n5opc6b0u92qsrjmas1', '2015-11-07', 'AM', NULL, NULL),
(235, 'ZJBX-0533', '112.196.136.78', '2015-11-07 04:37:54', '', '46tnsjl5tfreubl4k8kidi47n6', '2015-11-07', 'QF', NULL, NULL),
(236, 'admin', '112.196.136.78', '2015-11-07 05:29:43', '2015-11-07 05:29:59am', 'b82c5skh3motos25frardebcj6', '2015-11-07', 'AD', NULL, NULL),
(237, 'FTTP-8399', '112.196.136.78', '2015-11-07 05:30:08', '2015-11-07 06:58:53am', '22p0jc3vd4pvavcfs7hm4cp8t6', '2015-11-07', 'TM', NULL, NULL),
(238, 'RAUU-1599', '112.196.136.78', '2015-11-07 05:36:42', '', 'g4hja5j1i7jokddce8ltlrp392', '2015-11-07', 'AM', NULL, NULL),
(239, 'RAUU-1599', '112.196.136.78', '2015-11-07 05:39:40', '2015-11-07 07:42:59am', '09a8vdanfubqqvngtld6e8qfs3', '2015-11-07', 'AM', NULL, NULL),
(240, 'RAUU-1599', '112.196.136.78', '2015-11-07 06:59:18', '2015-11-07 07:42:26am', 'isoj2figjuevs3lpi31m5k3vv1', '2015-11-07', 'AM', NULL, NULL),
(241, 'admin', '112.196.136.78', '2015-11-07 07:42:29', '2015-11-07 09:35:14am', 'sq25a7gfdnk09mne6t61qpj2a4', '2015-11-07', 'AD', NULL, NULL),
(242, 'VUQQ-3341', '112.196.136.78', '2015-11-07 07:43:09', '', 'l6t0trbc96kq1nlc8vvnk0ksj3', '2015-11-07', 'AM', NULL, NULL),
(243, 'ZJBX-0533', '112.196.136.78', '2015-11-07 09:35:26', '2015-11-07 10:06:10am', '0hgg0huru4qn41jg8hugejm252', '2015-11-07', 'QF', NULL, NULL),
(244, 'admin', '112.196.136.78', '2015-11-07 10:06:13', '', 's0e3hrb3ak12ab8aiairqkcat0', '2015-11-07', 'AD', NULL, NULL),
(245, 'admin', '112.196.136.78', '2015-11-07 10:07:30', '2015-11-07 10:08:34am', 'p1ed43hc4qjsq2hsq5vchdk7c3', '2015-11-07', 'AD', NULL, NULL),
(246, 'RAUU-1599', '112.196.136.78', '2015-11-07 10:08:39', '', '4r3rallt1v86u36onmnk7mpcr0', '2015-11-07', 'AM', NULL, NULL),
(247, 'admin', '112.196.136.78', '2015-11-07 11:46:18', '2015-11-07 11:46:29am', '04pjcsfiuknvinfg8bt8o2sn72', '2015-11-07', 'AD', NULL, NULL),
(248, 'FTTP-8399', '112.196.136.78', '2015-11-07 11:46:32', '2015-11-07 11:49:05am', '6h1l88mte3tsi0i5u4t6c2hcs7', '2015-11-07', 'TM', NULL, NULL),
(249, 'admin', '112.196.136.78', '2015-11-07 11:49:16', '2015-11-07 11:49:30am', '7a3dt52b57jaufa2jo7vghc0n3', '2015-11-07', 'AD', NULL, NULL),
(250, 'VUQQ-3341', '112.196.136.78', '2015-11-07 11:49:38', '', 'cis6ai0lqsqs2gmgkaa1j7a5k7', '2015-11-07', 'AM', NULL, NULL),
(251, 'admin', '27.97.116.5', '2015-11-08 10:48:07', '', 'e1ua27el08vsl640eoje3carl1', '2015-11-08', 'AD', NULL, NULL),
(252, 'admin', '112.196.136.78', '2015-11-09 00:47:40', '2015-11-09 01:21:17am', 'vg87cncboaj6brhun00pu87202', '2015-11-09', 'AD', NULL, NULL),
(253, 'admin', '112.196.136.78', '2015-11-09 00:47:43', '2015-11-09 04:47:17am', 'dolnsqiijl4p8ssjgidti6ljg7', '2015-11-09', 'AD', NULL, NULL),
(254, 'VUQQ-3341', '112.196.136.78', '2015-11-09 00:51:28', '2015-11-09 06:49:09am', 'cmrfp5kni8s06dgib7ua1d82t1', '2015-11-09', 'AM', NULL, NULL),
(255, 'RAUU-1599', '112.196.136.78', '2015-11-09 00:51:50', '', '0fsiefh3aht36lsou91ec5bk57', '2015-11-09', 'AM', NULL, NULL),
(256, 'ZJBX-0533', '112.196.136.78', '2015-11-09 00:53:17', '', 'elvbg1ie1q6626a9i0auaarsr4', '2015-11-09', 'QF', NULL, NULL),
(257, 'ZJBX-0533', '112.196.136.78', '2015-11-09 00:54:48', '', 'ebaku2t3a9h3pmgn2uv2fpqd10', '2015-11-09', 'QF', NULL, NULL),
(258, 'RAUU-1599', '112.196.136.78', '2015-11-09 01:18:15', '2015-11-09 02:49:31am', 'blr1i1keeqmnqpjinjmduod5i2', '2015-11-09', 'AM', NULL, NULL),
(259, 'RAUU-1599', '112.196.136.78', '2015-11-09 01:21:21', '', 'sp0kqr0beo40976pheiavnroo3', '2015-11-09', 'AM', NULL, NULL),
(260, 'admin', '112.196.136.78', '2015-11-09 02:17:32', '2015-11-09 02:20:33am', 's6la7tm582fmilk6lc1liee943', '2015-11-09', 'AD', NULL, NULL),
(261, 'RAUU-1599', '112.196.136.78', '2015-11-09 02:20:38', '2015-11-09 05:21:17am', 'c8r8qk7364gkpo0hr87ckd1281', '2015-11-09', 'AM', NULL, NULL),
(262, 'admin', '112.196.136.78', '2015-11-09 02:49:40', '2015-11-09 06:43:43am', '401rcurr6noeoqkp11a2mr2sj2', '2015-11-09', 'AD', NULL, NULL),
(263, 'admin', '112.196.136.78', '2015-11-09 03:36:02', '', 'f1o5g6fgp4uj7ds1rqtfhmd9r7', '2015-11-09', 'AD', NULL, NULL),
(264, 'ZJBX-0533', '112.196.136.78', '2015-11-09 04:47:21', '2015-11-09 05:21:23am', 'rjb5dpij5tld9oii0nd9lb5tk6', '2015-11-09', 'QF', NULL, NULL),
(265, 'admin', '112.196.136.78', '2015-11-09 05:09:09', '', 'k7nno78sjjf82uqs4nt518hl97', '2015-11-09', 'AD', NULL, NULL),
(266, 'admin', '112.196.136.78', '2015-11-09 05:21:19', '2015-11-09 05:32:48am', 'hfp59lj5ck7e6plm1mbtjshc12', '2015-11-09', 'AD', NULL, NULL),
(267, 'admin', '112.196.136.78', '2015-11-09 05:21:26', '2015-11-09 05:21:36am', 'i6ghlaioi8qn785jk6ckd934p2', '2015-11-09', 'AD', NULL, NULL),
(268, 'VUQQ-3341', '112.196.136.78', '2015-11-09 05:21:39', '', 'midpf80hnrlrulqk81gg5d6u87', '2015-11-09', 'AM', NULL, NULL),
(269, 'RAUU-1599', '112.196.136.78', '2015-11-09 05:33:12', '', 'b19fsl1ddmh7i9gsneddioccq2', '2015-11-09', 'AM', NULL, NULL),
(270, 'FTTP-8399', '112.196.136.78', '2015-11-09 06:44:09', '', 'ibo60fnb8vvv5q8f3oftq53if4', '2015-11-09', 'TM', NULL, NULL),
(271, 'FTTP-8399', '112.196.136.78', '2015-11-09 06:49:20', '', 'o8tkkclgi2ctk46j0n63lk5tv2', '2015-11-09', 'TM', NULL, NULL),
(272, 'admin', '112.196.136.78', '2015-11-10 02:10:16', '', 'vsm7lt9dust09a1o9ohl3pr3e2', '2015-11-10', 'AD', NULL, NULL),
(273, 'FXIO-8905', '112.196.136.78', '2015-11-10 02:14:20', '2015-11-10 02:21:10am', 'hfc2cka1iuaamfmjnso0q72t41', '2015-11-10', 'AM', NULL, NULL),
(274, 'RAUU-1599', '112.196.136.78', '2015-11-10 02:15:49', '', 'tp2cda2d8qo8pldjuv96e10jl2', '2015-11-10', 'AM', NULL, NULL),
(275, 'VUQQ-3341', '112.196.136.78', '2015-11-10 02:21:26', '', '8g2hchpcavcoffltc7qfemknf5', '2015-11-10', 'AM', NULL, NULL),
(276, 'VUQQ-3341', '112.196.136.78', '2015-11-10 07:03:16', '', 'n8sulnehdmsgttebvrqa8nq187', '2015-11-10', 'AM', NULL, NULL),
(277, 'admin', '112.196.136.78', '2015-11-10 07:41:52', '', 'a7l85cuntnjjhu1pcet3lshf06', '2015-11-10', 'AD', NULL, NULL),
(278, 'admin', '66.49.237.16', '2015-11-10 14:31:04', '2015-11-10 15:07:57pm', '9lhp05p14u7sh28shjm8hdu870', '2015-11-10', 'AD', NULL, NULL),
(279, 'FXIO-8905', '66.49.237.16', '2015-11-10 14:53:53', '', '447rgidpk0pnp566g0kaeu0142', '2015-11-10', 'AM', NULL, NULL),
(280, 'FXIO-8905', '66.49.237.16', '2015-11-10 14:56:09', '2015-11-10 15:06:19pm', '9aj8nrh02kcf715hsin0eb0sn2', '2015-11-10', 'AM', NULL, NULL),
(281, 'RAUU-1599', '66.49.237.16', '2015-11-10 15:07:33', '2015-11-10 15:07:48pm', 'j97fdhr68u0si6214763h6mlo3', '2015-11-10', 'AM', NULL, NULL),
(282, 'RAUU-1599', '66.49.237.16', '2015-11-10 15:07:52', '', 'p0eg98a5vbd0ttl0pp05jeq1v4', '2015-11-10', 'AM', NULL, NULL),
(283, 'RAUU-1599', '66.49.237.16', '2015-11-10 15:08:00', '2015-11-10 15:08:03pm', 'rkvacndllkpfff0vsaofh10pa4', '2015-11-10', 'AM', NULL, NULL),
(284, 'admin', '66.49.237.16', '2015-11-10 15:08:09', '', 'kgp5hukf1o09e26tts3v1q1c04', '2015-11-10', 'AD', NULL, NULL),
(285, 'admin', '66.49.237.16', '2015-11-10 16:05:44', '', 'bttsits146cf9id7dldlaghc73', '2015-11-10', 'AD', NULL, NULL),
(286, 'admin', '66.49.237.16', '2015-11-10 16:06:14', '', '4g2pevlpcgtcc3gjm47mojsqm1', '2015-11-10', 'AD', NULL, NULL),
(287, 'admin', '112.196.136.78', '2015-11-11 01:35:26', '2015-11-11 01:38:18am', '1235kmrko3ombedbcubd2tn2s6', '2015-11-11', 'AD', NULL, NULL),
(288, 'admin', '112.196.136.78', '2015-11-11 01:38:22', '2015-11-11 01:38:26am', 'bb5f9m1m8hhfpf2spbdm59ehb5', '2015-11-11', 'AD', NULL, NULL),
(289, 'admin', '112.196.136.78', '2015-11-11 01:55:10', '', 'ls7e06h52k43sikbv4mlndlf52', '2015-11-11', 'AD', NULL, NULL),
(290, 'admin', '66.49.237.16', '2015-11-11 10:56:13', '', 'bfdjtj23ruql45j066emehp026', '2015-11-11', 'AD', NULL, NULL),
(291, 'admin', '66.49.237.16', '2015-11-11 13:16:04', '', 'r6c39nvq98aegd2s1f04djr2i3', '2015-11-11', 'AD', NULL, NULL),
(292, 'FXIO-8905', '66.49.237.16', '2015-11-11 13:16:25', '2015-11-11 13:25:55pm', 'i0fesmvivfvt3bs7e3sneod452', '2015-11-11', 'AM', NULL, NULL),
(293, 'FXIO-8905', '66.49.237.16', '2015-11-11 13:26:07', '2015-11-11 13:26:32pm', 'efcuqcbsr8ocorhneq9rqohhp3', '2015-11-11', 'AM', NULL, NULL),
(294, 'FXIO-8905', '66.49.237.16', '2015-11-11 13:26:37', '', 'o6q56paq2fe55qgvu1eon509e5', '2015-11-11', 'AM', NULL, NULL),
(295, 'admin', '66.49.237.16', '2015-11-12 12:47:56', '', 'k04c0pmbt9dich8btsdimdssb3', '2015-11-12', 'AD', NULL, NULL),
(296, 'admin', '59.161.188.117', '2015-11-12 13:43:22', '', 'jr51mje4ntp2ok0r4sefpfnv81', '2015-11-12', 'AD', NULL, NULL),
(297, 'admin', '112.196.136.78', '2015-11-14 01:06:41', '', '9dc3abfa1ehjnek6fuel4ael27', '2015-11-14', 'AD', NULL, NULL),
(298, 'admin', '112.196.136.78', '2015-11-15 22:52:18', '2015-11-15 23:24:28pm', 'vfetm6r3b2jh2iodo517mikts2', '2015-11-15', 'AD', NULL, NULL),
(299, 'CXBZ-9999', '112.196.136.78', '2015-11-15 23:16:17', '2015-11-15 23:16:49pm', 'qdph3l3an4dko9vpbj440fdf77', '2015-11-15', 'AM', NULL, NULL),
(300, 'FXIO-8905', '112.196.136.78', '2015-11-15 23:17:07', '2015-11-15 23:20:02pm', 'h04hspagligs15svcqulsmt0p4', '2015-11-15', 'AM', NULL, NULL),
(301, 'RAUU-1599', '112.196.136.78', '2015-11-15 23:20:19', '2015-11-16 03:54:50am', 'k58401043c59cbipfal8433j85', '2015-11-15', 'AM', NULL, NULL),
(302, 'admin', '112.196.136.78', '2015-11-15 23:24:31', '', '7n1i9s54430um74ji804tevq06', '2015-11-15', 'AD', NULL, NULL),
(303, 'admin', '112.196.136.78', '2015-11-16 00:45:47', '', 'ukafcgmlai4jsofdhbdsb595l0', '2015-11-16', 'AD', NULL, NULL),
(304, 'CXBZ-9999', '112.196.136.78', '2015-11-16 00:46:45', '2015-11-16 00:47:33am', 'm671va9963d7jhb9hlj4v8q4h6', '2015-11-16', 'AM', NULL, NULL),
(305, 'VUQQ-3341', '112.196.136.78', '2015-11-16 00:47:40', '', 'vdfa54bcbf8sc8ne6s1vge0j96', '2015-11-16', 'AM', NULL, NULL),
(306, 'admin', '112.196.136.78', '2015-11-16 03:12:34', '2015-11-16 03:47:00am', '56qehkq203d0i4supmo4i6m525', '2015-11-16', 'AD', NULL, NULL),
(307, 'FXIO-8905', '112.196.136.78', '2015-11-16 03:47:12', '', '1rsveijddsnh74u9tfnjstljg4', '2015-11-16', 'AM', NULL, NULL),
(308, 'admin', '112.196.136.78', '2015-11-16 03:54:54', '2015-11-16 06:25:33am', '7cglicipko2l31adjons0q4td6', '2015-11-16', 'AD', NULL, NULL),
(309, 'admin', '112.196.136.78', '2015-11-16 04:11:19', '', 'h3qeu0un4o3hlhgijiomjof6p4', '2015-11-16', 'AD', NULL, NULL),
(310, 'admin', '112.196.136.78', '2015-11-16 05:22:31', '', 'kk94lf3niifr3updldiaooa130', '2015-11-16', 'AD', NULL, NULL),
(311, 'admin', '112.196.136.78', '2015-11-16 06:24:57', '2015-11-16 06:33:54am', '8jfuoq66e34434k5j4u2e5kbt0', '2015-11-16', 'AD', NULL, NULL),
(312, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:25:42', '', '6hkpdr7arusjgeftrau4j1cft1', '2015-11-16', 'QF', NULL, NULL),
(313, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:25:50', '', '0i01db9loqrsmujjr1smkiblm5', '2015-11-16', 'QF', NULL, NULL),
(314, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:26:08', '', 't4vaa53uf0rjvmhu4vpp3fgfc0', '2015-11-16', 'QF', NULL, NULL),
(315, 'admin', '112.196.136.78', '2015-11-16 06:26:56', '2015-11-16 06:27:26am', '0oo84j98g81mq9pgtdhchasd13', '2015-11-16', 'AD', NULL, NULL),
(316, 'CXBZ-9999', '112.196.136.78', '2015-11-16 06:27:34', '', 'nop7kllulo89bi8vks2542fr06', '2015-11-16', 'AM', NULL, NULL),
(317, 'ZJBX-0533', '112.196.136.78', '2015-11-16 06:34:02', '', 'koqehudbc272jvaojgoel6rm84', '2015-11-16', 'QF', NULL, NULL),
(318, 'admin', '112.196.136.78', '2015-11-16 07:05:58', '2015-11-16 07:11:27am', '4vu049g0ejg66m97ngo5ilb6a1', '2015-11-16', 'AD', NULL, NULL),
(319, 'CXBZ-9999', '112.196.136.78', '2015-11-16 07:06:48', '2015-11-16 07:07:18am', 'h03o91njehip461pv3n76pqk05', '2015-11-16', 'AM', NULL, NULL),
(320, 'FTTP-8399', '112.196.136.78', '2015-11-16 07:07:27', '2015-11-16 07:07:56am', 'gogthrs1snlaacgmmasrf25tc4', '2015-11-16', 'TM', NULL, NULL),
(321, 'ZJBX-0533', '112.196.136.78', '2015-11-16 07:08:05', '', 'cme3ai3ei712qd1uqmq01f8aa3', '2015-11-16', 'QF', NULL, NULL),
(322, 'ZJBX-0533', '112.196.136.78', '2015-11-16 07:09:43', '2015-11-16 07:11:38am', '0ggsunn4d4usr160ic612uk7g4', '2015-11-16', 'QF', NULL, NULL),
(323, 'admin', '1.22.133.140', '2015-11-16 09:10:56', '2015-11-16 09:16:53am', 'gd8p5qo6eon292jrs2e3qmola1', '2015-11-16', 'AD', NULL, NULL),
(324, 'admin', '66.49.237.16', '2015-11-16 11:28:28', '', 'okcar2imgba4u346o1n45s93b5', '2015-11-16', 'AD', NULL, NULL),
(325, 'FXIO-8905', '66.49.237.16', '2015-11-16 11:44:19', '2015-11-16 11:55:49am', 'the9315kk1tkk88odt6c0q9k81', '2015-11-16', 'AM', NULL, NULL),
(326, 'FXIO-8905', '66.49.237.16', '2015-11-16 11:56:00', '', '0l38p93mmih5aebnolb3h2qtl6', '2015-11-16', 'AM', NULL, NULL),
(327, 'admin', '14.98.102.29', '2015-11-16 12:46:27', '', 'hvq4mmcki6ldlonvb892km7dn1', '2015-11-16', 'AD', NULL, NULL),
(328, 'VUQQ-3341', '14.98.102.29', '2015-11-16 13:05:01', '', 'mohhal8lflu7a6hmqtfu41ape2', '2015-11-16', 'AM', NULL, NULL),
(329, 'admin', '112.196.136.78', '2015-11-16 23:30:19', '', 'ac31l9bd3l2dgphvs4ft2tmm54', '2015-11-16', 'AD', NULL, NULL),
(330, 'VUQQ-3341', '112.196.136.78', '2015-11-16 23:34:12', '2015-11-16 23:34:20pm', '1ba9tnf7jd8pi22io8dktjahl5', '2015-11-16', 'AM', NULL, NULL),
(331, 'RAUU-1599', '112.196.136.78', '2015-11-16 23:34:40', '2015-11-16 23:34:54pm', 'ejr8911h4cn3okr8k440upont5', '2015-11-16', 'AM', NULL, NULL),
(332, 'FMVI-7264', '112.196.136.78', '2015-11-16 23:35:03', '2015-11-16 23:35:10pm', 'r50n0qt3qoc8bfmv2pgjng70s6', '2015-11-16', 'AM', NULL, NULL),
(333, 'FXIO-8905', '112.196.136.78', '2015-11-16 23:35:28', '2015-11-17 03:12:59am', 'i562un2q3dei0lb8pm34hg7f94', '2015-11-16', 'AM', NULL, NULL),
(334, 'admin', '112.196.136.78', '2015-11-17 00:18:04', '2015-11-17 00:18:25am', '9r17f8tg3qj8tb25adni3nqdh3', '2015-11-17', 'AD', NULL, NULL),
(335, 'FMVI-7264', '112.196.136.78', '2015-11-17 00:18:28', '2015-11-17 00:19:18am', 'jfraaufgioas6s8fqtboia7415', '2015-11-17', 'AM', NULL, NULL),
(336, 'admin', '112.196.136.78', '2015-11-17 00:19:03', '', 'dklm8lgoe38utels85301d6r45', '2015-11-17', 'AD', NULL, NULL),
(337, 'FXIO-8905', '112.196.136.78', '2015-11-17 00:19:25', '', 'a0j03anem4klvm7fobogbsikk4', '2015-11-17', 'AM', NULL, NULL),
(338, 'FXIO-8905', '112.196.136.78', '2015-11-17 03:15:32', '', '6vemlkso76jl11katt8379dm17', '2015-11-17', 'AM', NULL, NULL),
(339, 'admin', '66.49.237.16', '2015-11-17 13:52:06', '', 'br9qiggvb3dalpqu0m97n46h26', '2015-11-17', 'AD', NULL, NULL),
(340, 'FXIO-8905', '66.49.237.16', '2015-11-17 13:52:24', '', 'e20gnmugh2hg4ous2agdsss0a5', '2015-11-17', 'AM', NULL, NULL),
(341, 'admin', '66.49.237.16', '2015-11-17 13:56:14', '', 'mdfaprmtuh7mgor7813ndu5a01', '2015-11-17', 'AD', NULL, NULL),
(342, 'admin', '14.98.228.196', '2015-11-17 14:04:20', '', 'ehe0ueugeloconu9696vfc4ps1', '2015-11-17', 'AD', NULL, NULL),
(343, 'admin', '112.196.136.78', '2015-11-17 23:43:11', '2015-11-18 00:32:52am', '1gpk18tpmglu7q0p9h6jdk03p0', '2015-11-17', 'AD', NULL, NULL),
(344, 'FXIO-8905', '112.196.136.78', '2015-11-17 23:44:53', '2015-11-18 00:42:00am', 'l0h5s977p77fad7b7ikveaekd1', '2015-11-17', 'AM', NULL, NULL),
(345, 'ZJBX-0533', '112.196.136.78', '2015-11-17 23:53:01', '', 'g0vi6u8h2fpfkokjrcpjhhfvb5', '2015-11-17', 'QF', NULL, NULL),
(346, 'ZJBX-0533', '112.196.136.78', '2015-11-18 00:33:00', '2015-11-18 01:58:13am', '56d2428hbilkn3tsb7ldrgmrv5', '2015-11-18', 'QF', NULL, NULL),
(347, 'admin', '112.196.136.78', '2015-11-18 00:42:05', '', 'u6t8cbq55sg0b8o13hcb3g6601', '2015-11-18', 'AD', NULL, NULL),
(348, 'admin', '112.196.136.78', '2015-11-18 01:58:16', '', '4ah8a76jcl8vpl3hre52srpg96', '2015-11-18', 'AD', NULL, NULL),
(349, 'admin', '112.196.136.78', '2015-11-18 04:03:55', '2015-11-18 04:08:36am', 'r347qec9vuf3ondr526hoqk7v6', '2015-11-18', 'AD', NULL, NULL),
(350, 'admin', '112.196.136.78', '2015-11-18 04:09:11', '2015-11-18 05:04:07am', 'd5kspsqs83dj18idkbcqr16dk7', '2015-11-18', 'AD', NULL, NULL),
(351, 'FXIO-8905', '112.196.136.78', '2015-11-18 04:09:38', '2015-11-18 05:02:46am', 'mt1iokkh3b18ms9hokpd6cs1k7', '2015-11-18', 'AM', NULL, NULL),
(352, 'VUQQ-3341', '112.196.136.78', '2015-11-18 05:02:58', '2015-11-18 05:45:56am', 'eqjbms2enugjjctqibkg85ki83', '2015-11-18', 'AM', NULL, NULL),
(353, 'FXIO-8905', '112.196.136.78', '2015-11-18 05:04:21', '', 't6h5ti0b9p2qlvvld3m5rmr873', '2015-11-18', 'AM', NULL, NULL),
(354, 'admin', '112.196.136.78', '2015-11-18 05:18:33', '', '7m3rqagoedqskbn82no2s4ikb4', '2015-11-18', 'AD', NULL, NULL),
(355, 'FXIO-8905', '112.196.136.78', '2015-11-18 05:23:49', '', '6mahb719nspaadvi4r0gm0p6j5', '2015-11-18', 'AM', NULL, NULL),
(356, 'admin', '112.196.136.78', '2015-11-18 05:45:58', '2015-11-18 06:09:41am', 'q0k3rid3tjlcuh82i4sr537hm3', '2015-11-18', 'AD', NULL, NULL),
(357, 'VUQQ-3341', '112.196.136.78', '2015-11-18 06:52:55', '', '2au8d50qcl8rok01p1tle2ohc1', '2015-11-18', 'AM', NULL, NULL),
(358, 'admin', '112.196.136.78', '2015-11-18 07:41:03', '', 'hh06imq98j8e8hp709ta5lq230', '2015-11-18', 'AD', NULL, NULL),
(359, 'admin', '66.49.237.16', '2015-11-18 15:11:56', '2015-11-18 15:12:10pm', 'tgacm9t46s9ao4t54djeb61na2', '2015-11-18', 'AD', NULL, NULL),
(360, 'FXIO-8905', '66.49.237.16', '2015-11-18 15:12:12', '2015-11-18 15:14:13pm', 'v9q0d10eu16vesjt9n37leloq7', '2015-11-18', 'AM', NULL, NULL),
(361, 'admin', '66.49.237.16', '2015-11-18 15:14:20', '2015-11-18 15:17:43pm', 'm1p02j9irmp1bc95hmqfphirr4', '2015-11-18', 'AD', NULL, NULL),
(362, 'FXIO-8905', '66.49.237.16', '2015-11-18 15:17:46', '2015-11-18 15:21:56pm', 'kjero49jmns1u9lr9bk7umio87', '2015-11-18', 'AM', NULL, NULL),
(363, 'admin', '66.49.237.16', '2015-11-18 15:19:26', '2015-11-18 15:19:40pm', 'gmah7vgb0qv6vt38eg0l66o2u5', '2015-11-18', 'AD', NULL, NULL),
(364, 'ZJBX-0533', '66.49.237.16', '2015-11-18 15:19:43', '', 'urp9cmldhqaippbcj5pfakrgb0', '2015-11-18', 'QF', NULL, NULL),
(365, 'admin', '66.49.237.16', '2015-11-18 15:22:02', '2015-11-18 15:25:19pm', 'gp0f8iv8i1bnoiacbam2172ou5', '2015-11-18', 'AD', NULL, NULL),
(366, 'FMVI-7264', '66.49.237.16', '2015-11-18 15:25:22', '', '9r9n50m7fbch3ir36rccj8g302', '2015-11-18', 'AM', NULL, NULL),
(367, 'admin', '112.196.136.78', '2015-11-18 23:33:20', '2015-11-19 04:52:42am', 'qk3rgjb3v551g6tksvn5eoj1j2', '2015-11-18', 'AD', NULL, NULL),
(368, 'admin', '112.196.136.78', '2015-11-18 23:35:13', '', '9mejdtj16kam4e7cc9rnv5dli0', '2015-11-18', 'AD', NULL, NULL),
(369, 'admin', '112.196.136.78', '2015-11-19 01:06:54', '', 's4da6vpvdmgan97mjncfc2d264', '2015-11-19', 'AD', NULL, NULL),
(370, 'FMVI-7264', '112.196.136.78', '2015-11-19 01:16:42', '', '2dn5i8jacvheciabqn6mm6o2m1', '2015-11-19', 'AM', NULL, NULL),
(371, 'FXIO-8905', '112.196.136.78', '2015-11-19 02:20:09', '', 'tcpqac5l96oph360eptgh3esk4', '2015-11-19', 'AM', NULL, NULL),
(372, 'ZJBX-0533', '112.196.136.78', '2015-11-19 04:52:52', '2015-11-19 07:39:23am', '9g1smfbhh01ruj0l1re8up3q23', '2015-11-19', 'QF', NULL, NULL),
(373, 'admin', '112.196.136.78', '2015-11-19 06:03:54', '2015-11-19 06:05:04am', '7p5ur2b84g6avenupju21c0c76', '2015-11-19', 'AD', NULL, NULL),
(374, 'FTTP-8399', '112.196.136.78', '2015-11-19 06:05:14', '', 'la3b6jrgtralecih1fe83kghs1', '2015-11-19', 'TM', NULL, NULL),
(375, 'FXIO-8905', '104.131.14.167', '2015-11-19 06:15:06', '2015-11-19 06:15:35am', 'dv7djbn4sh74aqqe5ofgo4iu23', '2015-11-19', 'AM', NULL, NULL),
(376, 'VUQQ-3341', '104.131.14.167', '2015-11-19 06:15:45', '2015-11-19 06:16:01am', 'cku1haicc9rkul5onsnvbpa9k0', '2015-11-19', 'AM', NULL, NULL),
(377, 'RAUU-1599', '104.131.14.167', '2015-11-19 06:16:11', '2015-11-19 06:16:36am', '66rr53irvk4tps10ithtjbs1q6', '2015-11-19', 'AM', NULL, NULL);
INSERT INTO `mst_userlogon` (`logonid`, `userid`, `ipaddress`, `starttime`, `finishtime`, `sessionid`, `logondate`, `roletype`, `created_at`, `updated_at`) VALUES
(378, 'FMVI-7264', '104.131.14.167', '2015-11-19 06:16:46', '', 'ktgdo9uos15925trlrov8dj031', '2015-11-19', 'AM', NULL, NULL),
(379, 'FTTP-8399', '112.196.136.78', '2015-11-19 06:52:24', '', 'ej0r0hs20dt9mg11j19h5c9464', '2015-11-19', 'TM', NULL, NULL),
(380, 'FTTP-8399', '112.196.136.78', '2015-11-19 07:30:42', '', '0d0f2iqsffp13votlb6v4mg5c2', '2015-11-19', 'TM', NULL, NULL),
(381, 'FTTP-8399', '112.196.136.78', '2015-11-19 07:31:59', '', 'i2iffn74gkfapdn98jdrvppho6', '2015-11-19', 'TM', NULL, NULL),
(382, 'admin', '112.196.136.78', '2015-11-19 07:39:25', '', 'cichfsdj7b4rn5rsk6dlevklt7', '2015-11-19', 'AD', NULL, NULL),
(383, 'admin', '112.196.136.78', '2015-11-19 08:27:52', '2015-11-19 08:39:53am', 'd95scbsigeoba8s31s29r0gr07', '2015-11-19', 'AD', NULL, NULL),
(384, 'FXIO-8905', '112.196.136.78', '2015-11-19 08:28:19', '', 't49akj8e450t5ar389lh1qbc26', '2015-11-19', 'AM', NULL, NULL),
(385, 'admin', '112.196.136.78', '2015-11-19 08:39:56', '2015-11-19 08:40:07am', 'cp9dgte8r73rtf9suh6meiocc7', '2015-11-19', 'AD', NULL, NULL),
(386, 'FXIO-8905', '112.196.136.78', '2015-11-19 08:40:14', '2015-11-19 08:43:59am', 'd918pe4apnggqsm6hks8tojal4', '2015-11-19', 'AM', NULL, NULL),
(387, 'admin', '112.196.136.78', '2015-11-19 08:44:02', '2015-11-19 08:50:08am', '5g6sj3bk4ac9kl0fs5dmdanq42', '2015-11-19', 'AD', NULL, NULL),
(388, 'FXIO-8905', '112.196.136.78', '2015-11-19 08:50:19', '', 'jnjfiasb9da1g70hoad6dch7b5', '2015-11-19', 'AM', NULL, NULL),
(389, 'admin', '66.49.237.16', '2015-11-19 15:58:53', '', 'nsb02t877lq967810j72l1hah7', '2015-11-19', 'AD', NULL, NULL),
(390, 'admin', '66.49.237.16', '2015-11-19 16:02:11', '', 'ero1624mt3pgnmpvugt2ac8o32', '2015-11-19', 'AD', NULL, NULL),
(391, 'FXIO-8905', '66.49.237.16', '2015-11-19 16:03:44', '', 'cbimgufa47qk74upafimimve75', '2015-11-19', 'AM', NULL, NULL),
(392, 'admin', '112.196.136.78', '2015-11-19 23:58:30', '', 'bp1hiujm8b1l1d726cooe6f8o4', '2015-11-19', 'AD', NULL, NULL),
(393, 'admin', '112.196.136.78', '2015-11-20 00:39:09', '', 'fkel2dnuiiknfjdi9015mm8s74', '2015-11-20', 'AD', NULL, NULL),
(394, 'admin', '104.236.57.54', '2015-11-20 01:00:26', '2015-11-20 03:11:54am', 'fm4sg0ssommplbirnh7k4e0sc7', '2015-11-20', 'AD', NULL, NULL),
(395, 'FXIO-8905', '104.236.57.54', '2015-11-20 03:12:07', '', '26mt3goi5n5absg51mlujh6p86', '2015-11-20', 'AM', NULL, NULL),
(396, 'ZJBX-0533', '112.196.136.78', '2015-11-20 03:32:20', '', 'esdslg8ekp3ia9lctav736vmo5', '2015-11-20', 'QF', NULL, NULL),
(397, 'admin', '112.196.136.78', '2015-11-20 04:46:27', '2015-11-20 04:47:47am', 'q8ga2qau1pjmior8k2vjbcjj25', '2015-11-20', 'AD', NULL, NULL),
(398, 'admin', '112.196.136.78', '2015-11-20 04:47:52', '2015-11-20 04:47:58am', 'obc5m3hf5a1oj11skv0ho81l60', '2015-11-20', 'AD', NULL, NULL),
(399, 'VUQQ-3341', '112.196.136.78', '2015-11-20 04:48:12', '', '0rolqskp80s7p42muhofmm8q74', '2015-11-20', 'AM', NULL, NULL),
(400, 'admin', '66.49.237.16', '2015-11-20 15:08:55', '', 'nji2f6gihijeih9gdrhg79fbv0', '2015-11-20', 'AD', NULL, NULL),
(401, 'FXIO-8905', '66.49.237.16', '2015-11-20 15:09:54', '2015-11-20 15:10:16pm', 'hbje6n6dtsg814qttk6ijpfgh7', '2015-11-20', 'AM', NULL, NULL),
(402, 'ZJBX-0533', '66.49.237.16', '2015-11-20 15:10:22', '2015-11-20 15:49:37pm', 'crtb5qgpvm1p1k61quqn2t7bq4', '2015-11-20', 'QF', NULL, NULL),
(403, 'FXIO-8905', '66.49.237.16', '2015-11-20 15:49:47', '', 'lp0hpe8hjc3i7vuvs806po9g92', '2015-11-20', 'AM', NULL, NULL),
(404, 'admin', '112.196.136.78', '2015-11-22 23:22:12', '', 'ksppokhtaegvaac74k27m84i71', '2015-11-22', 'AD', NULL, NULL),
(405, 'FXIO-8905', '112.196.136.78', '2015-11-22 23:55:04', '2015-11-23 00:00:11am', 'f7he2c89v4fg7j3o43pb851hp6', '2015-11-22', 'AM', NULL, NULL),
(406, 'FTTP-8399', '104.131.161.230', '2015-11-22 23:57:54', '2015-11-23 00:56:18am', '88h7lms1tgirki1mmu2goc3pj6', '2015-11-22', 'TM', NULL, NULL),
(407, 'VUQQ-3341', '112.196.136.78', '2015-11-23 00:00:21', '2015-11-23 04:51:46am', '69rpagoi8debbep5hlm6jrdvo6', '2015-11-23', 'AM', NULL, NULL),
(408, 'admin', '112.196.136.78', '2015-11-23 00:10:13', '', 'ic9vth7tkn2vg2f8rdt5aqpvp5', '2015-11-23', 'AD', NULL, NULL),
(409, 'FMVI-7264', '112.196.136.78', '2015-11-23 00:11:09', '', 'te8rrqsriojtunehvb58n7kc11', '2015-11-23', 'AM', NULL, NULL),
(410, 'FXIO-8905', '112.196.136.78', '2015-11-23 01:01:08', '', 'ekn0m4otq7b1ca22r9l0fa8tj0', '2015-11-23', 'AM', NULL, NULL),
(411, 'admin', '112.196.136.78', '2015-11-23 01:14:04', '', 'en885o1ugpqfrmlgqkitvv3tt5', '2015-11-23', 'AD', NULL, NULL),
(412, 'ZJBX-0533', '104.131.161.230', '2015-11-23 01:31:34', '', 'soiila8v3d4ur7iosro0lgh5m4', '2015-11-23', 'QF', NULL, NULL),
(413, 'FXIO-8905', '112.196.136.78', '2015-11-23 02:14:13', '', '8tirqlv322msde0k2vlv99ftf4', '2015-11-23', 'AM', NULL, NULL),
(414, 'RAUU-1599', '112.196.136.78', '2015-11-23 02:14:29', '', '7odksbacnpr4plnhh3a4ao3n15', '2015-11-23', 'AM', NULL, NULL),
(415, 'ZJBX-0533', '112.196.136.78', '2015-11-23 03:34:13', '', 'ucqjhqq4m0kf3dcduu81v7i2u5', '2015-11-23', 'QF', NULL, NULL),
(416, 'FMVI-7264', '112.196.136.78', '2015-11-23 03:54:50', '', 'vrd8p3ksq3rpdaa35d6vtd36i4', '2015-11-23', 'AM', NULL, NULL),
(417, 'FXIO-8905', '112.196.136.78', '2015-11-23 04:52:06', '', 'su6ds7otpfqqjsd8tpqmleeib2', '2015-11-23', 'AM', NULL, NULL),
(418, 'FMVI-7264', '112.196.136.78', '2015-11-23 08:11:03', '2015-11-23 08:11:19am', 'ej413h15k3mvajjm9t32npr7i3', '2015-11-23', 'AM', NULL, NULL),
(419, 'FXIO-8905', '112.196.136.78', '2015-11-23 08:11:43', '', '9o0mtl6s2frahaln043qb5ii27', '2015-11-23', 'AM', NULL, NULL),
(420, 'admin', '66.49.237.16', '2015-11-24 14:07:36', '', '2kr6vhmck8j1mvn1n51srumah5', '2015-11-24', 'AD', NULL, NULL),
(421, 'admin', '112.196.136.78', '2015-11-25 01:32:52', '', '8foe99vc4uvil1b7ngrm1bvvn1', '2015-11-25', 'AD', NULL, NULL),
(422, 'FMVI-7264', '112.196.136.44', '2015-11-26 07:59:09', '2015-11-26 07:59:34am', '6ci6brmlcgtjt3ncf9dq636do3', '2015-11-26', 'AM', NULL, NULL),
(423, 'admin', '112.196.136.44', '2015-11-26 07:59:45', '', 'odvfkqig37ns3h9k7vbllb72l0', '2015-11-26', 'AD', NULL, NULL),
(424, 'admin', '66.49.157.92', '2015-11-26 08:00:18', '', 'h385t9ddh9nbjlfftj96ti05h7', '2015-11-26', 'AD', NULL, NULL),
(425, 'admin', '112.196.136.44', '2015-11-27 02:07:00', '2015-11-27 02:28:30am', 'k7fhhjc3871dl6ertbk897kjo4', '2015-11-27', 'AD', NULL, NULL),
(426, 'admin', '112.196.136.78', '2015-11-27 02:25:28', '2015-11-27 03:49:43am', 'c3upht443lc43kvegqi9mf0kr2', '2015-11-27', 'AD', NULL, NULL),
(427, 'admin', '112.196.136.44', '2015-11-27 03:17:18', '2015-11-27 03:18:05am', 'fbtcj7bm5047cf1s0ngrfb1370', '2015-11-27', 'AD', NULL, NULL),
(428, 'admin', '112.196.136.44', '2015-11-27 03:18:38', '2015-11-27 03:48:47am', 'ubt0ltuu4g14916kgh2rr0n610', '2015-11-27', 'AD', NULL, NULL),
(429, 'admin', '112.196.136.78', '2015-11-27 03:58:25', '2015-11-27 03:58:51am', 'psuiunu16fgk2t4058504ssc34', '2015-11-27', 'AD', NULL, NULL),
(430, 'admin', '112.196.136.44', '2015-11-27 04:06:00', '', 'nperir5i6ebk3s1j0d6obc9fh2', '2015-11-27', 'AD', NULL, NULL),
(431, 'admin', '112.196.136.44', '2015-11-27 04:06:48', '', '2s0h9edchi3imq49ch33qplkl7', '2015-11-27', 'AD', NULL, NULL),
(432, 'admin', '112.196.136.44', '2015-11-27 04:15:49', '', '0bl75ff7phcrkcg79krhj7opi5', '2015-11-27', 'AD', NULL, NULL),
(433, 'admin', '14.98.12.253', '2015-11-27 07:48:44', '', '3ejds2gjatb9665o71df2g2tt5', '2015-11-27', 'AD', NULL, NULL),
(434, 'admin', '66.49.237.16', '2015-11-27 07:49:41', '2015-11-27 07:55:50am', 'h4imh299sv9r5e83lssark8s47', '2015-11-27', 'AD', NULL, NULL),
(435, 'admin', '112.196.136.78', '2015-11-27 07:52:24', '', 'al7o5dfn1ujlo43l4p4lnnrl47', '2015-11-27', 'AD', NULL, NULL),
(436, 'admin', '66.49.237.16', '2015-11-27 07:56:01', '2015-11-27 07:56:42am', 'dapi4ojssfoa3e8t54t5bnc027', '2015-11-27', 'AD', NULL, NULL),
(437, 'admin', '14.98.12.253', '2015-11-27 08:08:11', '', 'nntpk8bdgng4mpi36qcpreeqv2', '2015-11-27', 'AD', NULL, NULL),
(438, 'admin', '14.98.12.253', '2015-11-27 08:10:45', '2015-11-27 08:11:59am', 'rteme37ohkqdn59nt1a9760606', '2015-11-27', 'AD', NULL, NULL),
(439, 'admin', '14.98.12.253', '2015-11-27 08:12:04', '', '16rbrlig71pqkso5pfadtn2tb7', '2015-11-27', 'AD', NULL, NULL),
(440, 'admin', '59.161.69.157', '2015-11-27 08:50:08', '', 'i006pa89jis11cnp7icf45pn11', '2015-11-27', 'AD', NULL, NULL),
(441, 'admin', '112.196.136.44', '2015-11-28 05:21:36', '', 'f6s1mkcbac4mupjpdomde9fhe4', '2015-11-28', 'AD', NULL, NULL),
(442, 'admin', '112.196.136.78', '2015-11-29 22:52:40', '2015-11-29 23:11:16pm', 'omv4v7j72ckofncdncoppuc3h0', '2015-11-29', 'AD', NULL, NULL),
(443, 'admin', '112.196.136.44', '2015-11-29 23:12:11', '2015-11-29 23:36:15pm', 'm3t2sp2438beo94ssfj2h32p20', '2015-11-29', 'AD', NULL, NULL),
(444, 'admin', '112.196.136.78', '2015-11-29 23:12:16', '2015-11-29 23:12:39pm', 'jd3uk297ntn93u1hf45ev9s5i6', '2015-11-29', 'AD', NULL, NULL),
(445, 'admin', '112.196.136.78', '2015-11-29 23:15:35', '2015-11-29 23:32:23pm', 'apedtv53jjd4t3cj0eigp9eh42', '2015-11-29', 'AD', NULL, NULL),
(446, 'admin', '112.196.136.78', '2015-11-29 23:27:51', '', '14up6es8okg63tjfshar02r143', '2015-11-29', 'AD', NULL, NULL),
(447, 'admin', '112.196.136.78', '2015-11-29 23:32:50', '2015-11-29 23:33:02pm', 'sac42is08mv83i03f6kjofsnp2', '2015-11-29', 'AD', NULL, NULL),
(448, 'admin', '112.196.136.78', '2015-11-29 23:37:06', '', 'bf78ssi7l9q24aiett7bs980a2', '2015-11-29', 'AD', NULL, NULL),
(449, 'admin', '112.196.136.44', '2015-11-29 23:39:30', '2015-11-30 00:29:03am', 'k1ulf7cisgdm8jidv76ra1v7m3', '2015-11-29', 'AD', NULL, NULL),
(450, 'admin', '112.196.136.78', '2015-11-30 00:01:56', '2015-11-30 07:13:51am', 'v26vo4bgddh31v1eokhi37i0t4', '2015-11-30', 'AD', NULL, NULL),
(451, 'FMVI-7264', '112.196.136.44', '2015-11-30 00:29:37', '2015-11-30 00:37:52am', 'i6pkilridah7uelia8ufvtdfn0', '2015-11-30', 'AM', NULL, NULL),
(452, 'admin', '112.196.136.44', '2015-11-30 00:38:09', '2015-11-30 00:43:31am', 'fushfoti7glf3idr80f15jsj53', '2015-11-30', 'AD', NULL, NULL),
(453, 'admin', '112.196.136.78', '2015-11-30 00:39:28', '', 'q1oj5kgkicj03a5s6qe3p546n3', '2015-11-30', 'AD', NULL, NULL),
(454, 'FMVI-7264', '112.196.136.78', '2015-11-30 00:41:36', '', 'slaucc6kp96tm0v2gp6pt3f0m0', '2015-11-30', 'AM', NULL, NULL),
(455, 'FMVI-7264', '112.196.136.44', '2015-11-30 00:48:56', '2015-11-30 00:53:27am', '7l42u9eoamgcakafnirq2mqpr0', '2015-11-30', 'AM', NULL, NULL),
(456, 'admin', '112.196.136.44', '2015-11-30 00:54:00', '2015-11-30 01:03:47am', 't782qmm2ofcf6n7f41j5jqj585', '2015-11-30', 'AD', NULL, NULL),
(457, 'FMVI-7264', '112.196.136.44', '2015-11-30 01:04:22', '2015-11-30 01:33:15am', '3r44q5g634233dk08f10q8qpl1', '2015-11-30', 'AM', NULL, NULL),
(458, 'admin', '112.196.136.78', '2015-11-30 01:04:32', '', 'mv8qddnnv8ru4vb55ivh951pt6', '2015-11-30', 'AD', NULL, NULL),
(459, 'FMVI-7264', '112.196.136.44', '2015-11-30 01:40:01', '', '0cmccb0oc7ne5ef21gb37h3sb3', '2015-11-30', 'AM', NULL, NULL),
(460, 'FMVI-7264', '112.196.136.78', '2015-11-30 01:56:52', '2015-11-30 05:29:54am', 'q6a802bks3mjm00p9j7inna1e2', '2015-11-30', 'AM', NULL, NULL),
(461, 'admin', '14.98.100.29', '2015-11-30 01:57:51', '', 'fb8tsor8a7avtokbrehiltl000', '2015-11-30', 'AD', NULL, NULL),
(462, 'admin', '112.196.136.78', '2015-11-30 03:11:43', '', 'kafp215khju891ju6rtbo1r0l7', '2015-11-30', 'AD', NULL, NULL),
(463, 'FMVI-7264', '112.196.136.44', '2015-11-30 03:15:30', '2015-11-30 03:37:38am', '4qasp78jlfa888e3f9qc6urth6', '2015-11-30', 'AM', NULL, NULL),
(464, 'admin', '112.196.136.44', '2015-11-30 03:37:44', '2015-11-30 03:44:29am', 'qb2674n1q8k4107pb81vfaid45', '2015-11-30', 'AD', NULL, NULL),
(465, 'admin', '112.196.136.44', '2015-11-30 03:44:37', '', '10b57bgekun0tts2f00t4l0707', '2015-11-30', 'AD', NULL, NULL),
(466, 'admin', '112.196.136.44', '2015-11-30 03:44:40', '2015-11-30 03:59:54am', '3vif8na3np3b1svs6u8hm5pdh0', '2015-11-30', 'AD', NULL, NULL),
(467, 'admin', '112.196.136.44', '2015-11-30 03:50:32', '', '3ogk0o60l3ietluqcm2hqit9g3', '2015-11-30', 'AD', NULL, NULL),
(468, 'admin', '112.196.136.44', '2015-11-30 03:54:04', '2015-11-30 03:58:05am', 'ppp34n3s88n27f774cdev1u394', '2015-11-30', 'AD', NULL, NULL),
(469, 'admin', '112.196.136.44', '2015-11-30 03:59:47', '', 'dejvljmodgirhqtiqrnph0qdl5', '2015-11-30', 'AD', NULL, NULL),
(470, 'FMVI-7264', '112.196.136.44', '2015-11-30 04:00:01', '2015-11-30 05:11:49am', 'k1ek1n6cbo69ac886p7apfulf1', '2015-11-30', 'AM', NULL, NULL),
(471, 'admin', '112.196.136.44', '2015-11-30 04:24:40', '', 'hoq8ls72qukelkfq0arlu6pne2', '2015-11-30', 'AD', NULL, NULL),
(472, 'admin', '112.196.136.44', '2015-11-30 04:27:09', '', 'ekg2hnb3va7e3jr4f6nr4biaf5', '2015-11-30', 'AD', NULL, NULL),
(473, 'FTTP-8399', '112.196.136.44', '2015-11-30 05:12:24', '', '3jppqgtf6o4826f76nasm9amb3', '2015-11-30', 'TM', NULL, NULL),
(474, 'admin', '112.196.136.44', '2015-11-30 05:14:37', '', 'quujsft0bq7ernt7pimd40st23', '2015-11-30', 'AD', NULL, NULL),
(475, 'FTTP-8399', '112.196.136.78', '2015-11-30 05:14:43', '2015-11-30 06:49:36am', 'eurba15s4ptcspfn7f9uu8vc20', '2015-11-30', 'TM', NULL, NULL),
(476, 'FTTP-8399', '112.196.136.78', '2015-11-30 05:30:28', '', 'id3tcobt143spo3od62g5prje2', '2015-11-30', 'TM', NULL, NULL),
(477, 'admin', '112.196.136.44', '2015-11-30 05:54:35', '', '2m9dsempgvgb750mj07v185fl3', '2015-11-30', 'AD', NULL, NULL),
(478, 'FTTP-8399', '112.196.136.44', '2015-11-30 05:55:24', '2015-11-30 06:06:50am', 'ekoufk75qv0g3dq7f4cce010u7', '2015-11-30', 'TM', NULL, NULL),
(479, 'FMVI-7264', '112.196.136.44', '2015-11-30 06:07:04', '2015-11-30 06:37:12am', 'ppndq1ih4fvco4ih3gjc5f8a65', '2015-11-30', 'AM', NULL, NULL),
(480, 'admin', '112.196.136.44', '2015-11-30 06:17:18', '', 'jecjb32tkb1h2178ef4ncmd2v6', '2015-11-30', 'AD', NULL, NULL),
(481, 'admin', '112.196.136.44', '2015-11-30 06:25:21', '', '9lq3detemcjhajqc7rtgf3aep2', '2015-11-30', 'AD', NULL, NULL),
(482, 'FTTP-8399', '112.196.136.44', '2015-11-30 06:37:20', '2015-11-30 06:42:23am', '3i86tntuv50bbp5unc5ui0f3p5', '2015-11-30', 'TM', NULL, NULL),
(483, 'ZJBX-0533', '112.196.136.44', '2015-11-30 06:48:09', '2015-11-30 06:48:28am', '3hr4mq21r0j3qjgul975l479q1', '2015-11-30', 'QF', NULL, NULL),
(484, 'ZJBX-0533', '112.196.136.44', '2015-11-30 06:48:31', '2015-11-30 07:27:55am', 'mpma262jva7hrrmtegpa059gi2', '2015-11-30', 'QF', NULL, NULL),
(485, 'ZJBX-0533', '112.196.136.78', '2015-11-30 06:49:45', '2015-11-30 07:14:55am', 'dvr3injeb8jna7d5qdekjoh487', '2015-11-30', 'QF', NULL, NULL),
(486, 'FTTP-8399', '112.196.136.78', '2015-11-30 06:50:11', '', '9nrpn4jgem3i9ipta3mnkivm02', '2015-11-30', 'TM', NULL, NULL),
(487, 'FMVI-7264', '112.196.136.78', '2015-11-30 07:14:22', '', '3m3kv27fl39k675niq5g9r8541', '2015-11-30', 'AM', NULL, NULL),
(488, 'FTTP-8399', '112.196.136.78', '2015-11-30 07:19:30', '2015-11-30 07:20:08am', 'v8sbg7e3ojsfo381vjn2hkn025', '2015-11-30', 'TM', NULL, NULL),
(489, 'FMVI-7264', '112.196.136.44', '2015-11-30 07:28:04', '2015-11-30 07:28:34am', '7n83l1u88v7o1napcn8b3q3i92', '2015-11-30', 'AM', NULL, NULL),
(490, 'admin', '112.196.136.44', '2015-11-30 07:28:47', '', 'pcm6eoen26uk7cccafj4i9pdd4', '2015-11-30', 'AD', NULL, NULL),
(491, 'admin', '112.196.136.44', '2015-11-30 07:46:52', '', 'audhrhknlsa4holft2ei33nq95', '2015-11-30', 'AD', NULL, NULL),
(492, 'admin', '66.49.237.16', '2015-11-30 09:00:22', '', 'dduv45or67ndp30m0igkc6qr04', '2015-11-30', 'AD', NULL, NULL),
(493, 'admin', '66.49.237.16', '2015-11-30 13:42:27', '', 'dkm5rb1lr3u8tkm7co8ll7l891', '2015-11-30', 'AD', NULL, NULL),
(494, 'admin', '112.196.136.44', '2015-11-30 23:20:46', '', 'h2ke4u8at6nfmpcun3orfhs9m1', '2015-11-30', 'AD', NULL, NULL),
(495, 'admin', '112.196.136.44', '2015-11-30 23:41:21', '', 'skj04bv5qs1pva4uf1ssqcs320', '2015-11-30', 'AD', NULL, NULL),
(496, 'admin', '112.196.136.78', '2015-11-30 23:46:21', '', 'ktp68dfmnmcitu7g699evsajc7', '2015-11-30', 'AD', NULL, NULL),
(497, 'admin', '112.196.136.78', '2015-12-01 00:00:47', '', '0e10s9votphl41prtnrkuqrlp7', '2015-12-01', 'AD', NULL, NULL),
(498, 'FTTP-8399', '112.196.136.78', '2015-12-01 00:01:32', '', 'fd5nnia17hlcl8488250beqdq5', '2015-12-01', 'TM', NULL, NULL),
(499, 'FMVI-7264', '112.196.136.78', '2015-12-01 00:03:12', '', 'sr38sit5b1qlhrikmv2shf62u6', '2015-12-01', 'AM', NULL, NULL),
(500, 'ZJBX-0533', '112.196.136.78', '2015-12-01 00:15:59', '', '2s7r9kf669r6lsjmc301u4i3g6', '2015-12-01', 'QF', NULL, NULL),
(501, 'FXIO-8905', '112.196.136.78', '2015-12-01 00:30:29', '', 'tv8dnbl0hhi1qfb1muis6ld1b6', '2015-12-01', 'AM', NULL, NULL),
(502, 'admin', '112.196.136.78', '2015-12-01 00:49:34', '', 'lj65hsqdu9jrca54es5ophchv2', '2015-12-01', 'AD', NULL, NULL),
(503, 'admin', '112.196.136.78', '2015-12-01 00:58:35', '2015-12-01 07:01:52am', 'jfoiq6ueci8dkt12v0kkqbk1s2', '2015-12-01', 'AD', NULL, NULL),
(504, 'admin', '112.196.136.44', '2015-12-01 01:38:17', '', '415v7j6uavb7p79nmnh67lcve1', '2015-12-01', 'AD', NULL, NULL),
(505, 'FMVI-7264', '112.196.136.44', '2015-12-01 01:40:03', '', 'r40sglna17g4rrjk2cb7vngou6', '2015-12-01', 'AM', NULL, NULL),
(506, 'FTTP-8399', '112.196.136.78', '2015-12-01 04:10:41', '', 're412d4ec4nv0u5oc6dlh587a0', '2015-12-01', 'TM', NULL, NULL),
(507, 'admin', '112.196.136.44', '2015-12-01 04:29:16', '', 'ejl6nqnm01a93dumea767dcfs5', '2015-12-01', 'AD', NULL, NULL),
(508, 'admin', '112.196.136.78', '2015-12-01 04:52:49', '', 'cv4utotdu8m10onrneksv198v6', '2015-12-01', 'AD', NULL, NULL),
(509, 'FMVI-7264', '112.196.136.78', '2015-12-01 07:02:19', '', 'tdua5nunl38ooo7h636pdsnr72', '2015-12-01', 'AM', NULL, NULL),
(510, 'admin', '112.196.136.78', '2015-12-01 23:33:15', '2015-12-02 02:22:07am', 'iqqvmieufejnl5kkvk29qgjlr6', '2015-12-01', 'AD', NULL, NULL),
(511, 'RAUU-1599', '112.196.136.78', '2015-12-01 23:38:35', '', 'e2ep29jrl2m4qecpjucqq61qj2', '2015-12-01', 'AM', NULL, NULL),
(512, 'ZJBX-0533', '112.196.136.78', '2015-12-02 02:09:14', '2015-12-02 05:21:01am', '1msn9f8tpp7vfhtru9nb6sgfh1', '2015-12-02', 'QF', NULL, NULL),
(513, 'FTTP-8399', '112.196.136.78', '2015-12-02 02:14:51', '', 'ou66pi7qjs2vd99c6j1p0k9jd3', '2015-12-02', 'TM', NULL, NULL),
(514, 'FMVI-7264', '112.196.136.78', '2015-12-02 02:16:13', '', 'gl8cf67177e2drtmhpg2gialq2', '2015-12-02', 'AM', NULL, NULL),
(515, 'ZJBX-0533', '112.196.136.78', '2015-12-02 02:22:14', '2015-12-02 05:24:44am', 's6usil5vigttecqu3rquchmc23', '2015-12-02', 'QF', NULL, NULL),
(516, 'admin', '112.196.136.78', '2015-12-02 05:21:26', '', '0nn076gp7fbublja6d4gkm1hv6', '2015-12-02', 'AD', NULL, NULL),
(517, 'admin', '112.196.136.78', '2015-12-02 05:24:49', '2015-12-02 05:32:41am', 'f6ah80t5eeojbpf8pdi2n63tp5', '2015-12-02', 'AD', NULL, NULL),
(518, 'ZJBX-0533', '112.196.136.78', '2015-12-02 05:32:44', '2015-12-02 06:06:05am', 'j6ra0i72efjpdnacq83lnchog7', '2015-12-02', 'QF', NULL, NULL),
(519, 'admin', '112.196.136.78', '2015-12-02 06:06:09', '2015-12-02 06:22:27am', 'sjggm9i8bcndl7ugn7a21vr101', '2015-12-02', 'AD', NULL, NULL),
(520, 'ZJBX-0533', '112.196.136.78', '2015-12-02 06:22:31', '2015-12-02 07:22:42am', 'qiq1o1tiuoqarqliuomju5e3q7', '2015-12-02', 'QF', NULL, NULL),
(521, 'admin', '1.23.147.56', '2015-12-02 06:36:16', '2015-12-02 06:39:15am', 'ci1728dn2pi19010t4fstirde6', '2015-12-02', 'AD', NULL, NULL),
(522, 'admin', '112.196.136.78', '2015-12-02 07:22:46', '', 'fif6gvlukksj8ir1il8nfudr42', '2015-12-02', 'AD', NULL, NULL),
(523, 'admin', '112.196.136.78', '2015-12-02 07:50:07', '', '7qrcnna61l6e9i4stc936ejkh1', '2015-12-02', 'AD', NULL, NULL),
(524, 'admin', '66.49.237.16', '2015-12-02 13:45:32', '2015-12-02 13:45:40pm', 'm0p12r4gv3n4k29t9f6baijiv3', '2015-12-02', 'AD', NULL, NULL),
(525, 'FXIO-8905', '66.49.237.16', '2015-12-02 13:45:43', '', '0lqev9caj7k4ogqk0rq0ah4c34', '2015-12-02', 'AM', NULL, NULL),
(526, 'admin', '112.196.136.78', '2015-12-02 23:06:24', '2015-12-03 07:52:56am', '6gcdp7q45uqub0tf6tcrb66731', '2015-12-02', 'AD', NULL, NULL),
(527, 'FMVI-7264', '112.196.136.78', '2015-12-02 23:08:47', '2015-12-03 08:01:32am', 'ngl0omakfkebbapvudl1v1doj0', '2015-12-02', 'AM', NULL, NULL),
(528, 'FXIO-8905', '112.196.136.78', '2015-12-02 23:11:16', '2015-12-03 00:47:39am', 'qo748s234nte8s3h95j2vttac4', '2015-12-02', 'AM', NULL, NULL),
(529, 'admin', '112.196.136.78', '2015-12-02 23:34:41', '', 'c02jgt35tlacklpp133f1rimi7', '2015-12-02', 'AD', NULL, NULL),
(530, 'FTTP-8399', '112.196.136.78', '2015-12-03 00:48:20', '2015-12-03 01:59:07am', 'hbfnmh9774tetijaimge1lr2q1', '2015-12-03', 'TM', NULL, NULL),
(531, 'FXIO-8905', '112.196.136.78', '2015-12-03 01:59:16', '2015-12-03 07:05:39am', 'ibe12lkbiegl4r4e6c054169k4', '2015-12-03', 'AM', NULL, NULL),
(532, 'admin', '112.196.136.78', '2015-12-03 02:01:52', '', 'jq8tn3bbs9p7lbe5bh7ll561j7', '2015-12-03', 'AD', NULL, NULL),
(533, 'admin', '112.196.136.78', '2015-12-03 03:39:50', '', '2nt8isql08huklolklr40l7db2', '2015-12-03', 'AD', NULL, NULL),
(534, 'admin', '112.196.136.78', '2015-12-03 04:48:02', '', '8kj2bqpnkgd51ho830j99t7la3', '2015-12-03', 'AD', NULL, NULL),
(535, 'admin', '112.196.136.78', '2015-12-03 04:53:22', '', '74ank0k3u67so1btb53j9jfh52', '2015-12-03', 'AD', NULL, NULL),
(536, 'admin', '112.196.136.78', '2015-12-03 05:03:07', '', 'gdpld5g7uichp84moofstv7g23', '2015-12-03', 'AD', NULL, NULL),
(537, 'admin', '112.196.136.78', '2015-12-03 07:06:00', '', '0gs6fqgipgon8aoq0pq4ibvu86', '2015-12-03', 'AD', NULL, NULL),
(538, 'admin', '112.196.136.78', '2015-12-03 07:57:48', '', 'grclihkvo8orbj3fs2g5j8l065', '2015-12-03', 'AD', NULL, NULL),
(539, 'admin', '112.196.136.78', '2015-12-03 08:01:39', '', '806bdcbd3j8kjcuk09smfl12v0', '2015-12-03', 'AD', NULL, NULL),
(540, 'admin', '112.196.136.78', '2015-12-03 08:07:32', '', 'ht51fcs967bulte2im8l1u19s3', '2015-12-03', 'AD', NULL, NULL),
(541, 'admin', '66.49.237.16', '2015-12-03 08:45:44', '', 'vqvtb136tir3q8st8ejlfkrq72', '2015-12-03', 'AD', NULL, NULL),
(542, 'admin', '66.49.237.16', '2015-12-03 08:46:35', '', '90h9jsn16jbgqkrcgukjb0u7f7', '2015-12-03', 'AD', NULL, NULL),
(543, 'admin', '112.196.136.78', '2015-12-03 23:34:54', '2015-12-04 04:53:35am', 'jpndp296bhs8kujg5iv9frvl30', '2015-12-03', 'AD', NULL, NULL),
(544, 'admin', '112.196.136.78', '2015-12-04 01:14:38', '', '2d6dc2n3va0vlgtd7oesup9986', '2015-12-04', 'AD', NULL, NULL),
(545, 'RAUU-1599', '112.196.136.78', '2015-12-04 01:15:48', '', 'krc5oversqe662012ag5kkmo62', '2015-12-04', 'AM', NULL, NULL),
(546, 'admin', '112.196.136.78', '2015-12-04 01:18:56', '', '3vhqll018iipdqvd0mq038i8j0', '2015-12-04', 'AD', NULL, NULL),
(547, 'admin', '112.196.136.78', '2015-12-04 01:19:28', '', '9eh1jp9irk2gqf33k648bm2ra7', '2015-12-04', 'AD', NULL, NULL),
(548, 'admin', '112.196.136.78', '2015-12-04 02:27:46', '', 'c7v2gp7vvgbe7gu87fh3p9enh5', '2015-12-04', 'AD', NULL, NULL),
(549, 'admin', '112.196.136.44', '2015-12-04 03:20:02', '', '040domab8pbprlpiiffgj8hlg0', '2015-12-04', 'AD', NULL, NULL),
(550, 'admin', '112.196.136.44', '2015-12-04 03:30:58', '', 'h29ue1qcla9b1l8u45s6mud8n6', '2015-12-04', 'AD', NULL, NULL),
(551, 'admin', '112.196.136.44', '2015-12-04 03:32:31', '', 'apai8l5g7nc4m4sqsiu7q9du37', '2015-12-04', 'AD', NULL, NULL),
(552, 'admin', '112.196.136.78', '2015-12-04 03:35:53', '', 'n53p1htc7am4gvjtp4ua4v8u03', '2015-12-04', 'AD', NULL, NULL),
(553, 'admin', '112.196.136.78', '2015-12-04 03:38:59', '', 'usf1d4ab2vargelfbi12u4fe52', '2015-12-04', 'AD', NULL, NULL),
(554, 'admin', '112.196.136.44', '2015-12-04 04:39:48', '', '4kp4r5ojt6s8fjeefvrkqtfiv3', '2015-12-04', 'AD', NULL, NULL),
(555, 'admin', '112.196.136.78', '2015-12-04 04:55:43', '', 'rnipou756rg0jiq5jpbd8bcqk6', '2015-12-04', 'AD', NULL, NULL),
(556, 'admin', '112.196.136.78', '2015-12-04 06:55:45', '', 'ammi1do12o4s2e7kqrfcu3srj1', '2015-12-04', 'AD', NULL, NULL),
(557, 'admin', '112.196.136.78', '2015-12-04 07:01:16', '', 'l9u39se11s95e9fvp49ke8i020', '2015-12-04', 'AD', NULL, NULL),
(558, 'admin', '112.196.136.78', '2015-12-04 07:03:22', '', 'nl4474lvfj8tkviu53u93kgm55', '2015-12-04', 'AD', NULL, NULL),
(559, 'admin', '112.196.136.78', '2015-12-04 07:06:15', '', '90kobqtp0640rn1mv6inot0un3', '2015-12-04', 'AD', NULL, NULL),
(560, 'admin', '66.49.237.16', '2015-12-04 11:16:43', '', 'q70gqp0usic6mmmcngj6o35210', '2015-12-04', 'AD', NULL, NULL),
(561, 'admin', '112.196.136.78', '2015-12-04 22:45:52', '2015-12-05 01:04:39am', 'pl467uq1lht1kgl91lbhdsaug0', '2015-12-04', 'AD', NULL, NULL),
(562, 'admin', '112.196.136.78', '2015-12-05 01:04:42', '2015-12-05 01:45:27am', 'vv9hpmvviogfuvuje0ae1dlnm4', '2015-12-05', 'AD', NULL, NULL),
(563, 'admin', '112.196.136.78', '2015-12-05 01:45:31', '2015-12-05 04:35:34am', 'qgl96u6lkj03joccms71uo6tp3', '2015-12-05', 'AD', NULL, NULL),
(564, 'admin', '112.196.136.78', '2015-12-05 04:35:38', '2015-12-05 04:54:55am', 'lk8k61g0jk7q9vkqjkod772vk0', '2015-12-05', 'AD', NULL, NULL),
(565, 'admin', '112.196.136.44', '2015-12-05 04:54:59', '', '2umtstpt4vre1uvqldfbgrfem0', '2015-12-05', 'AD', NULL, NULL),
(566, 'admin', '112.196.136.78', '2015-12-07 06:43:42', '', 'o10bs9foru3urbau6nabip2bh4', '2015-12-07', 'AD', NULL, NULL),
(567, 'admin', '112.196.136.78', '2015-12-07 07:09:16', '', 'joiha1vvjbil8dtuhhocdmuu37', '2015-12-07', 'AD', NULL, NULL),
(568, 'admin', '112.196.136.78', '2015-12-07 07:11:18', '', 'm37lq65h1oau58q66j3c3179p5', '2015-12-07', 'AD', NULL, NULL),
(569, 'admin', '112.196.136.78', '2015-12-07 07:23:59', '', 'ro28rdsak80j92tjhe9leg6ah2', '2015-12-07', 'AD', NULL, NULL),
(570, 'admin', '112.196.136.78', '2015-12-07 07:25:19', '2015-12-07 07:29:11am', 'qgifun6j82df3b63f2d69r98m7', '2015-12-07', 'AD', NULL, NULL),
(571, 'admin', '112.196.136.78', '2015-12-07 07:33:28', '2015-12-07 07:38:00am', 'lp2pnl8ecnu8lhh86b8ec46q90', '2015-12-07', 'AD', NULL, NULL),
(572, 'admin', '112.196.136.78', '2015-12-07 08:38:19', '', '6pbuook4p7aln7j7401m0t8a96', '2015-12-07', 'AD', NULL, NULL),
(573, 'admin', '112.196.136.78', '2015-12-07 08:42:59', '', 'v90jriqnc0lolf0ocaop431ae7', '2015-12-07', 'AD', NULL, NULL),
(574, 'admin', '112.196.136.78', '2015-12-10 23:33:56', '2015-12-10 23:34:30pm', 'knpffnmi49tibiig2uunv402v6', '2015-12-10', 'AD', NULL, NULL),
(575, 'admin', '112.196.136.78', '2015-12-10 23:34:57', '', 'a7pkqc4iuvceq31rgk7b8lron4', '2015-12-10', 'AD', NULL, NULL),
(576, 'admin', '112.196.136.78', '2015-12-11 00:53:35', '', 'trhaqmk21d7ut75j2p00t5nm20', '2015-12-11', 'AD', NULL, NULL),
(577, 'admin', '112.196.136.78', '2015-12-11 05:11:15', '', '2ndesa9fuart74nph22m9kq8b7', '2015-12-11', 'AD', NULL, NULL),
(578, 'admin', '112.196.136.78', '2015-12-11 05:39:21', '2015-12-11 05:39:43am', '242gon00shqu0es6b9rso9c5e7', '2015-12-11', 'AD', NULL, NULL),
(579, 'admin', '112.196.136.78', '2015-12-12 05:25:47', '', 'bvddo72nlaogpv6u4cb8m6j7i3', '2015-12-12', 'AD', NULL, NULL),
(580, 'admin', '112.196.136.78', '2015-12-14 07:11:58', '2015-12-14 07:12:05am', 'm7jfj5lb8tnck1ke5sem2n12i5', '2015-12-14', 'AD', NULL, NULL),
(581, 'admin', '112.196.136.78', '2015-12-14 07:12:27', '2015-12-14 07:13:16am', '79obctf8v3mb56i17qs9nu7ht2', '2015-12-14', 'AD', NULL, NULL),
(582, 'admin', '112.196.136.78', '2015-12-14 23:10:43', '2015-12-14 23:11:13pm', 'qcli65jp4urukrdso04jk2l526', '2015-12-14', 'AD', NULL, NULL),
(583, 'admin', '66.49.237.16', '2015-12-15 13:44:35', '2015-12-15 14:19:32pm', 'ri0651r213n786f924kv6526i3', '2015-12-15', 'AD', NULL, NULL),
(584, 'admin', '66.49.237.16', '2015-12-15 14:19:39', '', 'e07qj4frsvk2n9r09aqp7jn1c7', '2015-12-15', 'AD', NULL, NULL),
(585, 'admin', '66.49.237.16', '2015-12-15 14:28:47', '', 'mv4sjsvkpekpi2qmv1i5n8tc66', '2015-12-15', 'AD', NULL, NULL),
(586, 'admin', '66.49.237.16', '2015-12-15 14:54:12', '', 'up7tgkg98kohmmevsgsv9gnl02', '2015-12-15', 'AD', NULL, NULL),
(587, 'admin', '66.49.237.16', '2015-12-15 15:12:10', '', '587mem5c1drqg5qa88cc94snn6', '2015-12-15', 'AD', NULL, NULL),
(588, 'admin', '66.49.237.16', '2015-12-15 16:35:16', '', 'mjtgfk3fjqkbs8rmfv1ia46354', '2015-12-15', 'AD', NULL, NULL),
(589, 'admin', '112.196.136.78', '2015-12-16 00:25:08', '', 'cpa9gl0posoi7ro0hr012p8bv3', '2015-12-16', 'AD', NULL, NULL),
(590, 'admin', '66.49.157.92', '2015-12-16 07:44:47', '2015-12-16 07:49:56am', 'f7iu1p3j82r3ebd5r171filsv1', '2015-12-16', 'AD', NULL, NULL),
(591, 'ORYW-7613', '66.49.157.92', '2015-12-16 07:50:15', '2015-12-16 07:57:27am', 'dpcgitbniarq73rftvab4eb4n1', '2015-12-16', 'AM', NULL, NULL),
(592, 'admin', '66.49.237.16', '2015-12-16 15:13:34', '', 'aslsg34fk31flpd34snkd5d7d0', '2015-12-16', 'AD', NULL, NULL),
(593, 'admin', '66.49.237.16', '2015-12-16 16:36:25', '', 'js6d68hmdive6808vps9h77ch7', '2015-12-16', 'AD', NULL, NULL),
(594, 'admin', '69.171.142.218', '2015-12-17 11:30:36', '', 'ccc3rbv1p01pfcujiinagqkle6', '2015-12-17', 'AD', NULL, NULL),
(595, 'admin', '69.171.142.218', '2015-12-17 15:08:54', '', '4jvbgkc0erqqmu725qc7dpobl0', '2015-12-17', 'AD', NULL, NULL),
(596, 'admin', '112.196.136.78', '2015-12-17 23:03:25', '', 'dv56166qu8gq6o90fp0m4fp3p2', '2015-12-17', 'AD', NULL, NULL),
(597, 'admin', '112.196.136.78', '2015-12-17 23:21:37', '', 'nl9s6f9f0q3ikqte3ll7jfuch0', '2015-12-17', 'AD', NULL, NULL),
(598, 'admin', '112.196.136.78', '2015-12-18 04:04:20', '', 'jl7f5ufpso1dqo1a1bvc9bdni4', '2015-12-18', 'AD', NULL, NULL),
(599, 'admin', '112.196.136.78', '2015-12-18 07:56:16', '', 'slcri2jpsim4pm55n8edjd52l3', '2015-12-18', 'AD', NULL, NULL),
(600, 'admin', '112.196.136.78', '2015-12-18 22:53:36', '2015-12-19 04:15:23am', '76r0gg7lmc419surio3uhiu893', '2015-12-18', 'AD', NULL, NULL),
(601, 'admin', '112.196.136.78', '2015-12-19 04:15:29', '2015-12-19 06:32:00am', 'ugf1ekq5t6fqtftp30ika6gve2', '2015-12-19', 'AD', NULL, NULL),
(602, 'IXBB-4438', '112.196.136.78', '2015-12-19 05:28:19', '2015-12-19 07:00:04am', 'm1o77jmp68jmni1gvh7u403el1', '2015-12-19', 'AM', NULL, NULL),
(603, 'admin', '112.196.136.78', '2015-12-19 06:32:07', '', 'jlh1b6kf6ke1mng9kfncs6i6a6', '2015-12-19', 'AD', NULL, NULL),
(604, 'admin', '112.196.136.78', '2015-12-19 07:00:31', '', 'dtjfrl4d6ab9jaguf5t28vnse5', '2015-12-19', 'AD', NULL, NULL),
(605, 'admin', '112.196.136.78', '2015-12-19 08:26:51', '', '4jj7lbuuke91civv623roitq63', '2015-12-19', 'AD', NULL, NULL),
(606, 'admin', '112.196.136.78', '2015-12-20 23:18:32', '', '4ac6bt1mdjatf1ncgfjqefk356', '2015-12-20', 'AD', NULL, NULL),
(607, 'admin', '112.196.136.78', '2015-12-20 23:35:45', '', '8u5j20km39lnu350209hrtqr73', '2015-12-20', 'AD', NULL, NULL),
(608, 'IXBB-4438', '112.196.136.78', '2015-12-21 00:33:38', '', 'v3s0brrvs965dvgp9m60uk5se2', '2015-12-21', 'AM', NULL, NULL),
(609, 'admin', '112.196.136.78', '2015-12-21 00:51:02', '', 'iom10lk7k3j2l4ikg6810cume3', '2015-12-21', 'AD', NULL, NULL),
(610, 'admin', '112.196.136.78', '2015-12-21 07:10:26', '2015-12-21 07:53:40am', 'lt2202cgdcb12pejog6es3n7g4', '2015-12-21', 'AD', NULL, NULL),
(611, 'admin', '112.196.136.78', '2015-12-21 07:53:43', '', 'neeo6di6gtfu32lupogie2imt0', '2015-12-21', 'AD', NULL, NULL),
(612, 'IXBB-4438', '112.196.136.78', '2015-12-21 07:54:48', '', 'epg9pm5upjn5ppgqselk18u821', '2015-12-21', 'AM', NULL, NULL),
(613, 'admin', '69.171.142.218', '2015-12-23 14:24:29', '2015-12-23 16:00:04pm', 'n6l6vpklka19lpbj44s3d9d107', '2015-12-23', 'AD', NULL, NULL),
(614, 'IXBB-4438', '69.171.142.218', '2015-12-23 14:47:02', '2015-12-23 14:47:25pm', 'iu6sjs6m341q9msphuhh4n3sc0', '2015-12-23', 'AM', NULL, NULL),
(615, 'IXBB-4438', '69.171.142.218', '2015-12-23 14:47:34', '2015-12-23 15:00:29pm', '9kkqdn2lnbldmdns2joe71s2j6', '2015-12-23', 'AM', NULL, NULL),
(616, 'JUQI-3911', '69.171.142.218', '2015-12-23 15:00:35', '2015-12-23 15:01:56pm', 'hd8s79jebd63uvkrn1oa0vdig6', '2015-12-23', 'AM', NULL, NULL),
(617, 'JUQI-3911', '69.171.142.218', '2015-12-23 15:02:00', '2015-12-23 15:57:55pm', '1j2b38h4v9oqrtb8ehfsb7ns54', '2015-12-23', 'AM', NULL, NULL),
(618, 'YWFZ-6999', '69.171.142.218', '2015-12-23 15:06:59', '2015-12-23 15:09:01pm', 'khvtmb79peur43k9u8tlor2aa7', '2015-12-23', 'QF', NULL, NULL),
(619, 'IXBB-4438', '69.171.142.218', '2015-12-23 15:58:09', '2015-12-23 16:06:11pm', 'go6c3pd8bfo6m07shtgbbbfjk1', '2015-12-23', 'AM', NULL, NULL),
(620, 'JUQI-3911', '69.171.142.218', '2015-12-23 15:58:19', '', 'ckihs3f3hc90cilenob3fkjbu2', '2015-12-23', 'AM', NULL, NULL),
(621, 'YWFZ-6999', '69.171.142.218', '2015-12-23 16:00:09', '2015-12-23 16:07:25pm', 'vmka1q9k489f31bfi8mbu84to0', '2015-12-23', 'QF', NULL, NULL),
(622, 'admin', '69.171.142.218', '2015-12-23 16:06:23', '2015-12-23 16:07:32pm', '6sta4lnpg460m2q2sqkcg5gm36', '2015-12-23', 'AD', NULL, NULL),
(623, 'admin', '69.171.142.218', '2015-12-23 16:07:28', '', 'oua1pa1k1l11oflri3kne9r8e1', '2015-12-23', 'AD', NULL, NULL),
(624, 'PZOI-0348', '69.171.142.218', '2015-12-23 16:08:49', '2015-12-23 16:09:38pm', 'ak2edc9l7dr5oqp2kjrcdfqhq1', '2015-12-23', 'AD', NULL, NULL),
(625, 'PZOI-0348', '69.171.142.218', '2015-12-23 16:09:40', '', 'kmkvhnh2e5hgvfnchfk7hieco1', '2015-12-23', 'AD', NULL, NULL),
(626, 'admin', '112.196.136.78', '2015-12-23 23:43:25', '2015-12-24 04:43:48am', '3g7jseom1rnh03ecbugq46bel6', '2015-12-23', 'AD', NULL, NULL),
(627, 'YWFZ-6999', '112.196.136.78', '2015-12-24 00:49:03', '2015-12-24 01:10:44am', '2g4u1cmsgjhiikr5ci7osm39k7', '2015-12-24', 'QF', NULL, NULL),
(628, 'JUQI-3911', '112.196.136.78', '2015-12-24 01:11:33', '2015-12-24 01:12:02am', '0nt7cjgm42ph1tdnscepkd9t27', '2015-12-24', 'AM', NULL, NULL),
(629, 'IXBB-4438', '112.196.136.78', '2015-12-24 01:13:51', '2015-12-24 04:45:48am', 'icgki9s7p6t8hpcojaro491pv6', '2015-12-24', 'AM', NULL, NULL),
(630, 'admin', '112.196.136.78', '2015-12-24 04:34:48', '', 'hqpe8knaluu1tn4kt525f3jcp0', '2015-12-24', 'AD', NULL, NULL),
(631, 'admin', '112.196.136.78', '2015-12-24 04:44:10', '2015-12-24 04:44:17am', 'cik6egau3flv9th5cp5co44pu2', '2015-12-24', 'AD', NULL, NULL),
(632, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:46:10', '2015-12-24 04:46:22am', 'l4s13psseb5tri7pac8s6p2007', '2015-12-24', 'AM', NULL, NULL),
(633, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:46:42', '2015-12-24 04:46:44am', '7jmjf4ipp4bt4n8sfpimtmccu2', '2015-12-24', 'AM', NULL, NULL),
(634, 'admin', '112.196.136.78', '2015-12-24 04:50:38', '2015-12-24 04:50:43am', 'ob1moumdagkl0ftcs7giqngi86', '2015-12-24', 'AD', NULL, NULL),
(635, 'admin', '112.196.136.78', '2015-12-24 04:51:02', '2015-12-24 05:36:06am', 'cot46q4om4jlck3ckpv6599fd5', '2015-12-24', 'AD', NULL, NULL),
(636, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:52:25', '2015-12-24 05:21:13am', 'hnfrv0nuf9ieectm4e0in9k722', '2015-12-24', 'AM', NULL, NULL),
(637, 'IXBB-4438', '112.196.136.78', '2015-12-24 04:59:46', '', 'nk7qlpkjdovhmdc55iab16avg0', '2015-12-24', 'AM', NULL, NULL),
(638, 'IXBB-4438', '112.196.136.78', '2015-12-24 05:21:15', '2015-12-24 05:47:15am', 'og9vjkndlf78ni0d0bet29t2i3', '2015-12-24', 'AM', NULL, NULL),
(639, 'IXBB-4438', '112.196.136.78', '2015-12-24 05:36:15', '2015-12-24 06:18:19am', 'ct8elu7bfn9img4pq9m44g5js2', '2015-12-24', 'AM', NULL, NULL),
(640, 'admin', '112.196.136.78', '2015-12-24 05:47:30', '2015-12-24 06:02:47am', 'n4f6o7dn2rf6trabjns1phpk60', '2015-12-24', 'AD', NULL, NULL),
(641, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:02:59', '2015-12-24 06:18:03am', 'as63fmtc5la603nbv6tqceeio3', '2015-12-24', 'AM', NULL, NULL),
(642, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:18:06', '', 'cv6es6o2soaflvhdb4o3n3cth4', '2015-12-24', 'AM', NULL, NULL),
(643, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:18:09', '2015-12-24 06:18:56am', '2m565l964h0l3f01i18t9tg0r0', '2015-12-24', 'AM', NULL, NULL),
(644, 'admin', '112.196.136.78', '2015-12-24 06:18:25', '2015-12-24 06:30:59am', '5h9muelcm2vig24hon5b7r3uh1', '2015-12-24', 'AD', NULL, NULL),
(645, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:19:02', '', 'cjtt34fjt58ajhb6iqg7da88m2', '2015-12-24', 'AM', NULL, NULL),
(646, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:31:07', '2015-12-24 06:36:32am', 'i73a2ndst98fttjbmusc5ur695', '2015-12-24', 'AM', NULL, NULL),
(647, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:37:00', '2015-12-24 06:45:52am', '4imhqhmreb29q6b66ieiqbrla6', '2015-12-24', 'AM', NULL, NULL),
(648, 'JUQI-3911', '112.196.136.78', '2015-12-24 06:46:02', '', 'eg8d4bp2p4m1csrskpvo6cudv3', '2015-12-24', 'AM', NULL, NULL),
(649, 'JUQI-3911', '112.196.136.78', '2015-12-24 07:13:29', '', '27edg0580tps53m8hfqilj9io4', '2015-12-24', 'AM', NULL, NULL),
(650, 'admin', '112.196.136.78', '2015-12-24 23:13:22', '2015-12-25 06:19:40am', '5nm827ha51prpmno3ejsp9vsc1', '2015-12-24', 'AD', NULL, NULL),
(651, 'IXBB-4438', '112.196.136.78', '2015-12-24 23:31:28', '', 'g0c27dj527fhvjom3l7c4ciqi7', '2015-12-24', 'AM', NULL, NULL),
(652, 'admin', '112.196.136.78', '2015-12-25 00:10:55', '2015-12-25 00:45:20am', 'afqnsu6g61g2auktnrhp3v7nd5', '2015-12-25', 'AD', NULL, NULL),
(653, 'JUQI-3911', '112.196.136.78', '2015-12-25 00:20:33', '', 'oo0aiein258bfes4unq54ib3j1', '2015-12-25', 'AM', NULL, NULL),
(654, 'IXBB-4438', '112.196.136.78', '2015-12-25 00:21:32', '2015-12-25 06:47:35am', 'mnfsm6k57gpqvue02ut4uacna2', '2015-12-25', 'AM', NULL, NULL),
(655, 'JUQI-3911', '112.196.136.78', '2015-12-25 00:45:27', '2015-12-25 01:09:21am', 'uio22ek710hmuut3c2e6ong7c0', '2015-12-25', 'AM', NULL, NULL),
(656, 'admin', '112.196.136.78', '2015-12-25 01:09:27', '2015-12-25 01:16:12am', 'ioh6jo6ftaq2bj8lt0b3e69ku2', '2015-12-25', 'AD', NULL, NULL),
(657, 'admin', '112.196.136.78', '2015-12-25 01:17:03', '2015-12-25 06:36:46am', 'p4ktrq8l6r6qns6e46f02d1dg5', '2015-12-25', 'AD', NULL, NULL),
(658, 'YWFZ-6999', '112.196.136.78', '2015-12-25 01:17:46', '2015-12-25 01:34:13am', 'grll2pcfpc96nevoqqrl4jai56', '2015-12-25', 'QF', NULL, NULL),
(659, 'VYOC-6443', '112.196.136.78', '2015-12-25 01:34:17', '', '80onbhmj7j3qe9k6uujllp6j32', '2015-12-25', 'AM', NULL, NULL),
(660, 'admin', '112.196.136.78', '2015-12-25 06:19:52', '2015-12-25 06:19:59am', '31ji9641eh16b4r8rr3kshh291', '2015-12-25', 'AD', NULL, NULL),
(661, 'admin', '112.196.136.78', '2015-12-25 06:27:51', '', 's3kesp8h5tbadsbogdkorripd0', '2015-12-25', 'AD', NULL, NULL),
(662, 'JUQI-3911', '112.196.136.78', '2015-12-25 06:36:52', '2015-12-25 06:41:52am', 'us2q3c8k998dq02lk21ab9ao33', '2015-12-25', 'AM', NULL, NULL),
(663, 'admin', '112.196.136.78', '2015-12-25 06:41:56', '2015-12-25 06:42:10am', 'vbchp4ivjtm1uhq8d1f500mr56', '2015-12-25', 'AD', NULL, NULL),
(664, 'GPUJ-2680', '112.196.136.78', '2015-12-25 06:42:16', '2015-12-25 06:59:58am', '90rdomvsocb3kt51o679ouvht4', '2015-12-25', 'TM', NULL, NULL),
(665, 'admin', '112.196.136.78', '2015-12-25 06:47:43', '2015-12-25 07:01:32am', 'clprsu4403tvvgc7dfupsh4nd6', '2015-12-25', 'AD', NULL, NULL),
(666, 'YWFZ-6999', '112.196.136.78', '2015-12-25 07:00:02', '2015-12-25 07:01:39am', 'iht1rdirsnaqsediimr54rgqs3', '2015-12-25', 'QF', NULL, NULL),
(667, 'GPUJ-2680', '112.196.136.78', '2015-12-25 07:01:43', '', 'g4qo7t7sbaebtel72rac3sgr43', '2015-12-25', 'TM', NULL, NULL),
(668, 'admin', '112.196.136.78', '2015-12-26 00:11:29', '2015-12-26 04:23:50am', '342nee7gjanhikc745383esc94', '2015-12-26', 'AD', NULL, NULL),
(669, 'IXBB-4438', '112.196.136.78', '2015-12-26 00:13:07', '', 'mp37m6k3sc0g3u4fn2qsvteuk4', '2015-12-26', 'AM', NULL, NULL),
(670, 'IXBB-4438', '112.196.136.78', '2015-12-26 00:13:07', '2015-12-26 04:22:04am', 'ntop3ir6hkhqec5e9hbo8muh85', '2015-12-26', 'AM', NULL, NULL),
(671, 'YWFZ-6999', '112.196.136.78', '2015-12-26 01:29:02', '2015-12-26 03:28:43am', 'itl8rga55j7psid280hijf3pj0', '2015-12-26', 'QF', NULL, NULL),
(672, 'admin', '112.196.136.78', '2015-12-26 01:34:29', '2015-12-26 01:34:40am', 'h8i9ii97dt4e7193g8h4mfqt27', '2015-12-26', 'AD', NULL, NULL),
(673, 'IXBB-4438', '112.196.136.78', '2015-12-26 01:34:54', '2015-12-26 03:34:36am', '14d4ll2k71cv2g86hgk90q7v44', '2015-12-26', 'AM', NULL, NULL),
(674, 'admin', '112.196.136.78', '2015-12-26 01:39:06', '2015-12-26 05:54:28am', 'p9etqsiofa0hf41bhukrit5jt3', '2015-12-26', 'AD', NULL, NULL),
(675, 'YWFZ-6999', '112.196.136.78', '2015-12-26 01:40:26', '2015-12-26 03:42:48am', 'gmp5otg60io0sbvb8a8qodqj26', '2015-12-26', 'QF', NULL, NULL),
(676, 'GPUJ-2680', '112.196.136.78', '2015-12-26 03:31:29', '2015-12-26 03:34:37am', 'fi3kl3kalnnthldcoi3ohipjp4', '2015-12-26', 'TM', NULL, NULL),
(677, 'JUQI-3911', '112.196.136.78', '2015-12-26 03:34:42', '2015-12-26 03:35:18am', '92rtqbj0he4j7piee2rvmlca24', '2015-12-26', 'AM', NULL, NULL),
(678, 'admin', '112.196.136.78', '2015-12-26 03:35:28', '', '02ecuc66sj99r76vle5hrvgus3', '2015-12-26', 'AD', NULL, NULL),
(679, 'YWFZ-6999', '112.196.136.78', '2015-12-26 03:35:37', '2015-12-26 04:17:24am', '6mk0km395cpbnsol5lrcn3r082', '2015-12-26', 'QF', NULL, NULL),
(680, 'GPUJ-2680', '112.196.136.78', '2015-12-26 03:42:58', '2015-12-26 03:44:25am', 'm997jc9mprblrf68a04hc84vh5', '2015-12-26', 'TM', NULL, NULL),
(681, 'YWFZ-6999', '112.196.136.78', '2015-12-26 04:18:08', '2015-12-26 04:18:21am', 'v8vouiqv8j6qtpg21nu8fildl6', '2015-12-26', 'QF', NULL, NULL),
(682, 'IXBB-4438', '112.196.136.78', '2015-12-26 04:22:48', '', 'e348fvu76t1vassf1albh6opv3', '2015-12-26', 'AM', NULL, NULL),
(683, 'admin', '112.196.136.78', '2015-12-26 05:44:14', '2015-12-26 05:51:13am', 'i2o97rnnc9augkbfkldku493p3', '2015-12-26', 'AD', NULL, NULL),
(684, 'ZSWD-9699', '112.196.136.78', '2015-12-26 05:51:25', '2015-12-26 06:44:41am', '8j4k2mftnisl7778uceh1eaf83', '2015-12-26', 'AD', NULL, NULL),
(685, 'ZSWD-9699', '112.196.136.78', '2015-12-26 05:55:03', '2015-12-26 06:00:00am', '1cf9sfu5hot392bkb6och0e4l7', '2015-12-26', 'AD', NULL, NULL),
(686, 'JUQI-3911', '112.196.136.78', '2015-12-26 05:56:54', '2015-12-26 07:00:29am', 'rp6jebvpmenuaf1f2mkuem7930', '2015-12-26', 'AM', NULL, NULL),
(687, 'ZSWD-9699', '112.196.136.78', '2015-12-26 06:00:03', '2015-12-26 06:21:19am', 'op5os5pav3pdur6bnmcdj9htr6', '2015-12-26', 'AD', NULL, NULL),
(688, 'admin', '112.196.136.78', '2015-12-26 06:02:15', '', '7sibth08koo0pfpvk8nlhe11l2', '2015-12-26', 'AD', NULL, NULL),
(689, 'ZSWD-9699', '112.196.136.78', '2015-12-26 06:21:21', '2015-12-26 06:27:10am', '2jr2hnq1dpn1rld86ugc7lgnc6', '2015-12-26', 'QF', NULL, NULL),
(690, 'RQYY-2239', '112.196.136.78', '2015-12-26 06:27:26', '', 'bgscuetc73nlvl6ius4fm742f3', '2015-12-26', 'AM', NULL, NULL),
(691, 'admin', '112.196.136.78', '2015-12-26 06:44:45', '', 'omufm417ekpl5k3459d2kgul74', '2015-12-26', 'AD', NULL, NULL),
(692, 'IXBB-4438', '112.196.136.78', '2015-12-26 07:01:31', '', 'atol93qnva4q1i60t3snb6krc5', '2015-12-26', 'AM', NULL, NULL),
(693, 'admin', '99.224.27.68', '2015-12-28 10:16:11', '2015-12-28 10:25:44am', 'h6a6a8rf8neunhv7irfm77ca27', '2015-12-28', 'AD', NULL, NULL),
(694, 'admin', '99.224.27.68', '2015-12-28 10:25:49', '2015-12-28 10:28:06am', 'dpt9ejsu1d1d11vlm53ho5adj2', '2015-12-28', 'AD', NULL, NULL),
(695, 'PZOI-0348', '99.224.27.68', '2015-12-28 10:28:09', '2015-12-28 10:28:29am', '0j26jp0orcusljtslr0in5f2k2', '2015-12-28', 'QF', NULL, NULL),
(696, 'admin', '99.224.27.68', '2015-12-28 10:28:43', '2015-12-28 10:28:57am', 'log1jadm3vhpc9lmgeofm1oi74', '2015-12-28', 'AD', NULL, NULL),
(697, 'PZOI-0348', '99.224.27.68', '2015-12-28 10:29:00', '2015-12-28 10:29:20am', 'h2a694iq0o0l54n2626ddu4e91', '2015-12-28', 'AM', NULL, NULL),
(698, 'admin', '99.224.27.68', '2015-12-28 10:31:46', '', '3ad7o0p2rqnuscijjstt2oq1f6', '2015-12-28', 'AD', NULL, NULL),
(699, 'IXBB-4438', '99.224.27.68', '2015-12-28 10:42:05', '2015-12-28 10:42:41am', '3rofbai4vgkph686vlccivqh50', '2015-12-28', 'AM', NULL, NULL),
(700, 'JUQI-3911', '99.224.27.68', '2015-12-28 10:42:53', '2015-12-28 10:45:40am', 'rdirgh5aqfu75t1fqsh7mmobh5', '2015-12-28', 'AM', NULL, NULL),
(701, 'IXBB-4438', '99.224.27.68', '2015-12-28 10:45:56', '2015-12-28 10:46:44am', 'iagiilc067qj4n38mh549irmc7', '2015-12-28', 'AM', NULL, NULL),
(702, 'YWFZ-6999', '99.224.27.68', '2015-12-28 10:46:47', '2015-12-28 10:48:51am', 'cpv75i6n76hheuoo07s631uf26', '2015-12-28', 'QF', NULL, NULL),
(703, 'IXBB-4438', '99.224.27.68', '2015-12-28 10:48:55', '2015-12-28 10:52:32am', 'k9bbggkvkphc7do61kj2givq01', '2015-12-28', 'AM', NULL, NULL),
(704, 'GPUJ-2680', '99.224.27.68', '2015-12-28 10:52:44', '2015-12-28 10:53:02am', 'ep9gfjqpugluhbuthb4migjlu4', '2015-12-28', 'TM', NULL, NULL),
(705, 'YWFZ-6999', '99.224.27.68', '2015-12-28 10:53:18', '2015-12-28 10:53:52am', 'rre1qgpqkk5567fr1dp4os7cl4', '2015-12-28', 'QF', NULL, NULL),
(706, 'admin', '112.196.136.78', '2015-12-29 00:23:59', '', 'ff27njtgeqlvjdj802h0bmc232', '2015-12-29', 'AD', NULL, NULL),
(707, 'admin', '112.196.136.78', '2015-12-29 00:56:52', '2015-12-29 05:10:37am', 'ktqp019t9parolllku4disl836', '2015-12-29', 'AD', NULL, NULL),
(708, 'IXBB-4438', '112.196.136.78', '2015-12-29 00:57:43', '2015-12-29 02:03:30am', 'nrqu0kndcie61bj1uillit27h1', '2015-12-29', 'AM', NULL, NULL),
(709, 'YWFZ-6999', '112.196.136.78', '2015-12-29 02:03:42', '', '8aokcm8vg6ktq5ekv365gpmug0', '2015-12-29', 'QF', NULL, NULL),
(710, 'IXBB-4438', '112.196.136.78', '2015-12-29 02:23:21', '2015-12-29 04:45:00am', '65mueame9igd60skqee2fg5og5', '2015-12-29', 'AM', NULL, NULL),
(711, 'IXBB-4438', '112.196.136.78', '2015-12-29 04:45:12', '', 'hcgtp00ph6neg6m6f2ltfuvpr7', '2015-12-29', 'AM', NULL, NULL),
(712, 'JUQI-3911', '112.196.136.78', '2015-12-29 04:58:24', '2015-12-29 04:59:31am', '9ie64thrik8bbnemccispll0a2', '2015-12-29', 'AM', NULL, NULL),
(713, 'JUQI-3911', '112.196.136.78', '2015-12-29 04:59:39', '', 'supipg03vneh13o1klj1n10dg6', '2015-12-29', 'AM', NULL, NULL),
(714, 'admin', '112.196.136.78', '2015-12-29 05:10:46', '', 'fde36loj72nv0kks83npua5903', '2015-12-29', 'AD', NULL, NULL),
(715, 'admin', '112.196.136.78', '2015-12-29 06:33:37', '', 'h88lrsbi7js2mqr9m36n3b1nh2', '2015-12-29', 'AD', NULL, NULL),
(716, 'admin', '99.224.27.68', '2015-12-30 14:08:28', '', 'k72u4rl9i5il3ir699muctgnt5', '2015-12-30', 'AD', NULL, NULL),
(717, 'admin', '99.224.27.68', '2015-12-30 14:19:21', '', 'bqfdccp8sqcccpnupcuhcn2hf2', '2015-12-30', 'AD', NULL, NULL),
(718, 'admin', '99.224.27.68', '2015-12-30 14:19:27', '2015-12-30 14:24:15pm', 'f6tpescm2rlovemjik6kku7js0', '2015-12-30', 'AD', NULL, NULL),
(719, 'IXBB-4438', '99.224.27.68', '2015-12-30 14:24:21', '', 'khrbekfuqek4efbctdue3rf4j0', '2015-12-30', 'AM', NULL, NULL),
(720, 'admin', '112.196.136.78', '2015-12-30 23:26:32', '2015-12-30 23:26:46pm', '1nrstvrsoh1l3mqq1qo658oh14', '2015-12-30', 'AD', NULL, NULL),
(721, 'IXBB-4438', '112.196.136.78', '2015-12-30 23:26:56', '', '72tr7qn0tcgs1677lttu8puqo6', '2015-12-30', 'AM', NULL, NULL),
(722, 'admin', '112.196.136.78', '2015-12-31 01:09:13', '', 'co67s7a8o7v008r2c2ukajh360', '2015-12-31', 'AD', NULL, NULL),
(723, 'admin', '112.196.136.78', '2015-12-31 02:00:44', '', '0hb8tgtbrl4evhdsvov88du857', '2015-12-31', 'AD', NULL, NULL),
(724, 'admin', '112.196.136.78', '2015-12-31 02:04:03', '', 'rg7k31n5714j46jelqg5r5n0c6', '2015-12-31', 'AD', NULL, NULL),
(725, 'IXBB-4438', '112.196.136.78', '2015-12-31 02:09:43', '', '7skrdv0svch92dh20shjdp9bn7', '2015-12-31', 'AM', NULL, NULL),
(726, 'JUQI-3911', '112.196.136.78', '2015-12-31 02:10:28', '', 'ftfictr074k9o9mnmnitoh2pi6', '2015-12-31', 'AM', NULL, NULL),
(727, 'YWFZ-6999', '112.196.136.78', '2015-12-31 03:21:35', '', 'ppvig9hpu6v8pfhdmo86803rg3', '2015-12-31', 'QF', NULL, NULL),
(728, 'GPUJ-2680', '112.196.136.78', '2015-12-31 04:06:20', '2015-12-31 04:42:34am', '7b0latmadsl5e73f0a1co818i0', '2015-12-31', 'TM', NULL, NULL),
(729, 'admin', '112.196.136.78', '2015-12-31 04:42:39', '', 'ns3trubjgbqo25587dqudvm372', '2015-12-31', 'AD', NULL, NULL),
(730, 'admin', '112.196.136.78', '2015-12-31 05:52:11', '', 'n07sngk1nlnhpj92mv9odtpeu0', '2015-12-31', 'AD', NULL, NULL),
(731, 'IXBB-4438', '112.196.136.78', '2015-12-31 05:53:05', '', 'q43o54bfb8crb4ik5slfs8eos5', '2015-12-31', 'AM', NULL, NULL),
(732, 'admin', '112.196.136.78', '2016-01-04 04:05:52', '', 'imasv2tdtjeoojmqd8vrbkl1l0', '2016-01-04', 'AD', NULL, NULL),
(733, 'admin', '69.171.142.218', '2016-01-05 12:41:56', '', '3vakdj75b9r9pf7ijtvc3949d2', '2016-01-05', 'AD', NULL, NULL),
(734, 'IXBB-4438', '69.171.142.218', '2016-01-05 12:42:21', '', '9fhvvmdtpbv2d7vdeuvb3tu0r1', '2016-01-05', 'AM', NULL, NULL),
(735, 'admin', '69.171.142.218', '2016-01-05 14:05:02', '2016-01-05 14:41:51pm', '26njcqns322331ferm1furjup2', '2016-01-05', 'AD', NULL, NULL),
(736, 'IXBB-4438', '69.171.142.218', '2016-01-05 14:05:25', '', '73m71k3llg5eqor4pcnbmmcrf2', '2016-01-05', 'AM', NULL, NULL),
(737, 'JUQI-3911', '69.171.142.218', '2016-01-05 14:41:55', '2016-01-05 14:43:12pm', 'km4fr9agvt429kcqqm6u6d78n5', '2016-01-05', 'AM', NULL, NULL),
(738, 'admin', '69.171.142.218', '2016-01-05 14:43:16', '', '2130oh49k0rguvlucj1a7m0kh4', '2016-01-05', 'AD', NULL, NULL),
(739, 'IXBB-4438', '69.171.142.218', '2016-01-05 14:46:54', '', 'b9jaqkhb88dm873udp9nio4300', '2016-01-05', 'AM', NULL, NULL),
(740, 'admin', '112.196.136.78', '2016-01-05 23:18:23', '2016-01-05 23:18:36pm', '0bkpj464roq7l32imtbf6b63s1', '2016-01-05', 'AD', NULL, NULL),
(741, 'IXBB-4438', '112.196.136.78', '2016-01-05 23:18:49', '2016-01-06 00:28:49am', 'l8oqnt4r2eouki9i87not5k922', '2016-01-05', 'AM', NULL, NULL),
(742, 'admin', '112.196.136.78', '2016-01-05 23:28:01', '', '4tb5jrt9spjklhjpbvcvdtjfp7', '2016-01-05', 'AD', NULL, NULL),
(743, 'admin', '112.196.136.78', '2016-01-06 00:29:02', '', 'dv0acah05rb05sv9e0po8d2mp0', '2016-01-06', 'AD', NULL, NULL),
(744, 'IXBB-4438', '112.196.136.78', '2016-01-06 00:30:56', '', 'da54lmr0f4kkv10sv1jj2r9sm3', '2016-01-06', 'AM', NULL, NULL),
(745, 'JUQI-3911', '112.196.136.78', '2016-01-06 00:32:30', '', 'mjtq1i5l615jm7m052q2dr1uv6', '2016-01-06', 'AM', NULL, NULL),
(746, 'YWFZ-6999', '104.131.14.167', '2016-01-06 00:36:15', '', 'q0cj3e4c55595528bgpmocbru1', '2016-01-06', 'QF', NULL, NULL),
(747, 'admin', '69.171.142.218', '2016-01-06 09:38:49', '', 'urtmgs61a3cvlnhmlp78ee61f6', '2016-01-06', 'AD', NULL, NULL),
(748, 'admin', '69.171.142.218', '2016-01-06 09:56:47', '2016-01-06 09:56:49am', 'uu8ftsmg59dtf14ap5p0qvjqb3', '2016-01-06', 'AD', NULL, NULL),
(749, 'IXBB-4438', '69.171.142.218', '2016-01-06 09:56:56', '', 'uhcmce3fi5l98k1u1pnsp05ss5', '2016-01-06', 'AM', NULL, NULL),
(750, 'admin', '112.196.136.78', '2016-01-07 00:12:53', '', 'g5a95qspqccvekotked7c4tbn1', '2016-01-07', 'AD', NULL, NULL),
(751, 'IXBB-4438', '112.196.136.78', '2016-01-07 01:26:00', '', 'ilvejm193kq4o2n5ohonltnrh4', '2016-01-07', 'AM', NULL, NULL),
(752, 'YWFZ-6999', '112.196.136.78', '2016-01-07 02:14:06', '2016-01-07 04:17:23am', '032fp9rtrigejolirc5v1gosv6', '2016-01-07', 'QF', NULL, NULL),
(753, 'GPUJ-2680', '112.196.136.78', '2016-01-07 04:17:47', '', '1qa0ptphoqkrq5774ggnnmlf96', '2016-01-07', 'TM', NULL, NULL),
(754, 'admin', '112.196.136.78', '2016-01-07 07:55:46', '', 'aqi7t2o0ql1kuuo836nl37vtl0', '2016-01-07', 'AD', NULL, NULL),
(755, 'YWFZ-6999', '112.196.136.78', '2016-01-07 07:57:09', '2016-01-07 07:59:20am', 'htk7bu6f2de2i3vfo5vccv6br5', '2016-01-07', 'QF', NULL, NULL);
INSERT INTO `mst_userlogon` (`logonid`, `userid`, `ipaddress`, `starttime`, `finishtime`, `sessionid`, `logondate`, `roletype`, `created_at`, `updated_at`) VALUES
(756, 'IXBB-4438', '112.196.136.78', '2016-01-07 07:59:49', '', '8hp39ti7k4upl6fjulftlore74', '2016-01-07', 'AM', NULL, NULL),
(757, 'admin', '69.171.142.218', '2016-01-07 11:11:50', '', 'b000ooa1eua8i13qe0ekbo3234', '2016-01-07', 'AD', NULL, NULL),
(758, 'YWFZ-6999', '69.171.142.218', '2016-01-07 11:12:43', '', 'efjtt1egvukuepj5uke3go2mc5', '2016-01-07', 'QF', NULL, NULL),
(759, 'admin', '112.196.136.78', '2016-01-07 23:42:05', '', 'k5kv399jag7q33detd8rf32pl6', '2016-01-07', 'AD', NULL, NULL),
(760, 'admin', '112.196.136.78', '2016-01-08 00:00:51', '', '8jc7dvbuosi909bl708a3felm2', '2016-01-08', 'AD', NULL, NULL),
(761, 'admin', '112.196.136.78', '2016-01-08 01:48:02', '', '2nhg5jgm4dp234o7g4qgiiqiv4', '2016-01-08', 'AD', NULL, NULL),
(762, 'IXBB-4438', '112.196.136.78', '2016-01-08 02:02:31', '2016-01-08 05:00:24am', 'i12ktsj2spi5d87ftpakb083u1', '2016-01-08', 'AM', NULL, NULL),
(763, 'GPUJ-2680', '112.196.136.78', '2016-01-08 05:00:34', '2016-01-08 06:58:16am', 'mlii3e71npt5ju9bht40h5no01', '2016-01-08', 'TM', NULL, NULL),
(764, 'IXBB-4438', '112.196.136.78', '2016-01-08 06:58:49', '', '5t3c1psc52vre1hamif7cj0d35', '2016-01-08', 'AM', NULL, NULL),
(765, 'admin', '112.196.136.78', '2016-01-08 07:27:33', '', 'j24l66nrodeihiiaq3p8a97r90', '2016-01-08', 'AD', NULL, NULL),
(766, 'admin', '69.171.142.218', '2016-01-08 09:13:08', '', 'us0oc45eo2ud4lp4ffjj9jivk6', '2016-01-08', 'AD', NULL, NULL),
(767, 'admin', '69.171.142.218', '2016-01-08 09:28:00', '', 'krjphnl23queo7jaulp5jpp2m6', '2016-01-08', 'AD', NULL, NULL),
(768, 'IXBB-4438', '69.171.142.218', '2016-01-08 09:28:22', '', 'tkraoovj0sg0e9u7m8oimoukv7', '2016-01-08', 'AM', NULL, NULL),
(769, 'admin', '112.196.136.78', '2016-01-09 00:29:07', '', 'qt2ul7bevull36vjjp69tk4he6', '2016-01-09', 'AD', NULL, NULL),
(770, 'YWFZ-6999', '112.196.136.78', '2016-01-09 05:51:51', '', 'r7vfof15cj3pcat3opboihdur7', '2016-01-09', 'QF', NULL, NULL),
(771, 'IXBB-4438', '112.196.136.78', '2016-01-09 05:57:29', '', 'h92lq5s976uglpo30lqogfur53', '2016-01-09', 'AM', NULL, NULL),
(772, 'MSOX-3592', '45.55.199.109', '2016-01-09 06:39:11', '', 'i49a89flttvnd5666298tbrgq2', '2016-01-09', 'AM', NULL, NULL),
(773, 'admin', '112.196.136.78', '2016-01-09 06:50:42', '', 'ihu5m078qg9q64acalaq515dg5', '2016-01-09', 'AD', NULL, NULL),
(774, 'IXBB-4438', '112.196.136.78', '2016-01-09 06:51:28', '', 'p4esprbobn4v9tu0obel2h6tc0', '2016-01-09', 'AM', NULL, NULL),
(775, 'admin', '112.196.136.78', '2016-01-11 00:07:43', '', 'c7h3p7ncc7ojd0g47kvmau2iq5', '2016-01-11', 'AD', NULL, NULL),
(776, 'JUQI-3911', '112.196.136.78', '2016-01-11 05:32:16', '', 'q2dg0c72vs6lli1vhh22llgp02', '2016-01-11', 'AM', NULL, NULL),
(777, 'MSOX-3592', '112.196.136.78', '2016-01-11 05:33:18', '2016-01-11 05:34:57am', 'fi9v727lvavlumt1dmo7a4ffc2', '2016-01-11', 'AM', NULL, NULL),
(778, 'MSOX-3592', '112.196.136.78', '2016-01-11 05:35:07', '', '39v9ghc018k321d86341puaq64', '2016-01-11', 'AM', NULL, NULL),
(779, 'admin', '69.171.142.218', '2016-01-11 15:20:21', '', 'dmh0fv0hbsbbe5dgvoifn8v856', '2016-01-11', 'AD', NULL, NULL),
(780, 'admin', '112.196.136.78', '2016-01-12 01:45:06', '', 'tkpa2u7trmo9sod8ulci80de87', '2016-01-12', 'AD', NULL, NULL),
(781, 'IXBB-4438', '112.196.136.78', '2016-01-12 01:56:28', '2016-01-12 05:30:40am', 'q2gkgd0lbm7unb3t6bsvsvkpm1', '2016-01-12', 'AM', NULL, NULL),
(782, 'VYOC-6443', '112.196.136.78', '2016-01-12 05:31:05', '2016-01-12 05:37:17am', 'as4a1a86df3r807n6ml7g2dqf3', '2016-01-12', 'QF', NULL, NULL),
(783, 'YSZJ-4486', '112.196.136.78', '2016-01-12 05:37:33', '', 'tlmfjukipnpenqr2f7p5sg2h80', '2016-01-12', 'QF', NULL, NULL),
(784, 'YWFZ-6999', '112.196.136.78', '2016-01-12 05:38:56', '', 'j097l0o1jdsrkkn0pvo3njb3o3', '2016-01-12', 'QF', NULL, NULL),
(785, 'admin', '112.196.136.78', '2016-01-12 06:51:54', '2016-01-12 06:52:23am', 'q98m9dljm9nkee0vdvu1b0kdj3', '2016-01-12', 'AD', NULL, NULL),
(786, 'JUQI-3911', '112.196.136.78', '2016-01-12 06:52:30', '2016-01-12 06:55:24am', '486he98k7q0dcpd6alolhevla5', '2016-01-12', 'AM', NULL, NULL),
(787, 'admin', '112.196.136.78', '2016-01-12 06:55:32', '', 'ure7nfpocp4bcfbm9lb0741gb2', '2016-01-12', 'AD', NULL, NULL),
(788, 'admin', '69.171.142.218', '2016-01-12 08:24:17', '', '6qtkorrao6ajcri55d8ebgq3h0', '2016-01-12', 'AD', NULL, NULL),
(789, 'IXBB-4438', '69.171.142.218', '2016-01-12 08:26:57', '', 'muibrjrk65nmqfvlalmnnnosj4', '2016-01-12', 'AM', NULL, NULL),
(790, 'admin', '99.224.139.23', '2016-01-12 10:23:49', '', 'mp8berb68n7lfhfhdpjrrrd5g4', '2016-01-12', 'AD', NULL, NULL),
(791, 'admin', '69.171.142.218', '2016-01-12 12:26:43', '', '1vmh6orvh1gvlv6a2acp535q97', '2016-01-12', 'AD', NULL, NULL),
(792, 'admin', '69.171.142.218', '2016-01-12 13:36:37', '', 'jitenpa55kcvuf80k2mtf3tl00', '2016-01-12', 'AD', NULL, NULL),
(793, 'admin', '69.171.142.218', '2016-01-12 13:37:50', '', 'grcm1dqiqvkgmg0j1a9skkgiu0', '2016-01-12', 'AD', NULL, NULL),
(794, 'admin', '69.171.142.218', '2016-01-12 13:56:38', '', 's9kha4ju66qsri6miuljnv1l12', '2016-01-12', 'AD', NULL, NULL),
(795, 'admin', '69.171.142.218', '2016-01-12 14:02:40', '2016-01-12 14:04:24pm', 'idmibtscmue94kb7ghg8a74al3', '2016-01-12', 'AD', NULL, NULL),
(796, 'admin', '69.171.142.218', '2016-01-12 14:04:31', '', 'ci7ld3bhc8nem24vuhg10k2l83', '2016-01-12', 'AD', NULL, NULL),
(797, 'admin', '69.171.142.218', '2016-01-12 14:12:26', '', 'voesuspvtlfuns07rduc5ojj30', '2016-01-12', 'AD', NULL, NULL),
(798, 'admin', '112.196.136.78', '2016-01-12 23:47:00', '2016-01-13 04:33:13am', 'm1tmb4jssostepgig1bjnh4o20', '2016-01-12', 'AD', NULL, NULL),
(799, 'admin', '112.196.136.78', '2016-01-13 02:08:33', '', 'sns6fqlmrlj7aasm3t4jknhbv2', '2016-01-13', 'AD', NULL, NULL),
(800, 'admin', '112.196.136.78', '2016-01-13 04:32:42', '', '9epk2un2mthour0oqrsj8m06q3', '2016-01-13', 'AD', NULL, NULL),
(801, 'admin', '69.171.142.218', '2016-01-13 12:14:29', '', '953m32pertu8fnil2q0jppc2b0', '2016-01-13', 'AD', NULL, NULL),
(802, 'admin', '69.171.142.218', '2016-01-13 12:14:51', '', '7ciogo0n9o6umrt27s5qlli2r6', '2016-01-13', 'AD', NULL, NULL),
(803, 'admin', '69.171.142.218', '2016-01-13 12:19:19', '', '7biev57t5uqnplkpqjf52lr8b3', '2016-01-13', 'AD', NULL, NULL),
(804, 'admin', '69.171.142.218', '2016-01-13 12:21:25', '', 'hs35o953gppjatam7t29pi0p97', '2016-01-13', 'AD', NULL, NULL),
(805, 'admin', '69.171.142.218', '2016-01-13 12:22:28', '', 'ndrmie2a9a88mr3vv7v1rma8t2', '2016-01-13', 'AD', NULL, NULL),
(806, 'admin', '69.171.142.218', '2016-01-13 14:50:37', '', '97mco6v7p8tjsn1jegtpdfi3f1', '2016-01-13', 'AD', NULL, NULL),
(807, 'admin', '69.171.142.218', '2016-01-13 15:00:50', '', 'cvvtldmm3kgkc887vsfs4bqrh6', '2016-01-13', 'AD', NULL, NULL),
(808, 'admin', '112.196.136.78', '2016-01-13 23:34:37', '', 'cmdf9e6dvcnq40trhrf0ou6de4', '2016-01-13', 'AD', NULL, NULL),
(809, 'IXBB-4438', '112.196.136.78', '2016-01-13 23:57:12', '2016-01-14 03:39:21am', 'g5petieosbsuk80l0qjaq139h3', '2016-01-13', 'AM', NULL, NULL),
(810, 'JUQI-3911', '112.196.136.78', '2016-01-14 01:30:31', '2016-01-14 05:14:54am', 'r97ncj49lr65ovhurse8abk000', '2016-01-14', 'AM', NULL, NULL),
(811, 'YSZJ-4486', '104.236.225.168', '2016-01-14 01:37:35', '2016-01-14 05:15:26am', '3i24qccaducrvam51702rfdn83', '2016-01-14', 'QF', NULL, NULL),
(812, 'YWFZ-6999', '112.196.136.78', '2016-01-14 03:39:33', '', 'etng0f9k3phuic9vh0lsc6nse3', '2016-01-14', 'QF', NULL, NULL),
(813, 'admin', '112.196.136.78', '2016-01-14 05:13:03', '', '9k70mfkeq967emptremgq463p6', '2016-01-14', 'AD', NULL, NULL),
(814, 'admin', '112.196.136.78', '2016-01-14 05:20:34', '2016-01-14 05:33:08am', 'p2ot53rdvsusum2ccl1j6s2cd5', '2016-01-14', 'AD', NULL, NULL),
(815, 'admin', '112.196.136.78', '2016-01-14 05:31:18', '', 'bbo965d6gk7lr7ju9e599umbh7', '2016-01-14', 'AD', NULL, NULL),
(816, 'admin', '112.196.136.78', '2016-01-14 05:33:25', '', 'b98amn09bhb0u8b1j1ieg9lte1', '2016-01-14', 'AD', NULL, NULL),
(817, 'admin', '112.196.136.78', '2016-01-14 06:01:10', '', 'n1jrcbo73uh95lhj3cvv5m6js2', '2016-01-14', 'AD', NULL, NULL),
(818, 'admin', '112.196.136.78', '2016-01-14 06:54:29', '', 'pr5v6r5dre6gtve7049krftvi2', '2016-01-14', 'AD', NULL, NULL),
(819, 'admin', '112.196.136.78', '2016-01-14 07:50:15', '', '77d11e76rlmgrrjs8o0eghbur5', '2016-01-14', 'AD', NULL, NULL),
(820, 'admin', '112.196.136.78', '2016-01-14 07:51:16', '', 'dnatvvg6m474s0m13sl6361514', '2016-01-14', 'AD', NULL, NULL),
(821, 'admin', '69.171.142.218', '2016-01-14 09:09:30', '', '3qg4att5odfbo6logequb1aac4', '2016-01-14', 'AD', NULL, NULL),
(822, 'admin', '69.171.142.218', '2016-01-14 11:51:13', '', '2d80rt06drj5cfproulpun7fm1', '2016-01-14', 'AD', NULL, NULL),
(823, 'admin', '66.49.211.86', '2016-01-14 12:28:36', '2016-01-14 12:29:18pm', 'ubbrcml747om3i17sc3ikcjbf2', '2016-01-14', 'AD', NULL, NULL),
(824, 'admin', '69.171.142.218', '2016-01-14 15:11:53', '', 'oulojqdqolfpraaueeejop9tf5', '2016-01-14', 'AD', NULL, NULL),
(825, 'admin', '69.171.142.218', '2016-01-14 15:12:32', '2016-01-14 15:25:16pm', '632bdoq4is8qgbfj65n58ofva5', '2016-01-14', 'AD', NULL, NULL),
(826, 'admin', '69.171.142.218', '2016-01-14 15:28:07', '', 'eg9aqvs0d21itj3r8rnu4t0hl2', '2016-01-14', 'AD', NULL, NULL),
(827, 'admin', '66.49.211.86', '2016-01-14 16:28:26', '2016-01-14 16:31:54pm', 'srra49s09v8u3gn7jb0vu3bkk5', '2016-01-14', 'AD', NULL, NULL),
(828, 'admin', '69.171.142.218', '2016-01-14 16:29:57', '', '5ihrc27bt7rfl5nk3r66ao9jm2', '2016-01-14', 'AD', NULL, NULL),
(829, 'admin', '112.196.136.78', '2016-01-15 00:23:49', '', 'vhb2mhms257vccn1ecp7avpsc6', '2016-01-15', 'AD', NULL, NULL),
(830, 'admin', '112.196.136.78', '2016-01-15 03:35:22', '', '02fviedigf6b6soa1joe2voll6', '2016-01-15', 'AD', NULL, NULL),
(831, 'admin', '112.196.136.78', '2016-01-15 05:03:52', '', 'bm636bsfjfbgos8vu5ita3r1j4', '2016-01-15', 'AD', NULL, NULL),
(832, 'admin', '112.196.136.78', '2016-01-15 05:05:37', '', '2ik1sdl3uvvecohonvub1vpqf5', '2016-01-15', 'AD', NULL, NULL),
(833, 'admin', '112.196.136.78', '2016-01-15 05:05:37', '2016-01-15 05:23:39am', 'nh2u8ou0b14dq98g379fq918k6', '2016-01-15', 'AD', NULL, NULL),
(834, 'admin', '112.196.136.78', '2016-01-15 05:15:29', '', 'vlhllntale95geu2hfetqnmnt5', '2016-01-15', 'AD', NULL, NULL),
(835, 'admin', '112.196.136.78', '2016-01-15 05:23:49', '2016-01-15 07:16:19am', 'qndd0kbge7sob8mnb22nh5nvi7', '2016-01-15', 'AD', NULL, NULL),
(836, 'admin', '112.196.136.78', '2016-01-15 07:13:21', '', 'mohdp1032ao97an3bjlf7vr0i5', '2016-01-15', 'AD', NULL, NULL),
(837, 'admin', '112.196.136.78', '2016-01-15 07:16:35', '', 'd3vbip0npr39aqu1vfcnejv3q6', '2016-01-15', 'AD', NULL, NULL),
(838, 'admin', '112.196.136.78', '2016-01-15 07:35:54', '', 'ac0ot43b18gth3pd2r1vgkp0q4', '2016-01-15', 'AD', NULL, NULL),
(839, 'admin', '112.196.136.78', '2016-01-16 00:52:53', '', 'lad206vpaveu1a9nm6tkvvhh07', '2016-01-16', 'AD', NULL, NULL),
(840, 'admin', '112.196.136.78', '2016-01-16 00:54:20', '', '868dvlgspkilkssd5go49ksdc1', '2016-01-16', 'AD', NULL, NULL),
(841, 'admin', '112.196.136.78', '2016-01-16 00:55:50', '', 'c7p5pvmf0c66noq132jh26rtt0', '2016-01-16', 'AD', NULL, NULL),
(842, 'admin', '112.196.136.78', '2016-01-16 03:40:10', '', 'k6i849luii4adibrh2qp7o3ki7', '2016-01-16', 'AD', NULL, NULL),
(843, 'admin', '69.171.142.218', '2016-01-18 08:46:10', '', 'tplemtco3ttpfqnln3k9sfori1', '2016-01-18', 'AD', NULL, NULL),
(844, 'IXBB-4438', '69.171.142.218', '2016-01-18 08:58:19', '', 'pb8ibdphbe2kanbtjp19osdu05', '2016-01-18', 'AM', NULL, NULL),
(845, 'IXBB-4438', '69.171.142.218', '2016-01-18 09:06:31', '2016-01-18 09:48:05am', 'l3q508ka1potsj2q2g6teili86', '2016-01-18', 'AM', NULL, NULL),
(846, 'YSZJ-4486', '69.171.142.218', '2016-01-18 09:48:08', '', 'ek5atlfdneup1d0la4gqgfksu0', '2016-01-18', 'QF', NULL, NULL),
(847, 'admin', '69.171.142.218', '2016-01-18 11:51:00', '', 'juqkr22hnqqtpbmjp3fes7kla3', '2016-01-18', 'AD', NULL, NULL),
(848, 'OORQ-0229', '69.171.142.218', '2016-01-18 11:53:47', '', 'ur18h772e1cihcmmjb91qkvc91', '2016-01-18', 'AM', NULL, NULL),
(849, 'admin', '112.196.136.78', '2016-01-18 23:26:02', '', '5r9qkoqu932q6bst866vtm4df1', '2016-01-18', 'AD', NULL, NULL),
(850, 'admin', '112.196.136.78', '2016-01-19 00:42:36', '', 'cssjqnvmjhisbgk02k2cm8adg2', '2016-01-19', 'AD', NULL, NULL),
(851, 'admin', '112.196.136.78', '2016-01-19 00:49:07', '', '9k6dp7r57kb8vr0rj8nk3aj2d3', '2016-01-19', 'AD', NULL, NULL),
(852, 'admin', '69.171.142.218', '2016-01-19 09:00:04', '', 'l9jbk8hv8aij33sti84bc7lul7', '2016-01-19', 'AD', NULL, NULL),
(853, 'admin', '69.171.142.218', '2016-01-19 09:00:55', '', 'v5rm0jr2n8cmipb7fpc6ipp6b1', '2016-01-19', 'AD', NULL, NULL),
(854, 'admin', '69.171.142.218', '2016-01-19 09:01:47', '', 'u3c7fvconfanf1v8je6psubga6', '2016-01-19', 'AD', NULL, NULL),
(855, 'admin', '66.49.211.86', '2016-01-19 09:05:26', '2016-01-19 09:14:08am', 'ld1kssfd5l9ccetbtbr2esdrl4', '2016-01-19', 'AD', NULL, NULL),
(856, 'admin', '69.171.142.218', '2016-01-19 09:05:58', '', 'ibfr6mn0ud0n3a73ba3siqpua1', '2016-01-19', 'AD', NULL, NULL),
(857, 'admin', '69.171.142.218', '2016-01-19 09:08:23', '', '0mt5o0pg0v64ssd3od8heh92o3', '2016-01-19', 'AD', NULL, NULL),
(858, 'admin', '69.171.142.218', '2016-01-19 09:12:46', '', 'rd4elpfjugbvgjg0en8a7krt30', '2016-01-19', 'AD', NULL, NULL),
(859, 'admin', '66.49.211.86', '2016-01-19 09:14:45', '2016-01-19 09:16:09am', 'casqiqn1r3d55mcbrd07esi622', '2016-01-19', 'AD', NULL, NULL),
(860, 'admin', '66.49.211.86', '2016-01-19 09:33:43', '2016-01-19 09:34:18am', '45dm48gg4s4r9cjaesuvdknko3', '2016-01-19', 'AD', NULL, NULL),
(861, 'OORQ-0229', '66.49.211.86', '2016-01-19 09:34:43', '2016-01-19 11:11:24am', 'fofv5v4hu3n9ec8kr8catp46d0', '2016-01-19', 'AM', NULL, NULL),
(862, 'admin', '69.171.142.218', '2016-01-19 09:36:10', '', 't65l507u3qki2dldhdtpe8hao2', '2016-01-19', 'AD', NULL, NULL),
(863, 'OORQ-0229', '69.171.142.218', '2016-01-19 09:41:42', '', '1mnrgmcf0kvu5pg2vtcqn7vao6', '2016-01-19', 'AM', NULL, NULL),
(864, 'OORQ-0229', '69.171.142.218', '2016-01-19 10:11:55', '', '304d8nh7p67uo5dsp639sn3833', '2016-01-19', 'AM', NULL, NULL),
(865, 'admin', '66.49.211.86', '2016-01-20 10:53:01', '', 'gikl515oah3e91ffb4r67s2p56', '2016-01-20', 'AD', NULL, NULL),
(866, 'admin', '112.196.136.78', '2016-01-21 07:21:57', '', 'kkcs572685dldr1s8nhap87s34', '2016-01-21', 'AD', NULL, NULL),
(867, 'admin', '69.171.142.218', '2016-01-21 10:49:00', '2016-01-21 10:49:09am', 'j2qr08p2vq3hvm3rq8oinqalc3', '2016-01-21', 'AD', NULL, NULL),
(868, 'IXBB-4438', '69.171.142.218', '2016-01-21 10:49:17', '2016-01-21 10:56:49am', '5lkr656kf1stin8ancb2ti7k24', '2016-01-21', 'AM', NULL, NULL),
(869, 'admin', '69.171.142.218', '2016-01-21 10:56:54', '', '2uoehgbjafc2isregf7kmfa057', '2016-01-21', 'AD', NULL, NULL),
(870, 'admin', '69.171.142.218', '2016-01-25 11:56:15', '', 'lte45u5j98g8psnncljibfu0k1', '2016-01-25', 'AD', NULL, NULL),
(871, 'admin', '69.171.142.218', '2016-01-25 12:01:31', '', 'gemg6ujsm6miqfi81sfdlhd530', '2016-01-25', 'AD', NULL, NULL),
(872, 'admin', '66.49.211.86', '2016-01-26 08:12:53', '2016-01-26 08:13:14am', '5j9eiohs1fjouah68lqomqd1h7', '2016-01-26', 'AD', NULL, NULL),
(873, 'admin', '1.39.49.146', '2016-01-27 06:51:15', '2016-01-27 06:58:20am', '6a6gq3up3gcfqgf026ncnj3i17', '2016-01-27', 'AD', NULL, NULL),
(874, 'admin', '67.55.16.226', '2016-01-27 11:07:28', '', 'h0pm204fdinqnkgeleic831137', '2016-01-27', 'AD', NULL, NULL),
(875, 'admin', '67.55.16.226', '2016-01-27 12:21:05', '', 'f976c9akpvf59kcqt5bqhkj1e3', '2016-01-27', 'AD', NULL, NULL),
(876, 'admin', '189.102.169.77', '2016-01-27 14:25:46', '', 'ar2o4uogjgq0jhbv4eft2c8g20', '2016-01-27', 'AD', NULL, NULL),
(877, 'admin', '67.55.16.226', '2016-01-27 15:42:35', '', 'imb6i6pmcmustre4olh09liuv3', '2016-01-27', 'AD', NULL, NULL),
(878, 'admin', '67.55.16.226', '2016-01-28 09:29:33', '', '3mfcj8is0sl8rrrf18dfqoijj0', '2016-01-28', 'AD', NULL, NULL),
(879, 'admin', '67.55.16.226', '2016-01-28 12:42:21', '', '0q2b89e5086h0m872uh89ranq2', '2016-01-28', 'AD', NULL, NULL),
(880, 'admin', '125.62.103.136', '2016-01-28 14:52:33', '', 'moi1da01iarrkadst63gsf1u25', '2016-01-28', 'AD', NULL, NULL),
(881, 'admin', '66.49.250.8', '2016-01-28 15:34:36', '2016-01-28 15:34:45pm', 'b39374nlst3ftqst2cb0qph5k2', '2016-01-28', 'AD', NULL, NULL),
(882, 'admin', '182.70.216.60', '2016-01-28 16:09:13', '', '582virek8nanhpp5u2k4h6g8f3', '2016-01-28', 'AD', NULL, NULL),
(883, 'admin', '43.229.224.130', '2016-01-29 03:18:20', '', 'tg8affpbd5bciob8l04j5plb73', '2016-01-29', 'AD', NULL, NULL),
(884, 'admin', '43.229.224.130', '2016-01-29 03:48:42', '', 'f0bclghpd1ob7etaa9pkc60kp5', '2016-01-29', 'AD', NULL, NULL),
(885, 'admin', '103.44.18.3', '2016-01-29 11:35:38', '', 'jfhqulmdmi0halb022eg46el12', '2016-01-29', 'AD', NULL, NULL),
(886, 'admin', '67.55.16.226', '2016-01-29 14:00:46', '', '6ouun8mnr6pccpij38j10aifk5', '2016-01-29', 'AD', NULL, NULL),
(887, 'admin', '67.55.16.226', '2016-01-29 16:40:51', '', '6b5790fcq5hoe02jdaegb1e5c0', '2016-01-29', 'AD', NULL, NULL),
(888, 'admin', '67.55.16.226', '2016-01-29 16:44:27', '', 'aeu72ft2pp6nsq2a0ttoqq54l7', '2016-01-29', 'AD', NULL, NULL),
(889, 'admin', '191.181.15.233', '2016-01-29 17:11:12', '', 'poe8g8mk38m9drjmoeuf11k6m0', '2016-01-29', 'AD', NULL, NULL),
(890, 'admin', '191.181.15.233', '2016-02-01 08:57:43', '', '5s6srsfd533t54dqnnj3eun752', '2016-02-01', 'AD', NULL, NULL),
(891, 'admin', '66.49.230.207', '2016-02-01 10:18:44', '', '1dlsa2e6ipo2g8rns4v5jurkl6', '2016-02-01', 'AD', NULL, NULL),
(892, 'admin', '66.49.230.207', '2016-02-01 11:43:29', '', '97m2ggnjrmhv0fmpvtbg8k8v45', '2016-02-01', 'AD', NULL, NULL),
(893, 'admin', '66.49.230.207', '2016-02-01 12:08:32', '', 'sae5n032hoduqa3adjoesqn8e5', '2016-02-01', 'AD', NULL, NULL),
(894, 'admin', '68.228.72.198', '2016-02-01 12:42:26', '', 'qh227dcji3gl4qlmo5emao9fg1', '2016-02-01', 'AD', NULL, NULL),
(895, 'admin', '66.49.230.207', '2016-02-01 14:28:29', '', 'dltvkgi0eo5kaipgbbmfe3qd97', '2016-02-01', 'AD', NULL, NULL),
(896, 'admin', '105.96.200.187', '2016-02-01 15:17:58', '', '567ict3t1q019ctktkonmv0f94', '2016-02-01', 'AD', NULL, NULL),
(897, 'admin', '105.96.200.187', '2016-02-01 15:18:36', '', 'ffp3jqcn649392363g3c93fse6', '2016-02-01', 'AD', NULL, NULL),
(898, 'admin', '68.55.116.144', '2016-02-02 10:03:46', '', 'lhbn1599ed17n11hl29so8ds20', '2016-02-02', 'AD', NULL, NULL),
(899, 'admin', '85.90.202.232', '2016-02-02 11:17:22', '', 'u3urdohan1quqc0t97mii75fr6', '2016-02-02', 'AD', NULL, NULL),
(900, 'admin', '66.49.230.207', '2016-02-02 11:21:13', '', '1rlbrn9iung8grrlg1fvirtv93', '2016-02-02', 'AD', NULL, NULL),
(901, 'admin', '85.90.202.232', '2016-02-02 11:22:47', '', '4687tru2tjcdpkaoghllm2lop6', '2016-02-02', 'AD', NULL, NULL),
(902, 'admin', '85.139.207.27', '2016-02-02 11:24:53', '', 'gqoum5qeodu69u16jb6bqkojm7', '2016-02-02', 'AD', NULL, NULL),
(903, 'admin', '85.139.207.27', '2016-02-02 11:26:39', '', 'vpgvc1nq6pcani2ku9pko0us52', '2016-02-02', 'AD', NULL, NULL),
(904, 'admin', '82.103.130.198', '2016-02-02 11:32:14', '', 'r7kr65icfhda3s1m6frn9qrgo6', '2016-02-02', 'AD', NULL, NULL),
(905, 'admin', '178.157.82.37', '2016-02-02 11:36:51', '', 'a0l0mfpd5002l7mqabp8ldu6f2', '2016-02-02', 'AD', NULL, NULL),
(906, 'admin', '89.28.85.210', '2016-02-02 13:45:52', '', '3r7e94l64qibfld6v16kprn9m0', '2016-02-02', 'AD', NULL, NULL),
(907, 'admin', '212.109.29.41', '2016-02-02 13:46:27', '', '0ng2u12nopuakm13i6k8lv59c1', '2016-02-02', 'AD', NULL, NULL),
(908, 'admin', '193.106.163.188', '2016-02-02 13:47:16', '', 'lu8ott27kukov1og7og0997ls4', '2016-02-02', 'AD', NULL, NULL),
(909, 'admin', '123.236.192.153', '2016-02-02 15:50:23', '', '5dtr7koc2m0u85frujdvnrqdi3', '2016-02-02', 'AD', NULL, NULL),
(910, 'admin', '103.255.5.60', '2016-02-02 16:24:42', '', '4u9k3vpbbqhjjr5flrr3ncl3v6', '2016-02-02', 'AD', NULL, NULL),
(911, 'admin', '182.70.207.244', '2016-02-02 16:33:51', '', '8ifmmgd45nbsvick1nt9l3bg77', '2016-02-02', 'AD', NULL, NULL),
(912, 'admin', '103.255.5.60', '2016-02-02 16:48:22', '', '36ni8cuot65m2lbkb3bem14kq7', '2016-02-02', 'AD', NULL, NULL),
(913, 'admin', '193.106.163.188', '2016-02-03 00:57:49', '', '9qk0qiv2v9m0oe7ho1f6tulng0', '2016-02-03', 'AD', NULL, NULL),
(914, 'admin', '103.255.7.30', '2016-02-03 03:23:48', '2016-02-03 03:24:29am', 'bc5n3oec4kqeju94j3ctro9dq1', '2016-02-03', 'AD', NULL, NULL),
(915, 'admin', '103.255.7.30', '2016-02-03 03:24:33', '2016-02-03 04:01:50am', 'chl0a6u3in448g8s3ultn9dnm3', '2016-02-03', 'AD', NULL, NULL),
(916, 'admin', '182.185.211.129', '2016-02-03 04:02:03', '', 'lqclac3mslbvqk8s5tch5ep2e7', '2016-02-03', 'AD', NULL, NULL),
(917, 'admin', '85.90.202.232', '2016-02-03 04:24:15', '', 'efmthtdgjmupcrmimitijorg30', '2016-02-03', 'AD', NULL, NULL),
(918, 'admin', '182.185.155.49', '2016-02-03 05:34:33', '', 'ruj7qvatjsiambdgeb50u4k0c6', '2016-02-03', 'AD', NULL, NULL),
(919, 'admin', '96.95.174.17', '2016-02-03 09:51:28', '', '3bglk92istooa0rccutdv3nu40', '2016-02-03', 'AD', NULL, NULL),
(920, 'admin', '96.95.174.17', '2016-02-03 09:55:52', '', 'k4k2me22odlidcfoteaj0rqbc6', '2016-02-03', 'AD', NULL, NULL),
(921, 'admin', '68.55.116.144', '2016-02-03 11:52:19', '', '5rla2k7o2rncua6k76gq1sinr7', '2016-02-03', 'AD', NULL, NULL),
(922, 'admin', '122.168.201.150', '2016-02-03 13:05:48', '', 'oo02nc3u0q590slaandb0dhrc5', '2016-02-03', 'AD', NULL, NULL),
(923, 'admin', '66.49.230.207', '2016-02-03 16:19:45', '', 'brqv1inhucjvgvtldg4g38v651', '2016-02-03', 'AD', NULL, NULL),
(924, 'admin', '123.236.192.153', '2016-02-03 17:18:19', '', '7idf7d94f0vjlvggnqh5ab57p4', '2016-02-03', 'AD', NULL, NULL),
(925, 'admin', '49.156.148.228', '2016-02-04 04:02:05', '', 'dsp3depks2m8haidi5j7ugg9u5', '2016-02-04', 'AD', NULL, NULL),
(926, 'admin', '68.55.116.144', '2016-02-04 10:04:46', '', 'dqh9gc9sbknb0pc3ksm7fk4f91', '2016-02-04', 'AD', NULL, NULL),
(927, 'admin', '66.49.230.207', '2016-02-04 10:41:40', '', 's0grm9l84ha3i4693simkkrvk4', '2016-02-04', 'AD', NULL, NULL),
(928, 'admin', '66.49.230.207', '2016-02-04 11:56:03', '', 'pbl4m7tmtfieikl7mmmck8ft74', '2016-02-04', 'AD', NULL, NULL),
(929, 'admin', '66.49.230.207', '2016-02-04 12:29:26', '', 'b8p20u1c8nflt52t45aq8g3261', '2016-02-04', 'AD', NULL, NULL),
(930, 'admin', '66.49.230.207', '2016-02-04 12:33:16', '', 'qbu27i6rsjkc9vbonndlg77h92', '2016-02-04', 'AD', NULL, NULL),
(931, 'admin', '66.49.230.207', '2016-02-04 13:56:20', '', 'olofgnquonirqea3gmbmp8jtk4', '2016-02-04', 'AD', NULL, NULL),
(932, 'admin', '66.49.230.207', '2016-02-05 10:59:05', '', '2imt3pqt792kl43i1cvg9689c0', '2016-02-05', 'AD', NULL, NULL),
(933, 'admin', '66.49.230.207', '2016-02-05 11:25:09', '', 'hpu7n639ifft4k5d7ngdjt4hm5', '2016-02-05', 'AD', NULL, NULL),
(934, 'admin', '68.55.116.144', '2016-02-05 13:38:35', '', 'q3bpglj5h0r8ml5rbb8jbfivt1', '2016-02-05', 'AD', NULL, NULL),
(935, 'admin', '182.70.180.191', '2016-02-08 06:39:47', '', '1qr1durvko42kvknqigbfg5tg4', '2016-02-08', 'AD', NULL, NULL),
(936, 'admin', '182.70.180.191', '2016-02-08 09:23:39', '2016-02-08 09:52:45am', 'g7bu051fms5hnvufhqb58url16', '2016-02-08', 'AD', NULL, NULL),
(937, 'admin', '182.70.179.183', '2016-02-09 08:57:51', '', 'gu34ujbrhe2ub0l0av30qoco07', '2016-02-09', 'AD', NULL, NULL),
(938, 'admin', '122.175.176.131', '2016-02-10 05:06:14', '', 'lkfpckq01naaqtnd9gfd0gruo1', '2016-02-10', 'AD', NULL, NULL),
(939, 'admin', '122.175.176.131', '2016-02-10 06:44:31', '', '4lknf5rj3l2b14q67d9ivktcd3', '2016-02-10', 'AD', NULL, NULL),
(940, 'admin', '122.175.176.131', '2016-02-10 13:29:36', '', 'd85agfmgu5rc5nmscrprau5mb0', '2016-02-10', 'AD', NULL, NULL),
(941, 'admin', '122.175.218.0', '2016-02-11 04:00:22', '', 'p1a0r04fitq2gtl5a7ibi7per4', '2016-02-11', 'AD', NULL, NULL),
(942, 'admin', '122.175.176.131', '2016-02-11 04:49:07', '', '10qpnsiopi3u2oahvaesqt9ep0', '2016-02-11', 'AD', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_userrole`
--

CREATE TABLE `mst_userrole` (
  `roleid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `rolename` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_userrole`
--

INSERT INTO `mst_userrole` (`roleid`, `name`, `isactive`, `code`, `rolename`) VALUES
(1, 'Account Manager', '1', 'AM', 'Account Manager'),
(2, 'Quote Facilitator', '1', 'QF', 'Quote Facilitator'),
(3, 'Trigger Man', '1', 'TM', 'Trigger Man'),
(4, 'Administrator', '1', 'AD', 'Sub Administrator'),
(5, 'Administrator', '1', 'AD', 'Administrator'),
(18, 'Account Manager', '1', 'AM', 'Partner');

-- --------------------------------------------------------

--
-- Table structure for table `old_useractivitypurge`
--

CREATE TABLE `old_useractivitypurge` (
  `actionid` int(10) UNSIGNED NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_master`
--

CREATE TABLE `page_master` (
  `p_id` int(10) UNSIGNED NOT NULL,
  `p_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `p_show_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_path` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_notification`
--

CREATE TABLE `page_notification` (
  `n_id` int(10) UNSIGNED NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `n_variable` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `final_noti` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `n_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_advertisment`
--

CREATE TABLE `post_advertisment` (
  `postid` int(10) UNSIGNED NOT NULL,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pdate` datetime NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `industry` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ptype` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerrefno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pakaging` text COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `targetprice` int(11) NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `tempid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `archivepost` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `buyer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotefacility` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `act_inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_customer`
--

CREATE TABLE `post_customer` (
  `custid` int(10) UNSIGNED NOT NULL,
  `refno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `edate` date NOT NULL,
  `refuserid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `adduserid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_quotation`
--

CREATE TABLE `post_quotation` (
  `quoteid` int(10) UNSIGNED NOT NULL,
  `quotationno` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` datetime NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `declinemsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offerdeclinemsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptmsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offercrdate` datetime NOT NULL,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quationstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `offerstatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `accdate` date DEFAULT NULL,
  `rejdate` date DEFAULT NULL,
  `lastcntdate` date DEFAULT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `counetr_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `postuserid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `readunreadst` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productimport`
--

CREATE TABLE `productimport` (
  `productid` bigint(20) UNSIGNED NOT NULL,
  `productno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcat` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` text COLLATE utf8_unicode_ci,
  `packaging` text COLLATE utf8_unicode_ci,
  `shippingcond` text COLLATE utf8_unicode_ci,
  `qtypercase` bigint(20) DEFAULT NULL,
  `des` text COLLATE utf8_unicode_ci,
  `mpm` double(8,2) DEFAULT NULL,
  `prefixone` text COLLATE utf8_unicode_ci,
  `codeone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixtwo` tinyint(4) DEFAULT NULL,
  `codetwo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixthree` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codethree` text COLLATE utf8_unicode_ci,
  `img1` text COLLATE utf8_unicode_ci,
  `img2` text COLLATE utf8_unicode_ci,
  `img3` text COLLATE utf8_unicode_ci,
  `img4` text COLLATE utf8_unicode_ci,
  `img5` text COLLATE utf8_unicode_ci,
  `img6` text COLLATE utf8_unicode_ci,
  `img7` text COLLATE utf8_unicode_ci,
  `img8` text COLLATE utf8_unicode_ci,
  `addedby` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_counter`
--

CREATE TABLE `quotation_counter` (
  `counterid` int(10) UNSIGNED NOT NULL,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_date` datetime NOT NULL,
  `counter_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `counter_quantity` int(11) NOT NULL,
  `counter_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counetr_status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(10) UNSIGNED NOT NULL,
  `ind_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Index_val` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `test1`
--

CREATE TABLE `test1` (
  `id` int(10) UNSIGNED NOT NULL,
  `industry` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tm_accepted_offers`
--

CREATE TABLE `tm_accepted_offers` (
  `acc_offr_id` int(10) UNSIGNED NOT NULL,
  `counterid` int(11) NOT NULL,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_date` datetime NOT NULL,
  `acc_price` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_quantity` int(11) NOT NULL,
  `acc_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `acc_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `noti_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tm_userID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `offerType` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countdata`
--
ALTER TABLE `countdata`
  ADD PRIMARY KEY (`recordcount`);

--
-- Indexes for table `count_notification`
--
ALTER TABLE `count_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maskproname`
--
ALTER TABLE `maskproname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mastpage`
--
ALTER TABLE `mastpage`
  ADD PRIMARY KEY (`PageId`);

--
-- Indexes for table `mastrolepermission`
--
ALTER TABLE `mastrolepermission`
  ADD PRIMARY KEY (`RoleId`,`PageId`);

--
-- Indexes for table `mst_admin_report`
--
ALTER TABLE `mst_admin_report`
  ADD PRIMARY KEY (`reportid`);

--
-- Indexes for table `mst_brand`
--
ALTER TABLE `mst_brand`
  ADD PRIMARY KEY (`brandid`);

--
-- Indexes for table `mst_category`
--
ALTER TABLE `mst_category`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `mst_country`
--
ALTER TABLE `mst_country`
  ADD PRIMARY KEY (`countryid`);

--
-- Indexes for table `mst_currency`
--
ALTER TABLE `mst_currency`
  ADD PRIMARY KEY (`currencyid`);

--
-- Indexes for table `mst_exp_daterange`
--
ALTER TABLE `mst_exp_daterange`
  ADD PRIMARY KEY (`exprangeid`);

--
-- Indexes for table `mst_industry`
--
ALTER TABLE `mst_industry`
  ADD PRIMARY KEY (`industryid`);

--
-- Indexes for table `mst_language`
--
ALTER TABLE `mst_language`
  ADD PRIMARY KEY (`languageid`);

--
-- Indexes for table `mst_location`
--
ALTER TABLE `mst_location`
  ADD PRIMARY KEY (`locationid`);

--
-- Indexes for table `mst_mngnotification`
--
ALTER TABLE `mst_mngnotification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_model`
--
ALTER TABLE `mst_model`
  ADD PRIMARY KEY (`modelid`);

--
-- Indexes for table `mst_notification`
--
ALTER TABLE `mst_notification`
  ADD PRIMARY KEY (`notiid`);

--
-- Indexes for table `mst_pagename`
--
ALTER TABLE `mst_pagename`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `mst_product`
--
ALTER TABLE `mst_product`
  ADD PRIMARY KEY (`productid`);

--
-- Indexes for table `mst_productcode`
--
ALTER TABLE `mst_productcode`
  ADD PRIMARY KEY (`productcodeid`);

--
-- Indexes for table `mst_productimages`
--
ALTER TABLE `mst_productimages`
  ADD PRIMARY KEY (`productimageid`);

--
-- Indexes for table `mst_productprefix`
--
ALTER TABLE `mst_productprefix`
  ADD PRIMARY KEY (`proprefixid`);

--
-- Indexes for table `mst_productreport`
--
ALTER TABLE `mst_productreport`
  ADD PRIMARY KEY (`preportid`);

--
-- Indexes for table `mst_productsimilar`
--
ALTER TABLE `mst_productsimilar`
  ADD PRIMARY KEY (`similarid`);

--
-- Indexes for table `mst_report`
--
ALTER TABLE `mst_report`
  ADD PRIMARY KEY (`reportid`);

--
-- Indexes for table `mst_security`
--
ALTER TABLE `mst_security`
  ADD PRIMARY KEY (`securityid`);

--
-- Indexes for table `mst_shipping`
--
ALTER TABLE `mst_shipping`
  ADD PRIMARY KEY (`shipid`);

--
-- Indexes for table `mst_subcategory`
--
ALTER TABLE `mst_subcategory`
  ADD PRIMARY KEY (`subcatid`);

--
-- Indexes for table `mst_timeframe`
--
ALTER TABLE `mst_timeframe`
  ADD PRIMARY KEY (`timeframeid`);

--
-- Indexes for table `mst_timezone`
--
ALTER TABLE `mst_timezone`
  ADD PRIMARY KEY (`timezoneid`);

--
-- Indexes for table `mst_uom`
--
ALTER TABLE `mst_uom`
  ADD PRIMARY KEY (`uomid`);

--
-- Indexes for table `mst_useraction`
--
ALTER TABLE `mst_useraction`
  ADD PRIMARY KEY (`actionid`);

--
-- Indexes for table `mst_userlogin`
--
ALTER TABLE `mst_userlogin`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `mst_userlogon`
--
ALTER TABLE `mst_userlogon`
  ADD PRIMARY KEY (`logonid`);

--
-- Indexes for table `mst_userrole`
--
ALTER TABLE `mst_userrole`
  ADD PRIMARY KEY (`roleid`);

--
-- Indexes for table `old_useractivitypurge`
--
ALTER TABLE `old_useractivitypurge`
  ADD PRIMARY KEY (`actionid`);

--
-- Indexes for table `page_master`
--
ALTER TABLE `page_master`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `page_notification`
--
ALTER TABLE `page_notification`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `post_advertisment`
--
ALTER TABLE `post_advertisment`
  ADD PRIMARY KEY (`postid`);

--
-- Indexes for table `post_customer`
--
ALTER TABLE `post_customer`
  ADD PRIMARY KEY (`custid`);

--
-- Indexes for table `post_quotation`
--
ALTER TABLE `post_quotation`
  ADD PRIMARY KEY (`quoteid`);

--
-- Indexes for table `productimport`
--
ALTER TABLE `productimport`
  ADD PRIMARY KEY (`productid`);

--
-- Indexes for table `quotation_counter`
--
ALTER TABLE `quotation_counter`
  ADD PRIMARY KEY (`counterid`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test1`
--
ALTER TABLE `test1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tm_accepted_offers`
--
ALTER TABLE `tm_accepted_offers`
  ADD PRIMARY KEY (`acc_offr_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countdata`
--
ALTER TABLE `countdata`
  MODIFY `recordcount` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `count_notification`
--
ALTER TABLE `count_notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `maskproname`
--
ALTER TABLE `maskproname`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mastpage`
--
ALTER TABLE `mastpage`
  MODIFY `PageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_admin_report`
--
ALTER TABLE `mst_admin_report`
  MODIFY `reportid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_brand`
--
ALTER TABLE `mst_brand`
  MODIFY `brandid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_category`
--
ALTER TABLE `mst_category`
  MODIFY `catid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mst_country`
--
ALTER TABLE `mst_country`
  MODIFY `countryid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_currency`
--
ALTER TABLE `mst_currency`
  MODIFY `currencyid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_exp_daterange`
--
ALTER TABLE `mst_exp_daterange`
  MODIFY `exprangeid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_industry`
--
ALTER TABLE `mst_industry`
  MODIFY `industryid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mst_language`
--
ALTER TABLE `mst_language`
  MODIFY `languageid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_location`
--
ALTER TABLE `mst_location`
  MODIFY `locationid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_mngnotification`
--
ALTER TABLE `mst_mngnotification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_model`
--
ALTER TABLE `mst_model`
  MODIFY `modelid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_notification`
--
ALTER TABLE `mst_notification`
  MODIFY `notiid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_pagename`
--
ALTER TABLE `mst_pagename`
  MODIFY `rowid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_product`
--
ALTER TABLE `mst_product`
  MODIFY `productid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_productcode`
--
ALTER TABLE `mst_productcode`
  MODIFY `productcodeid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_productimages`
--
ALTER TABLE `mst_productimages`
  MODIFY `productimageid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_productprefix`
--
ALTER TABLE `mst_productprefix`
  MODIFY `proprefixid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_productreport`
--
ALTER TABLE `mst_productreport`
  MODIFY `preportid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_productsimilar`
--
ALTER TABLE `mst_productsimilar`
  MODIFY `similarid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_report`
--
ALTER TABLE `mst_report`
  MODIFY `reportid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_security`
--
ALTER TABLE `mst_security`
  MODIFY `securityid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_shipping`
--
ALTER TABLE `mst_shipping`
  MODIFY `shipid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_subcategory`
--
ALTER TABLE `mst_subcategory`
  MODIFY `subcatid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_timeframe`
--
ALTER TABLE `mst_timeframe`
  MODIFY `timeframeid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_timezone`
--
ALTER TABLE `mst_timezone`
  MODIFY `timezoneid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419;
--
-- AUTO_INCREMENT for table `mst_uom`
--
ALTER TABLE `mst_uom`
  MODIFY `uomid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_useraction`
--
ALTER TABLE `mst_useraction`
  MODIFY `actionid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mst_userlogon`
--
ALTER TABLE `mst_userlogon`
  MODIFY `logonid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=943;
--
-- AUTO_INCREMENT for table `mst_userrole`
--
ALTER TABLE `mst_userrole`
  MODIFY `roleid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `old_useractivitypurge`
--
ALTER TABLE `old_useractivitypurge`
  MODIFY `actionid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_master`
--
ALTER TABLE `page_master`
  MODIFY `p_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_notification`
--
ALTER TABLE `page_notification`
  MODIFY `n_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_advertisment`
--
ALTER TABLE `post_advertisment`
  MODIFY `postid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_customer`
--
ALTER TABLE `post_customer`
  MODIFY `custid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_quotation`
--
ALTER TABLE `post_quotation`
  MODIFY `quoteid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `productimport`
--
ALTER TABLE `productimport`
  MODIFY `productid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quotation_counter`
--
ALTER TABLE `quotation_counter`
  MODIFY `counterid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `test1`
--
ALTER TABLE `test1`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_accepted_offers`
--
ALTER TABLE `tm_accepted_offers`
  MODIFY `acc_offr_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
