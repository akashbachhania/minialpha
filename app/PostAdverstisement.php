<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostAdverstisement extends Model
{
    protected $table = 'post_advertisment';
    protected $primaryKey = 'postid';
    
    public function product(){
    	return $this->belongsTo('App\MstProduct','productno','productno');
    }

    public function postCustomer(){
    	return $this->belongsTo('App\PostCustomer','customerrefno','refno');
    }

    public function user(){
    	return $this->belongsTo('App\User','userid','userid');
    }

}
