<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstCurrency extends Model
{
    protected $table = 'mst_currency';
    protected $primaryKey = 'currencyid';
}
