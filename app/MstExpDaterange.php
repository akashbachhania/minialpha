<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstExpDaterange extends Model
{
    protected $table = 'mst_exp_daterange';
    protected $primaryKey = 'exprangeid';
}
