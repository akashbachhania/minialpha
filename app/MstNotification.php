<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstNotification extends Model
{
    protected $table = 'mst_notification';
    protected $primaryKey = 'notiid';

    function product(){
    	return $this->belongsTo('\App\MstProduct','pro_id','productid');
    }
}
