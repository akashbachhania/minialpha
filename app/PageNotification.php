<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageNotification extends Model
{
    protected $table = 'page_notification';
    protected $primaryKey = 'n_id';
}