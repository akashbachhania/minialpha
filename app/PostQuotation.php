<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostQuotation extends Model
{
    protected $table = 'post_quotation';
    protected $primaryKey = 'quoteid';

    public function product(){
    	return $this->belongsTo('App\MstProduct','productno','productno');    	
    }

    public function postAdverstisment(){
    	return $this->belongsTo('App\PostAdverstisement','postno','postno');    	
    }
}
