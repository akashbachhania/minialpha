<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstUom extends Model
{
    protected $table = 'mst_uom';
    protected $primaryKey = 'uomid';
}
