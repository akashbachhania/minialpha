<?php

namespace App;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

// use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mst_userlogin';
    protected $primaryKey = 'userid';
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userid', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function userLogon(){
      return $this->hasMany('App\UserLogon','userid');
    }

    public function userRole(){
      return $this->belongsTo('App\UserRole','roleid');
    }

    public function getEmailForPasswordReset(){
        
    }

    public function industry()
    {
        return $this->belongsTo('App\Industry', 'industry');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'catid');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brandid');
    }

    public function subcategory(){
        return $this->belongsTo('App\SubCategory','subcatid');
    }
}
