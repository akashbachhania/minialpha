<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstCountry extends Model
{
    protected $table = 'mst_country';
    protected $primaryKey = 'countryid';
}
