<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstLanguage extends Model
{	
	protected $table = 'mst_language';
    protected $primaryKey = 'languageid';
}
