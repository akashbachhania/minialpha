<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstPackaging extends Model
{
    protected $table = 'mst_packaging';
    protected $primaryKey = 'pkgid';
}
