<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstLocation extends Model
{
    protected $table = 'mst_location';
    protected $primaryKey = 'locationid';
}
