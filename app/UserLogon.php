<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserLogon extends Model
{
    protected $table = 'mst_userlogon';

    

    public function user()
    {
      return $this->belongsTo('App\User', 'userid');
    }
}
