<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'mst_brand';
    protected $primaryKey = 'brandid';

    function industry(){
    	return $this->belongsTo('App\Industry','industryid');
    }
}
