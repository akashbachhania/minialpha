<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrefix extends Model
{
    //
    protected $table = 'mst_productprefix';
    protected $primaryKey = 'proprefixid';
}
