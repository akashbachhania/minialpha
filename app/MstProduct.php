<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProduct extends Model
{
    protected $table = 'mst_product';
    protected $primaryKey = 'productid';
    
    public function industry()
    {
        return $this->belongsTo('App\Industry', 'industryid');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'catid');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brandid');
    }

    public function productCode()
    {
        return $this->hasMany('App\MstProductCode', 'pid', 'productid');
    }

    public function subcategory(){
        return $this->belongsTo('App\SubCategory','subcatid');
    }

    public function productImage(){
        return $this->hasMany('App\MstProductImage','pid','productid');
    }
    
}
