<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstProductImage extends Model
{
    protected $table = 'mst_productimages';
    protected $primaryKey = 'productimageid';

	function product(){
        return $this->belongsTo('App\MstProduct','pid');
    }
}
