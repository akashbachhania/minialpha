<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role' => ['required'],
            'timezone' => ['required'],
            'pass' => ['required_with:conpass', 'confirmed'],
            'selectedindustry[]' => ['required'],
            'selectcategory[]' => ['required'],
            'selectsubcat[]' => ['required'],
            'selectbrands[]' => ['required'],
            'authpass' => ['required']
        ];
    }
}
