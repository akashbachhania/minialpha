<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use SSH;

class TestController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $databasename = 'minialpha';
        $now            = str_replace(":", "", date("Y-m-d H: i: s"));
        $outputfilename = $databasename . '-' . $now . '.sql';
        $outputfilename = str_replace(" ", "-", $outputfilename);
        
        $commands = ([
            'cd /var/www/html/public/sql',
            'mysqldump -uroot -plogin123#123# minialpha> '.$outputfilename,
        ]);
        SSH::run($commands, function($line)
        {
            echo $line.PHP_EOL;
        });
    }
}
