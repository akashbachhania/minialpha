<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductPrefix;
use App\UserActivity;
use App\MstProduct;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Image;
use App\MstProductCode;
use App\MstProductImage;
use Session;
use App\MstNotification;
use App\MstProductReport;
class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ProductPrefixs=ProductPrefix::where('isactive','1')->orderBy('name')->get();
        $packagings = \App\MstPackaging::where('isactive','1')->orderBy('name')->get();
        $shippings = \App\MstShipping::where('isactive','1')->orderBy('name')->lists('name')->toArray();
        
        $data = array(
                'ProductPrefixs' => $ProductPrefixs,
                'packagings' => $packagings,
                'shippings' => $shippings
            );

        return View('productCatalog.addProduct',$data);
    }
    public function product_ajax(Request $request){
        $product=MstProduct::where('name','=',$request->pname)->count();
        if($product > 0)
        {
            echo 'exist';
        }
        else
        {
            echo '1';
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
    }
    public function randStringAlpha($len = 3)
    {
    $str = "";
    $chars = array_merge(range('A', 'Z'));
    for ($i = 0; $i < $len; $i++)
        {
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float)$sec + ((float)$usec * 100000);
        mt_srand($seed);
        $str.= $chars[mt_rand(0, (count($chars) - 1)) ];
        }

    return $str;
    }

    public function randStringNumeric($len = 3)
    {
        $str = "";
        $chars = array_merge(range(0, 9));
        for ($i = 0; $i < $len; $i++)
            {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float)$sec + ((float)$usec * 100000);
            mt_srand($seed);
            $str.= $chars[mt_rand(0, (count($chars) - 1)) ];
            }

        return $str;
    }

   public function randStringNumeric2($len = 3)
    {
        $str = "";
        $chars = array_merge(range(0, 9));
        for ($i = 0; $i < $len; $i++)
            {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float)$sec + ((float)$usec * 100000);
            mt_srand($seed);
            $str.= $chars[mt_rand(0, (count($chars) - 1)) ];
            }

        return $str;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $realName=$request->file('file');

       
   
        $productno = $this->randStringAlpha() . "-" . $this->randStringNumeric() . "-" . $this->randStringNumeric2();
        date_default_timezone_set('US/Eastern');
 
        $myDateTime = date('Y-m-d H:i:sa');
        if(Auth::user()->userRole->code=='AM')
            $status=$status = 'WAITING';
        else
            $status = 'APPROVED';

        $scondition = implode(',', $request->scondition);

        $product=new MstProduct;

        $product->productno             =   $productno;
        $product->date                  =   $myDateTime;
        $product->name                  =   $request->pname;
        $product->industryid            =   $request->industry;
        $product->brandid               =   $request->bname;
        $product->catid                 =   $request->category;
        $product->subcatid              =   $request->scategory;
        $product->shipingcondition      =   $scondition;
        $product->caseweight            =   $request->cweight;
        $product->qtypercase            =   $request->qtycase;
        $product->description           =   $request->desc;
        $product->manufacture           =   $request->mname;
        $product->pakaging              =   $request->packaging;
        $product->addedby               =   Auth::user()->userid;
        $product->mpm                   =   $request->MPM;
        $product->pstatus               =   $status;
        $product->roletype              =   Auth::user()->userRole->code;


        $i=1;
        foreach ($realName as $file) {
            if(!empty($file)){
       
                $rand = time() . '_' . uniqid();
                
                $imageName = $rand.'.'.$file->getClientOriginalExtension();
                $path=public_path('productimg'). '/'.$imageName;
                $thumbnail_path=public_path('productimg/thumb'). '/'.$imageName;
                $file->move(base_path() . '/public/productimg/', $imageName);
                
                $this->Thumbnail($path, $thumbnail_path,300,300);
                $product->img0                  =   $imageName; 

                $MstProductImage=new MstProductImage;

                $MstProductImage->pid           =   $product->productid;
                $MstProductImage->imageurl      =   $imageName;
                $MstProductImage->srno          =   $i;

                $MstProductImage->save();
            }
     
        }
        $product->save();
        $ProductPrefix=ProductPrefix::find($request->pcode1);
      
        $productcode=$ProductPrefix->name.':'.$request->code1;
        $MstProductCode= new MstProductCode;
        if(Auth::user()->userRole->code=='AD'){
            

            $MstProductCode->pid            =    $product->productid;
            $MstProductCode->prefixid       =    $request->pcode1;  
            $MstProductCode->code           =    $request->code1;
            $MstProductCode->status         =    'APPROVED';
            $MstProductCode->productcode    =    $productcode;
            $MstProductCode->refbyid        =    Auth::user()->userid;
            $MstProductCode->approveid      =    Auth::user()->userid;
            $MstProductCode->refdate        =    $myDateTime;
            $MstProductCode->approvedate    =    $myDateTime;
            $MstProductCode->save();

        }
        else{
            $MstProductCode->pid            =    $product->productid;
            $MstProductCode->prefixid       =    $request->pcode1;  
            $MstProductCode->code           =    $request->code1;
            $MstProductCode->productcode    =    $productcode;
            $MstProductCode->refbyid        =    Auth::user()->userid;
            $MstProductCode->refdate        =    $myDateTime;
         
            $MstProductCode->save();
        }
        if($request->pcode2!='' && $request->code2!=''){

            $MstProductCode= new MstProductCode;
            $ProductPrefix=ProductPrefix::find($request->pcode2);
            $productcode=$ProductPrefix->name.':'.$request->code2;

            if(Auth::user()->userRole->code=='AD'){
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode2;  
                $MstProductCode->code           =    $request->code2;
                $MstProductCode->status         =    'APPROVED';
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->approveid      =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
                $MstProductCode->approvedate    =    $myDateTime;
                $MstProductCode->save();

            }
            else{
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode2;  
                $MstProductCode->code           =    $request->code2;
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
             
                $MstProductCode->save();
            }
        }
        if($request->pcode3!='' && $request->code3!=''){

            $MstProductCode= new MstProductCode;
            $ProductPrefix=ProductPrefix::find($request->pcode3);
           
            $productcode=$ProductPrefix->name.':'.$request->code3;

            if(Auth::user()->userRole->code=='AD'){
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode3;  
                $MstProductCode->code           =    $request->code3;
                $MstProductCode->status         =    'APPROVED';
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->approveid      =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
                $MstProductCode->approvedate    =    $myDateTime;
                $MstProductCode->save();

            }
            else{
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode3;  
                $MstProductCode->code           =    $request->code3;
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
             
                $MstProductCode->save();
            }
        }
        if ($status == 'WAITING')
        {
            $action = 'Suggested Product';
        }
        else
        {
            $action = 'Added Product';
        }
        $useraction = new UserActivity;

        $useraction->userid = Auth::user()->userid;
        $useraction->ipaddress = $request->ip();
        $useraction->sessionid = Session::get('_token');
        $useraction->actiondate = date('Y-m-d');
        $useraction->actiontime = date('Y-m-d H:i:sa');
        $useraction->actionname = $action;

        $useraction->save();
        return redirect('product/browse');
    }
   public function Thumbnail($url, $filename, $width = 100, $height = true) {

        // download and create gd image
        $image = ImageCreateFromString(file_get_contents($url));

        // calculate resized ratio
        // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
        $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;

        // create image 
        $output = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));

        // save image
        ImageJPEG($output, $filename, 95); 

        // return resized image
        return $output; // if you need to use it
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = $this->productimage($id);
        
        if((Auth::user()->roletype == "AD")||(Auth::user()->roletype == "SA"))
        {

            $productCode = MstProduct::find($id)->productCode;
        }
        else
        {
            $statusCode = "APPROVED";

            $productCode = MstProduct::find($id)->productCode->where('status','=',$statusCode);
            
        }

        $productPrefixs=ProductPrefix::all();   

        $product = MstProduct::find($id);

        $relatedProducts = MstProduct::where('catid',$product->catid)->where('productid','!=',$id)->get();
        // echo "<pre>"; print_r($relatedProducts);die;
        $data = array(
                'image'=>$image,
                'product'=>$product,
                'productCode'=>$productCode,
                'productPrefixs'=>$productPrefixs,
                'relatedProducts'=>$relatedProducts
                );

        return View('productCatalog.detail-product-view',$data)->with('title','Trading Soft');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ProductPrefixs=ProductPrefix::all();

        $productInfo = MstProduct::find($id);

        $productImage = $this->productimage($id);
        // echo "<pre>"; print_r($productInfo->productCode);die;
        return View('productCatalog/editProduct',['ProductPrefixs'=>$ProductPrefixs,'productInfo'=>$productInfo,'productImage'=>$productImage])->with('title','Trading Soft::Edit Product');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
      // echo "<pre>"; print_r($_POST);die;
        $realName=$request->file('file');

        date_default_timezone_set('US/Eastern');
 
        $myDateTime = date('Y-m-d H:i:sa');

        $product= MstProduct::find($id);

        $product->date                  =   $myDateTime;
        $product->name                  =   $request->pname;
        $product->industryid            =   $request->industry;
        $product->brandid               =   $request->bname;
        $product->catid                 =   $request->category;
        $product->subcatid              =   $request->scategory;
        $product->shipingcondition      =   $request->scondition;
        $product->caseweight            =   $request->cweight;
        $product->qtypercase            =   $request->qtycase;
        $product->description           =   $request->desc;
        $product->manufacture           =   $request->mname;
        $product->pakaging             =    $request->packaging;
        $product->addedby               =   Auth::user()->userid;
        $product->mpm                   =   $request->MPM;
        $product->pstatus               =   $request->pstatus;
        $product->roletype              =   Auth::user()->userRole->code;
        $product->save();
        $i=1;
        foreach ($realName as $file) {
            if(!empty($file)){
       
                $rand = time() . '_' . uniqid();
                
                $imageName = $rand.'.'.$file->getClientOriginalExtension();
                $path=public_path(). '/productimg/'.$imageName;
                $thumbnail_path=public_path(). '/productimg/thumb/'.$imageName;
                $file->move(base_path() . '/public/productimg/', $imageName);
                
                $this->Thumbnail($path, $thumbnail_path,300,300);

                $MstProductImage = new MstProductImage;

                $MstProductImage->pid           =   $product->productid;
                $MstProductImage->imageurl      =   $imageName;
                $MstProductImage->srno          =   $i;

                $MstProductImage->save();
            }
     
        }
        $ProductPrefix=ProductPrefix::find($request->pcode1);
      
        $productcode=$ProductPrefix->name.':'.$request->code1;
        $MstProductCode= new MstProductCode;
        if(Auth::user()->userRole->code=='AD'){
            

            $MstProductCode->pid            =    $product->productid;
            $MstProductCode->prefixid       =    $request->pcode1;  
            $MstProductCode->code           =    $request->code1;
            $MstProductCode->status         =    'APPROVED';
            $MstProductCode->productcode    =    $productcode;
            $MstProductCode->refbyid        =    Auth::user()->userid;
            $MstProductCode->approveid      =    Auth::user()->userid;
            $MstProductCode->refdate        =    $myDateTime;
            $MstProductCode->approvedate    =    $myDateTime;
            $MstProductCode->save();

        }
        else{
            $MstProductCode->pid            =    $product->productid;
            $MstProductCode->prefixid       =    $request->pcode1;  
            $MstProductCode->code           =    $request->code1;
            $MstProductCode->productcode    =    $productcode;
            $MstProductCode->refbyid        =    Auth::user()->userid;
            $MstProductCode->refdate        =    $myDateTime;
         
            $MstProductCode->save();
        }
        if($request->pcode2!='' && $request->code2!=''){

            $MstProductCode= new MstProductCode;
            $ProductPrefix=ProductPrefix::find($request->pcode2);
            $productcode=$ProductPrefix->name.':'.$request->code2;

            if(Auth::user()->userRole->code=='AD'){
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode2;  
                $MstProductCode->code           =    $request->code2;
                $MstProductCode->status         =    'APPROVED';
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->approveid      =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
                $MstProductCode->approvedate    =    $myDateTime;
                $MstProductCode->save();

            }
            else{
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode2;  
                $MstProductCode->code           =    $request->code2;
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
             
                $MstProductCode->save();
            }
        }
        if($request->pcode3!='' && $request->code3!=''){

            $MstProductCode= new MstProductCode;
            $ProductPrefix=ProductPrefix::find($request->pcode3);
           
            $productcode=$ProductPrefix->name.':'.$request->code3;

            if(Auth::user()->userRole->code=='AD'){
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode3;  
                $MstProductCode->code           =    $request->code3;
                $MstProductCode->status         =    'APPROVED';
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->approveid      =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
                $MstProductCode->approvedate    =    $myDateTime;
                $MstProductCode->save();

            }
            else{
                $MstProductCode->pid            =    $product->productid;
                $MstProductCode->prefixid       =    $request->pcode3;  
                $MstProductCode->code           =    $request->code3;
                $MstProductCode->productcode    =    $productcode;
                $MstProductCode->refbyid        =    Auth::user()->userid;
                $MstProductCode->refdate        =    $myDateTime;
             
                $MstProductCode->save();
            }
        }

        $useraction = new UserActivity;

        $useraction->userid = Auth::user()->userid;
        $useraction->ipaddress = $request->ip();
        $useraction->sessionid = Session::get('_token');
        $useraction->actiondate = date('Y-m-d');
        $useraction->actiontime = date('Y-m-d H:i:sa');

        $useraction->actionname = 'Product Updated';

        $useraction->save();
        return redirect('product/browse');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $product = MstProduct::find($request->id);

        $product->delete();

        $useraction = new UserActivity;

        $useraction->userid = Auth::user()->userid;
        $useraction->ipaddress = $request->ip();
        $useraction->sessionid = Session::get('_token');
        $useraction->actiondate = date('Y-m-d');
        $useraction->actiontime = date('Y-m-d H:i:sa');
        $useraction->actionname = 'Deleted Product';

        $useraction->save();
    }


    public function browseProduct(){

        $checkVal = $this->checkPermission(7);

        return View('productCatalog.browse',['checkVal',$checkVal])->with('title','Browse Product');
    }

    public function browseProduct_ajax(Request $request){
        
        if (empty(Auth::user()->industry) == false) {
            // $industry  = Auth::user()->industry;
            $territory = explode(',', Auth::user()->industry);
            // $territory = ' AND industryid IN (' . $industry . ')';
        }
        
        if (empty(Auth::user()->category) == false) {
            // $category  = Auth::user()->category;
            $territory1 = explode(',', Auth::user()->category);
            // $territory = $territory . ' AND catid IN (' . $category . ')';
        }
        
        if (empty(Auth::user()->brand) == false) {
            // $brand     = $_SESSION['brand'];
            // $territory = $territory . ' AND brandid IN (' . $brand . ')';
            $territory2 = explode(',', Auth::user()->brand);
        }
        if( isset($request->productActionType) && ($request->productActionType == 'group_filter') ){
            

            if (Auth::user()->roletype == "AD") {
                
                $products = MstProduct::where('pstatus','<>','1')->whereIn('industryid',explode(',', $request->industryData))->whereIn('catid',explode(',', $request->categoryData))->whereIn('subcatid',explode(',', $request->scategoryData))->whereIn('brandid',explode(',', $request->brandData))->orderBy('name','ASC')->get();

            
            }
            else
            {
                $products = MstProduct::where('pstatus','APPROVED','1')->whereIn('industryid',$territory)->whereIn('catid',$territory1)->whereIn('brandid',$territory2)->orderBy('name','ASC')->get();

            }

            // return \Response::json($record);
        }

        else{
            if (Auth::user()->roletype == "AD") {
                
                $products = MstProduct::where('pstatus','<>','1')->whereIn('industryid',$territory)->whereIn('catid',$territory1)->whereIn('brandid',$territory2)->orderBy('name','ASC')->get();

            }
            else
            {
                $products = MstProduct::where('pstatus','APPROVED')->whereIn('industryid',$territory)->whereIn('catid',$territory1)->whereIn('brandid',$territory2)->orderBy('name','ASC')->get();

            }
        }

        // $products = MstProduct::orderBy('name')->get();
        
        // $territory1 = '';

        $i = 0;
        $code = '';
        $record=array();
        if( Auth::user()->roletype == 'AM' ){
            foreach ($products as $product) {
                $code = MstProduct::find($product->productid)->productCode->lists('productcode')->toArray();
                $code = implode(',', $code);

                $record[$i]['productid']     =   '<a href="detail-product-view/' . $product->productid . '">'.$product->productno.'</a>';
                $record[$i]['industry']      =   $product->industry->name;
                $record[$i]['category']      =   isset($product->category->name) ? $product->category->name : '';
                $record[$i]['brand']         =   isset($product->brand->name) ? $product->brand->name : '';
                $record[$i]['productname']   =   $product->name; 
                $record[$i]['productcodes']  =   $code;
            $i++;
            }
        }   

        if( Auth::user()->roletype != 'AM' ){

            foreach ($products as $product) {
                $code = MstProduct::find($product->productid)->productCode->lists('productcode')->toArray();
                $code = implode(',', $code);
                // echo "<pre>"; print_r($code); 
                $record[$i]['productid']     =   '<a href="detail-product-view/' . $product->productid . '">'.$product->productno.'</a>';
                $record[$i]['industry']      =   $product->industry->name;
                $record[$i]['category']      =   isset($product->category->name) ? $product->category->name : '';
                $record[$i]['brand']         =   isset($product->brand->name) ? $product->brand->name : '';
                $record[$i]['productname']   =   $product->name; 
                $record[$i]['productcodes']  =   $code;
                $record[$i]['action']        =   '<a href="edit-product/'.$product->productid.'"><i class="fa fa-pencil-square-o"></i></a> &nbsp;&nbsp;<a  href="javascript:deleteme(\'' . $product->productid . '\')"><i class="fa fa-trash-o"></i></a>';
            $i++;
            }
        }
        return \Response::json($record);
       /* $territory1 = '';


        if (empty(Auth::user()->industry) == false) {
            $industry1  = Auth::user()->industry;
            $territory1 = ' AND p.industryid IN (' . $industry1 . ')';
        }
        
        if (empty(Auth::user()->category) == false) {
            $category1  = Auth::user()->category;
            $territory1 = $territory1 . ' AND p.catid IN (' . $category1 . ')';
        }
        
        
        if (empty(Auth::user()->brand) == false) {
            $brand1     = Auth::user()->brand;
            $territory1 = $territory1 . ' AND p.brandid IN (' . $brand1 . ')';
        }
        if (Auth::user()->roletype == "AD") {


            $product = \DB::table('mst_product as p')
                            ->leftJoin('mst_industry as i','p.industryid','=','i.industryid')
                            ->leftJoin('mst_category as c','p.catid','=','c.catid')
                            ->leftJoin('mst_brand as b','p.brandid','=','b.brandid')
                            ->where('p.pstatus','<>','1')
                            ->orderBy('p.name','ASC')
                            ->get();
        }
            $i = 0;
            foreach ($product as $value) {
                $record[$i]['productid'] = $value->productno;
                $record[$i]['industry'] = $value->industry;
                $record[$i]['category'] = $value->category;
                $record[$i]['brand'] = $value->brand;
                $record[$i]['name'] = $value->productName;
                $record[$i]['code'] = 'DIN:123';
                $record[$i]['action'] = '';
                $i++;
            }*/

    }


    function productimage($id){

        $image = MstProductImage::where('pid','=',$id)->orderBy('srno','ASC')->get();

        return $image;
    }

    public function addProductPrefix_ajax(Request $request){
        $prefix = ProductPrefix::find($request->prefix);

        $productcode = $prefix->name . " : " . $request->code;

        if (Auth::user()->roletype == 'AD'){

            $status = "APPROVED";
        
        }
        else{

            $status = "WAITING";
        }

        if (Auth::user()->roletype != 'AM'){

            $action = 'Added Product Code';
            
            $mstProductCode = new MstProductCode;

            $mstProductCode->pid = $request->pid;
            $mstProductCode->prefixid = $request->prefix;
            $mstProductCode->code = $request->code;
            $mstProductCode->status = $status;
            $mstProductCode->productcode = $productcode;
            $mstProductCode->refbyid = Auth::user()->userid;
            $mstProductCode->approveid = Auth::user()->userid;
            $mstProductCode->refdate = date('Y-m-d H:i:sa');
            $mstProductCode->approvedate = date('Y-m-d H:i:sa');

            $mstProductCode->save();

        }
        else{

            $action = 'Suggested New Product Code';
            
            $mstProductCode = new MstProductCode;

            $mstProductCode->pid = $request->pid;
            $mstProductCode->prefixid = $request->prefix;
            $mstProductCode->code = $request->code;
            $mstProductCode->status = $status;
            $mstProductCode->productcode = $productCode;
            $mstProductCode->refbyid = Auth::user()->userid;
            $mstProductCode->refdate = date('Y-m-d H:i:sa');
            
            $mstProductCode->save();
 
            $msgn = 'Product Code Sent For Approval';
            $useridn = 'admin';
            $date = date('Y-m-d H:i:sa');
            $typen = 'Audio';
            $statusn = 'Unread';

            $mstNotification = new MstNotification;

            $mstNotification->pro_id = $request->pid;
            $mstNotification->userid = $useridn;
            $mstNotification->msgdate = date('Y-m-d H:i:sa');
            $mstNotification->msg = $msgn;
            $mstNotification->type = $typen;
            $mstNotification->status = $statusn;
            $mstNotification->fromuserid = Auth::user()->userid;

            $mstNotification->save();

        }

            $arr = array(
                'name' => 'Record Saved Sucessfully',
                'msg' => 'SUCCESS',
                'pid' => $request->pid
            );

            $useraction = new UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = Session::get('_token');
            $useraction->actiondate = date('Y-m-d H:i:sa');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = $action;

            $useraction->save();

            return \Response::json($arr);

    }

    public function suggestedProduct(){
        return View('productCatalog.suggestedProduct')->with('title','Trading Soft:: Suggested Product');
    }

    public function suggestedProduct_ajax(){

        if(Auth::user()->roletype == "AD"){

            $result = MstProduct::where('pstatus','<>','APPROVED')->get();

        }
        else{

       
            $result = MstProduct::where('pstatus','=','REJECTED')->where('addedby',Auth::user()->userid)->get();
    
        }
        $i = 0;
        $code = '';
        $record = array();
        foreach ($result as $value) {
            foreach ($value->productCode as $values) {
                
                $code .= $values->productcode . '  ';
                
            }
            $record[$i]['dateSuggested'] = $value->date;
            $record[$i]['suggestedBy'] = $value->addedby;
            $record[$i]['productId'] =  $value->productno;
            $record[$i]['industry'] = $value->industry->name;
            $record[$i]['brand'] = $value->brand->name;
            $record[$i]['productName'] = '<a href="#">'. $value->name . '</a>';
            $record[$i]['productCode'] = $code;
            $record[$i]['status'] = $value->pstatus;

            $i++;
        }        

        return \Response::json($record);
    }



    public function suggestedProductCode(){
        return View('productCatalog.suggestedProductCode')->with('title','Trading Soft::Suggested Product Code');
    }

    public function suggestedProductCode_ajax(){
        $i = 0;
            $products = MstProductCode::where('status','<>','APPROVED')->get();
            $record = array();
            foreach($products as $product){
                $record[$i]['suggestdate'] = $product->refdate;
                $record[$i]['userid'] = $product->refbyid;
                $record[$i]['industry'] = $product->product->industry->name;
                $record[$i]['brand'] = $product->product->brand->name;
                $record[$i]['productName'] = $product->product->name;
                $record[$i]['productCode'] = $product->productcode;
                $record[$i]['status'] = $product->status;
                // $record[$i]['edit'] = '<a href="suggest-product-update/' . $product->pid . '/'.$product->productcodeid.'"><i class="fa fa-pencil-square-o"></i></a>';
            
            $i++;
            }

            return \Response::json($record);

    }

    public function reportedProduct(){
       return View('productCatalog.reportedProduct')->with('title','Trading Soft::Reported Product');
    }

    public function reportedProduct_ajax(){
                // $sql = "SELECT mp.date, mp.addedby, mpr.preportid,mp.productid, i.name AS industry, c.name AS category, b.name AS brand, mp.name AS productName,mpr.reviewbyid,mpr.isactive
                // FROM mst_productreport mpr
                // LEFT JOIN mst_product mp ON mpr.productid = mp.productid
                // LEFT JOIN mst_industry i ON mp.industryid = i.industryid
                // LEFT JOIN mst_category c ON mp.catid = c.catid
                // LEFT JOIN mst_brand b ON mp.brandid = b.brandid order by brand limit $iDisplayStart ,$iDisplayLength ";

        $result = MstProductReport::all();
        $i = 0;
        $record = array();
        foreach ($result as $value) {
          $record[$i]['suggestdate'] = $value->product->date;
          $record[$i]['userid'] = $value->product->addedby;
          $record[$i]['industry'] = $value->product->industry->name;
          $record[$i]['brand'] = $value->product->brand->name;
          $record[$i]['productName'] = '<a href="update-reported-product/' . $value->product->productid . '/' . $value->preportid .'" style="display:none;"></a>'.$value->product->name;
          $record[$i]['reportCode'] = $value->reviewbyid; 
          if($value->isactive=='1'){

             $msg='<span style="color:red">Open</span>';
          }
          else{ 

             $msg='<span style="color:green">Closed</span>';
          }
          $record[$i]['msg'] = $msg; 
        $i++;

        }

        return \Response::json($record);

    }

    public function createNewAdd($id = null){

        if((Auth::user()->roletype =="AD") || ( Auth::user()->roletype == "SA") ){
            $x='style="background:#ffffff"';
        }
        else{
            $x='style="background:#f9f9f9"';
        }

        $currencies = \App\MstCurrency::orderBy('name')->get();

        $uom = \App\MstUom::orderBy('name')->get();
        
        $locations = \App\MstLocation::orderBy('name')->get();
 
        $expdateranges = \App\MstExpDaterange::orderBy('exprangeid','asc')->get();

        $timeframes = \App\MstTimeframe::orderBy('timeframeid')->get();

        $countries = \App\MstCountry::orderBy('name')->get();

        if(Auth::user()->userid=="UUZZ-9984" || Auth::user()->roletype == "SA"){
            $cusrefnos = \App\PostCustomer::where('isactive','1')->orderBy('refno')->get();
        }
        else{
            $cusrefnos = \App\PostCustomer::where('isactive','1')->where('refuserid',Auth::user()->userid)->orderBy('refno')->get();
        }

        $languages = \App\MstLanguage::orderBy('name')->get();

        $data = array(
                'x' => $x,
                'currencies'=>$currencies,
                'uoms'=>$uom,
                'locations'=> $locations,
                'expdateranges' => $expdateranges,
                'timeframes' => $timeframes,
                'countries' => $countries,
                'cusrefnos' => $cusrefnos,
                'languages' => $languages
             );

        return View('productCatalog.createNewAdd',$data)->with('title','Trading Soft::Create New Add');
    }

    public function fetchalldata(Request $request){
        
        $proid = $request->pro_id;
        
        $json  = array();
        
        $key = "ChangedSettings";
        
        $sth = \App\MstProduct::where('productid',$proid)->get();

        foreach ($sth as $result) {
        
            $productid              = isset($result->productid)? $result->productid : '';     
            $productno              = isset($result->productno)?$result->productno:'';
            $date                   = isset($result->date)?$result->date:'';          
            $name                   = mb_convert_encoding(isset($result->name)?$result->name:'', "HTML-ENTITIES", "ISO-8859-1");
            $pakaging               = isset($result->pakaging)?$result->pakaging:'';      
            $shipingcondition       = isset($result->shipingcondition)?$result->shipingcondition:'';
            $caseweight             = isset($result->caseweight)?$result->caseweight:'';    
            $qtypercase             = isset($result->qtypercase)?$result->qtypercase:'';
            $description            = isset($result->description)?$result->description:'';   
            $manufacture            = isset($result->manufacture)?$result->manufacture:'';
            $mpm                    = isset($result->mpm)?$result->mpm:'';           
            $addedby                = isset($result->addedby)?$result->addedby:'';
            $isactive               = isset($result->isactive)?$result->isactive:'';      
            $roletype               = isset($result->roletype)?$result->roletype:'';
            $industry               = mb_convert_encoding(isset($result->industry->name)?$result->industry->name:'Not Applicable', "HTML-ENTITIES", "ISO-8859-1");      
            $category               = mb_convert_encoding(isset($result->category->name)?$result->category->name:'Not Applicable', "HTML-ENTITIES", "ISO-8859-1");
            $subcategory            = mb_convert_encoding(isset($result->subcategory->name)?$result->subcategory->name:'Not Applicable', "HTML-ENTITIES", "ISO-8859-1");   
            $brand                  = mb_convert_encoding(isset($result->brand->name)?$result->brand->name:'Not Applicable', "HTML-ENTITIES", "ISO-8859-1");
            

            if((Auth::user()->roletype=="AD")||(Auth::user()->roletype=="SA")){

                $result1 = $result->productCode->lists('productcode')->toArray();
                
                $productcodes = implode(', ', $result1);
                
            }
            else{

                $result1 = $value->productCode->lists('productcode')->toArray();

                $productcodes = implode(', ', $result1);

            }

            $results = count($result->productImage);
            
            if($results!=0)
            {
                if (strpos($result->productImage[0]->imageurl,'http://') !== false)
                {
                   
                   $images = "<div class='BigImage'><img src='".$result->productImage[0]->imageurl."' alt='' width='258px'></div>";
                   
                }
                else
                {
                    
                    $images = "<div class='BigImage'><div class='MagicZoom' id='image'><div class='MagicZoom'><a href='".url('/')."/productimg/".$result->productImage[0]->imageurl."' id='imageshref' class='MagicZoom' rel='zoom-position:inner;zoom-fade:true'><img id='bimg' src='".url('/')."/productimg/thumb/".$result->productImage[0]->imageurl ."' alt='' width='300'></a></div></div></div>";
                   
                }
            }
            else
            {
              
                $images = "<div class='BigImage'><img src=".url('/')."'/productimg/image4.jpg' alt='' width='258px'></div>";
              
            }

            $arr = array(
                    'productid' => $productid,                  
                    'productno' => $productno,
                    'date' => $date,                            
                    'name' => $name,
                    'pakaging' => $pakaging,                    
                    'shipingcondition' => $shipingcondition,
                    'caseweight' => $caseweight,                
                    'qtypercase' => $qtypercase,
                    'description' => $description,              
                    'manufacture' => $manufacture,
                    'mpm' => $mpm,                              
                    'addedby' => $addedby,
                    'isactive' => $isactive,                    
                    'roletype' => $roletype,
                    'industry' => $industry,                    
                    'category' => $category,
                    'subcategory' => $subcategory,              
                    'brand' => $brand,
                    'productcodes' => $productcodes,
                    'image'=>$images
                    );
        }
        
        return $arr;
    }

    public function viewProductad($id){
    
        $productid = explode(':', $id);

        $product = \App\MstProduct::find($productid[0]);

        $results = count($product->productImage);

        $data = array(
                'productid' => $productid,
                'product' => $product,
                'results' => $results
            );

        return View('productCatalog.viewProductad',$data)->with('title','Trading Soft::View Buy Ad');
    }

    public function viewProductadTable(Request $request){
        
        $proid = explode(':', $request->pro_id);
        
         if (Auth::user()->roletype == "AD") {
        
            $sql = \App\PostAdverstisement::where('ptype',$proid[1])->with(['product'=>function($query) use ($proid){
                $query->where('productid',$proid[0])->get();
            }])->get();
         }
         else
         {
            $sql = \App\PostAdverstisement::where('ptype',$proid[1])->where('act_inactive','0')->with(['product'=>function($query) use ($proid){
                        $query->where('productid',$proid[0])->get();
                    }])->with(['postCustomer'=>function($query){
                        $query->where('isactive','1')->get();
                    }])->with(['user'=>function($query){
                        $query->where('isactive','1')->get();
                    }])->get();
         }

         $record = array();
         $i = 0;
         foreach ($sql as $value) {
             
             if(Auth::user()->roletype == "AD"){

                $records[$i]['postno'] = $value->postno . '<a href="posting-detail-view.php?post_id=' . $value->postno . '" style="display:none;"></a>';

                $records[$i]['sdate'] = date('F d Y',strtotime($value->pdate));
                
                $records[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';
                $records[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';
                $records[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';
                $records[$i]['ptype'] = ucwords(strtolower($value->ptype));
                $records[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';
                $records[$i]['productName'] = isset($value->product->name)?$value->product->name:'';
                $records[$i]['timeframe'] = isset($value->timeframe)?$value->timeframe:'';
                $records[$i]['deleteid'] = '<a  href="javascript:deleteme(\'' . $value->postno . '\')"><i class="fa fa-trash-o"></i></a>'; 
                                
            }
            else{

                $records[$i]['postno'] = $value->postno . '<a href="posting-detail-view.php?post_id=' . $value->postno . '" style="display:none;"></a>';

                $records[$i]['sdate'] = date('F d Y',strtotime($value->pdate));
                $records[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';
                $records[$i]['category'] = isset($value->product->category->name)?$value->category->brand->name:'';
                $records[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';
                $records[$i]['ptype'] = ucwords(strtolower($value->ptype));
                $records[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';
                $records[$i]['productName'] = isset($value->product->name)?$value->product->name:'';
                $records[$i]['timeframe'] = isset($value->timeframe)?$value->timeframe:''; 
                            
            }

            $i++;
         }

         return \Response::json($record);
    }

    public function checkPermission($key){

        $data = \App\MastRolePermission::where('RoleId',Auth::user()->roleid)->where('PageId',$key)->get();
        return $data;
    }    
}