<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Crypt;
use Auth;
use SSH;
use Session;
class PostingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        
        if($request->ajax()){
            
            $result = \App\PostAdverstisement::whereNotIn('pstatus',['Completed','CANCELLED'])->where('customerrefno',$request->postuserid)->orderBy('industry')->get();

            $record = array();
            $i = 0;

            foreach ($result as $value) {
                
                $sdate = date('F d Y - h:i A',strtotime($value->pdate));
                $postid = isset($value->postid)?$value->postid:'';
                $postno = isset($value->postno)?$value->postno:'';
                $brand = (isset($value->product->brand->name)?$value->product->brand->name:"") .'<a href="'.url("/").'/posting/posting-detail-view/' . $postno . '" style=display:none>View</a>';
                $ptype = strtoupper(isset($value->ptype)?$value->ptype:'');
                $quantity = isset($value->quantity)?$value->quantity:'';
                $productName = isset($value->product->name)?$value->product->name:'';
                $price = isset($value->targetprice)?:'';
                $currency = isset($value->currency)?$value->currency:'';
                $customerrefno = isset($value->customerrefno)?$value->customerrefno:'';
                $poststatus = isset($value->pstatus)?$value->pstatus:'';
                
                $useridstataus = isset($value->user->isactive)?$value->user->isactive:'';
                $act_inactive = isset($value->act_inactive)?$value->act_inactive:'';
                $productstatus = isset($value->product->pstatus)?$value->product->pstatus:'';
                $customerstatus = isset($value->postCustomer->isactive)?$value->postCustomer->isactive:'';


                $productCheck = '<input type="checkbox" class="form-control" name="checkProduct" id="checkProduct" value="' . $postid . '" />';
                
                if ($poststatus == 'NEW'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                         <a href="edit-add.php?post_id=' . $postno . '&key=13">Edit</a>&nbsp;|&nbsp;
                         <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                         
                }
                if ($poststatus == 'Submitted'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                        <a href="edit-add.php?post_id=' . $postno . '&key=13">Edit</a>&nbsp;|&nbsp;
                        <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                         
                }


                if ($poststatus == 'Countered'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                         <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                }


                if ($poststatus == 'Completed'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="repost.php?post_id=' . $postno . '&key=13">Repost</a>';
                }
                if ($poststatus == 'EXPIRED'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="repost.php?post_id=' . $postno . '&key=13">Repost</a>';
                }
                if ($poststatus == 'Declined'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="repost.php?post_id=' . $postno . '&key=13">Repost</a>';
                }
                if ($poststatus == 'Accepted'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' . $postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="repost.php?post_id=' . $postno . '&key=13">Repost</a>';
                }

                if ($poststatus == 'CANCELLED'){

                    $edit = '<a href="myposting-detail-view.php?post_id=' .$postno . '&key=13">View</a>&nbsp;|&nbsp;
                             <a href="repost.php?post_id=' . $postno . '&key=13">Repost</a>';
                }
                
                if(Auth::user()->roletype == "AD"){
                
                     if ( ($useridstataus=='1') && ($act_inactive=='0') && ($productstatus!='REJECTED') && ($customerstatus=='1') && ($poststatus !='EXPIRED')) {
                        $edit1 = '<span class="label label-sm label-success"> Enable </span>';
                    
                     } else {
                        
                        if($poststatus =='EXPIRED'){

                            $edit1 = '<span class="label label-sm label-danger">EXPIRED</span>';
                        }
                        else{

                            $edit1 = '<span class="label label-sm label-danger">Disable</span>';
                        }
                     }
                }

                if( Auth::user()->roletype == 'AD' ){
                    $record[$i]['postno'] = $postno;
                    $record[$i]['sdate'] = $sdate;
                    $record[$i]['brand'] = $brand;
                    $record[$i]['ptype'] = $ptype;
                    $record[$i]['quantity'] = $quantity;
                    $record[$i]['productName'] = $productName;
                    $record[$i]['price'] = $price;
                    $record[$i]['currency'] = $currency;
                    $record[$i]['customerrefno'] = $customerrefno;
                    $record[$i]['edit1'] = $edit1;
                }
                else{
                    $record[$i]['postno'] = $postno;
                    $record[$i]['sdate'] = $sdate;
                    $record[$i]['brand'] = $brand;
                    $record[$i]['ptype'] = $ptype;
                    $record[$i]['quantity'] = $quantity;
                    $record[$i]['productName'] = $productName;
                    $record[$i]['price'] = $price;
                    $record[$i]['currency'] = $currency;
                    $record[$i]['customerrefno'] = $customerrefno;
                    $record[$i]['poststatus'] = $poststatus;
                    $record[$i]['edit1'] = $edit1;
                }

                $i++;
            }//foreach loop end
            return \Response::json($record);
        }//ajax if condition end

        return View('posting.userposting')->with('title','Trading Soft::Customer Postings');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {   
        if((Auth::user()->roletype == "AD")||(Auth::user()->roletype == "SA")){ 
            $color='#ffffff';
        }
        else{

            $color='#f9f9f9';
        }
        $currencies = \App\MstCurrency::orderBy('name')->get();
        $uoms = \App\MstUom::orderBy('name')->get();
        $locations = \App\MstLocation::orderBy('name')->get();
        $expdateranges = \App\MstExpDaterange::orderBy('name')->get();

        if(Auth::user()->userid=="UUZZ-9984" || Auth::user()->roletype == "SA"){
            $cusrefnos = \App\PostCustomer::where('isactive','1')->orderBy('refno')->get();
        }
        else{
            $cusrefnos = \App\PostCustomer::where('isactive','1')->where('refuserid',Auth::user()->userid)->orderBy('refno')->get();
        }

        $languages = \App\MstLanguage::orderBy('name')->get();
        $timeframes = \App\MstTimeframe::orderBy('name')->get();
        $countries = \App\MstCountry::orderBy('name')->get();

        $checkVal = $this->checkPermission();

        $data = array(
                'x'=>$color,
                'currencies'=>$currencies,
                'uoms'=> $uoms,
                'locations'=>$locations,
                'expdateranges'=>$expdateranges,
                'cusrefnos'=>$cusrefnos,
                'languages'=>$languages,
                'timeframes'=>$timeframes,
                'countries'=>$countries,
                'checkVal'=>$checkVal
            );

        return View('posting.create',$data)->with('title','Trading Soft');

    // {
        //
          // return View('posting.addposting')->with('title','User Postings');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $realName=$request->file('uploadFile');

    
        $rand = time() . '_' . uniqid();
        
        $fileName = time().'.'.$realName->getClientOriginalExtension();
        
        $path=public_path('postingImport'). '/'.$fileName;
         
        $realName->move(base_path() . '/public/postingImport/', $fileName);
        $rpath = 'public/postingImport/'.$fileName;
 
            try {
                ini_set('max_execution_time', 2000); 
                \Excel::filter('chunk')->load($rpath)->chunk(300, function($reader) {
                    
                    foreach ($reader as $row1) {


                          \App\PostAdverstisement::insert([
                            'productno'    => trim($row1["productid"]),
                            'industry'     => trim($row1["industry"]),
                            'ptype'        => trim($row1["type"]),
                            'targetprice'  => trim($row1["targetprice"]),
                            'currency'     => trim($row1["currency"]),
                            'uom'          => trim($row1["unitofmeasurement"]),
                            'quantity'     => trim($row1["quantity"]),
                            'location'     => trim($row1["location"]),
                            'expdate'      => trim($row1["expirydaterange"]),
                            'expirydate'   => trim($row1["exactexpirydate"]),
                            'customerrefno'=> trim($row1["customerrefno"]),
                            'language'     => trim($row1["packaginglanguage"]),
                            'timeframe'    => trim($row1["timeframe"]),
                            'country'      => trim($row1["country"]),
                          ]);

                    }
                });
                \Session::flash('success', 'Postings uploaded successfully.');
                return \Redirect::to(url('posting/import-post'))->withInput()->withErrors(["msg"=>"Posting data imported successfully"]);          
            } catch (Exception $e) {
                \Session::flash('error', $e->getMessage());
                return \Redirect::to(url('posting/import-post'))->withInput()->withErrors(["msg"=>$e->getMessage()]);          
            }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postingCatalog(){
        
        return View('posting.postingCatalog')->with('title','Trading Soft::Posting Catalog');
    }

    public function postingCatalog_ajax(){

        $result = \App\PostAdverstisement::orderBy('postno')->get();
        
        $i = 0;
        
        $record = array();
        foreach ($result as $value) {
            $enableaction = '';
            if ( (isset($value->user->isactive) && ($value->user->isactive =='1')) && 
                 ( isset($value->act_inactive) && ($value->act_inactive=='0')) && 
                 ( isset($value->product->pstatus) && ($value->product->pstatus!='REJECTED')) && 
                 ( isset($value->postCustomer->isactive) && ($value->postCustomer->isactive=='1')) && 
                 ( isset($value->pstatus) && ($value->pstatus !='EXPIRED') ) ) {
                
                $enableaction .= '<span class="label label-sm label-success"> Enable </span>';
            
            } else {
                if(isset($value->pstatus) && ($value->pstatus =='EXPIRED'))
                {
                $enableaction .= '<span class="label label-sm label-danger">EXPIRED</span>';
                }
                else
                {
                $enableaction .= '<span class="label label-sm label-danger">Disable</span>';
                }
            }
            $postnumber = isset($value->postno)?$value->postno:"";
            if ($value->userid != Auth::user()->userid && Auth::user()->roletype == "AM") {

                if(( isset($value->product->pstatus) && ($value->product->pstatus!='Accepted')) && 
                    ( isset($value->product->pstatus) && ($value->product->pstatus!='Declined'))){

                    if (Auth::user()->roletype == "AD") {

                            $record[$i]['productCheck'] = '<input type="checkbox" class="" name="checkProduct" id="checkProduct" value="' . isset($value->postid)?$value->postid:'' . '" />';

                            $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber.'">'.$postnumber.'</a>';

                            $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));

                            $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                            $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                            $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                            $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                            $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                            $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';

                            $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?:'', "HTML-ENTITIES", "ISO-8859-1");

                            $record[$i]['enableaction'] = $enableaction;

                    } else {
                        if ($value->act_inactive == 0) {

                            $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber. '">'.$postnumber.'</a>';

                            $record[$i]['sdate'] = date('F d Y - h:i A',strtotime( isset($value->pdate)?$value->pdate:'' ));

                            $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                            $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                            $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                            $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                            $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                            $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';

                            $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?$value->timeframe:'', "HTML-ENTITIES", "ISO-8859-1");;
                         }
                    }
                }
                
            }
             else {
                if (Auth::user()->roletype == "AD") {
                    $record[$i]['productCheck'] = '<input type="checkbox" class="" name="checkProduct" id="checkProduct" value="' . $value->postid . '" />';

                    $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber. '">'.$postnumber.'</a>';

                    $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));

                    $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                    $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                    $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                    $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                    $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                    $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';

                    $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?$value->timeframe:'', "HTML-ENTITIES", "ISO-8859-1");

                    $record[$i]['enableaction'] = $enableaction;

                } else {
                    if ($value->act_inactive == 0) {

                        
                        $record[$i]['postno'] = '<a href="posting-detail-view/' .$postnumber. '">'.$postnumber.'</a>';

                        $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));

                        $record[$i]['industry'] = isset($value->product->industry->name)?$value->product->industry->name:'';

                        $record[$i]['category'] = isset($value->product->category->name)?$value->product->category->name:'';

                        $record[$i]['brand'] = isset($value->product->brand->name)?$value->product->brand->name:'';

                        $record[$i]['ptype'] = ucwords(strtolower( isset($value->ptype)?$value->ptype:'' ));

                        $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';

                        $record[$i]['productName'] = isset($value->product->name)?$value->product->name:"";

                        $record[$i]['timeframe'] = mb_convert_encoding(isset($value->timeframe)?$value->timeframe:'', "HTML-ENTITIES", "ISO-8859-1");


                    }
                }
            }     
            
            $i++;
        }
        return \Response::json($record);
    }


    public function importPost(){

        return View('posting.importPost')->with('title','Trading Soft::Import Post');
    }

    public function myOffers(){
        return View('posting.myOffers')->with('title','Trading Soft::My Offers');
    }

    public function myOffersAjax(){
        $result = \App\PostQuotation::where('userid',Auth::user()->userid)->where('type','OFFER')->with(['product'=>function($query){
            $query->where('pstatus','APPROVED')->get();
        }])->get();

        $record = array();
        $i = 0;
        foreach ($result as $value) {

            $row = \App\QuotationCounter::where('quotationno',$value->quotationno)->get();

            foreach ($row as $values) {

                if(isset($values->quotationno) && ($values->quotationno == '')){

                    $qtypercase    = isset($value->quantity)?$value->quantity:'';
                    $offerPrice    = isset($value->price)?$value->price:'';
                    $offertimeframe  = isset($value->timeframe)?$value->timeframe:'';
                    $expdate      = isset($value->expdate)?$value->expdate:'';
                    $quationstatus = isset($value->quationstatus)?$value->quationstatus:'';
                
                }
                else{

                    $qtypercase    = isset($values->counter_quantity)?$values->counter_quantity:'';
                    $offerPrice    = isset($values->counter_price)?$values->counter_price:'';
                    $offertimeframe= isset($values->counter_timeframe)?$values->counter_timeframe:'';
                    $expdate       = isset($values->counter_expdate)?$values->counter_expdate:'';
                    $quationstatus = isset($values->counetr_status)?$values->counetr_status:'';
                }

            }    
                

            $record[$i]['postno'] = isset($value->postno)?$value->postno:'';
            
            $record[$i]['quotationno'] = '<a href="my-offer_detailview.php?key=47&post_id=' . isset($value->postno)?$value->postno:'' . '&quotationno='.isset($value->quotationno)?$value->quotationno:''.'" style="display:none;">' . isset($value->quotationno)?$value->quotationno:''. '</a>'. isset($value->quotationno)?$value->quotationno:'';
            
            $record[$i]['offerdate'] = date('F d Y - h:i A',strtotime(isset($value->offercrdate)?$value->offercrdate:'') ) .' EST';
            
            $record[$i]['IndustryName'] = isset($value->product->industry->name)?$value->product->industry->name:'';
            
            $record[$i]['brandName'] = isset($value->product->brand->name)?$value->product->brand->name:'';
            
            $record[$i]['producttype'] = isset($value->ptype) ? $value->ptype : '';
            
            $record[$i]['qtypercase'] = isset($qtypercase) ? $qtypercase : '';
            
            $record[$i]['productName'] = mb_convert_encoding(isset($value->product->name)?$value->product->name:'', "HTML-ENTITIES", "utf8");
            
            $record[$i]['offerPrice'] = isset($offerPrice) ? $offerPrice : '';
            
            $record[$i]['offercurrency'] = isset($value->currency)?$value->currency:'';
            
            $record[$i]['offertimeframe'] = isset($offertimeframe) ? $offertimeframe :'';
            
            $record[$i]['expdate'] = isset($expdate) ? $expdate : '';
            
            $record[$i]['quationstatus'] = isset($quationstatus) ? $quationstatus : '';         

            $i++;
        }

        return \Response::json($record);
    }

    public function checkPermission(){

        $data = \App\MastRolePermission::select('RoleId','ViewP','AddP','EditP','DeleteP')->where('RoleId',Auth::user()->roleid)->where('PageId','12')->get();
        return $data;
    }    

    public function search( Request $request ){
            $q = $request->search;
            if (empty(Auth::user()->industry) == false) {
                    $industry1  = str_split(str_ireplace(Auth::user()->industry, ',', ''));

                    // $territory1 = ' AND mst_product.industryid IN (' . $industry1 . ')';
                }
                
                if (empty(Auth::user()->category) == false) {
                    $category1  = Auth::user()->category;
                    // $territory1 = $territory1 . ' AND mst_product.catid IN (' . $category1 . ')';
                }
            $pc = \App\MstProductCode::where('productcode','LIKE','%'.$q.'%')->lists('pid')->toArray();
            if (!empty(Auth::user()->industry)) {
                  
              $sth = \App\MstProduct::where('pstatus','=','APPROVED')->whereIn('industryid',$industry1)->where('name','LIKE','%'.$q.'%')->orWhereIn('productid',$pc)->orderBy('productid')->limit('5')->get();                    

            } 
            else {
                  
               $sth = \App\MstProduct::where('pstatus','=','APPROVED')->where('name','LIKE','%'.$q.'%')->orWhereIn('productid',$pc)->orderBy('productid')->limit('5')->get();
            }

            foreach ($sth as $value) {
                $productid = $value->productid;
                
                $username = $value->name;
                
                $productcode = '';
                
                foreach ($value->productCode as $prc) {
                    $productcode .= $prc->productcode; 
                }



                 $username = str_replace('"', '', $username);
                $username = str_replace('\'', '', $username);
                $username = mb_convert_encoding($username, "HTML-ENTITIES", "ISO-8859-1");
                $final    = $productid . ':' . $username;
                $b_username = '<strong style="color:blue;">' . $q . '</strong>';
                $final_username = str_ireplace($q, $b_username, $username);
                $finalproductcode =  str_ireplace($q, $b_username, $productcode);
                ?>
                <div class="show" align="left" onclick="hello('<?php echo $final; ?>')";>
<?php
                echo '<span class="name">'. $final_username .'&nbsp;&nbsp;('. $finalproductcode .')</span>';

                echo '</div>'; 

            }


    }


    public function fetch_productdetail(Request $request){
        $id = $request->pro_id;
        
        $sql_result = \App\MstProduct::where('productid',$id)->get();

        foreach ($sql_result as $value) {

            $productno = $value->productno;
            $name = mb_convert_encoding($value->name, "HTML-ENTITIES", "utf8");
            $industryid = ($value->industry->name !='' )?$value->industry->name:'Not Applicable';
            $catid = ($value->category->name !='' )?$value->category->name:'Not Applicable';
            $subcatid = ($value->subcategory->name !='' )?$value->subcategory->name:'Not Applicable';
            $brandid = ($value->brand->name !='' )?$value->brand->name:'Not Applicable';
            $manufacture = $value->manufacture;
            $caseweight = $value->caseweight;
            $qtypercase = $value->qtypercase;
            $pakaging = $value->pakaging;
            $shipingcondition = $value->shipingcondition;
            $description = mb_convert_encoding($value->description, "HTML-ENTITIES", "utf8");

            if((Auth::user()->roletype=="AD")||(Auth::user()->roletype=="SA")){

                $result = $value->productCode;

                $productcodes = '';
                
                foreach ($result as $values) {
                    
                    $productcodes .= $values->productcode;

                }
            }
            else{

                $result = $value->productCode;

                $productcodes = '';
                
                foreach ($result as $values) {
                    
                    $productcodes .= $values->productcode;

                }
            }

                $results=count($value->productImage);
                // echo "<pre>"; print_r($value->productImage);die;
                 if($results!=0)
                 {
                    if (strpos($value->productImage[0]->imageurl,'http://') !== false)
                    {
                       
                       $images = "<div class='BigImage'><img src='<?php echo $productimage[0]['imageurl'] ?>' alt='' width='258px'></div>";
                       
                    }
                    else
                    {
                        
                        $images = "<div class='BigImage'><div class='MagicZoom' id='image'><div class='MagicZoom'><a href='".url('/')."/productimg/".$value->productImage[0]->imageurl."' id='imageshref' class='MagicZoom' rel='zoom-position:inner;zoom-fade:true'><img id='bimg' src='".url('/')."/productimg/thumb/".$value->productImage[0]->imageurl ."' alt='' width='300'></a></div></div></div>";
                       
                    }
                  }
                  else
                  {
                      
                      $images = "<div class='BigImage'><img src=".public_path()."'/productimg/image4.jpg' alt='' width='258px'></div>";
                      
                  }
        }

        $arr = array(
            'productno' => $productno,
            'name' => $name,
            'industry' => $industryid,
            'category' => $catid,
            'subcategory' => $subcatid,
            'brand' => $brandid,
            'manufacture' => $manufacture,
            'caseweight' => $caseweight,
            'qtypercase' => $qtypercase,
            'pakaging' => $pakaging,
            'shipingcondition' => $shipingcondition,
            'productcodes' => $productcodes,
            'description' => $description,
            'image'=>$images
        );

        return $arr;
    }

    public function createPosting(Request $request){

        if ($request->createadd) {

            $postAdverstisement = new \App\PostAdverstisement;

            $postAdverstisement->userid = Auth::user()->userid;
            $postAdverstisement->pdate = date('Y-m-d H:i:sa');
            $postAdverstisement->productno = $request->addprono;
            $postAdverstisement->ptype = $request->addtype;
            $postAdverstisement->currency = $request->addcurrency;
            $postAdverstisement->uom = $request->adduom;
            $postAdverstisement->quantity = $request->addqty;
            $postAdverstisement->location = $request->addlocation;
            $postAdverstisement->expdate = $request->addexpdrange;
            
            if (empty($request->datepicker)) {
                $datepicker = "";
            } else {
                $formdatepicker = $request->datepicker;
                $datepicker     = date("Y-m-d", strtotime($formdatepicker));
            }

            $postAdverstisement->expirydate = $datepicker;
            $postAdverstisement->customerrefno = $request->addcurrefno;

            $postAdverstisement->timeframe = $request->addtimeframe;
            
            $langtemp         = '';
            $langname         = '';
            $countname       = '';
            $counttemp       = '';

            foreach ($request->addcountry as $value) {
                
                $cnty = \App\MstCountry::where('name',$value)->get();

                if (empty($counttemp)) {
                    $counttemp = $value;
                    $countname = $cnty[0]->name;
                } else {
                    $counttemp = $langtemp . ',' . $value;
                    $countname = $countname . ',' . $cnty[0]->name;
                }
            }



            foreach ($request->selectedlanguage as $values) {
                $lang = \App\MstLanguage::where('languageid',$values)->get();

                if (empty($langtemp)) {
                    $langtemp = $values;
                    $langname = $lang[0]->name;
                } else {
                    $langtemp = $langtemp . ',' . $values;
                    $langname = $langname . ',' . $lang[0]->name;
                }
            }
            $postAdverstisement->pakaging = $langname;
            $postAdverstisement->language = $langtemp;
            $postAdverstisement->country = $countname;
            $postAdverstisement->pstatus = "NEW";
            $postAdverstisement->targetprice = $request->addprice;
            $postAdverstisement->postno = $this->checkduplicy();
            
            $seller ='';
            $buyer='';
            if($request->addtype=='BUY'){

               $buyer = Auth::user()->userid;
            }
            else{

               $seller = Auth::user()->userid;
            }

            $postAdverstisement->buyer = $buyer;
            $postAdverstisement->seller = $seller; 
            

            $postAdverstisement->save();

            $useraction = new \App\UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = \Session::get('_token');
            $useraction->actiondate = date('Y-m-d H:i:sa');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Added Posting';

            $useraction->save();
            
            return redirect('posting/myPosting');            
            // header("location:my-posting.php?page=addpost&key=13");
        }
    }

    function randStringNumeric22($len=6){

        $str   = "";
        
        $chars = array_merge(range(0, 9));
        
        for ($i = 0; $i < $len; $i++) {
        
            list($usec, $sec) = explode(' ', microtime());
        
            $seed = (float) $sec + ((float) $usec * 100000);
        
            mt_srand($seed);
        
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        
        }
        
        return $str;
    
    }

    function randStringAlpha2($len = 1){

        $str   = "";
        $chars = array_merge(range('A', 'Z'));
        for ($i = 0; $i < $len; $i++) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
            mt_srand($seed);
            $str .= $chars[mt_rand(0, (count($chars) - 1))];
        }
        return $str;
    }
            
    function checkduplicy(){

        $postno= $this->randStringNumeric22();

        $sth = \App\PostAdverstisement::where('postno',$postno)->get();

        $r1 = count( $sth );

        if($r1>0)
        {
           $this->checkduplicy();
        }
        else
        {
          return $postno;
        }
    }

    public function postingDetailView($id){

        if((Auth::user()->roletype =="AD") || ( Auth::user()->roletype == "SA") ){
            $x='style="background:#ffffff"';
        }
        else{
            $x='style="background:#f9f9f9"';
        }

        $postStatus = \App\PostAdverstisement::where('postno',$id)->where('act_inactive','0')->whereNotIn('pstatus',['COMPLETED','CANCELLED','EXPIRED'])->with(['postCustomer'=>function($query){
                $query->where('isactive','1')->get();
            }])->with(['user'=>function($query){
                $query->where('isactive','1')->get();
            }])->with(['product'=>function($query){
                $query->whereIn('pstatus',['APPROVED','PROCESSING','WAITING'])->get();
            }])->count();

        $postingStatus = \App\PostAdverstisement::where('postno',$id)->whereIn('pstatus',['EXPIRED'])->count();


        $checkVal = $this->checkPermission();

        $ml = \App\MstLanguage::orderBy('name')->get();

        $currencies = \App\MstCurrency::orderBy('name')->get();

        $uom = \App\MstUom::orderBy('name')->get();
        
        $locations = \App\MstLocation::orderBy('name')->get();
 
        $expdateranges = \App\MstExpDaterange::orderBy('exprangeid','asc')->get();

        $timeframes = \App\MstTimeframe::orderBy('timeframeid')->get();

        $countries = \App\MstCountry::orderBy('name')->get();

        $data = array(
                'postStatus' => $postStatus,
                'postingStatus'=> $postingStatus,
                'checkVal'=>$checkVal,
                'x' => $x,
                'ml' => $ml,
                'currencies'=>$currencies,
                'uom'=>$uom,
                'locations'=> $locations,
                'expdateranges' => $expdateranges,
                'timeframes' => $timeframes,
                'countries' => $countries
             );

        return View('posting.detailView',$data)->with('title','Trading Soft:: Posting Detail View');
    }

    public function detailViewDelete(Request $request){

        if($request->action == 'delete'){

            $sqlcnt = \App\PostQuotation::where( 'postno',$request->postno )->count();
            
            if ($sqlcnt > 0) {

                return redirect('postingCatalog');
                // header("location:posting-catalog.php?error=Posting is under process.");
            } else {
                
                $pa = \App\PostAdverstisement::where('postno',$request->postno)->delete();
                
                return redirect('postingCatalog');
                // header("location:posting-catalog.php?success=Deleted successfully.");
            }
        }
        if($_REQUEST["action"]=='enableaction')
        {
            $pa = \App\PostAdverstisement::where('postno',$request->postno)->update(['act_inactive'=>1]);
 
            echo "1";

        }
        if($_REQUEST["action"]=='disableaction')
        {
            $pa = \App\PostAdverstisement::where('postno',$request->postno)->update(['act_inactive'=>0]);

            echo "1";

        }
    }

    public function fetchFunction(Request $request){

        if ($request->type == 'fetchallposts') {
            
            $postno = $request->postno;
            
            $result = \App\PostAdverstisement::where('postno',$postno)->get();

            foreach ($result as $value) {
                
                $postid           = isset($value->postid)?$value->postid:'';
                $userid           = isset($value->userid)?$value->userid:'';
                $ptype            = ucwords(strtolower(isset($value->ptype)?$value->ptype:''));  
                $currency         = isset($value->currency)?$value->currency:'';
                $productno        = isset($value->productno)?$value->productno:'';          
                $uom              = isset($value->uom)?$value->uom:'';
                $quantity         = isset($value->quantity)?$value->quantity:'';           
                $location         = isset($value->location)?$value->location:'';
                $expdate          = isset($value->expdate)?$value->expdate:'';            
                $expirydate       = isset($value->expirydate)?$value->expirydate:'';
                $customerrefno    = isset($value->customerrefno)?$value->customerrefno:'';      
                $pakaging         = isset($value->product->pakaging)?$value->product->pakaging:'';
                $language         = isset($value->language)?$value->language:'';           
                $name             = mb_convert_encoding(isset($value->product->name)?$value->product->name:'', "HTML-ENTITIES", "utf8");
                $industryid       = isset($value->product->industryid)?$value->product->industryid:'';         
                $brandid          = isset($value->product->brandid)?$value->product->brandid:'';
                $catid            = isset($value->product->catid)?$value->product->catid:'';              
                $subcatid         = isset($value->product->subcatid)?$value->product->subcatid:'';
                // $pakaging         = $value->pakaging;           
                $shipingcondition = isset($value->product->shipingcondition)?$value->product->shipingcondition:'';
                $caseweight       = isset($value->product->caseweight)?$value->product->caseweight:'';         
                $qtypercase       = isset($value->product->qtypercase)?$value->product->qtypercase:'';
                $description      = mb_convert_encoding(isset($value->product->description)?$value->product->description:'', "HTML-ENTITIES", "utf8");   
                $addedby          = isset($value->product->addedby)?$value->product->addedby:'';
                $isactive         = mb_convert_encoding(isset($value->product->isactive)?$value->product->isactive:'', "HTML-ENTITIES", "utf8");
                $roletype         = isset($value->product->roletype)?$value->product->roletype:'';
                $industry         = mb_convert_encoding(isset($value->product->industry->name) ? $value->product->industry->name : 'Not Applicable', "HTML-ENTITIES", "utf8");
                $category         = mb_convert_encoding(isset($value->product->category->name) ? $value->product->category->name : 'Not Applicable', "HTML-ENTITIES", "utf8");
                $subcategory      = mb_convert_encoding(isset($value->product->subcategory->name) ? $value->product->subcategory->name : 'Not Applicable', "HTML-ENTITIES", "utf8");         
                $brand            = mb_convert_encoding(isset($value->product->brand->name) ? $value->product->brand->name : 'Not Applicable', "HTML-ENTITIES", "utf8");
                $productid        = isset($value->product->productid)?$value->product->productid:'';          
                $targetprice      = isset($value->targetprice)?$value->targetprice:'';
                $timeframe        = isset($value->timeframe)?$value->timeframe:'';          
                $pstatus          = isset($value->pstatus)?$value->pstatus:'';
                $manufacture      = isset($value->product->manufacture)?$value->product->manufacture:'';        
                $packlang         = isset($value->pakaging)?$value->pakaging:'';
                $postno           = isset($value->postno)?$value->postno:'';             
                $pdate            = isset($value->pdate)?$value->pdate:'';
                $country          = isset($value->country)?$value->country:'';
                
                if((Auth::user()->roletype=="AD")||(Auth::user()->roletype=="SA")){

                    $result = isset($value->product->productCode)?$value->product->productCode:array();

                    $productcodes = '';
                    
                    foreach ($result as $values) {
                        
                        $productcodes .= $values->productcode;

                    }
                }
                else{

                    $result = isset($value->product->productCode)?$value->product->productCode:array();

                    $productcodes = '';
                    
                    foreach ($result as $values) {
                        
                        $productcodes .= $values->productcode;

                    }
                }

                $results=count(isset($value->product->productImage)?$value->product->productImage:'');

                 if($results!=0)
                 {
                    if (strpos($value->product->productImage[0]->imageurl,'http://') !== false)
                    {
                       
                       $images = "<div class='BigImage'><img src='<?php echo $productimage[0]['imageurl'] ?>' alt='' width='258px'></div>";
                       
                    }
                    else
                    {
                        
                        $images = "<div class='BigImage'><div class='MagicZoom' id='image'><div class='MagicZoom'><a href='".url('/')."/productimg/".$value->product->productImage[0]->imageurl."' id='imageshref' class='MagicZoom' rel='zoom-position:inner;zoom-fade:true'><img id='bimg' src='".url('/')."/productimg/thumb/".$value->product->productImage[0]->imageurl ."' alt='' width='300'></a></div></div></div>";
                       //$images = '<a class="MagicZoom" href="http://magictoolbox.sirv.com/images/magiczoomplus/jeans-1-z.jpg"><img src="http://magictoolbox.sirv.com/images/magiczoomplus/jeans-1-z.jpg?scale.height=400" alt=""/></a>';
                    }
                  }
                  else
                  {
                      
                      $images = "<div class='BigImage'><img src='".url('/')."/productimg/image4.jpg' alt='' width='258px'></div>";
                      
                  }
            }

            $arr = array(
                    'postid' => $postid,                    
                    'userid' => $userid,
                    'ptype' => $ptype,                      
                    'currency' => $currency,
                    'productno' => $productno,              
                    'uom' => $uom,
                    'quantity' => $quantity,                
                    'location' => $location,
                    'expdate' => $expdate,                  
                    'expirydate' => $expirydate,
                    'customerrefno' => $customerrefno,      
                    'pakaging' => $pakaging,
                    'language' => $language,                
                    'name' => $name,
                    'industryid' => $industryid,            
                    'brandid' => $brandid,
                    'catid' => $catid,                      
                    'subcatid' => $subcatid,
                    'pakaging' => $pakaging,                
                    'shipingcondition' => $shipingcondition,
                    'caseweight' => $caseweight,            
                    'qtypercase' => $qtypercase,
                    'description' => $description,          
                    'addedby' => $addedby,
                    'isactive' => $isactive,                
                    'roletype' => $roletype,
                    'industry' => $industry,                
                    'category' => $category,
                    'subcategory' => $subcategory,          
                    'brand' => $brand,
                    'productid' => $productid,              
                    'targetprice' => $targetprice,
                    'timeframe' => $timeframe,              
                    'pstatus' => $pstatus,
                    'manufacture' => $manufacture,          
                    'packlang' => $packlang,
                    'postno' => $postno,                    
                    'pdate' => $pdate,
                    'country' => $country,
                    'image' => $images,
                    'productcodes' =>$productcodes
                );
                // $conn->close();
                return $arr;
        }
    }

    public function addOffer(Request $request){

        if (empty($request->datepicker)) {
            $datepicker = "";
        } else {
            $formdatepicker = $request->datepicker;
            $datepicker     = date("Y-m-d", strtotime($formdatepicker));
        }

        $lg = '';
        $cn = '';

        $statement1 = new \App\PostQuotation;

        $statement1->quotationno = $this->randStringNumeric22(6) . "-" . $this->randStringAlpha2();
        $statement1->postno = $request->postno;
        $statement1->productno = $request->pro_id;
        $statement1->price = $request->price;
        $statement1->currency = $request->offerCurrency;
        $statement1->uom = $request->offerUoM;
        $statement1->quantity = $request->offerqty;
        $statement1->location = $request->offerlocation;
        $statement1->expdate = $request->offerdaterange;
        $statement1->expirydate  = $datepicker;

        for ($i=0; $i < count($request->offerlanguage); $i++) { 
            if( $i == (count($request->offerlanguage) - 1) ){
                $lg .= $request->offerlanguage[$i];
            }else{
                $lg .= $request->offerlanguage[$i] . ',';    
            }
            
        }


        $statement1->language = $lg;
        
        for ($i=0; $i < count($request->addcountry); $i++) { 
            if( $i == (count($request->addcountry) - 1) ){
                $cn .= $request->addcountry[$i];
            }else{
                $cn .= $request->addcountry[$i] . ',';    
            }
            
        }

        $statement1->country = $cn;
        $statement1->timeframe = $request->offertimeframe;
        $statement1->details = "New Offer By Account Manager";
        $statement1->offercrdate = date('Y-m-d H:i:sa');
        $statement1->userid = Auth::user()->userid;
        $statement1->quationstatus = "Submitted";
        $statement1->postuserid = $request->puserid;
        $statement1->type = "OFFER";

        $statement1->save();
        
        $statement2 = new \App\UserActivity;

        $statement2->userid = Auth::user()->userid;
        $statement2->ipaddress = $request->ip();
        $statement2->sessionid = \Session::get('_token');
        $statement2->actiondate = date('Y-m-d');
        $statement2->actiontime = date('Y-m-d H:i:sa');
        $statement2->actionname = 'Submitted Offer';

        $statement2->save();

        return '1';
    }

    public function myPosting(){

        $checkVal = $this->checkPermission();       
        
        return View('posting.myPosting',['checkVal'=>$checkVal])->with('title','Trading Soft::My Postings');
    }

    public function myPosting_ajax(){

        $territory1 = array();
        $territory2 = array();
        if ( empty( Auth::user()->industry ) == false ) {
            $industry1  = Auth::user()->industry;
            $territory1 = explode(',', $industry1);
            // $territory1 = ' AND mp.industryid IN (' . $industry1 . ')';
        }
        
        if (empty( Auth::user()->category ) == false) {
            $category1  = Auth::user()->category;
            $territory2 = explode(',', $category1);
            // $territory1 = $territory1 . ' AND mp.catid IN (' . $category1 . ')';
        }

        $sql = \App\PostAdverstisement::where( 'userid',Auth::user()->userid )->where( 'act_inactive','0' )->with(['postCustomer'=>function($query){
                    $query->where('isactive','1')->get();
                }])->with(['user'=>function($query){
                    $query->where('isactive','1')->get();
                }])->with(['product'=>function($query) use ($territory1) {
                    $query->where('pstatus','APPROVED')->whereIn('industryid',$territory1);
                }])->with(['product'=>function($query) use ($territory2) {
                    $query->orWhere('pstatus','APPROVED')->orWhereIn('catid',$territory2);
                }])->get();

         $record = array();
         $i = 0;

         foreach ($sql as $value) {

            $countro = \App\PostQuotation::where('postno',$value->postno)->count();
            
            $poststatus = isset($value->pstatus)?$value->pstatus:'';

            $postno = $value->postno;

            if ($poststatus == 'NEW') {
                $newpost = $this->mypoststatus($postno);
                $edit1   = '<a href="myposting-detail-view/' . $postno .'">View</a>&nbsp;|&nbsp;';
                if ($newpost == 'NEW') {
                    if ($countro <= 0) {
                        $edit2 = '<a href="edit-add/' . $postno . '">Edit</a>&nbsp;|&nbsp;';
                    } else {
                        $edit2 = '';
                    }
                } else {
                    $edit2 = '';
                }
                $edit3 = '<a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                $edit  = $edit1 . $edit2 . $edit3;
            }

            if ($poststatus == 'Submitted') {
                $newpost = $this->mypoststatus($postno);
                $edit1   = '<a href="myposting-detail-view/' . $postno. '">View</a>&nbsp;|&nbsp;';
                if ($newpost == 'NEW') {
                    if ($countro <= 0) {
                        $edit2 = '<a href="edit-add/' . $postno. '">Edit</a>&nbsp;|&nbsp;';
                    } else {
                        $edit2 = '';
                    }
                } else {
                    $edit2 = '';
                }
                $edit3 = '<a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
                $edit  = $edit1 . $edit2 . $edit3;
            }

            if ($poststatus == 'Countered') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="javascript:cancelme(\'' . $postno . '\')">Cancel</a>';
            }
            
            if ($poststatus == 'Completed') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
            if ($poststatus == 'EXPIRED') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
            
            if ($poststatus == 'CANCELLED') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
             if ($poststatus == 'Declined') {
                $edit = '<a href="myposting-detail-view/' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '"">Repost</a>';
            }
            if ($poststatus == 'Accepted') {
                $edit = '<a href="myposting-detail-view' . $postno . '">View</a>&nbsp;|&nbsp;
                         <a href="repost/' . $postno . '">Repost</a>';
            }
           
            if ($poststatus == 'CANCELLED') {
                $poststatus = "CANCELLED";
            } 
            else if($poststatus == 'Completed')
            {
                $poststatus = "COMPLETED";
            }
            else if($poststatus == 'EXPIRED')
            {
                $poststatus = "EXPIRED";
            }
            else if($poststatus == 'NEW')
            {
                $poststatus = "NEW";
            }
            else if($poststatus == 'Countered')
            {
                $poststatus = "Countered";
            }
            else if($poststatus == 'Declined')
            {
                $poststatus = "Declined";
            }
            else if($poststatus == 'Submitted')
            {
                $poststatus = "Submitted";
            }
            else if($poststatus == 'Accepted')
            {
                $poststatus = "Accepted";
            }
            else {
                $poststatus = $this->mypoststatus($postno);
            }

            $record[$i]['postno'] = $value->postno;
            $record[$i]['sdate'] = date('F d Y',strtotime( isset($value->pdate)?$value->pdate:'' ));
            $record[$i]['brand'] = isset($value->product->brand->name) ? $value->product->brand->name : '';
            $record[$i]['ptype'] = ucwords(strtolower(isset($value->ptype)?$value->ptype:''));
            $record[$i]['quantity'] = isset($value->quantity)?$value->quantity:'';
            $record[$i]['productName'] = isset($value->product->name)?$value->product->name:'';
            $record[$i]['price'] = isset($value->targetprice)?$value->targetprice:'';
            $record[$i]['currency'] = isset($value->currency)?$value->currency:'';
            $record[$i]['customerrefno'] = isset($value->customerrefno)?$value->customerrefno:'';
            $record[$i]['poststatus'] = $poststatus;
            // $record[$i]['edit'] = $edit;

            $i++;   
         }

         return \Response::json($record);
    }

    function mypoststatus($id)
    {
    
        $r1=0;  $r2=0;
        $r3=0;  $r4=0;
        $r5=0;  $r6=0;

        $r1 = \App\QuotationCounter::where('postno',$id)->count();

        $r2 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->count();

        $r3 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->where(function($query){
                $query->where('counetr_status','Accepted')->orWhere('offerCounterStatus','Accepted');
                })->count();

        $r4 = \App\QuotationCounter::where('postno',$id)->where(function($query){
                    $query->where('counetr_status','Accepted')->orWhere('offerCounterStatus','Accepted');
                })->count();
        
        $r5 = \App\PostQuotation::where('postno',$id)->whereIn('type',['QUOTATION','OFFER'])->where(        function($query){
                        $query->where('counetr_status','Declined')->orWhere('offerCounterStatus','Declined');
                    })->count();
        
        $r6 = \App\QuotationCounter::where('postno',$id)->where(function($query){
                    $query->where('counetr_status','Declined')->orWhere('offerCounterStatus','Declined');
                })->count();
        
        if($r1 > 0 && $r2>0)
        {
            if($r3>0 || $r4 >0)
            {
            return 'ACCEPTED';
            }
            elseif($r5>0 || $r6 >0)
            {
            return 'DECLINED';
            }
            else
            {
            return 'COUNTERED';
            }
        
        }
        elseif($r1<=0 && $r2>0)
        {   if($r3>0 || $r4 >0)
            {
            return 'ACCEPTED';
            }
            elseif($r5>0 || $r6 >0)
            {
            return 'DECLINED';
            }
            else
            {
            return 'SUBMITTED';
            }
        }
        else
        { 
        return 'NEW';
        }
    }

    public function cancelPosting(Request $request){
        if ($request->Type == 'cancelpost') {
            
            $statement1 = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('postno',$request->postid)->update(['pstatus'=>'CANCELLED','archivepost'=>'1']);
            
            $useraction = new \App\UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = \Session::get('_token');
            $useraction->actiondate = date('Y-m-d H:i:sa');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Cancelled Posting';

            $useraction->save();

        }       
    }

    
}