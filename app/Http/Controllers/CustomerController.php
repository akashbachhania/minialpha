<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MastRolePermission;
use App\PostCustomer;
use Auth;
use App\User;
use Session;
class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$checkVal = $this->checkPermission();

    	$users = User::where('roletype','AM')->get();

    	return View('customer.index',['checkVal'=>$checkVal,'users'=>$users])->with('title','Trading Soft::Customers');
    }

    public function customerTable_ajax(Request $request){

    	$checkVal = $this->checkPermission();
    	
    	if ($request->statusActionType == 'shatusChange') {
	        
	        $checkUser  = explode(',', str_ireplace('\'', '', $request->checkcust));
	        
	        $statusData = $request->statusData;
	        
	        if ($statusData == 'ACTIVE') {
	            
	            $sql = \App\PostCustomer::whereIn('custid',$checkUser)->update(['isactive'=>'1']);
	            
	        }
	        
	        if ($statusData == 'DEACTIVE') {
	            
	            $sql = \App\PostCustomer::whereIn('custid',$checkUser)->update(['isactive'=>'0']);

	        }

	        $useraction = new \App\UserActivity;

            $useraction->userid = Auth::user()->userid;
            $useraction->ipaddress = $request->ip();
            $useraction->sessionid = \Session::get('_token');
            $useraction->actiondate = date('Y-m-d');
            $useraction->actiontime = date('Y-m-d H:i:sa');
            $useraction->actionname = 'Updated Customer Status';

            $useraction->save();
            return 'Success';
	    }

		if( $request->Type != '' ){
			switch ($request->Type) {

	    		case 'exist':
	    				  
					  $pname  = $request->pname;
				      
				      $sql    = PostCustomer::where('refno',$pname)->exists();
		
				      if ($sql) {
				        echo 'exist';
				      } else {
				          echo '1';
				      }
					break;
	    		case 'add':
	    			
	    			$name      = $request->name;
				    $status    = $request->status;
				    $refuserid = Auth::user()->userid;
				    $acm       = $request->acm;
				    $edate     = date('Y-m-d');
				    $roletype  = Auth::user()->roletype;
				    $adduserid = Auth::user()->userid;
				    $result    = \App\PostCustomer::where('refno',$name)->count();

				    if( $result > 0 ){

				    	if ( $result > 0 ) {
		                    
		                    $arr = array(
		                        'name' => 'Record Already Exists',
		                        'msg' => 'EROOR'
		                    );
							
							return $arr;
		                
		                } else {

		                	$sql = new \App\PostCustomer;

		                	$sql->refno = $name;
		                	$sql->refuserid = $refuserid;
		                	$sql->edate = $edate;
		                	$sql->roletype = $roletype;
		                	$sql->adduserid = $adduserid;
		                	$sql->isactive = $status;

		                	$sql->save();

		                    $arr     = array(
		                        'name' => 'Record Saved Sucessfully',
		                        'msg' => 'SUCCESS'
		                    );

		                    $useraction = new \App\UserActivity;

		                    $useraction->userid = Auth::user()->userid;
		                    $useraction->ipaddress = $request->ip();
		                    $useraction->sessionid = Session::get('_token');
		                    $useraction->actiondate = date('Y-m-d');
		                    $useraction->actiontime = date('Y-m-d H:i:sa');
		                    $useraction->actionname = 'Added Customer';

		                    $useraction->save();

		                    return $arr;
		                }

				    }
				    else{

				    	if ($roletype == "AD") {
				    		
				    		$sql = new \App\PostCustomer;

				    		$sql->refno = $name;
				    		$sql->refuserid = $acm;
				    		$sql->edate = $edate;
				    		$sql->roletype = $roletype;
				    		$sql->adduserid = $adduserid;
				    		$sql->isactive = $status;

				    		$sql->save();

			            } else {
			                
			                $sql = new \App\PostCustomer;

				    		$sql->refno = $name;
				    		$sql->refuserid = $refuserid;
				    		$sql->edate = $edate;
				    		$sql->roletype = $roletype;
				    		$sql->adduserid = $adduserid;
				    		$sql->isactive = $status;

				    		$sql->save();

			            }
			            
			            $arr     = array(
			                'name' => 'Record Saved Sucessfully',
			                'msg' => 'SUCCESS'
			            );
			            
			            $useraction = new \App\UserActivity;

	                    $useraction->userid = Auth::user()->userid;
	                    $useraction->ipaddress = $request->ip();
	                    $useraction->sessionid = Session::get('_token');
	                    $useraction->actiondate = date('Y-m-d');
	                    $useraction->actiontime = date('Y-m-d H:i:sa');
	                    $useraction->actionname = 'Added Customer';

	                    $useraction->save();

	                    return $arr;
				    }

	    			break;

	    		case 'update':
	    			
	    			$name   = $request->name;
			        $status = $request->status;
			        $id     = $request->id;

			        $result = \App\PostCustomer::where('custid',$id)->update(['refno'=>$name,'isactive'=>$status]);

			        $arr     = array(
                        'name' => 'Update Sucessfully',
                        'msg' => 'SUCCESS'
                    );

			        $useraction = new \App\UserActivity;

                    $useraction->userid = Auth::user()->userid;
                    $useraction->ipaddress = $request->ip();
                    $useraction->sessionid = Session::get('_token');
                    $useraction->actiondate = date('Y-m-d');
                    $useraction->actiontime = date('Y-m-d H:i:sa');
                    $useraction->actionname = 'Updated Customer';

                    $useraction->save();

                    return $arr;
	    			
	    			break;	

	    		case 'delete':

	    			$id = $request->id;
	    			$deleteData = explode('||', $id);

	    			$sql = \App\PostAdverstisement::where('customerrefno',$deleteData[1])->count();

	    			if( $sql > 0 ){
	    				$arr = array(
	                        'name' => 'Related Record Exists',
	                        'msg' => 'ERROR'
	                    );

	                    return $arr;
	    			}
	    			else{
	    				$result = \App\PostCustomer::where('custid',$deleteData[0])->delete();

	    				$useraction = new \App\UserActivity;

	                    $useraction->userid = Auth::user()->userid;
	                    $useraction->ipaddress = $request->ip();
	                    $useraction->sessionid = Session::get('_token');
	                    $useraction->actiondate = date('Y-m-d');
	                    $useraction->actiontime = date('Y-m-d H:i:sa');
	                    $useraction->actionname = 'Deleted Customer';

	                    $useraction->save();

	                    $arr     = array(
	                        'name' => 'Related Record Exists',
	                        'msg' => 'A1'
	                    );

	                    return $arr;
	    			}

	    			break;

	    		case 'search':

	    			$id = $request->id;
	    			
	    			$sql = \App\PostCustomer::find($id);

	    			$arr = array(
			            'name' => $sql->refno,
			            'status' => $sql->isactive,
			            'refuserid' => $sql->refuserid
			        );

			        return $arr;


	    			break;

	    		default:

					break;
			}	
		}
		else{
			if (Auth::user()->roletype == 'AM'){

	    		$result = PostCustomer::where('refuserid',Auth::user()->userid)->get();

	    	}
		    else{

		    	$result = PostCustomer::all();

		    }

		    $record = array();
		    $i = 0;

		    foreach ($result as $value) {
		    	$record[$i]['userCheck'] = '<input type="checkbox" class="" name="checkcust" id="checkcust" value="' . $value->custid . '" />';
		    	$record[$i]['edate'] = date("M d Y", strtotime($value->edate));
		    	$record[$i]['refno'] = $value->refno;
		    	$record[$i]['refuserid'] = $value->refuserid;
		    	
		    	if ($value->isactive == '1'){
	                $record[$i]['msg'] = '<span style="color:green">Active</span>';
		    	}
	            else{
	                $record[$i]['msg'] = '<span style="color:red">Inactive</span>';
	            }
		    	
		    	if (Auth::user()->roletype == "AD") {
	                $edit = '<a href="javascript:editme(\'' . $value->custid . '\')">Edit</a> &nbsp;|&nbsp;<a  href="javascript:deleteme(\'' . $value->custid . '||' . $value->refno . '\')">Delete</a>&nbsp;|&nbsp;
				<a href="'.url('/').'/posting/user-posting/' . $value->refno . '">Postings</a>&nbsp;|&nbsp;<a  href="posting_pie_chart/Postings/' . $value->refno . '/Generate">Reports</a>';
	            } else {
	                if ($checkVal[0]['pageEdit'] == '1') {
	                    $edit1 = '<a href="javascript:editme(\'' . $value->custid . '\')">Edit</a> &nbsp;|&nbsp;';
	                }
	                
	                if ($checkVal[0]['pageDelete'] == '1') {
	                    $edit2 = '<a  href="javascript:deleteme(\'' . $value->custid . '||' . $value->refno . '\')">Delete</a>&nbsp;|&nbsp;';
	                }
	                
	                $edit3 = '<a href="'.url('/').'/posting/user-posting/' . $value->refno . '">Postings</a>&nbsp;';
	               $edit  = $edit1 . $edit2 . $edit3;
	            }

		    	$record[$i]['edit'] = $edit;
		    	
		    	$i++;
		    }

		    return \Response::json($record);
		}    	
    	
    }

    public function checkPermission(){

        $data = MastRolePermission::select('RoleId','ViewP','AddP','EditP','DeleteP')->where('RoleId',Auth::user()->roleid)->where('PageId','4')->get();
        return $data;
    }
    
}
