<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('report.index')->with('title','Trading Soft::Report');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function functions(Request $request){

        if ($request->actionmode == "industaction") {

            $roletype = Auth::user()->roletype;
            $userid   = Auth::user()->userid;
            $status   = 1;
            $iid      = explode(',', Auth::user()->industry);

            if ($roletype == 'AM') {
                if (empty($iid) == false) {
                    
                    $sth = \App\Industry::where('isactive',$status)->whereIn('industry',$iid)->orderBy('name')->get();

                    foreach ($sth as $result) {
                        
                        $industryid = $result->industryid;
                        
                        $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                        
                        $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);    
                    
                    }

                    return \Response::json($json);

                } 
                else if (empty($iid) == true) {

                    $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();

                    foreach ($sth as $result) {
                        
                        $industryid=$result->industryid;

                        $name=mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                        
                        $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);
                    
                    }
                    
                    return \Response::json($json);
                }
            }
            if ($roletype == 'AD') {
                
                $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();

                foreach ($sth as $result) {
                        
                    $industryid=$result->industryid;

                    $name=mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                    
                    $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);
                
                }
                
                return \Response::json($json);
            }

            if ($roletype == 'SA') {
                
                $sth = \App\Industry::where('isactive',$status)->orderBy('name')->get();

                foreach ($sth as $result) {
                        
                    $industryid=$result->industryid;

                    $name=mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                    
                    $json['itemData'][] = array('industryid'=>$industryid,  'name'=>$name);
                
                }
                
                return \Response::json($json);
            }
        }

        if ($request->actionmode == 'selectreportindustry') {
    
            $industry = array(
                $request->ind_id
            );

            $in   = implode(',', $_POST['ind_id']);
            $status = 1;

            $stmt = \App\Category::where('isactive',$status)->whereIn('industryid',$request->ind_id)->orderBy('name')->get();

            
            if( count($stmt) ){
                foreach ($stmt as $result) {
                    $catid = $result->catid;
                    $industryid = $result->industryid;
                    $isactive = $result->isactive;        
                    $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");        
                    
                    $json['itemData'][] = array(
                                            'catid'=>$catid, 
                                            'name'=>$name,
                                            'industryid'=>$industryid,  
                                            'isactive'=>$isactive
                                        );
                }
            }
            else{
                $json['itemData'][] =array();    
            }
            //select brands
            $stmt2 = \App\Brand::where('isactive',$status)->whereIn('industryid',$request->ind_id)->orderBy('name')->get();

            if(count($stmt2) > 0) {
                foreach ($stmt2 as $result2) {

                    $brandid=$result2->brandid;
                    
                    $industryid=$result2->industryid;
                    
                    $isactive=$result2->isactive;
                    
                    $name=mb_convert_encoding($result2->name, "HTML-ENTITIES", "ISO-8859-1");
                
                    $json['brandData'][] =array(
                                'sql'=>'SELECT brandid, name, isactive, industryid FROM mst_brand WHERE isactive = :isactive AND industryid IN (' . $in . ') ORDER BY name',

                                'brandid'=>$brandid,    
                                'name'=>$name,
                                'industryid'=>$industryid,  
                                'isactive'=>$isactive
                                );
                }
            
            } 
            else {
                
                $json['brandData'][] = array(
                                'brandid'=>'0',
                                'sql'=>'SELECT brandid, name, isactive, industryid FROM mst_brand WHERE isactive = :isactive AND industryid IN (' . $in . ') ORDER BY name',
                                'count'=>count($stmt2));
            }
            return \Response::json($json);
        }

        if ($request->actionmode == 'selectsubcategory') {

            $industry = array(
                $request->cat_id
            );

            $status = 1;

            $userid   = Auth::user()->userid;

            $subcatid = explode(',', Auth::user()->subcategory); 

            $roletype = Auth::user()->roletype;

            if (empty($subcatid)) {

                $stmt = \App\SubCategory::where('isactive',$status)->whereIn('catid',$industry)->orderBy('name')->get();

                foreach ($stmt as $result) {
                    $json['itemData'][] = $result;
                }

                return \Response::json($json);
            }
            else
            {

                $stmt = \App\SubCategory::where('isactive',$status)->whereIn('catid',$industry)->whereIn('subcatid',$subcatid)->orderBy('name')->get();
                
                foreach ($stmt as $result2) {
                    $json['itemData'][] = $result2;
                }
                
                return \Response::json($json);
            }
        }

        if ($request->actionmode == 'customeraction') {
            
            $status= 1;
            
            $sth = \App\PostCustomer::where('isactive',$status)->orderBy('refno')->get();

            foreach ($sth as $result) {

                $custid = $result->custid;
            
                $name = mb_convert_encoding($result->refno, "HTML-ENTITIES", "ISO-8859-1");
            
                $json['itemData'][] = array(
                                        'custid'=>$custid,  
                                        'refno'=>$name
                                    );
            }
            
            return \Response::json($json);
        }


        if ($_POST['actionmode'] == 'alluserid') {
            
            $status= 1;
            
            $sth = \App\User::where('isactive',$status)->orderBy('userid')->get();

            foreach ($sth as $result) {
            
                $userid = $result->userid;

                $name = mb_convert_encoding($result->userid, "HTML-ENTITIES", "ISO-8859-1");
                
                $json['itemData'][] = array(
                                        'userid'=>$userid,  
                                        'userid'=>$name
                                    );   
            
            }
            
            return \Response::json($json);
        }


        if ($request->actionmode == 'alllocation') {
         
            $status= 1;
            
            $sth = \App\MstLocation::where('isactive',$status)->orderBy('name')->get();

            foreach ($sth as $result) {
            
                $locationid = $result->locationid;

                $name = mb_convert_encoding($result->name, "HTML-ENTITIES", "ISO-8859-1");
                
                $json['itemData'][] = array(
                                        'locationid'=>$locationid,
                                        'name'=>$name
                                    );   
            
            }
            
            return \Response::json($json);
        }

    }

    public function posting_pie_chart(Request $request){

        $where = "1=1";
        if(!empty($request->to && $request->from))
        {
            $fromdate = strtotime($request->from);
            $fromdate = date("Y-m-d",$fromdate);
            $todate = strtotime($request->to);
            $todate = date("Y-m-d",$todate);
            $where= $where." && pdate between '".$fromdate."' AND  '".$todate." '";
        } else {
            $fromdate = "";
            $todate = "";
        }
        if(empty($request->selectedindustry))
        {
            $selectedindustry=array();
        }

        $selectedindustry = $this->unsetbyvalue($request->selectedindustry,'all');

        $selectcategory = $this->unsetbyvalue($request->selectcategory,'all');
        $selectbrands = $this->unsetbyvalue($request->selectbrands,'all');
        $selectsubcat = $this->unsetbyvalue($request->selectsubcat,'all');
        $alluserid = $this->unsetbyvalue($request->alluserid,'all');
        $customerdata = $this->unsetbyvalue($request->customerdata,'all');
        $type = $this->unsetbyvalue($request->type,'all');
        $alllocation = $this->unsetbyvalue($request->alllocation,'all');
        $status = $this->unsetbyvalue($request->status,'all');
        
        $sec = $request->Section;
        
        if(!empty($selectedindustry)){
            $selectedindustry = implode(',',$selectedindustry); 
        } else {
            $selectedindustry = "";
        }
        if(!empty($selectcategory)){
            $selectcategory = implode(',',$selectcategory); 
        } else {
            $selectcategory = "";   
        }
        if(!empty($selectbrands)){
            $selectbrands = implode(',',$selectbrands); 
        } else {
            $selectbrands = ""; 
        }
        if(!empty($selectsubcat)){
            $selectsubcat = implode(',',$selectsubcat); 
        } else {
            $selectsubcat = ""; 
        }
        if(!empty($alluserid)){
            $alluserid = implode(',',$alluserid);   
        } else {
            $alluserid = "";    
        }
        if(!empty($customerdata)){
            $customerdata = implode(',',$customerdata); 
        } else {
            $customerdata = ""; 
        }
        if(!empty($type)){
            $type = implode(',',$type); 
        } else {
            $type = ""; 
        }
        if(!empty($alllocation)){
            $alllocation = implode(',',$alllocation);   
        } else {
            $alllocation = "";  
        }
        if(!empty($status)){
            $status = implode(',',$status); 
        } else {
            $status = "";   
        }
        if (!empty($industry))
        {  

            $result = \App\Industry::where('industryid',$industry)->get();

            $indname=$result[0]->name;
        
        }
            
        if (!empty($cat_id))
        {
            
            $result1 = \App\Category::where('catid',$cat_id)->get(); 

             $catname=$result1[0]->name;
        }
        if (!empty($sub_cat))
        {
            $result2 = \App\SubCategory::where('subcatid',$sub_cat)->get();

            $subcatname=$result2[0]->name;
        }
        if (!empty($bname))
        {
            
            $result3 = \App\Brand::where('brandid',$bname)->get();

            $brandname=$result3[0]->name;
        }
        $headTitle = '';
        if(isset($filtername)=="all") 
        {
        
          $headTitle = 'All ' . $sec;
        
        }
        else
        {
            
          $headTitle = $sec; 

            if(!empty($indname))
            {
                if(!empty($catname))
                {
                  if(!empty($subcatname))
                   {
                      if(!empty($brandname))
                      {
                        $headTitle .= "By  Brands";
                      }
                      else
                      {
                       $headTitle .= "By  Subcategory";
                      }
                   }
                   else
                   {
                      $headTitle .= "By  category";
                   }
                }
                else
                {
                  $headTitle .= "By  industry";
                }
            } 
            
        }       


        $lastselected1 = '';
        unset($_GET['addproduct']);
        foreach($_GET as $key=>$value){
            if(!empty($_GET["$key"])){
                $lastfilledparam = $key;
            }
        }
        $renamearray = array(
                       'Section'=>'All '.$request->Section,
                       'from'=>$request->Section."  By Date",
                       'to'=>$request->Section."  By Date",
                       'selectedindustry'=>$request->Section." By Industry",
                       'selectcategory'=>$request->Section." By Category",
                       'selectsubcat'=>$request->Section." By Sub-Category",
                       'selectbrands'=>$request->Section." By Brands",
                       'searchid'=>$request->Section." By Product Name",
                       'customerdata'=>$request->Section." By Customers",
                       'status'=>$request->Section." By Status",
                       'alluserid'=>$request->Section." By Users",
                       'type'=>$request->Section." By Type",
                       'alllocation'=>$request->Section." By Location"
                            );
        $ra = $renamearray[$lastfilledparam];
        // echo $renamearray[$lastfilledparam];die;
        return View('report.pieChart',['sec'=>$sec,'headTitle'=>$headTitle,'ra'=>$ra])->with('title','Trading soft::Posting Pie Chart');
    }

    function unsetbyvalue($array=array(),$value=''){
        if(empty($array))
        {
            $array=array();
        }
        else
        {
            $fields = array_flip($array);
            unset($fields[$value]);
            $fields = array_flip($fields);
            return $fields;
        }
    }

}
