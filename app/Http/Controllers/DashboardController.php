<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;

class DashboardController extends Controller
{

    public function __construct(){

        $this->middleware('auth');
    }

    public function index(){

    	if( Auth::user()->roletype == 'AM' ){
    		
    		$sqlq = \App\PostQuotation::where( 'postuserid',Auth::user()->userid )->where( 'readunreadst','0' )->whereIn('type',['QUOTATION'])->whereHas('product',function($query){
    				$query->where( 'pstatus','APPROVED' );
    		})->orderBy('quoteid','DESC')->limit(15)->offset(0)->get();
    	}	

		else if (Auth::user()->roletype == 'TM') {

			$sqlq = \App\TMAcceptedOffers::where( 'noti_status','Unread' )->limit(15)->offset(0)->get();

		} else if (Auth::user()->roletype == 'SA') {

			$sqlq="";

		} else if (Auth::user()->roletype == 'AD') {

			$sqlq="";

		}    	
		$swlw = '';
		if( Auth::user()->roletype == 'QF' ){

			$sqlq = \App\PostQuotation::where('userid','!=',Auth::user()->userid)->where('readunreadst','0')->where('type','!=','QUOTATION')->get();

			if( !empty($sqlq)  && (count($sqlq) > 0) ){
							
				foreach( $sqlq as $value ){

					$swlw = '<li>
								<span class="text">
									<a href="quotepost-detail-view.php?postno='. $value->postno .'&quotationno='. $value->quotationno .'&ptype='. $value->ptype .'&productno='. $value->productno .'" class="ofercss">
										'. $value->details .' - '. $value->ptype .'&nbsp;'. $value->quantity .'&nbsp;'. $value->product->name .'
									</a>
								</span>
								<small class="label label-default pull-right date">Just Now</small>
							</li>';
					
					$sqlqm = \App\QuotationCounter::where('quotationno',$value->quotationno)->where('acceptedBy',Auth::user()->userid)->get();
				
					if( !empty($sqlqm)  && (count($sqlqm) > 0) ){

						foreach ($sqlqm as $sqlqmresult) {
							
							$swlw .= '<li>
										 <span class="text>
										 '.$sqlqmresult->counter_msg.' - '.$sqlqmresult->postAdverstisement->ptype.'&nbsp;' . $sqlqmresult->counter_quantity . '&nbsp;'.$sqlqmresult->product->name.'
										 </span>
									  	 <small class="label label-default pull-right date">Just Now</small>
									  </li>';

						}

					}

				}

			}

		}


		$mypostings = \App\PostAdverstisement::where('userid',Auth::user()->userid)->whereNotIn('pstatus',['Accepted','Completed','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('product',function($query){
				$query->where('pstatus','APPROVED');
			})->whereHas('postCustomer',function($query){
				$query->where('isactive','1');
			})->whereHas('user',function($query){
				$query->where('isactive','1');
			})->orderBy('pdate','DESC')->limit(10)->offset(0)->get();

		$industry='';
		
		if (empty(Auth::user()->industry) == false) {

			$industry  = explode(',', Auth::user()->industry);
		
		}
		else
		{
		    $industry = \App\Industry::where('isactive','1')->lists('industryid')->toArray();
		}

		$recentpostings = \App\PostAdverstisement::whereNotIn('pstatus',['Accepted','Completed','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('product',function($query) use ($industry){
				
					$query->where('pstatus','APPROVED')->whereHas('industry',function($query) use ($industry){
						$query->whereIn('industryid',$industry);
					});
			
			})->whereHas('postCustomer',function($query){
				$query->where('isactive','1');
			})->whereHas('user',function($query){
				$query->where('isactive','1');
			})->limit(10)->offset(0)->orderBy('pdate','DESC')->get();

		$data = array(
				'mypostcount' => count($this->mypostcount()),
				'allpostcount' => count($this->allpostcount()),
				'allproductCount' => count($this->allproductCount()),
				'allofferCount' => count($this->allofferCount()),
				'allqouteCount' => count($this->allqoutCount()),
				'allofferacceptedCount' => count($this->allofferaccCount()),
				'sqlq' => $sqlq,
				'swlw' => $swlw,
				'mypostings'=>$mypostings,
				'recentpostings' => $recentpostings
			);

    	return view('dashboard.dashboard',$data)->with(['title'=>'Dashboard','page_title'=>'Dashboard','page_description'=>'Welcome '. Auth::user()->userid]);
    }


    function mypostcount(){
    
      $result = \App\MastRolePermission::where('PageId','13')->where('RoleId',Auth::user()->roleid)->get();


	  
		 if( !empty($result[0]) && ($result[0]['ViewP'] == '1') ){
		 	
		 	$sth = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('act_inactive','0')->whereHas('user',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('postCustomer',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('product',function($query){
		 			$query->where('pstatus','APPROVED');
		 		})->get();


		 }
		 else{
		 	$sth = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('act_inactive','0')->whereHas('user',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('postCustomer',function($query){
		 			$query->where('isactive','1');
		 		})->whereHas('product',function($query){
		 			$query->where('pstatus','APPROVED');
		 		})->get();

		 		// $sth = \App\PostAdverstisement::where('userid',Auth::user()->userid)->where('act_inactive','0')->whereHas('user', function($q)
					// {
					//     $q->where('isactive','1');

					// })->whereHas('postCustomer',function($query){
				 // 			$query->where('isactive','1')->get();
				 // 		})->get();die('asd');
					// 		 }    
    	 
    	}

	    return $sth;
	}

	function allpostcount(){

	    $territory1 = array();
        $territory2 = array();
	    $industryid  = isset(Auth::user()->industry)?explode(',', Auth::user()->industry):array();
	    $category2  = Auth::user()->category;

	  	if (empty(Auth::user()->industry) == false) {
            $industry1  = Auth::user()->industry;
            $territory1 = explode(',', $industry1);
            // $territory1 = ' AND mp.industryid IN (' . $industry1 . ')';
        }
        
        if (empty(Auth::user()->category) == false) {
            $category1  = Auth::user()->category;
            $territory2 = explode(',', $category1);
            // $territory1 = $territory1 . ' AND mp.catid IN (' . $category1 . ')';
        }
	    
	    if(Auth::user()->roletype != "AD"){

	    	$result1 = \App\MastRolePermission::where('PageId','12')->where('RoleId',Auth::user()->roleid)->get();
			
			if(!empty($result[0]) && ($result1[0]['ViewP'] == '1')){

				$sth = \App\PostAdverstisement::whereNotIn('pstatus',['COMPLETED','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('user',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('postCustomer',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('product',function($query) use ($territory1){
			 			$query->where('pstatus','APPROVED')->whereIn('industryid',$territory1);
			 		})->whereHas('product',function($query) use ($territory2){
			 			$query->whereIn('catid',$territory2);
			 		})->get();

			}else{

				$sth = \App\PostAdverstisement::whereNotIn('pstatus',['COMPLETED','CANCELLED','EXPIRED'])->where('act_inactive','0')->whereHas('user',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('postCustomer',function($query){
			 			$query->where('isactive','1');
			 		})->whereHas('product',function($query) use ($territory1){
			 			$query->where('pstatus','APPROVED')->whereIn('industryid',$territory1);
			 		})->whereHas('product',function($query) use ($territory2){
			 			$query->whereIn('catid',$territory2);
			 		})->get();

			}
	   
		}
		else{

			$sth = \App\PostAdverstisement::whereNotIn('pstatus',['COMPLETED','CANCELLED'])->whereHas('product',function($query) use ($industryid){
				$query->whereIn('industryid',$industryid);
			})->get();


		}

	    return $sth;
		
	}	

	function allproductCount(){

		$territory = array();
		$territory1 = array();
		$territory2 = array();

		if (empty(Auth::user()->industry) == false){

			$industry = Auth::user()->industry;
			$territory = explode(',', Auth::user()->industry);
			// $territory = ' AND industryid IN (' . $industry . ')';
		
		}

		if (empty(Auth::user()->category) == false){

			$category = Auth::user()->category;
			$territory1 = explode(',', Auth::user()->category);
			// $territory = $territory . ' AND catid IN (' . $category . ')';
		
		}
		if (empty(Auth::user()->brand) == false){

			$brand = Auth::user()->brand;
			$territory2 = explode(',', Auth::user()->brand);
			// $territory = $territory . ' AND brandid IN (' . $brand . ')';
		}
		
		if (Auth::user()->roletype == "AD") {

			$sth = \App\MstProduct::where('pstatus','<>','1')->whereIn('industryid',$territory)->whereIn('catid',$territory1)->whereIn('brandid',$territory2)->get();

		}
		else
		{
			$result1 = \App\MastRolePermission::where('PageId','7')->where('RoleId',Auth::user()->roleid)->get();
			
			if(!empty($result[0]) && ($result1[0]['pageView']=='1'))
			{
				$sth = \App\MstProduct::where('pstatus','APPROVED')->whereIn('industryid',$territory)->whereIn('catid',$territory1)->whereIn('brandid',$territory2)->get();
			}
			else{

				$sth = \App\MstProduct::where('pstatus','APPROVED')->whereIn('industryid',$territory)->whereIn('catid',$territory1)->whereIn('brandid',$territory2)->get();

			}

		}
	    
	    return $sth;
	}

	function allofferCount(){
		
		$result1 = \App\MastRolePermission::where('PageId','47')->where('RoleId',Auth::user()->roleid)->get();

		if( count($result1) > 0 ){
			foreach ($result1 as $value) {
				if( !empty($result[0]) && $value->ViewP == '1'){

					$sth = \App\PostQuotation::where('userid',Auth::user()->userid)->where('type','OFFER')->whereHas('product',function($query){
							$query->where('pstatus','APPROVED');
						})->get();

				}
				
			}
		}
		else{
					
			$sth = \App\PostQuotation::all();
		
		}

	    return $sth;
	}

	function allqoutCount(){

		$sth = \App\PostQuotation::where('postuserid',Auth::user()->userid)->where('type','QUOTATION')->get();

	    return $sth;
	}

	function allofferaccCount(){

		$territory1 = array();
		$territory2 = array();

	 	if (empty(Auth::user()->industry) == false) {

            $industry1  = Auth::user()->industry;
            $territory1 = explode(',', Auth::user()->industry);
            // $territory1 = ' AND mp.industryid IN (' . $industry1 . ')';
        }
        
        if (empty(Auth::user()->category) == false) {

            $category1  = Auth::user()->category;
            $territory2 = explode(',', Auth::user()->category);
            // $territory1 = $territory1 . ' AND mp.catid IN (' . $category1 . ')';
        
        }

        $result1 = \App\MastRolePermission::where('PageId','12')->where('RoleId',Auth::user()->roleid)->get();
			
		if(!empty($result[0]) && ($result1[0]['ViewP']=='1'))
		{	
			$sth = \App\PostAdverstisement::where('userid','<>',Auth::user()->userid)->whereIn('pstatus',['Accepted','Declined'])->whereHas('product',function($query) use ($territory1){
					$query->whereIn('industryid',$territory1);
			})->whereHas('product',function($query) use ($territory2){

					$query->whereIn('catid',$territory2);
			
			})->get();

		}
		else{
			
			$sth = \App\PostAdverstisement::where('userid','<>',Auth::user()->userid)->whereIn('pstatus',['Accepted','Declined'])->whereHas('product',function($query) use ($territory1){
					$query->whereIn('industryid',$territory1);
			})->whereHas('product',function($query) use ($territory2){

					$query->whereIn('catid',$territory2);
			
			})->get();

		}
		
    	return $sth;
	}
}
