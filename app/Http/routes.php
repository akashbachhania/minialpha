<?php
     date_default_timezone_set('US/Eastern');
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/','Auth\AuthController@showloginform');
    Route::get('dashboard','DashboardController@index');

	Route::get('userdetails','UserController@index');
    Route::post('userdetail_ajax','UserController@userdetail_ajax');


	Route::get('activity','UserController@activity');
	Route::get('activity/{id}','UserController@activity');
	Route::get('activity_ajax','UserController@activity_ajax');
    Route::get('activity_ajax/{id}','UserController@activity_ajax');
    
    Route::get('create','UserController@create');
	
    Route::get('exportActivity','UserController@exportActivity');
	Route::get('exportActivity/{id}','UserController@exportActivity');
	
    Route::post('save','UserController@store');
	Route::get('checkauthpass','UserController@checkAuthPass');
    Route::get('edit/{id}', 'UserController@edit');
    Route::post('update/{id}','UserController@update');
    Route::get('delete/{id}','UserController@destroy');

    Route::post('multiselect','UserController@multiselect');
    
    Route::post('userreports','UserController@userreports');

    //Route for orders
    Route::get('order/acceptedoffers','OrderController@index');

    // Route::get('posting/{id}','PostingController@index');
    // Route::get('posting/add','PostingController@create');

    Route::get('order/completedoffers','OrderController@index');
    Route::get('order/offers_ajax/{type}','OrderController@offers_ajax');

    // Route::get('posting/{id}','PostingController@index');
    Route::get('posting/postingCatalog','PostingController@postingCatalog');
    Route::post('posting/postingCatalog_ajax','PostingController@postingCatalog_ajax');
    Route::get('posting/create','PostingController@create');
    Route::post('posting/createPosting','PostingController@createPosting');
    Route::post('posting/search','PostingController@search');
    Route::post('posting/fetch_productdetail','PostingController@fetch_productdetail');
    Route::get('posting/import-post','PostingController@importPost');
    Route::post('posting/importPostStore','PostingController@store');
    Route::get('posting/my-offers','PostingController@myOffers');
    Route::post('posting/my-offers-ajax','PostingController@myOffersAjax');
    Route::get('posting/posting-detail-view/{id}','PostingController@postingDetailView');
    Route::post('posting/detailViewDelete','PostingController@detailViewDelete');
    Route::post('posting/fetchFunction','PostingController@fetchFunction');
    Route::post('posting/addOffer','PostingController@addOffer');
    Route::get('posting/myPosting','PostingController@myPosting');
    Route::post('posting/myPosting_ajax','PostingController@myPosting_ajax');
    Route::post('posting/cancelPosting','PostingController@cancelPosting');

    Route::get('posting/sample', function () {

        return \Response::download(public_path() . '/posting.csv');
    });

    Route::get('posting/user-posting/{refno}','PostingController@index');
    Route::post('posting/user-posting_ajax','PostingController@index');
        
    Route::get('setting/industry','SettingController@industry');
    Route::post('setting/industry_ajax','SettingController@industry_ajax');
    
    Route::get('setting/category','SettingController@category');
    Route::post('setting/category_ajax','SettingController@category_ajax');

    Route::get('setting/subcategory','SettingController@subcategory');
    Route::post('setting/subcategory_ajax','SettingController@subcategory_ajax');

    Route::get('setting/brand','SettingController@brand');
    Route::post('setting/brand_ajax','SettingController@brand_ajax');

    Route::get('setting/mstsecurity','SettingController@mstsecurity');
    Route::post('setting/mstsecurity_ajax','SettingController@mstsecurity_ajax');

    Route::get('setting/dbbackup','SettingController@backupPage');
    Route::get('setting/dbrestore','SettingController@restorePage');


    Route::get('setting/backup','SettingController@backup');
    Route::post('setting/restore','SettingController@restore');

    Route::get('setting/packaging','SettingController@packaging');
    Route::post('setting/packaging_ajax','SettingController@packaging_ajax');

    Route::get('setting/shippingConditions','SettingController@shippingConditions');
    Route::post('setting/shippingConditions_ajax','SettingController@shippingConditions_ajax');

    Route::get('product/add','ProductController@index');
    Route::get('product/browse','ProductController@browseProduct');

    Route::post('product/browseProduct_ajax','ProductController@browseProduct_ajax');

    Route::get('product/detail-product-view/{id}','ProductController@show');

    Route::post('product/ajax','ProductController@product_ajax');
    Route::post('product/store','ProductController@store');

    Route::post('product/addProductPrefix_ajax','ProductController@addProductPrefix_ajax');

    Route::get('product/suggestedProduct','ProductController@suggestedProduct');
    Route::post('product/suggestedProduct_ajax','ProductController@suggestedProduct_ajax');

    Route::get('product/suggestedProductCode','ProductController@suggestedProductCode');
    Route::post('product/suggestedProductCode_ajax','ProductController@suggestedProductCode_ajax');

    Route::get('product/reportedProduct','ProductController@reportedProduct');
    Route::post('product/reportedProduct_ajax','ProductController@reportedProduct_ajax');

    Route::get('product/edit-product/{id}','ProductController@edit');
    Route::post('product/update-product/{id}','ProductController@update');
    Route::post('product/product_delete','ProductController@destroy');

    Route::get('product/create-new-add/{id}','ProductController@createNewAdd');
    Route::post('product/fetchalldata','ProductController@fetchalldata');
    
    Route::get('product/view-productad/{id}','ProductController@viewProductad');

    Route::post('product/viewProductadTable','ProductController@viewProductadTable');

    Route::get('customer/browse','CustomerController@index');
    Route::post('customer/customerTable_ajax','CustomerController@customerTable_ajax');

    Route::get('quoteQueue/quoteandpost','QuoteQueueController@quoteandpost');
    Route::post('quoteQueue/quoteandpost_ajax','QuoteQueueController@quoteandpost_ajax');
    Route::post('quoteQueue/quoteandpost1_ajax','QuoteQueueController@quoteandpost1_ajax');

    Route::get('quoteQueue/quotepostDetailView/{postno}/{productno}/{ptype}','QuoteQueueController@quotepostDetailView');
    Route::post('quoteQueue/quoteDetailViewFunction','QuoteQueueController@quoteDetailViewFunction');
    Route::post('quoteQueue/fetchFunction','QuoteQueueController@fetchFunction');
    Route::post('quoteQueue/offerHistory','QuoteQueueController@offerHistory');
    Route::post('quoteQueue/activeSellPosting','QuoteQueueController@activeSellPosting');
    Route::post('quoteQueue/activeBuyPosting','QuoteQueueController@activeBuyPosting');
    Route::post('quoteQueue/historicalTransaction','QuoteQueueController@historicalTransaction');
    Route::post('quoteQueue/addofferbyqf','QuoteQueueController@addofferbyqf');

    Route::get('report','ReportController@index');
    Route::post('report/functions','ReportController@functions');
    Route::get('report/posting_pie_chart','ReportController@posting_pie_chart');


    Route::get('setting/mySetting','SettingController@mySetting');
    Route::post('setting/updateMySetting','SettingController@updateMySetting');    

    Route::get('setting/notificationSetting','SettingController@notificationSetting');
    Route::post('setting/notificationSetting_ajax','SettingController@notificationSetting_ajax');

    Route::get('setting/importData','SettingController@importData');

    Route::get('sample/{sample}','SettingController@sample');

    Route::get('setting/location','SettingController@location');
    Route::post('setting/location_ajax','SettingController@location_ajax');

    Route::get('setting/termsAndConditions','SettingController@termsAndConditions');
    Route::post('setting/termsAndConditions_ajax','SettingController@termsAndConditions_ajax');

    Route::get('setting/maskProductName','SettingController@maskProductName');
    Route::post('setting/maskProductName_ajax','SettingController@maskProductName_ajax');

    Route::get('setting/roleList','SettingController@roleList');
    Route::post('setting/roleList_ajax','SettingController@roleList_ajax');

    Route::get('setting/menuPermission','SettingController@menuPermission');
    Route::post('setting/menuPermission_ajax','SettingController@menuPermission_ajax');

    Route::post('setting/rolePermissionUpdate','SettingController@rolePermissionUpdate');
});
