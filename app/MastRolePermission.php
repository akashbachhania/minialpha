<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MastRolePermission extends Model
{
    protected $table = 'MastRolePermission';

    function mastpage(){
    	return $this->belongsTo('App\MastPage','PageId','PageId');
    }
}
