<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstShipping extends Model
{
    protected $table = 'mst_shipping';
    protected $primaryKey = 'shipid';
}
