<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MstSecurity extends Model
{
    protected $table = 'mst_security';
    protected $primaryKey = 'securityid';
}
