<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $table = 'mst_industry';
    protected $primaryKey = 'industryid';

    function category(){
    	return $this->hasMany('App\Category','industryid','industryid');
    }
    function brand(){
    	return $this->hasMany('App\Brand','industryid','industryid');
    }
}
