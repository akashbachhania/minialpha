-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2016 at 07:55 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `miniaplha`
--

-- --------------------------------------------------------

--
-- Table structure for table `countdata`
--

CREATE TABLE IF NOT EXISTS `countdata` (
  `id` int(11) NOT NULL,
  `recordcount` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`recordcount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `count_notification`
--

CREATE TABLE IF NOT EXISTS `count_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tmsg` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maskproname`
--

CREATE TABLE IF NOT EXISTS `maskproname` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `maskstatus` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mastpage`
--

CREATE TABLE IF NOT EXISTS `mastpage` (
  `PageId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PageName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DisplayName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Parent_Id` int(11) NOT NULL,
  `Level_Idx` int(11) NOT NULL,
  `Idx` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=48 ;

--
-- Dumping data for table `mastpage`
--

INSERT INTO `mastpage` (`PageId`, `PageName`, `Module`, `DisplayName`, `Parent_Id`, `Level_Idx`, `Idx`, `created_at`, `updated_at`) VALUES
(3, '', 'Customers', 'Customers', 0, 0, 0, '2016-02-18 00:15:04', '2016-02-18 00:15:04'),
(4, 'mstcustomer', 'Customers', 'Browse Customers', 3, 0, 1, '2016-02-18 00:15:05', '2016-02-18 00:15:05'),
(5, '', 'Product Catalog', 'Product Catalog', 0, 0, 0, '2016-02-18 00:15:05', '2016-02-18 00:15:05'),
(6, 'add-product', 'Product Catalog', 'Add New Product', 5, 0, 1, '2016-02-18 00:15:06', '2016-02-18 00:15:06'),
(7, 'product-catalog', 'Product Catalog', 'Browse Products', 5, 0, 2, '2016-02-18 00:15:06', '2016-02-18 00:15:06'),
(8, 'suggested-product', 'Product Catalog', 'Suggested Product', 5, 0, 3, '2016-02-18 00:15:06', '2016-02-18 00:15:06'),
(9, 'suggested-product-code', 'Product Catalog', 'Suggested Product Code', 5, 0, 4, '2016-02-18 00:15:06', '2016-02-18 00:15:06'),
(10, 'reported-product', 'Product Catalog', 'Reported Product', 5, 0, 5, '2016-02-18 00:15:06', '2016-02-18 00:15:06'),
(11, '', 'Postings', 'Postings', 0, 0, 0, '2016-02-18 00:15:06', '2016-02-18 00:15:06'),
(12, 'posting-catalog', 'Postings', 'Browse Postings', 11, 0, 1, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(13, 'my-posting', 'Postings', 'My Postings', 11, 0, 2, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(14, 'import-post', 'Postings', 'Import Postings', 11, 0, 4, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(15, '', 'Product List Download', 'Products List Download', 0, 0, 0, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(16, 'product-listdownload', 'Product List Download', 'Products List Download', 15, 0, 1, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(17, '', 'Quote Queue', 'Quote Queue', 0, 0, 0, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(18, 'quoteandpost_que', 'Quote Queue', 'Quote Queue', 17, 0, 1, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(19, '', 'Orders', 'Orders', 0, 0, 0, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(20, 'tm-accepted-offers', 'Orders', 'Accepted Offers', 19, 0, 1, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(21, 'tm-completed-offers', 'Orders', 'Completed Offers', 19, 0, 2, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(22, 'tm-cancelled-order', 'Orders', 'Cancelled Orders', 19, 0, 3, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(23, '', 'Settings', 'Settings', 0, 0, 0, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(24, 'my-setting', 'Settings', 'My Settings', 23, 0, 1, '2016-02-18 00:15:07', '2016-02-18 00:15:07'),
(25, 'notification-setting', 'Settings', 'Notifications', 23, 0, 2, '2016-02-18 00:15:08', '2016-02-18 00:15:08'),
(26, 'notification', 'Settings', 'Notifications Settings', 23, 0, 3, '2016-02-18 00:15:08', '2016-02-18 00:15:08'),
(27, 'import-data', 'Settings', 'Import Data', 23, 0, 4, '2016-02-18 00:15:08', '2016-02-18 00:15:08'),
(28, 'mstindustry', 'Settings', 'Industry List', 23, 0, 5, '2016-02-18 00:15:08', '2016-02-18 00:15:08'),
(29, 'mstcategory', 'Settings', 'Category List', 23, 0, 6, '2016-02-18 00:15:08', '2016-02-18 00:15:08'),
(30, 'mstsubcategory', 'Settings', 'Sub Category List', 23, 0, 7, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(31, 'mstbrand', 'Settings', 'Brands List', 23, 0, 8, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(32, 'mstlocation', 'Settings', 'Location', 23, 0, 9, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(34, 'mstsecurity-que', 'Settings', 'Security Question List', 23, 0, 11, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(35, 'mstterms-condition', 'Settings', 'Terms & Conditions', 23, 0, 12, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(36, 'databackup', 'Settings', 'Database Backup', 23, 0, 13, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(37, 'maskproductname', 'Settings', 'Website Settings', 23, 0, 15, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(38, 'menu_permission', 'Settings', 'Menu Permission', 23, 0, 16, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(39, '', 'Reports', 'Reports', 0, 0, 0, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(40, 'report', 'Reports', 'Reports', 39, 0, 1, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(41, '', 'Users', 'Users', 0, 0, 0, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(42, 'userdetails', 'Users', 'View Users', 41, 0, 1, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(43, 'activity', 'Users', 'Users Activity', 41, 0, 2, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(44, 'create', 'Users', 'Create New User', 41, 0, 3, '2016-02-18 00:15:09', '2016-02-18 00:15:09'),
(45, 'mstrole', 'Settings', 'Role List', 23, 0, 17, '2016-02-18 00:15:10', '2016-02-18 00:15:10'),
(46, 'databaserestore', 'Settings', 'Database Restore', 23, 0, 14, '2016-02-18 00:15:10', '2016-02-18 00:15:10'),
(47, 'my-offer', 'Postings', 'My Offers', 11, 0, 3, '2016-02-18 00:15:10', '2016-02-18 00:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `mastrolepermission`
--

CREATE TABLE IF NOT EXISTS `mastrolepermission` (
  `RoleId` int(11) NOT NULL,
  `PageId` int(11) NOT NULL,
  `ViewP` int(11) NOT NULL,
  `AddP` int(11) NOT NULL,
  `EditP` int(11) NOT NULL,
  `DeleteP` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`RoleId`,`PageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mastrolepermission`
--

INSERT INTO `mastrolepermission` (`RoleId`, `PageId`, `ViewP`, `AddP`, `EditP`, `DeleteP`, `pagePrint`, `pageExport`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, 1, 1, 1, 0, NULL, NULL),
(1, 6, 1, 1, 1, 1, 1, 1, NULL, NULL),
(1, 7, 1, 1, 1, 1, 1, 1, NULL, NULL),
(1, 12, 1, 1, 1, 0, 1, 0, NULL, NULL),
(1, 13, 1, 1, 1, 0, 0, 0, NULL, NULL),
(1, 14, 1, 1, 0, 0, 0, 0, NULL, NULL),
(1, 16, 1, 1, 1, 0, 1, 1, NULL, NULL),
(1, 24, 1, 1, 1, 0, 0, 0, NULL, NULL),
(1, 25, 1, 1, 1, 0, 0, 0, NULL, NULL),
(1, 27, 1, 1, 1, 0, 1, 0, NULL, NULL),
(1, 47, 1, 1, 1, 0, 0, 0, NULL, NULL),
(2, 18, 1, 1, 1, 0, 0, 0, NULL, NULL),
(2, 24, 1, 1, 1, 1, 0, 0, NULL, NULL),
(2, 25, 1, 1, 1, 1, 0, 0, NULL, NULL),
(3, 20, 1, 1, 1, 1, 0, 0, NULL, NULL),
(3, 21, 1, 1, 1, 1, 0, 0, NULL, NULL),
(3, 22, 1, 1, 1, 1, 0, 0, NULL, NULL),
(3, 24, 1, 1, 1, 1, 0, 0, NULL, NULL),
(3, 25, 1, 1, 1, 1, 0, 0, NULL, NULL),
(4, 4, 1, 1, 1, 1, 0, 0, NULL, NULL),
(4, 6, 1, 1, 1, 1, 1, 0, NULL, NULL),
(4, 7, 1, 1, 1, 1, 1, 0, NULL, NULL),
(4, 8, 1, 1, 1, 1, 1, 0, NULL, NULL),
(4, 9, 1, 1, 1, 1, 1, 0, NULL, NULL),
(4, 10, 1, 1, 1, 1, 1, 0, NULL, NULL),
(4, 12, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 13, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 14, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 16, 1, 0, 0, 0, 0, 0, NULL, NULL),
(4, 18, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 20, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 21, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 22, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 24, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 25, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 26, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 27, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 28, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 29, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 30, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 31, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 32, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 34, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 35, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 36, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 37, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 38, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 40, 1, 0, 0, 0, 0, 0, NULL, NULL),
(4, 42, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 43, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 44, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 45, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 46, 1, 1, 1, 0, 1, 0, NULL, NULL),
(4, 47, 1, 1, 1, 0, 1, 0, NULL, NULL),
(5, 4, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 6, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 7, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 8, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 9, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 10, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 12, 1, 0, 1, 1, 0, 0, NULL, NULL),
(5, 13, 1, 1, 1, 0, 0, 0, NULL, NULL),
(5, 14, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 16, 1, 1, 1, 1, 1, 1, NULL, NULL),
(5, 18, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 20, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 21, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 22, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 24, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 25, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 27, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 28, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 29, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 30, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 31, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 32, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 34, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 35, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 36, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 37, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 38, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 40, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 42, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 43, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 44, 1, 1, 1, 1, 0, 0, NULL, NULL),
(5, 45, 1, 0, 0, 0, 0, 0, NULL, NULL),
(5, 46, 1, 1, 0, 0, 0, 0, NULL, NULL),
(6, 4, 1, 0, 0, 0, 0, 0, NULL, NULL),
(14, 20, 1, 0, 0, 0, 0, 0, NULL, NULL),
(14, 21, 0, 0, 1, 0, 0, 0, NULL, NULL),
(14, 22, 0, 1, 0, 1, 0, 0, NULL, NULL),
(14, 24, 0, 1, 0, 0, 0, 0, NULL, NULL),
(14, 25, 0, 0, 1, 0, 0, 0, NULL, NULL),
(14, 26, 0, 0, 0, 1, 0, 0, NULL, NULL),
(14, 27, 0, 0, 0, 1, 0, 0, NULL, NULL),
(14, 28, 0, 0, 1, 0, 0, 0, NULL, NULL),
(14, 29, 0, 1, 0, 0, 0, 0, NULL, NULL),
(14, 31, 1, 0, 0, 0, 0, 0, NULL, NULL),
(14, 32, 0, 1, 0, 0, 0, 0, NULL, NULL),
(14, 36, 0, 0, 1, 0, 0, 0, NULL, NULL),
(14, 38, 1, 0, 0, 0, 0, 0, NULL, NULL),
(15, 4, 1, 1, 0, 0, 0, 0, NULL, NULL),
(15, 6, 1, 1, 1, 0, 0, 0, NULL, NULL),
(15, 7, 1, 1, 1, 0, 0, 0, NULL, NULL),
(17, 4, 1, 1, 1, 0, 0, 0, NULL, NULL),
(18, 4, 1, 1, 1, 1, 0, 0, NULL, NULL),
(18, 6, 1, 1, 0, 0, 0, 0, NULL, NULL),
(18, 7, 1, 1, 0, 0, 0, 0, NULL, NULL),
(18, 12, 1, 1, 0, 0, 0, 0, NULL, NULL),
(18, 13, 1, 1, 0, 0, 0, 0, NULL, NULL),
(18, 14, 1, 1, 0, 0, 0, 0, NULL, NULL),
(18, 24, 1, 0, 0, 0, 0, 0, NULL, NULL),
(18, 25, 1, 0, 0, 0, 0, 0, NULL, NULL),
(18, 47, 1, 1, 0, 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_permission`
--

CREATE TABLE IF NOT EXISTS `menu_permission` (
  `rowId` int(11) NOT NULL,
  `pageView` int(11) NOT NULL,
  `pageAdd` int(11) NOT NULL,
  `pageEdit` int(11) NOT NULL,
  `pageDelete` int(11) NOT NULL,
  `pagePrint` int(11) NOT NULL,
  `pageExport` int(11) NOT NULL,
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_09_090421_countdata', 1),
('2016_02_09_090448_count_nitification', 1),
('2016_02_09_092454_maskproname', 1),
('2016_02_09_092506_MastPage', 1),
('2016_02_09_092525_MastRolePermission', 1),
('2016_02_09_092544_menu_permission', 1),
('2016_02_09_092618_mst_admin_report', 1),
('2016_02_09_092641_mst_brand', 1),
('2016_02_09_092652_mst_category', 1),
('2016_02_09_092700_mst_country', 1),
('2016_02_09_092726_mst_currency', 1),
('2016_02_09_092746_mst_exp_daterange', 1),
('2016_02_09_092807_mst_industry', 1),
('2016_02_09_093639_mst_language', 1),
('2016_02_09_093701_mst_location', 1),
('2016_02_09_093731_mst_mngnotification', 1),
('2016_02_09_093741_mst_model', 1),
('2016_02_09_093749_mst_notification', 1),
('2016_02_09_093807_mst_pageName', 1),
('2016_02_09_093821_mst_product', 1),
('2016_02_09_093833_mst_productcode', 1),
('2016_02_09_115416_mst_productimages', 1),
('2016_02_09_115442_mst_productprefix', 1),
('2016_02_09_115451_mst_productreport', 1),
('2016_02_09_115505_mst_productsimilar', 1),
('2016_02_09_115517_mst_report', 1),
('2016_02_09_115601_mst_security', 1),
('2016_02_09_115625_mst_shipping', 1),
('2016_02_09_115651_mst_subcategory', 1),
('2016_02_09_115708_mst_timeframe', 1),
('2016_02_09_115738_mst_timezone', 1),
('2016_02_09_115747_mst_uom', 1),
('2016_02_09_115800_mst_useraction', 1),
('2016_02_09_115808_mst_userlogin', 1),
('2016_02_09_115817_mst_userlogon', 1),
('2016_02_09_115826_mst_userrole', 1),
('2016_02_09_115907_old_useractivitypurge', 1),
('2016_02_09_115948_page_master', 1),
('2016_02_09_115958_page_notification', 1),
('2016_02_09_120017_post_advertisment', 1),
('2016_02_09_120037_post_customer', 1),
('2016_02_09_144025_post_quotation', 1),
('2016_02_09_144047_productimport', 1),
('2016_02_09_144115_quotation_counter', 1),
('2016_02_09_144152_test', 1),
('2016_02_09_144157_test1', 1),
('2016_02_09_144213_tm_accepted_offers', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_admin_report`
--

CREATE TABLE IF NOT EXISTS `mst_admin_report` (
  `reportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reportsql` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_brand`
--

CREATE TABLE IF NOT EXISTS `mst_brand` (
  `brandid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `industryid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`brandid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_category`
--

CREATE TABLE IF NOT EXISTS `mst_category` (
  `catid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `industryid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_country`
--

CREATE TABLE IF NOT EXISTS `mst_country` (
  `countryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`countryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_currency`
--

CREATE TABLE IF NOT EXISTS `mst_currency` (
  `currencyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`currencyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_exp_daterange`
--

CREATE TABLE IF NOT EXISTS `mst_exp_daterange` (
  `exprangeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`exprangeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_industry`
--

CREATE TABLE IF NOT EXISTS `mst_industry` (
  `industryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`industryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_language`
--

CREATE TABLE IF NOT EXISTS `mst_language` (
  `languageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`languageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_location`
--

CREATE TABLE IF NOT EXISTS `mst_location` (
  `locationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`locationid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_mngnotification`
--

CREATE TABLE IF NOT EXISTS `mst_mngnotification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagename` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_model`
--

CREATE TABLE IF NOT EXISTS `mst_model` (
  `modelid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`modelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_notification`
--

CREATE TABLE IF NOT EXISTS `mst_notification` (
  `notiid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `msgdate` datetime NOT NULL,
  `msg` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fromuserid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `preportid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notiid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_pagename`
--

CREATE TABLE IF NOT EXISTS `mst_pagename` (
  `rowid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pageCheckAdd` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckEdit` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckDelete` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckPrint` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `pageCheckExport` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `userid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_product`
--

CREATE TABLE IF NOT EXISTS `mst_product` (
  `productid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `brandid` int(11) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `subcatid` int(11) DEFAULT NULL,
  `pakaging` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipingcondition` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qtypercase` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `manufacture` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpm` int(11) DEFAULT NULL,
  `addedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `roletype` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img0` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img3` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img4` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img5` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img6` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img7` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code1` int(11) NOT NULL,
  `codevalue1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code2` int(11) NOT NULL,
  `codevalue2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code3` int(11) DEFAULT NULL,
  `codevalue3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productcode`
--

CREATE TABLE IF NOT EXISTS `mst_productcode` (
  `productcodeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `prefixid` int(11) NOT NULL,
  `refbyid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `approveid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `productcode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `refdate` datetime NOT NULL,
  `approvedate` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productcodeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productimages`
--

CREATE TABLE IF NOT EXISTS `mst_productimages` (
  `productimageid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `imageurl` text COLLATE utf8_unicode_ci NOT NULL,
  `srno` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productimageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productprefix`
--

CREATE TABLE IF NOT EXISTS `mst_productprefix` (
  `proprefixid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`proprefixid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productreport`
--

CREATE TABLE IF NOT EXISTS `mst_productreport` (
  `preportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `reviewbyid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `reviewdate` date NOT NULL,
  `reviewremark` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `reviewstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approvedby` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `approveddate` date NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`preportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_productsimilar`
--

CREATE TABLE IF NOT EXISTS `mst_productsimilar` (
  `similarid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `spid` int(11) NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`similarid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_report`
--

CREATE TABLE IF NOT EXISTS `mst_report` (
  `reportid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_security`
--

CREATE TABLE IF NOT EXISTS `mst_security` (
  `securityid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qtype` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`securityid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_shipping`
--

CREATE TABLE IF NOT EXISTS `mst_shipping` (
  `shipid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`shipid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_subcategory`
--

CREATE TABLE IF NOT EXISTS `mst_subcategory` (
  `subcatid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `industryid` int(11) NOT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subcatid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_timeframe`
--

CREATE TABLE IF NOT EXISTS `mst_timeframe` (
  `timeframeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`timeframeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_timezone`
--

CREATE TABLE IF NOT EXISTS `mst_timezone` (
  `timezoneid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `timezonevalue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`timezoneid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=417 ;

--
-- Dumping data for table `mst_timezone`
--

INSERT INTO `mst_timezone` (`timezoneid`, `name`, `isactive`, `timezonevalue`, `created_at`, `updated_at`) VALUES
(1, 'Africa/Abidjan', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:34', '2016-02-18 00:14:34'),
(2, 'Africa/Accra', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:34', '2016-02-18 00:14:34'),
(3, 'Africa/Addis_Ababa', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:34', '2016-02-18 03:14:34'),
(4, 'Africa/Algiers', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:34', '2016-02-18 01:14:34'),
(5, 'Africa/Asmara', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:34', '2016-02-18 03:14:34'),
(6, 'Africa/Bamako', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:34', '2016-02-18 00:14:34'),
(7, 'Africa/Bangui', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:34', '2016-02-18 01:14:34'),
(8, 'Africa/Banjul', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:34', '2016-02-18 00:14:34'),
(9, 'Africa/Bissau', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:34', '2016-02-18 00:14:34'),
(10, 'Africa/Blantyre', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:34', '2016-02-18 02:14:34'),
(11, 'Africa/Brazzaville', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:34', '2016-02-18 01:14:34'),
(12, 'Africa/Bujumbura', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:34', '2016-02-18 02:14:34'),
(13, 'Africa/Cairo', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:34', '2016-02-18 02:14:34'),
(14, 'Africa/Casablanca', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:34', '2016-02-18 00:14:34'),
(15, 'Africa/Ceuta', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:35', '2016-02-18 01:14:35'),
(16, 'Africa/Conakry', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:35', '2016-02-18 00:14:35'),
(17, 'Africa/Dakar', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:35', '2016-02-18 00:14:35'),
(18, 'Africa/Dar_es_Salaam', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:35', '2016-02-18 03:14:35'),
(19, 'Africa/Djibouti', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:35', '2016-02-18 03:14:35'),
(20, 'Africa/Douala', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:35', '2016-02-18 01:14:35'),
(21, 'Africa/El_Aaiun', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:35', '2016-02-18 00:14:35'),
(22, 'Africa/Freetown', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:35', '2016-02-18 00:14:35'),
(23, 'Africa/Gaborone', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:35', '2016-02-18 02:14:35'),
(24, 'Africa/Harare', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:35', '2016-02-18 02:14:35'),
(25, 'Africa/Johannesburg', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:35', '2016-02-18 02:14:35'),
(26, 'Africa/Juba', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:35', '2016-02-18 03:14:35'),
(27, 'Africa/Kampala', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:35', '2016-02-18 03:14:35'),
(28, 'Africa/Khartoum', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:35', '2016-02-18 03:14:35'),
(29, 'Africa/Kigali', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:35', '2016-02-18 02:14:35'),
(30, 'Africa/Kinshasa', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:35', '2016-02-18 01:14:35'),
(31, 'Africa/Lagos', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:35', '2016-02-18 01:14:35'),
(32, 'Africa/Libreville', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:35', '2016-02-18 01:14:35'),
(33, 'Africa/Lome', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:35', '2016-02-18 00:14:35'),
(34, 'Africa/Luanda', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:35', '2016-02-18 01:14:35'),
(35, 'Africa/Lubumbashi', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:35', '2016-02-18 02:14:35'),
(36, 'Africa/Lusaka', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:35', '2016-02-18 02:14:35'),
(37, 'Africa/Malabo', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:36', '2016-02-18 01:14:36'),
(38, 'Africa/Maputo', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:36', '2016-02-18 02:14:36'),
(39, 'Africa/Maseru', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:36', '2016-02-18 02:14:36'),
(40, 'Africa/Mbabane', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:36', '2016-02-18 02:14:36'),
(41, 'Africa/Mogadishu', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:36', '2016-02-18 03:14:36'),
(42, 'Africa/Monrovia', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:36', '2016-02-18 00:14:36'),
(43, 'Africa/Nairobi', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:36', '2016-02-18 03:14:36'),
(44, 'Africa/Ndjamena', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:36', '2016-02-18 01:14:36'),
(45, 'Africa/Niamey', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:36', '2016-02-18 01:14:36'),
(46, 'Africa/Nouakchott', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:36', '2016-02-18 00:14:36'),
(47, 'Africa/Ouagadougou', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:36', '2016-02-18 00:14:36'),
(48, 'Africa/Porto-Novo', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:36', '2016-02-18 01:14:36'),
(49, 'Africa/Sao_Tome', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:36', '2016-02-18 00:14:36'),
(50, 'Africa/Tripoli', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:36', '2016-02-18 02:14:36'),
(51, 'Africa/Tunis', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:36', '2016-02-18 01:14:36'),
(52, 'Africa/Windhoek', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:36', '2016-02-18 02:14:36'),
(53, 'America/Adak', '1', 'UTC/GMT -10:00', '2016-02-17 02:14:36', '2016-02-17 02:14:36'),
(54, 'America/Anchorage', '1', 'UTC/GMT -09:00', '2016-02-17 03:14:36', '2016-02-17 03:14:36'),
(55, 'America/Anguilla', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:36', '2016-02-17 20:14:36'),
(56, 'America/Antigua', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:36', '2016-02-17 20:14:36'),
(57, 'America/Araguaina', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:36', '2016-02-17 21:14:36'),
(58, 'America/Argentina/Buenos_Aires', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(59, 'America/Argentina/Catamarca', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(60, 'America/Argentina/Cordoba', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(61, 'America/Argentina/Jujuy', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(62, 'America/Argentina/La_Rioja', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(63, 'America/Argentina/Mendoza', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(64, 'America/Argentina/Rio_Gallegos', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(65, 'America/Argentina/Salta', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(66, 'America/Argentina/San_Juan', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(67, 'America/Argentina/San_Luis', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(68, 'America/Argentina/Tucuman', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(69, 'America/Argentina/Ushuaia', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(70, 'America/Aruba', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:37', '2016-02-17 20:14:37'),
(71, 'America/Asuncion', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(72, 'America/Atikokan', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:37', '2016-02-18 07:14:37'),
(73, 'America/Bahia', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(74, 'America/Bahia_Banderas', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:37', '2016-02-17 06:14:37'),
(75, 'America/Barbados', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:37', '2016-02-17 20:14:37'),
(76, 'America/Belem', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:37', '2016-02-17 21:14:37'),
(77, 'America/Belize', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:37', '2016-02-17 06:14:37'),
(78, 'America/Blanc-Sablon', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:37', '2016-02-17 20:14:37'),
(79, 'America/Boa_Vista', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:37', '2016-02-17 20:14:37'),
(80, 'America/Bogota', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:37', '2016-02-18 07:14:37'),
(81, 'America/Boise', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:37', '2016-02-17 05:14:37'),
(82, 'America/Cambridge_Bay', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:38', '2016-02-17 05:14:38'),
(83, 'America/Campo_Grande', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:38', '2016-02-17 21:14:38'),
(84, 'America/Cancun', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:38', '2016-02-17 06:14:38'),
(85, 'America/Caracas', '1', 'UTC/GMT -04:30', '2016-02-17 19:44:38', '2016-02-17 19:44:38'),
(86, 'America/Cayenne', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:38', '2016-02-17 21:14:38'),
(87, 'America/Cayman', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:38', '2016-02-18 07:14:38'),
(88, 'America/Chicago', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:38', '2016-02-17 06:14:38'),
(89, 'America/Chihuahua', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:38', '2016-02-17 05:14:38'),
(90, 'America/Costa_Rica', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:38', '2016-02-17 06:14:38'),
(91, 'America/Creston', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:38', '2016-02-17 05:14:38'),
(92, 'America/Cuiaba', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:38', '2016-02-17 21:14:38'),
(93, 'America/Curacao', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:38', '2016-02-17 20:14:38'),
(94, 'America/Danmarkshavn', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:38', '2016-02-18 00:14:38'),
(95, 'America/Dawson', '1', 'UTC/GMT -08:00', '2016-02-17 04:14:38', '2016-02-17 04:14:38'),
(96, 'America/Dawson_Creek', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:38', '2016-02-17 05:14:38'),
(97, 'America/Denver', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:38', '2016-02-17 05:14:38'),
(98, 'America/Detroit', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:38', '2016-02-18 07:14:38'),
(99, 'America/Dominica', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:38', '2016-02-17 20:14:38'),
(100, 'America/Edmonton', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:38', '2016-02-17 05:14:38'),
(101, 'America/Eirunepe', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:38', '2016-02-18 07:14:38'),
(102, 'America/El_Salvador', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:39', '2016-02-17 06:14:39'),
(103, 'America/Fortaleza', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:39', '2016-02-17 21:14:39'),
(104, 'America/Glace_Bay', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:39', '2016-02-17 20:14:39'),
(105, 'America/Godthab', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:39', '2016-02-17 21:14:39'),
(106, 'America/Goose_Bay', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:39', '2016-02-17 20:14:39'),
(107, 'America/Grand_Turk', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(108, 'America/Grenada', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:39', '2016-02-17 20:14:39'),
(109, 'America/Guadeloupe', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:39', '2016-02-17 20:14:39'),
(110, 'America/Guatemala', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:39', '2016-02-17 06:14:39'),
(111, 'America/Guayaquil', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(112, 'America/Guyana', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:39', '2016-02-17 20:14:39'),
(113, 'America/Halifax', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:39', '2016-02-17 20:14:39'),
(114, 'America/Havana', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(115, 'America/Hermosillo', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:39', '2016-02-17 05:14:39'),
(116, 'America/Indiana/Indianapolis', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(117, 'America/Indiana/Knox', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:39', '2016-02-17 06:14:39'),
(118, 'America/Indiana/Marengo', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(119, 'America/Indiana/Petersburg', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(120, 'America/Indiana/Tell_City', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:39', '2016-02-17 06:14:39'),
(121, 'America/Indiana/Vevay', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(122, 'America/Indiana/Vincennes', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(123, 'America/Indiana/Winamac', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(124, 'America/Inuvik', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:39', '2016-02-17 05:14:39'),
(125, 'America/Iqaluit', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(126, 'America/Jamaica', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(127, 'America/Juneau', '1', 'UTC/GMT -09:00', '2016-02-17 03:14:39', '2016-02-17 03:14:39'),
(128, 'America/Kentucky/Louisville', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(129, 'America/Kentucky/Monticello', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:39', '2016-02-18 07:14:39'),
(130, 'America/Kralendijk', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(131, 'America/La_Paz', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(132, 'America/Lima', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:40', '2016-02-18 07:14:40'),
(133, 'America/Los_Angeles', '1', 'UTC/GMT -08:00', '2016-02-17 04:14:40', '2016-02-17 04:14:40'),
(134, 'America/Lower_Princes', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(135, 'America/Maceio', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:40', '2016-02-17 21:14:40'),
(136, 'America/Managua', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:40', '2016-02-17 06:14:40'),
(137, 'America/Manaus', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(138, 'America/Marigot', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(139, 'America/Martinique', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(140, 'America/Matamoros', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:40', '2016-02-17 06:14:40'),
(141, 'America/Mazatlan', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:40', '2016-02-17 05:14:40'),
(142, 'America/Menominee', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:40', '2016-02-17 06:14:40'),
(143, 'America/Merida', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:40', '2016-02-17 06:14:40'),
(144, 'America/Metlakatla', '1', 'UTC/GMT -08:00', '2016-02-17 04:14:40', '2016-02-17 04:14:40'),
(145, 'America/Mexico_City', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:40', '2016-02-17 06:14:40'),
(146, 'America/Miquelon', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:40', '2016-02-17 21:14:40'),
(147, 'America/Moncton', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(148, 'America/Monterrey', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:40', '2016-02-17 06:14:40'),
(149, 'America/Montevideo', '1', 'UTC/GMT -02:00', '2016-02-17 22:14:40', '2016-02-17 22:14:40'),
(150, 'America/Montserrat', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:40', '2016-02-17 20:14:40'),
(151, 'America/Nassau', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:40', '2016-02-18 07:14:40'),
(152, 'America/New_York', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:40', '2016-02-18 07:14:40'),
(153, 'America/Nipigon', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:40', '2016-02-18 07:14:40'),
(154, 'America/Nome', '1', 'UTC/GMT -09:00', '2016-02-17 03:14:40', '2016-02-17 03:14:40'),
(155, 'America/Noronha', '1', 'UTC/GMT -02:00', '2016-02-17 22:14:41', '2016-02-17 22:14:41'),
(156, 'America/North_Dakota/Beulah', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:41', '2016-02-17 06:14:41'),
(157, 'America/North_Dakota/Center', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:41', '2016-02-17 06:14:41'),
(158, 'America/North_Dakota/New_Salem', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:41', '2016-02-17 06:14:41'),
(159, 'America/Ojinaga', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:41', '2016-02-17 05:14:41'),
(160, 'America/Panama', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:41', '2016-02-18 07:14:41'),
(161, 'America/Pangnirtung', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:41', '2016-02-18 07:14:41'),
(162, 'America/Paramaribo', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:41', '2016-02-17 21:14:41'),
(163, 'America/Phoenix', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:41', '2016-02-17 05:14:41'),
(164, 'America/Port-au-Prince', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:41', '2016-02-18 07:14:41'),
(165, 'America/Port_of_Spain', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:41', '2016-02-17 20:14:41'),
(166, 'America/Porto_Velho', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:41', '2016-02-17 20:14:41'),
(167, 'America/Puerto_Rico', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:41', '2016-02-17 20:14:41'),
(168, 'America/Rainy_River', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:41', '2016-02-17 06:14:41'),
(169, 'America/Rankin_Inlet', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:41', '2016-02-17 06:14:41'),
(170, 'America/Recife', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:41', '2016-02-17 21:14:41'),
(171, 'America/Regina', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:41', '2016-02-17 06:14:41'),
(172, 'America/Resolute', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:41', '2016-02-17 06:14:41'),
(173, 'America/Rio_Branco', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:41', '2016-02-18 07:14:41'),
(174, 'America/Santa_Isabel', '1', 'UTC/GMT -08:00', '2016-02-17 04:14:41', '2016-02-17 04:14:41'),
(175, 'America/Santarem', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:41', '2016-02-17 21:14:41'),
(176, 'America/Santiago', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:41', '2016-02-17 21:14:41'),
(177, 'America/Santo_Domingo', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:41', '2016-02-17 20:14:41'),
(178, 'America/Sao_Paulo', '1', 'UTC/GMT -02:00', '2016-02-17 22:14:41', '2016-02-17 22:14:41'),
(179, 'America/Scoresbysund', '1', 'UTC/GMT -01:00', '2016-02-17 23:14:41', '2016-02-17 23:14:41'),
(180, 'America/Sitka', '1', 'UTC/GMT -09:00', '2016-02-17 03:14:41', '2016-02-17 03:14:41'),
(181, 'America/St_Barthelemy', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:41', '2016-02-17 20:14:41'),
(182, 'America/St_Johns', '1', 'UTC/GMT -03:30', '2016-02-17 20:44:41', '2016-02-17 20:44:41'),
(183, 'America/St_Kitts', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:41', '2016-02-17 20:14:41'),
(184, 'America/St_Lucia', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:42', '2016-02-17 20:14:42'),
(185, 'America/St_Thomas', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:42', '2016-02-17 20:14:42'),
(186, 'America/St_Vincent', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:42', '2016-02-17 20:14:42'),
(187, 'America/Swift_Current', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:42', '2016-02-17 06:14:42'),
(188, 'America/Tegucigalpa', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:42', '2016-02-17 06:14:42'),
(189, 'America/Thule', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:42', '2016-02-17 20:14:42'),
(190, 'America/Thunder_Bay', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:42', '2016-02-18 07:14:42'),
(191, 'America/Tijuana', '1', 'UTC/GMT -08:00', '2016-02-17 04:14:42', '2016-02-17 04:14:42'),
(192, 'America/Toronto', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:42', '2016-02-18 07:14:42'),
(193, 'America/Tortola', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:42', '2016-02-17 20:14:42'),
(194, 'America/Vancouver', '1', 'UTC/GMT -08:00', '2016-02-17 04:14:42', '2016-02-17 04:14:42'),
(195, 'America/Whitehorse', '1', 'UTC/GMT -08:00', '2016-02-17 04:14:42', '2016-02-17 04:14:42'),
(196, 'America/Winnipeg', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:42', '2016-02-17 06:14:42'),
(197, 'America/Yakutat', '1', 'UTC/GMT -09:00', '2016-02-17 03:14:42', '2016-02-17 03:14:42'),
(198, 'America/Yellowknife', '1', 'UTC/GMT -07:00', '2016-02-17 05:14:42', '2016-02-17 05:14:42'),
(199, 'Antarctica/Casey', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:42', '2016-02-17 20:14:42'),
(200, 'Antarctica/Davis', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:42', '2016-02-18 07:14:42'),
(201, 'Antarctica/DumontDUrville', '1', 'UTC/GMT +10:00', '2016-02-17 22:14:42', '2016-02-17 22:14:42'),
(202, 'Antarctica/Macquarie', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:42', '2016-02-17 23:14:42'),
(203, 'Antarctica/Mawson', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:42', '2016-02-18 05:14:42'),
(204, 'Antarctica/McMurdo', '1', 'UTC/GMT +13:00', '2016-02-18 01:14:42', '2016-02-18 01:14:42'),
(205, 'Antarctica/Palmer', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:42', '2016-02-17 21:14:42'),
(206, 'Antarctica/Rothera', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:42', '2016-02-17 21:14:42'),
(207, 'Antarctica/Syowa', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:42', '2016-02-18 03:14:42'),
(208, 'Antarctica/Vostok', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:42', '2016-02-18 06:14:42'),
(209, 'Arctic/Longyearbyen', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:42', '2016-02-18 01:14:42'),
(210, 'Asia/Aden', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:42', '2016-02-18 03:14:42'),
(211, 'Asia/Almaty', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:43', '2016-02-18 06:14:43'),
(212, 'Asia/Amman', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:43', '2016-02-18 02:14:43'),
(213, 'Asia/Anadyr', '1', 'UTC/GMT +12:00', '2016-02-18 00:14:43', '2016-02-18 00:14:43'),
(214, 'Asia/Aqtau', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:43', '2016-02-18 05:14:43'),
(215, 'Asia/Aqtobe', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:43', '2016-02-18 05:14:43'),
(216, 'Asia/Ashgabat', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:43', '2016-02-18 05:14:43'),
(217, 'Asia/Baghdad', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:43', '2016-02-18 03:14:43'),
(218, 'Asia/Bahrain', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:43', '2016-02-18 03:14:43'),
(219, 'Asia/Baku', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:43', '2016-02-18 04:14:43'),
(220, 'Asia/Bangkok', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:43', '2016-02-18 07:14:43'),
(221, 'Asia/Beirut', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:43', '2016-02-18 02:14:43'),
(222, 'Asia/Bishkek', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:43', '2016-02-18 06:14:43'),
(223, 'Asia/Brunei', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:43', '2016-02-17 20:14:43'),
(224, 'Asia/Choibalsan', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:43', '2016-02-17 20:14:43'),
(225, 'Asia/Chongqing', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:43', '2016-02-17 20:14:43'),
(226, 'Asia/Colombo', '1', 'UTC/GMT +05:30', '2016-02-18 05:44:43', '2016-02-18 05:44:43'),
(227, 'Asia/Damascus', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:43', '2016-02-18 02:14:43'),
(228, 'Asia/Dhaka', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:43', '2016-02-18 06:14:43'),
(229, 'Asia/Dili', '1', 'UTC/GMT +09:00', '2016-02-17 21:14:43', '2016-02-17 21:14:43'),
(230, 'Asia/Dubai', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:43', '2016-02-18 04:14:43'),
(231, 'Asia/Dushanbe', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:43', '2016-02-18 05:14:43'),
(232, 'Asia/Gaza', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:43', '2016-02-18 02:14:43'),
(233, 'Asia/Harbin', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:43', '2016-02-17 20:14:43'),
(234, 'Asia/Hebron', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:43', '2016-02-18 02:14:43'),
(235, 'Asia/Ho_Chi_Minh', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:43', '2016-02-18 07:14:43'),
(236, 'Asia/Hong_Kong', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:43', '2016-02-17 20:14:43'),
(237, 'Asia/Hovd', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:43', '2016-02-18 07:14:43'),
(238, 'Asia/Irkutsk', '1', 'UTC/GMT +09:00', '2016-02-17 21:14:43', '2016-02-17 21:14:43'),
(239, 'Asia/Jakarta', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:44', '2016-02-18 07:14:44'),
(240, 'Asia/Jayapura', '1', 'UTC/GMT +09:00', '2016-02-17 21:14:44', '2016-02-17 21:14:44'),
(241, 'Asia/Jerusalem', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:44', '2016-02-18 02:14:44'),
(242, 'Asia/Kabul', '1', 'UTC/GMT +04:30', '2016-02-18 04:44:44', '2016-02-18 04:44:44'),
(243, 'Asia/Kamchatka', '1', 'UTC/GMT +12:00', '2016-02-18 00:14:44', '2016-02-18 00:14:44'),
(244, 'Asia/Karachi', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:44', '2016-02-18 05:14:44'),
(245, 'Asia/Kashgar', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:44', '2016-02-17 20:14:44'),
(246, 'Asia/Kathmandu', '1', 'UTC/GMT +05:45', '2016-02-18 05:59:44', '2016-02-18 05:59:44'),
(247, 'Asia/Khandyga', '1', 'UTC/GMT +10:00', '2016-02-17 22:14:44', '2016-02-17 22:14:44'),
(248, 'Asia/Kolkata', '1', 'UTC/GMT +05:30', '2016-02-18 05:44:44', '2016-02-18 05:44:44'),
(249, 'Asia/Krasnoyarsk', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:44', '2016-02-17 20:14:44'),
(250, 'Asia/Kuala_Lumpur', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:44', '2016-02-17 20:14:44'),
(251, 'Asia/Kuching', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:44', '2016-02-17 20:14:44'),
(252, 'Asia/Kuwait', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:44', '2016-02-18 03:14:44'),
(253, 'Asia/Macau', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:44', '2016-02-17 20:14:44'),
(254, 'Asia/Magadan', '1', 'UTC/GMT +12:00', '2016-02-18 00:14:44', '2016-02-18 00:14:44'),
(255, 'Asia/Makassar', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:44', '2016-02-17 20:14:44'),
(256, 'Asia/Manila', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:44', '2016-02-17 20:14:44'),
(257, 'Asia/Muscat', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:44', '2016-02-18 04:14:44'),
(258, 'Asia/Nicosia', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:44', '2016-02-18 02:14:44'),
(259, 'Asia/Novokuznetsk', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:45', '2016-02-18 07:14:45'),
(260, 'Asia/Novosibirsk', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:45', '2016-02-18 07:14:45'),
(261, 'Asia/Omsk', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:45', '2016-02-18 07:14:45'),
(262, 'Asia/Oral', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:45', '2016-02-18 05:14:45'),
(263, 'Asia/Phnom_Penh', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:45', '2016-02-18 07:14:45'),
(264, 'Asia/Pontianak', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:45', '2016-02-18 07:14:45'),
(265, 'Asia/Pyongyang', '1', 'UTC/GMT +09:00', '2016-02-17 21:14:45', '2016-02-17 21:14:45'),
(266, 'Asia/Qatar', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:46', '2016-02-18 03:14:46'),
(267, 'Asia/Qyzylorda', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:46', '2016-02-18 06:14:46'),
(268, 'Asia/Rangoon', '1', 'UTC/GMT +06:30', '2016-02-18 06:44:46', '2016-02-18 06:44:46'),
(269, 'Asia/Riyadh', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:46', '2016-02-18 03:14:46'),
(270, 'Asia/Sakhalin', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:46', '2016-02-17 23:14:46'),
(271, 'Asia/Samarkand', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:46', '2016-02-18 05:14:46'),
(272, 'Asia/Seoul', '1', 'UTC/GMT +09:00', '2016-02-17 21:14:46', '2016-02-17 21:14:46'),
(273, 'Asia/Shanghai', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:46', '2016-02-17 20:14:46'),
(274, 'Asia/Singapore', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:47', '2016-02-17 20:14:47'),
(275, 'Asia/Taipei', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:47', '2016-02-17 20:14:47'),
(276, 'Asia/Tashkent', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:47', '2016-02-18 05:14:47'),
(277, 'Asia/Tbilisi', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:47', '2016-02-18 04:14:47'),
(278, 'Asia/Tehran', '1', 'UTC/GMT +03:30', '2016-02-18 03:44:47', '2016-02-18 03:44:47'),
(279, 'Asia/Thimphu', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:47', '2016-02-18 06:14:47'),
(280, 'Asia/Tokyo', '1', 'UTC/GMT +09:00', '2016-02-17 21:14:47', '2016-02-17 21:14:47'),
(281, 'Asia/Ulaanbaatar', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:48', '2016-02-17 20:14:48'),
(282, 'Asia/Urumqi', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:48', '2016-02-17 20:14:48'),
(283, 'Asia/Ust-Nera', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:48', '2016-02-17 23:14:48'),
(284, 'Asia/Vientiane', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:48', '2016-02-18 07:14:48'),
(285, 'Asia/Vladivostok', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:48', '2016-02-17 23:14:48'),
(286, 'Asia/Yakutsk', '1', 'UTC/GMT +10:00', '2016-02-17 22:14:48', '2016-02-17 22:14:48'),
(287, 'Asia/Yekaterinburg', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:48', '2016-02-18 06:14:48'),
(288, 'Asia/Yerevan', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:48', '2016-02-18 04:14:48'),
(289, 'Atlantic/Azores', '1', 'UTC/GMT -01:00', '2016-02-17 23:14:49', '2016-02-17 23:14:49'),
(290, 'Atlantic/Bermuda', '1', 'UTC/GMT -04:00', '2016-02-17 20:14:49', '2016-02-17 20:14:49'),
(291, 'Atlantic/Canary', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:49', '2016-02-18 00:14:49'),
(292, 'Atlantic/Cape_Verde', '1', 'UTC/GMT -01:00', '2016-02-17 23:14:49', '2016-02-17 23:14:49'),
(293, 'Atlantic/Faroe', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:49', '2016-02-18 00:14:49'),
(294, 'Atlantic/Madeira', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:49', '2016-02-18 00:14:49'),
(295, 'Atlantic/Reykjavik', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:49', '2016-02-18 00:14:49'),
(296, 'Atlantic/South_Georgia', '1', 'UTC/GMT -02:00', '2016-02-17 22:14:49', '2016-02-17 22:14:49'),
(297, 'Atlantic/St_Helena', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:49', '2016-02-18 00:14:49'),
(298, 'Atlantic/Stanley', '1', 'UTC/GMT -03:00', '2016-02-17 21:14:49', '2016-02-17 21:14:49'),
(299, 'Australia/Adelaide', '1', 'UTC/GMT +10:30', '2016-02-17 22:44:50', '2016-02-17 22:44:50'),
(300, 'Australia/Brisbane', '1', 'UTC/GMT +10:00', '2016-02-17 22:14:50', '2016-02-17 22:14:50'),
(301, 'Australia/Broken_Hill', '1', 'UTC/GMT +10:30', '2016-02-17 22:44:50', '2016-02-17 22:44:50'),
(302, 'Australia/Currie', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:50', '2016-02-17 23:14:50'),
(303, 'Australia/Darwin', '1', 'UTC/GMT +09:30', '2016-02-17 21:44:50', '2016-02-17 21:44:50'),
(304, 'Australia/Eucla', '1', 'UTC/GMT +08:45', '2016-02-17 20:59:50', '2016-02-17 20:59:50'),
(305, 'Australia/Hobart', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:50', '2016-02-17 23:14:50'),
(306, 'Australia/Lindeman', '1', 'UTC/GMT +10:00', '2016-02-17 22:14:50', '2016-02-17 22:14:50'),
(307, 'Australia/Lord_Howe', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:50', '2016-02-17 23:14:50'),
(308, 'Australia/Melbourne', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:51', '2016-02-17 23:14:51'),
(309, 'Australia/Perth', '1', 'UTC/GMT +08:00', '2016-02-17 20:14:51', '2016-02-17 20:14:51'),
(310, 'Australia/Sydney', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:51', '2016-02-17 23:14:51'),
(311, 'Europe/Amsterdam', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:51', '2016-02-18 01:14:51'),
(312, 'Europe/Andorra', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:51', '2016-02-18 01:14:51'),
(313, 'Europe/Athens', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:51', '2016-02-18 02:14:51'),
(314, 'Europe/Belgrade', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:51', '2016-02-18 01:14:51'),
(315, 'Europe/Berlin', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:51', '2016-02-18 01:14:51'),
(316, 'Europe/Bratislava', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:51', '2016-02-18 01:14:51'),
(317, 'Europe/Brussels', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:52', '2016-02-18 01:14:52'),
(318, 'Europe/Bucharest', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:52', '2016-02-18 02:14:52'),
(319, 'Europe/Budapest', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:52', '2016-02-18 01:14:52'),
(320, 'Europe/Busingen', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:52', '2016-02-18 01:14:52'),
(321, 'Europe/Chisinau', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:52', '2016-02-18 02:14:52'),
(322, 'Europe/Copenhagen', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:52', '2016-02-18 01:14:52'),
(323, 'Europe/Dublin', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:52', '2016-02-18 00:14:52'),
(324, 'Europe/Gibraltar', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:52', '2016-02-18 01:14:52'),
(325, 'Europe/Guernsey', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:52', '2016-02-18 00:14:52'),
(326, 'Europe/Helsinki', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:52', '2016-02-18 02:14:52'),
(327, 'Europe/Isle_of_Man', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:52', '2016-02-18 00:14:52'),
(328, 'Europe/Istanbul', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:52', '2016-02-18 02:14:52'),
(329, 'Europe/Jersey', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:52', '2016-02-18 00:14:52'),
(330, 'Europe/Kaliningrad', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:53', '2016-02-18 03:14:53'),
(331, 'Europe/Kiev', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:53', '2016-02-18 02:14:53'),
(332, 'Europe/Lisbon', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:53', '2016-02-18 00:14:53'),
(333, 'Europe/Ljubljana', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:53', '2016-02-18 01:14:53'),
(334, 'Europe/London', '1', 'UTC/GMT +00:00', '2016-02-18 00:14:53', '2016-02-18 00:14:53'),
(335, 'Europe/Luxembourg', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:53', '2016-02-18 01:14:53'),
(336, 'Europe/Madrid', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:53', '2016-02-18 01:14:53'),
(337, 'Europe/Malta', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:53', '2016-02-18 01:14:53'),
(338, 'Europe/Mariehamn', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:53', '2016-02-18 02:14:53'),
(339, 'Europe/Minsk', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:53', '2016-02-18 03:14:53'),
(340, 'Europe/Monaco', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:53', '2016-02-18 01:14:53'),
(341, 'Europe/Moscow', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:53', '2016-02-18 04:14:53'),
(342, 'Europe/Oslo', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:53', '2016-02-18 01:14:53'),
(343, 'Europe/Paris', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:54', '2016-02-18 01:14:54'),
(344, 'Europe/Podgorica', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:54', '2016-02-18 01:14:54'),
(345, 'Europe/Prague', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:54', '2016-02-18 01:14:54'),
(346, 'Europe/Riga', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:54', '2016-02-18 02:14:54'),
(347, 'Europe/Rome', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:54', '2016-02-18 01:14:54'),
(348, 'Europe/Samara', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:54', '2016-02-18 04:14:54'),
(349, 'Europe/San_Marino', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:54', '2016-02-18 01:14:54'),
(350, 'Europe/Sarajevo', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:54', '2016-02-18 01:14:54'),
(351, 'Europe/Simferopol', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:54', '2016-02-18 02:14:54'),
(352, 'Europe/Skopje', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:54', '2016-02-18 01:14:54'),
(353, 'Europe/Sofia', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:55', '2016-02-18 02:14:55'),
(354, 'Europe/Stockholm', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:55', '2016-02-18 01:14:55'),
(355, 'Europe/Tallinn', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:55', '2016-02-18 02:14:55'),
(356, 'Europe/Tirane', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:55', '2016-02-18 01:14:55'),
(357, 'Europe/Uzhgorod', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:55', '2016-02-18 02:14:55'),
(358, 'Europe/Vaduz', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:55', '2016-02-18 01:14:55'),
(359, 'Europe/Vatican', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:56', '2016-02-18 01:14:56'),
(360, 'Europe/Vienna', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:56', '2016-02-18 01:14:56'),
(361, 'Europe/Vilnius', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:56', '2016-02-18 02:14:56'),
(362, 'Europe/Volgograd', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:56', '2016-02-18 04:14:56'),
(363, 'Europe/Warsaw', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:56', '2016-02-18 01:14:56'),
(364, 'Europe/Zagreb', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:56', '2016-02-18 01:14:56'),
(365, 'Europe/Zaporozhye', '1', 'UTC/GMT +02:00', '2016-02-18 02:14:56', '2016-02-18 02:14:56'),
(366, 'Europe/Zurich', '1', 'UTC/GMT +01:00', '2016-02-18 01:14:56', '2016-02-18 01:14:56'),
(367, 'Indian/Antananarivo', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:56', '2016-02-18 03:14:56'),
(368, 'Indian/Chagos', '1', 'UTC/GMT +06:00', '2016-02-18 06:14:57', '2016-02-18 06:14:57'),
(369, 'Indian/Christmas', '1', 'UTC/GMT +07:00', '2016-02-18 07:14:57', '2016-02-18 07:14:57'),
(370, 'Indian/Cocos', '1', 'UTC/GMT +06:30', '2016-02-18 06:44:57', '2016-02-18 06:44:57'),
(371, 'Indian/Comoro', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:57', '2016-02-18 03:14:57'),
(372, 'Indian/Kerguelen', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:57', '2016-02-18 05:14:57'),
(373, 'Indian/Mahe', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:57', '2016-02-18 04:14:57'),
(374, 'Indian/Maldives', '1', 'UTC/GMT +05:00', '2016-02-18 05:14:57', '2016-02-18 05:14:57'),
(375, 'Indian/Mauritius', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:57', '2016-02-18 04:14:57'),
(376, 'Indian/Mayotte', '1', 'UTC/GMT +03:00', '2016-02-18 03:14:57', '2016-02-18 03:14:57'),
(377, 'Indian/Reunion', '1', 'UTC/GMT +04:00', '2016-02-18 04:14:57', '2016-02-18 04:14:57'),
(378, 'Pacific/Apia', '1', 'UTC/GMT +14:00', '2016-02-18 02:14:57', '2016-02-18 02:14:57'),
(379, 'Pacific/Auckland', '1', 'UTC/GMT +13:00', '2016-02-18 01:14:57', '2016-02-18 01:14:57'),
(380, 'Pacific/Chatham', '1', 'UTC/GMT +13:45', '2016-02-18 01:59:57', '2016-02-18 01:59:57'),
(381, 'Pacific/Chuuk', '1', 'UTC/GMT +10:00', '2016-02-17 22:14:58', '2016-02-17 22:14:58'),
(382, 'Pacific/Easter', '1', 'UTC/GMT -05:00', '2016-02-18 07:14:58', '2016-02-18 07:14:58'),
(383, 'Pacific/Efate', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:58', '2016-02-17 23:14:58'),
(384, 'Pacific/Enderbury', '1', 'UTC/GMT +13:00', '2016-02-18 01:14:58', '2016-02-18 01:14:58'),
(385, 'Pacific/Fakaofo', '1', 'UTC/GMT +13:00', '2016-02-18 01:14:58', '2016-02-18 01:14:58'),
(386, 'Pacific/Fiji', '1', 'UTC/GMT +12:00', '2016-02-18 00:14:58', '2016-02-18 00:14:58'),
(387, 'Pacific/Funafuti', '1', 'UTC/GMT +12:00', '2016-02-18 00:14:58', '2016-02-18 00:14:58'),
(388, 'Pacific/Galapagos', '1', 'UTC/GMT -06:00', '2016-02-17 06:14:58', '2016-02-17 06:14:58'),
(389, 'Pacific/Gambier', '1', 'UTC/GMT -09:00', '2016-02-17 03:14:58', '2016-02-17 03:14:58'),
(390, 'Pacific/Guadalcanal', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:59', '2016-02-17 23:14:59'),
(391, 'Pacific/Guam', '1', 'UTC/GMT +10:00', '2016-02-17 22:14:59', '2016-02-17 22:14:59'),
(392, 'Pacific/Honolulu', '1', 'UTC/GMT -10:00', '2016-02-17 02:14:59', '2016-02-17 02:14:59'),
(393, 'Pacific/Johnston', '1', 'UTC/GMT -10:00', '2016-02-17 02:14:59', '2016-02-17 02:14:59'),
(394, 'Pacific/Kiritimati', '1', 'UTC/GMT +14:00', '2016-02-18 02:14:59', '2016-02-18 02:14:59'),
(395, 'Pacific/Kosrae', '1', 'UTC/GMT +11:00', '2016-02-17 23:14:59', '2016-02-17 23:14:59'),
(396, 'Pacific/Kwajalein', '1', 'UTC/GMT +12:00', '2016-02-18 00:15:00', '2016-02-18 00:15:00'),
(397, 'Pacific/Majuro', '1', 'UTC/GMT +12:00', '2016-02-18 00:15:00', '2016-02-18 00:15:00'),
(398, 'Pacific/Marquesas', '1', 'UTC/GMT -09:30', '2016-02-17 02:45:00', '2016-02-17 02:45:00'),
(399, 'Pacific/Midway', '1', 'UTC/GMT -11:00', '2016-02-17 01:15:00', '2016-02-17 01:15:00'),
(400, 'Pacific/Nauru', '1', 'UTC/GMT +12:00', '2016-02-18 00:15:00', '2016-02-18 00:15:00'),
(401, 'Pacific/Niue', '1', 'UTC/GMT -11:00', '2016-02-17 01:15:00', '2016-02-17 01:15:00'),
(402, 'Pacific/Norfolk', '1', 'UTC/GMT +11:30', '2016-02-17 23:45:00', '2016-02-17 23:45:00'),
(403, 'Pacific/Noumea', '1', 'UTC/GMT +11:00', '2016-02-17 23:15:01', '2016-02-17 23:15:01'),
(404, 'Pacific/Pago_Pago', '1', 'UTC/GMT -11:00', '2016-02-17 01:15:01', '2016-02-17 01:15:01'),
(405, 'Pacific/Palau', '1', 'UTC/GMT +09:00', '2016-02-17 21:15:01', '2016-02-17 21:15:01'),
(406, 'Pacific/Pitcairn', '1', 'UTC/GMT -08:00', '2016-02-17 04:15:01', '2016-02-17 04:15:01'),
(407, 'Pacific/Pohnpei', '1', 'UTC/GMT +11:00', '2016-02-17 23:15:01', '2016-02-17 23:15:01'),
(408, 'Pacific/Port_Moresby', '1', 'UTC/GMT +10:00', '2016-02-17 22:15:02', '2016-02-17 22:15:02'),
(409, 'Pacific/Rarotonga', '1', 'UTC/GMT -10:00', '2016-02-17 02:15:02', '2016-02-17 02:15:02'),
(410, 'Pacific/Saipan', '1', 'UTC/GMT +10:00', '2016-02-17 22:15:02', '2016-02-17 22:15:02'),
(411, 'Pacific/Tahiti', '1', 'UTC/GMT -10:00', '2016-02-17 02:15:02', '2016-02-17 02:15:02'),
(412, 'Pacific/Tarawa', '1', 'UTC/GMT +12:00', '2016-02-18 00:15:03', '2016-02-18 00:15:03'),
(413, 'Pacific/Tongatapu', '1', 'UTC/GMT +13:00', '2016-02-18 01:15:03', '2016-02-18 01:15:03'),
(414, 'Pacific/Wake', '1', 'UTC/GMT +12:00', '2016-02-18 00:15:03', '2016-02-18 00:15:03'),
(415, 'Pacific/Wallis', '1', 'UTC/GMT +12:00', '2016-02-18 00:15:03', '2016-02-18 00:15:03'),
(416, 'UTC', '1', 'UTC/GMT +00:00', '2016-02-18 00:15:03', '2016-02-18 00:15:03');

-- --------------------------------------------------------

--
-- Table structure for table `mst_uom`
--

CREATE TABLE IF NOT EXISTS `mst_uom` (
  `uomid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uomid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_useraction`
--

CREATE TABLE IF NOT EXISTS `mst_useraction` (
  `actionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mst_useraction`
--

INSERT INTO `mst_useraction` (`actionid`, `userid`, `ipaddress`, `sessionid`, `actiondate`, `actiontime`, `actionname`, `created_at`, `updated_at`) VALUES
(1, 'UUZZ-9984', '127.0.0.1', 'NYfPrPoCEce4pfx9UwpfOghXF8ekfNmiQZRgJHn9', '2016-02-22', '2016-02-22 17:57:05', 'Logged In', '2016-02-22 12:27:05', '2016-02-22 12:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `mst_userlogin`
--

CREATE TABLE IF NOT EXISTS `mst_userlogin` (
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authorizedpass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `ftlogin` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `timezoneid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `secquiz1` int(11) DEFAULT NULL,
  `secquiz2` int(11) DEFAULT NULL,
  `secquiz3` int(11) DEFAULT NULL,
  `secans1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secans3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visualnoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `audionoti` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `industry` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_userlogin`
--

INSERT INTO `mst_userlogin` (`roletype`, `roleid`, `userid`, `password`, `authorizedpass`, `isactive`, `ftlogin`, `timezoneid`, `secquiz1`, `secquiz2`, `secquiz3`, `secans1`, `secans2`, `secans3`, `visualnoti`, `audionoti`, `industry`, `category`, `subcategory`, `brand`, `creationdate`, `created_at`, `updated_at`) VALUES
('AD', 5, 'UUZZ-9984', '$2y$10$d5q7D/a9AguEeaYOYFyvduN2KHYi9yqAoom3jWstdKYVRwZPyApO.', '$2y$10$f8wPKAGph1hWzt/GsvVqoeBftsg5Hjct2oAODu8xXW1AXBnKPIVNO', '1', 'Y', 'Asia/Kolkata', 6, 8, 3, 'father', 'mother', 'animal', '1', '1', NULL, NULL, NULL, NULL, '2016-02-17 16:01:00', '2016-02-17 10:31:00', '2016-02-17 10:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `mst_userlogon`
--

CREATE TABLE IF NOT EXISTS `mst_userlogon` (
  `logonid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.0',
  `starttime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `finishtime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logondate` date NOT NULL,
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`logonid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `mst_userlogon`
--

INSERT INTO `mst_userlogon` (`logonid`, `userid`, `ipaddress`, `starttime`, `finishtime`, `sessionid`, `logondate`, `roletype`, `created_at`, `updated_at`) VALUES
(1, 'UUZZ-9984', '127.0.0.1', '2016-02-18 17:02:23pm', '', 'QANheAg0mrKdxgMT1dKkeoPxhhkYh68MZ5OJbbzN', '2016-02-18', 'AD', '2016-02-18 11:32:23', '2016-02-18 11:32:23'),
(2, 'UUZZ-9984', '127.0.0.1', '2016-02-18 17:31:25pm', '', 'QANheAg0mrKdxgMT1dKkeoPxhhkYh68MZ5OJbbzN', '2016-02-18', 'AD', '2016-02-18 12:01:25', '2016-02-18 12:01:25'),
(3, 'UUZZ-9984', '127.0.0.1', '2016-02-22 17:57:05pm', '', 'NYfPrPoCEce4pfx9UwpfOghXF8ekfNmiQZRgJHn9', '2016-02-22', 'AD', '2016-02-22 12:27:05', '2016-02-22 12:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `mst_userrole`
--

CREATE TABLE IF NOT EXISTS `mst_userrole` (
  `roleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `rolename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mst_userrole`
--

INSERT INTO `mst_userrole` (`roleid`, `name`, `isactive`, `code`, `rolename`) VALUES
(1, 'Account Manager', '1', 'AM', 'Account Manager'),
(2, 'Quote Facilitator', '1', 'QF', 'Quote Facilitator'),
(3, 'Trigger Man', '1', 'TM', 'Trigger Man'),
(4, 'Administrator', '1', 'AD', 'Sub Administrator'),
(5, 'Administrator', '1', 'AD', 'Administrator'),
(6, 'Account Manager', '1', 'AM', 'Partner');

-- --------------------------------------------------------

--
-- Table structure for table `old_useractivitypurge`
--

CREATE TABLE IF NOT EXISTS `old_useractivitypurge` (
  `actionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actiondate` date NOT NULL,
  `actiontime` datetime NOT NULL,
  `actionname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_master`
--

CREATE TABLE IF NOT EXISTS `page_master` (
  `p_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `p_show_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_path` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_notification`
--

CREATE TABLE IF NOT EXISTS `page_notification` (
  `n_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `p_key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `n_variable` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `final_noti` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `n_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`n_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_advertisment`
--

CREATE TABLE IF NOT EXISTS `post_advertisment` (
  `postid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pdate` datetime NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `industry` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ptype` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerrefno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pakaging` text COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `targetprice` int(11) NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `tempid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `archivepost` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `buyer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotefacility` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `act_inactive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`postid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_customer`
--

CREATE TABLE IF NOT EXISTS `post_customer` (
  `custid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `refno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `edate` date NOT NULL,
  `refuserid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `roletype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `adduserid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_quotation`
--

CREATE TABLE IF NOT EXISTS `post_quotation` (
  `quoteid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `postno` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `uom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expirydate` datetime NOT NULL,
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `timeframe` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `declinemsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offerdeclinemsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptmsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offercrdate` datetime NOT NULL,
  `userid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quationstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `offerstatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `accdate` date DEFAULT NULL,
  `rejdate` date DEFAULT NULL,
  `lastcntdate` date DEFAULT NULL,
  `isactive` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `counetr_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `postuserid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `readunreadst` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`quoteid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productimport`
--

CREATE TABLE IF NOT EXISTS `productimport` (
  `productid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `productno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcat` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caseweight` text COLLATE utf8_unicode_ci,
  `packaging` text COLLATE utf8_unicode_ci,
  `shippingcond` text COLLATE utf8_unicode_ci,
  `qtypercase` bigint(20) DEFAULT NULL,
  `des` text COLLATE utf8_unicode_ci,
  `mpm` double(8,2) DEFAULT NULL,
  `prefixone` text COLLATE utf8_unicode_ci,
  `codeone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixtwo` tinyint(4) DEFAULT NULL,
  `codetwo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefixthree` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codethree` text COLLATE utf8_unicode_ci,
  `img1` text COLLATE utf8_unicode_ci,
  `img2` text COLLATE utf8_unicode_ci,
  `img3` text COLLATE utf8_unicode_ci,
  `img4` text COLLATE utf8_unicode_ci,
  `img5` text COLLATE utf8_unicode_ci,
  `img6` text COLLATE utf8_unicode_ci,
  `img7` text COLLATE utf8_unicode_ci,
  `img8` text COLLATE utf8_unicode_ci,
  `addedby` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pstatus` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_counter`
--

CREATE TABLE IF NOT EXISTS `quotation_counter` (
  `counterid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_date` datetime NOT NULL,
  `counter_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `counter_quantity` int(11) NOT NULL,
  `counter_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `counter_msg` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offeracceptmsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counetr_status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `offerCounterStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`counterid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ind_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Index_val` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test1`
--

CREATE TABLE IF NOT EXISTS `test1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `industry` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tm_accepted_offers`
--

CREATE TABLE IF NOT EXISTS `tm_accepted_offers` (
  `acc_offr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `counterid` int(11) NOT NULL,
  `quotationno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoteid` int(11) NOT NULL,
  `postno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acceptedBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_date` datetime NOT NULL,
  `acc_price` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acc_quantity` int(11) NOT NULL,
  `acc_timeframe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `acc_expdate` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `noti_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tm_userID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `offerType` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`acc_offr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
